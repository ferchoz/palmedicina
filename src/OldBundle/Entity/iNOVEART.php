<?php

namespace OldBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * iNOVEDADES
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class iNOVEART
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Fecha", type="datetime", nullable=true)
     */
    private $fecha;

    /**
     * @var string
     *
     * @ORM\Column(name="Codigoe", type="string", length=255, nullable=true)
     */
    private $codigoe;

    /**
     * @var string
     *
     * @ORM\Column(name="Razon", type="string", length=255, nullable=true)
     */
    private $razon;

    /**
     * @var string
     *
     * @ORM\Column(name="Codart", type="string", length=255, nullable=true)
     */
    private $codart;

    /**
     * @var string
     *
     * @ORM\Column(name="Codigop", type="string", length=255, nullable=true)
     */
    private $codigop;

    /**
     * @var string
     *
     * @ORM\Column(name="Nombre", type="string", length=255, nullable=true)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="Cuil", type="string", length=255, nullable=true)
     */
    private $cuil;

    /**
     * @var string
     *
     * @ORM\Column(name="Direccion", type="string", length=255, nullable=true)
     */
    private $direccion;

    /**
     * @var string
     *
     * @ORM\Column(name="Localidad", type="string", length=255, nullable=true)
     */
    private $localidad;

    /**
     * @var string
     *
     * @ORM\Column(name="Telefono", type="string", length=255, nullable=true)
     */
    private $telefono;

    /**
     * @var string
     *
     * @ORM\Column(name="Tiponov", type="string", length=255, nullable=true)
     */
    private $tiponov;

    /**
     * @var string
     *
     * @ORM\Column(name="Tipoacc", type="string", length=255, nullable=true)
     */
    private $tipoacc;

    /**
     * @var string
     *
     * @ORM\Column(name="Profesional", type="string", length=255, nullable=true)
     */
    private $profesional;

    /**
     * @var bigint
     *
     * @ORM\Column(name="Enlace", type="bigint", nullable=true)
     */
    private $enlace;

    /**
     * @var string
     *
     * @ORM\Column(name="Ren1", type="string", length=255, nullable=true)
     */
    private $ren1;

    /**
     * @var string
     *
     * @ORM\Column(name="Ren2", type="string", length=255, nullable=true)
     */
    private $ren2;

    /**
     * @var string
     *
     * @ORM\Column(name="Ren3", type="string", length=255, nullable=true)
     */
    private $ren3;

    /**
     * @var string
     *
     * @ORM\Column(name="Ren4", type="string", length=255, nullable=true)
     */
    private $ren4;

    /**
     * @var string
     *
     * @ORM\Column(name="Ren5", type="string", length=255, nullable=true)
     */
    private $ren5;

    /**
     * @var string
     *
     * @ORM\Column(name="Ren6", type="string", length=255, nullable=true)
     */
    private $ren6;

    /**
     * @var string
     *
     * @ORM\Column(name="Ren7", type="string", length=255, nullable=true)
     */
    private $ren7;

    /**
     * @var string
     *
     * @ORM\Column(name="Ren8", type="string", length=255, nullable=true)
     */
    private $ren8;

    /**
     * @var string
     *
     * @ORM\Column(name="Ren9", type="string", length=255, nullable=true)
     */
    private $ren9;

    /**
     * @var string
     *
     * @ORM\Column(name="Ren10", type="string", length=255, nullable=true)
     */
    private $ren10;

    /**
     * @var string
     *
     * @ORM\Column(name="Ren11", type="string", length=255, nullable=true)
     */
    private $ren11;

    /**
     * @var string
     *
     * @ORM\Column(name="Ren12", type="string", length=255, nullable=true)
     */
    private $ren12;

    /**
     * @var string
     *
     * @ORM\Column(name="Ren13", type="string", length=255, nullable=true)
     */
    private $ren13;

    /**
     * @var string
     *
     * @ORM\Column(name="Ren14", type="string", length=255, nullable=true)
     */
    private $ren14;

    /**
     * @var string
     *
     * @ORM\Column(name="Ren15", type="string", length=255, nullable=true)
     */
    private $ren15;

    /**
     * @var string
     *
     * @ORM\Column(name="Ren16", type="string", length=255, nullable=true)
     */
    private $ren16;

    /**
     * @var string
     *
     * @ORM\Column(name="Ren17", type="string", length=255, nullable=true)
     */
    private $ren17;

    /**
     * @var string
     *
     * @ORM\Column(name="Ren18", type="string", length=255, nullable=true)
     */
    private $ren18;

    /**
     * @var string
     *
     * @ORM\Column(name="Ren19", type="string", length=255, nullable=true)
     */
    private $ren19;

    /**
     * @var string
     *
     * @ORM\Column(name="Ren20", type="string", length=255, nullable=true)
     */
    private $ren20;

    /**
     * @var string
     *
     * @ORM\Column(name="Ren21", type="string", length=255, nullable=true)
     */
    private $ren21;

    /**
     * @var string
     *
     * @ORM\Column(name="Ren22", type="string", length=255, nullable=true)
     */
    private $ren22;

    /**
     * @var string
     *
     * @ORM\Column(name="Ren23", type="string", length=255, nullable=true)
     */
    private $ren23;

    /**
     * @var string
     *
     * @ORM\Column(name="Ren24", type="string", length=255, nullable=true)
     */
    private $ren24;

    /**
     * @var string
     *
     * @ORM\Column(name="Ren25", type="string", length=255, nullable=true)
     */
    private $ren25;

    /**
     * @var string
     *
     * @ORM\Column(name="Ren26", type="string", length=255, nullable=true)
     */
    private $ren26;

    /**
     * @var string
     *
     * @ORM\Column(name="Ren27", type="string", length=255, nullable=true)
     */
    private $ren27;

    /**
     * @var string
     *
     * @ORM\Column(name="Ren28", type="string", length=255, nullable=true)
     */
    private $ren28;

    /**
     * @var string
     *
     * @ORM\Column(name="Ren29", type="string", length=255, nullable=true)
     */
    private $ren29;

    /**
     * @var string
     *
     * @ORM\Column(name="Ren30", type="string", length=255, nullable=true)
     */
    private $ren30;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=40, nullable=true)
     */
    private $estado;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return iNOVEDADES
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set codigoe
     *
     * @param string $codigoe
     * @return iNOVEDADES
     */
    public function setCodigoe($codigoe)
    {
        $this->codigoe = $codigoe;

        return $this;
    }

    /**
     * Get codigoe
     *
     * @return string 
     */
    public function getCodigoe()
    {
        return $this->codigoe;
    }

    /**
     * Set razon
     *
     * @param string $razon
     * @return iNOVEDADES
     */
    public function setRazon($razon)
    {
        $this->razon = $razon;

        return $this;
    }

    /**
     * Get razon
     *
     * @return string 
     */
    public function getRazon()
    {
        return $this->razon;
    }

    /**
     * Set codart
     *
     * @param string $codart
     * @return iNOVEDADES
     */
    public function setCodart($codart)
    {
        $this->codart = $codart;

        return $this;
    }

    /**
     * Get codart
     *
     * @return string 
     */
    public function getCodart()
    {
        return $this->codart;
    }

    /**
     * Set codigop
     *
     * @param string $codigop
     * @return iNOVEDADES
     */
    public function setCodigop($codigop)
    {
        $this->codigop = $codigop;

        return $this;
    }

    /**
     * Get codigop
     *
     * @return string 
     */
    public function getCodigop()
    {
        return $this->codigop;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return iNOVEDADES
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set tiponov
     *
     * @param string $tiponov
     * @return iNOVEDADES
     */
    public function setTiponov($tiponov)
    {
        $this->tiponov = $tiponov;

        return $this;
    }

    /**
     * Get tiponov
     *
     * @return string 
     */
    public function getTiponov()
    {
        return $this->tiponov;
    }

    /**
     * Set ren1
     *
     * @param string $ren1
     * @return iNOVEDADES
     */
    public function setRen1($ren1)
    {
        $this->ren1 = $ren1;

        return $this;
    }

    /**
     * Get ren1
     *
     * @return string 
     */
    public function getRen1()
    {
        return $this->ren1;
    }

    /**
     * Set ren2
     *
     * @param string $ren2
     * @return iNOVEDADES
     */
    public function setRen2($ren2)
    {
        $this->ren2 = $ren2;

        return $this;
    }

    /**
     * Get ren2
     *
     * @return string 
     */
    public function getRen2()
    {
        return $this->ren2;
    }

    /**
     * Set ren3
     *
     * @param string $ren3
     * @return iNOVEDADES
     */
    public function setRen3($ren3)
    {
        $this->ren3 = $ren3;

        return $this;
    }

    /**
     * Get ren3
     *
     * @return string 
     */
    public function getRen3()
    {
        return $this->ren3;
    }

    /**
     * Set ren4
     *
     * @param string $ren4
     * @return iNOVEDADES
     */
    public function setRen4($ren4)
    {
        $this->ren4 = $ren4;

        return $this;
    }

    /**
     * Get ren4
     *
     * @return string 
     */
    public function getRen4()
    {
        return $this->ren4;
    }

    /**
     * Set ren5
     *
     * @param string $ren5
     * @return iNOVEDADES
     */
    public function setRen5($ren5)
    {
        $this->ren5 = $ren5;

        return $this;
    }

    /**
     * Get ren5
     *
     * @return string 
     */
    public function getRen5()
    {
        return $this->ren5;
    }

    /**
     * Set ren6
     *
     * @param string $ren6
     * @return iNOVEDADES
     */
    public function setRen6($ren6)
    {
        $this->ren6 = $ren6;

        return $this;
    }

    /**
     * Get ren6
     *
     * @return string 
     */
    public function getRen6()
    {
        return $this->ren6;
    }

    /**
     * Set ren7
     *
     * @param string $ren7
     * @return iNOVEDADES
     */
    public function setRen7($ren7)
    {
        $this->ren7 = $ren7;

        return $this;
    }

    /**
     * Get ren7
     *
     * @return string 
     */
    public function getRen7()
    {
        return $this->ren7;
    }

    /**
     * Set ren8
     *
     * @param string $ren8
     * @return iNOVEDADES
     */
    public function setRen8($ren8)
    {
        $this->ren8 = $ren8;

        return $this;
    }

    /**
     * Get ren8
     *
     * @return string 
     */
    public function getRen8()
    {
        return $this->ren8;
    }

    /**
     * Set ren9
     *
     * @param string $ren9
     * @return iNOVEDADES
     */
    public function setRen9($ren9)
    {
        $this->ren9 = $ren9;

        return $this;
    }

    /**
     * Get ren9
     *
     * @return string 
     */
    public function getRen9()
    {
        return $this->ren9;
    }

    /**
     * Set ren10
     *
     * @param string $ren10
     * @return iNOVEDADES
     */
    public function setRen10($ren10)
    {
        $this->ren10 = $ren10;

        return $this;
    }

    /**
     * Get ren10
     *
     * @return string 
     */
    public function getRen10()
    {
        return $this->ren10;
    }

    /**
     * Set enlace
     *
     * @param integer $enlace
     * @return iNOVEDADES
     */
    public function setEnlace($enlace)
    {
        $this->enlace = $enlace;

        return $this;
    }

    /**
     * Get enlace
     *
     * @return integer 
     */
    public function getEnlace()
    {
        return $this->enlace;
    }

    /**
     * Set ren11
     *
     * @param string $ren11
     * @return iNOVEART
     */
    public function setRen11($ren11)
    {
        $this->ren11 = $ren11;

        return $this;
    }

    /**
     * Get ren11
     *
     * @return string 
     */
    public function getRen11()
    {
        return $this->ren11;
    }

    /**
     * Set ren12
     *
     * @param string $ren12
     * @return iNOVEART
     */
    public function setRen12($ren12)
    {
        $this->ren12 = $ren12;

        return $this;
    }

    /**
     * Get ren12
     *
     * @return string 
     */
    public function getRen12()
    {
        return $this->ren12;
    }

    /**
     * Set ren13
     *
     * @param string $ren13
     * @return iNOVEART
     */
    public function setRen13($ren13)
    {
        $this->ren13 = $ren13;

        return $this;
    }

    /**
     * Get ren13
     *
     * @return string 
     */
    public function getRen13()
    {
        return $this->ren13;
    }

    /**
     * Set ren14
     *
     * @param string $ren14
     * @return iNOVEART
     */
    public function setRen14($ren14)
    {
        $this->ren14 = $ren14;

        return $this;
    }

    /**
     * Get ren14
     *
     * @return string 
     */
    public function getRen14()
    {
        return $this->ren14;
    }

    /**
     * Set ren15
     *
     * @param string $ren15
     * @return iNOVEART
     */
    public function setRen15($ren15)
    {
        $this->ren15 = $ren15;

        return $this;
    }

    /**
     * Get ren15
     *
     * @return string 
     */
    public function getRen15()
    {
        return $this->ren15;
    }

    /**
     * Set ren16
     *
     * @param string $ren16
     * @return iNOVEART
     */
    public function setRen16($ren16)
    {
        $this->ren16 = $ren16;

        return $this;
    }

    /**
     * Get ren16
     *
     * @return string 
     */
    public function getRen16()
    {
        return $this->ren16;
    }

    /**
     * Set ren17
     *
     * @param string $ren17
     * @return iNOVEART
     */
    public function setRen17($ren17)
    {
        $this->ren17 = $ren17;

        return $this;
    }

    /**
     * Get ren17
     *
     * @return string 
     */
    public function getRen17()
    {
        return $this->ren17;
    }

    /**
     * Set ren18
     *
     * @param string $ren18
     * @return iNOVEART
     */
    public function setRen18($ren18)
    {
        $this->ren18 = $ren18;

        return $this;
    }

    /**
     * Get ren18
     *
     * @return string 
     */
    public function getRen18()
    {
        return $this->ren18;
    }

    /**
     * Set ren19
     *
     * @param string $ren19
     * @return iNOVEART
     */
    public function setRen19($ren19)
    {
        $this->ren19 = $ren19;

        return $this;
    }

    /**
     * Get ren19
     *
     * @return string 
     */
    public function getRen19()
    {
        return $this->ren19;
    }

    /**
     * Set ren20
     *
     * @param string $ren20
     * @return iNOVEART
     */
    public function setRen20($ren20)
    {
        $this->ren20 = $ren20;

        return $this;
    }

    /**
     * Get ren20
     *
     * @return string 
     */
    public function getRen20()
    {
        return $this->ren20;
    }

    /**
     * Set ren21
     *
     * @param string $ren21
     * @return iNOVEART
     */
    public function setRen21($ren21)
    {
        $this->ren21 = $ren21;

        return $this;
    }

    /**
     * Get ren21
     *
     * @return string 
     */
    public function getRen21()
    {
        return $this->ren21;
    }

    /**
     * Set ren22
     *
     * @param string $ren22
     * @return iNOVEART
     */
    public function setRen22($ren22)
    {
        $this->ren22 = $ren22;

        return $this;
    }

    /**
     * Get ren22
     *
     * @return string 
     */
    public function getRen22()
    {
        return $this->ren22;
    }

    /**
     * Set ren23
     *
     * @param string $ren23
     * @return iNOVEART
     */
    public function setRen23($ren23)
    {
        $this->ren23 = $ren23;

        return $this;
    }

    /**
     * Get ren23
     *
     * @return string 
     */
    public function getRen23()
    {
        return $this->ren23;
    }

    /**
     * Set ren24
     *
     * @param string $ren24
     * @return iNOVEART
     */
    public function setRen24($ren24)
    {
        $this->ren24 = $ren24;

        return $this;
    }

    /**
     * Get ren24
     *
     * @return string 
     */
    public function getRen24()
    {
        return $this->ren24;
    }

    /**
     * Set ren25
     *
     * @param string $ren25
     * @return iNOVEART
     */
    public function setRen25($ren25)
    {
        $this->ren25 = $ren25;

        return $this;
    }

    /**
     * Get ren25
     *
     * @return string 
     */
    public function getRen25()
    {
        return $this->ren25;
    }

    /**
     * Set ren26
     *
     * @param string $ren26
     * @return iNOVEART
     */
    public function setRen26($ren26)
    {
        $this->ren26 = $ren26;

        return $this;
    }

    /**
     * Get ren26
     *
     * @return string 
     */
    public function getRen26()
    {
        return $this->ren26;
    }

    /**
     * Set ren27
     *
     * @param string $ren27
     * @return iNOVEART
     */
    public function setRen27($ren27)
    {
        $this->ren27 = $ren27;

        return $this;
    }

    /**
     * Get ren27
     *
     * @return string 
     */
    public function getRen27()
    {
        return $this->ren27;
    }

    /**
     * Set ren28
     *
     * @param string $ren28
     * @return iNOVEART
     */
    public function setRen28($ren28)
    {
        $this->ren28 = $ren28;

        return $this;
    }

    /**
     * Get ren28
     *
     * @return string 
     */
    public function getRen28()
    {
        return $this->ren28;
    }

    /**
     * Set ren29
     *
     * @param string $ren29
     * @return iNOVEART
     */
    public function setRen29($ren29)
    {
        $this->ren29 = $ren29;

        return $this;
    }

    /**
     * Get ren29
     *
     * @return string 
     */
    public function getRen29()
    {
        return $this->ren29;
    }

    /**
     * Set ren30
     *
     * @param string $ren30
     * @return iNOVEART
     */
    public function setRen30($ren30)
    {
        $this->ren30 = $ren30;

        return $this;
    }

    /**
     * Get ren30
     *
     * @return string 
     */
    public function getRen30()
    {
        return $this->ren30;
    }

    /**
     * Set cuil
     *
     * @param string $cuil
     * @return iNOVEART
     */
    public function setCuil($cuil)
    {
        $this->cuil = $cuil;

        return $this;
    }

    /**
     * Get cuil
     *
     * @return string 
     */
    public function getCuil()
    {
        return $this->cuil;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     * @return iNOVEART
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string 
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set localidad
     *
     * @param string $localidad
     * @return iNOVEART
     */
    public function setLocalidad($localidad)
    {
        $this->localidad = $localidad;

        return $this;
    }

    /**
     * Get localidad
     *
     * @return string 
     */
    public function getLocalidad()
    {
        return $this->localidad;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     * @return iNOVEART
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string 
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set tipoacc
     *
     * @param string $tipoacc
     * @return iNOVEART
     */
    public function setTipoacc($tipoacc)
    {
        $this->tipoacc = $tipoacc;

        return $this;
    }

    /**
     * Get tipoacc
     *
     * @return string 
     */
    public function getTipoacc()
    {
        return $this->tipoacc;
    }

    /**
     * Set profesional
     *
     * @param string $profesional
     * @return iNOVEART
     */
    public function setProfesional($profesional)
    {
        $this->profesional = $profesional;

        return $this;
    }

    /**
     * Get profesional
     *
     * @return string 
     */
    public function getProfesional()
    {
        return $this->profesional;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return iNOVEART
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }
}
