/* Add PrettyPhoto to Images
================================================== */
jQOld(document).ready(function () {
	jQOld("a[data-rel^='prettyPhoto']").prettyPhoto({
		hook: 'data-rel', /* the attribute tag to use for prettyPhoto hooks. default: 'rel'. For HTML5, use "data-rel" or similar. */
		animation_speed:'fast',
		slideshow:3000,
		autoplay_slideshow:false,
		opacity:0.80,
		show_title:false,
		theme:'pp_default', /* light_rounded / dark_rounded / light_square / dark_square / facebook */
		overlay_gallery:false,
		social_tools:false
	});
});

		
/* Add Class "current-menu-item" to menu items
================================================== */
jQOld(document).ready(function () {
	var url = jQOld(location).attr('pathname');
	var path = url.substring(1);
	if (path == "") {pat="home.html"};
	jQOld('.sf-menu li a[href="' + path + '"]').parent().addClass(".current-menu-item");
});

		
/* Remove Image Width and Height Attr
================================================== */
jQOld(document).ready(function () {
	var a = jQOld(window).width();
	if (a<980) {
	jQOld('img:not(.flickr-widget img, .service-3 .icon img)').removeAttr("width").removeAttr("height");
	}
});
jQOld(window).resize(function () {
	var a = jQOld(window).width();
	if (a<980) {
	jQOld('img:not(.flickr-widget img, .service-3 .icon img)').removeAttr("width").removeAttr("height");
	}
});


/* Add tooltip
================================================== */
jQOld(document).ready(function () {
	jQOld('a[data-rel="tipsy"]').tipsy({fade: true, gravity: 'n'});
});


/* Create Select Responsive menu
================================================== */
jQOld(document).ready(function () {
	
	// DOM ready
	jQOld(function() {
		
		// Create the dropdown base
		jQOld("<select />").appendTo("#menu-wrapper");
		
		// Create default option "Go to..."
		jQOld("<option />", {
		 "selected": "selected",
		 "value"   : "",
		 "text"    : "Go to..."
		}).appendTo("#menu-wrapper select");
		
		// Populate dropdown with menu items
		jQOld("#menu-wrapper a").each(function() {
		var el = jQOld(this);
		
		if (jQOld(el).parents('.sub-menu .sub-menu .sub-menu').length >= 1) {
			jQOld('<option />', {
			 'value'   : el.attr('href'),
			 'text'    : '- - - ' + el.text()
			}).appendTo('#menu-wrapper select');
		}
		else if (jQOld(el).parents('.sub-menu .sub-menu').length >= 1) {
			jQOld('<option />', {
			 'value'   : el.attr('href'),
			 'text'    : '- - ' + el.text()
			}).appendTo('#menu-wrapper select');
		}
		else if (jQOld(el).parents('.sub-menu').length >= 1) {
			jQOld('<option />', {
			 'value'   : el.attr('href'),
			 'text'    : '- ' + el.text()
			}).appendTo('#menu-wrapper select');
		}
		else {
			jQOld('<option />', {
			 'value'   : el.attr('href'),
			 'text'    : el.text()
			}).appendTo('#menu-wrapper select');
		}
		
		});
		
		// To make dropdown actually work
		// To make more unobtrusive: http://css-tricks.com/4064-unobtrusive-page-changer/
		jQOld("#menu-wrapper select").change(function() {
		window.location = jQOld(this).find("option:selected").val();
		});
	
	});
	 
});


/* slogan width resize
================================================== */
jQOld(window).resize(function () {
	var a = jQOld(window).width();
	if (a<980) {
		jQOld('#slogan li').css({width:"auto"});
	} else { jQOld('#slogan li').css({width:"860px"}); }
});


/* client width resize
================================================== */
jQOld(window).resize(function() {
	jQOld(".clients-wrap").width(jQOld(".clients-wrapper").width());
});


/* Set height For Post Date
================================================== */
jQOld(document).ready(function () {
	el = jQOld('#postsinglepage.posts .date-wrap, .posts .date-wrap');
	var a = el.height();
	var b = el.parent().height();
	a = ((b-a)/2);
	el.css("padding-top",a);
});
jQOld(window).resize(function () {
	el = jQOld('.posts .date-wrap');
	var a = el.height();
	var b = el.parent().height();
	a = ((b-a)/2);
	el.css("padding-top",a);
});


/* Animate Zoom Icon
================================================== */

jQOld(document).ready(function () {
	jQOld(".posts .featured-thumbnail a.image-wrapper, #portfoliosinglepage a.image-wrapper, .portfolio-item-wrapper:not(.gallery) a.image-wrapper, .featured-thumbnail.image-frame a.image-wrapper").hover(function(){
		jQOld(this).animate({ opacity: 0.7 }, 500, 'easeOutExpo');
		jQOld(this).find('span').animate({ top: '0'}, 200, 'easeOutExpo');
	}, function(){
		jQOld(this).find('span').animate({ top: '100%'}, 200, 'easeInExpo', function(){
			jQOld(this).css('top','-100%');
		});
		jQOld(this).animate({ opacity: 1 }, 500, 'easeInExpo');
		});
});


/* Animate Zoom Icon for ralated post/portfolio
================================================== */

jQOld(document).ready(function () {
	jQOld(".related .image-wrap").hover(function(){
		jQOld(this).children('.zoom-icon.related-post').animate({ left: '0'}, 500, 'easeOutExpo');
	}, function(){
		jQOld(this).children('.zoom-icon.related-post').animate({ left: '100%'}, 500, 'easeInExpo', function(){
			jQOld(this).css('left','-100%');
		});
	});
});


/* Animate Zoom Icon for gallery style
================================================== */

jQOld(document).ready(function () {
	jQOld(".gallery .image-wrap").hover(function(){
		jQOld(this).children('.zoom-icon.gallery-port').animate({ left: '0'}, 500, 'easeOutExpo');
	}, function(){
		jQOld(this).children('.zoom-icon.gallery-port').animate({ left: '100%'}, 500, 'easeInExpo', function(){
			jQOld(this).css('left','-100%');
		});
	});
});


/* Created button effect
================================================== */
jQOld(document).ready(function() {
	
	jQOld(".shortcode-button").children("img").attr('width', function(){ 
			return jQOld(this).parent().width()+30; 
		}).attr('height', function(){ 
			return jQOld(this).parent().height()+12; 
		})
		
	jQOld(".shortcode-button").mouseenter(function(){
		jQOld(this).find("img").attr("src", function(){
			var value = jQOld(this).attr("src").replace("buttonlight", "buttonlightb");
			return value;
		});
	});
	
	jQOld(".shortcode-button").mouseleave(function(){
		jQOld(this).find('img').attr('src', function(){ 
			var value = jQOld(this).attr("src").replace("buttonlightb", "buttonlight");
			return value;
		});
	});
});


/* Social icon effect
================================================== */
jQOld(document).ready(function() {
	jQOld(".social-icon a").mouseenter(function() {
		jQOld(this).fadeTo(400,.5);
		jQOld(this).children('img').animate({"top":"-20px","opacity":"0"},400,function() {
		jQOld(this).css({"top":"0px"}).fadeTo(200,1);
		});
	});
	jQOld(".social-icon a").mouseleave(function() {
		jQOld(this).fadeTo(400,1);
	});
});


/* Scroll Top
================================================== */
jQOld(document).ready(function() {
	jQOld(window).scroll(function () {
		if (jQOld(this).scrollTop() > 300) {
			jQOld('#backtotop').fadeIn();
		} else {
			jQOld('#backtotop').fadeOut();
		}
	});

	jQOld('#backtotop, div.divider .divider-gotop').click(function () {
		jQOld('body,html').stop(false, false).animate({
			scrollTop: 0
		}, 800);
		return false;
	});
});


/* Progress Bar
================================================== */
jQOld(document).ready(function() {
	jQOld('.progress-bar-meter img').each(function() {
		jQOld(this).width(jQOld(this).parent().width());
		jQOld(this).height(jQOld(this).parent().height());
	});
});
jQOld(window).load(function() {
	jQOld('.progress-bar-wrapper').each(function() {
		var a = jQOld(this).find('.value').text();
		jQOld(this).find('.progress-bar-meter').css('width','0');
		jQOld(this).find('.progress-bar-meter').css('display','block').delay(1000).animate({'width':a});
	});
});


/* Flickr Preview
================================================== */
this.imagePreview = function(){
	
	xOffset = 30;
	yOffset = 10;
	var winwid = jQOld(window).width();
	winwid = winwid/2;
	
	jQOld("a.flickr-image").hover(function(e){
		jQOld("body").append("<div id='flickr-preview'><img src='"+ jQOld(this).attr('alt') +"' alt='preview' /></div>");	
		if (e.pageX < winwid) { var a = (e.pageX + xOffset) } else { a = (e.pageX - xOffset - jQOld("#flickr-preview img").width())}
		jQOld("#flickr-preview")
			.css("top",(e.pageY - yOffset) + "px")
			.css("left",a + "px")
			.fadeIn("fast");						
    },
	function(){
		jQOld("#flickr-preview").remove();
    });	
	jQOld("a.flickr-image").mousemove(function(e){
		if (e.pageX < winwid) { var a = (e.pageX + xOffset) } else { a = (e.pageX - xOffset - jQOld("#flickr-preview img").width())}
		jQOld("#flickr-preview")
			.css("top",(e.pageY - yOffset) + "px")
			.css("left",a + "px");
	});			
};

jQOld(document).ready(function(){
	imagePreview();
});


/* Accordion
================================================== */
jQOld(document).ready(function(){
	
	jQOld("ul.pa-accordion li").each(function(){
		jQOld(this).children(".accordion-content").css('height', function() { 
			return jQOld(this).height(); 
		});
		
		if (jQOld(this).index() > 0) {
			jQOld(this).children(".accordion-content").css('display','none');
		} else {
			jQOld(this).addClass('active').find(".accordion-head-sign").html("&minus;");
			jQOld(this).siblings("li").find(".accordion-head-sign").html("&#43;");
		}
		
		jQOld(this).children(".accordion-head").bind("click", function() {
			jQOld(this).parent().addClass(function() {
				if (jQOld(this).hasClass("active")) return "";
				return "active";
			});
			jQOld(this).siblings(".accordion-content").slideDown();
			jQOld(this).parent().find(".accordion-head-sign").html("&minus;");
			jQOld(this).parent().siblings("li").children(".accordion-content").slideUp();
			jQOld(this).parent().siblings("li").removeClass("active");
			jQOld(this).parent().siblings("li").find(".accordion-head-sign").html("&#43;");
		});
	});
});
	

/* Toggle
================================================== */
jQOld(document).ready(function(){
		
	jQOld("ul.pa-toggle li").each(function(){
		jQOld(this).children(".toggle-content").css('height', function() { 
			return jQOld(this).height(); 
		});
		
		jQOld(this).children(".toggle-content").css('display','none');
		jQOld(this).find(".toggle-head-sign").html("&#43;");
		
		jQOld(this).children(".toggle-head").bind("click", function() {
		
			if (jQOld(this).parent().hasClass("active")) jQOld(this).parent().removeClass("active");
			else jQOld(this).parent().addClass("active");
			
			jQOld(this).find(".toggle-head-sign").html(function() {
				if (jQOld(this).parent().parent().hasClass("active")) return "&minus;";
				else return "&#43;";
			});
			jQOld(this).siblings(".toggle-content").slideToggle();
		});
	});
	
	jQOld("ul.pa-toggle").find(".toggle-content.active").siblings(".toggle-head").trigger('click');
});
	

/* Tab
================================================== */
jQOld(document).ready(function(){
	
	var tabs = jQOld('ul.tabs');

	tabs.each(function(i) {
		// get tabs
		var tab = jQOld(this).find('> li > a');
		tab.click(function(e) {
			// get tab's location
			var contentLocation = jQOld(this).attr('href');
			// Let go if not a hashed one
			if(contentLocation.charAt(0)=="#") {
				e.preventDefault();
				// add class active
				tab.removeClass('active');
				jQOld(this).addClass('active');
				// show tab content & add active class
				jQOld(contentLocation).fadeIn(1000).addClass('active').siblings().hide().removeClass('active');

			}
		});
	});
});
		

/* jcarousellite
================================================== */
jQOld(window).load(function() {

	// Call jcarousellite
	var carousellite = jQOld('.jcarousellite');
	carousellite.jCarouselLite({ btnNext: ".next", btnPrev: ".prev", visible: 1 });
	
});
		

/* create equal height for content and sidebar
================================================== */
jQOld(window).load(function() {
	
	var a = jQOld('#content').height();
	var b = jQOld('.grid_3.bothleft').height();
	var c = jQOld('.grid_3.bothright').height();
	var d = jQOld('.grid_4.indent.pleft').height();
	var e = jQOld('.grid_4.indent.pright').height();
	var columns = [a,b,c,d,e];
	var currentTallest = 0;
	
	for (i=0; i<columns.length; i++) {
		if (columns[i] > currentTallest) { currentTallest = columns[i] }
	}
	
	if (jQOld('#main div').hasClass('sidebar')) {
		jQOld('#content').css('min-height',currentTallest);
		jQOld('.grid_3.bothleft').css('min-height',currentTallest);
		jQOld('.grid_3.bothright').css('min-height',currentTallest);
		jQOld('.grid_4.indent.pleft').css('min-height',currentTallest);
		jQOld('.grid_4.indent.pright').css('min-height',currentTallest);
	}
});
		

/* create equal height for portfolio item
================================================== */
function equalHeight(group) {
	var tallest = 0;
	group.each(function() {
		var thisHeight = jQOld(this).height();
		if(thisHeight > tallest) {
			tallest = thisHeight;
		}
	});
	group.height(tallest);
}

jQOld(document).ready(function() {
		
	/*equalHeight(jQOld('#portfolio-item-wrapper li'));*/
	
	var a = jQOld('#port-content-wrapper > div');
	if (a.length > 1) {
		equalHeight(jQOld('#port-content-wrapper > div'));
	}
});



/* contact form
================================================== */
jQOld(document).ready(function() {
	
	jQOld('#contactForm').submit(function() {
		jQOld('#contactForm .error-message').remove();
		var hasError = false;
		jQOld('#contactForm .requiredField').each(function() {
            var labelText = jQOld(this).parent().prev('label').attr('data-rel');
			if(jQOld.trim(jQOld(this).val()) == '' || jQOld.trim(jQOld(this).val()) == labelText) {
            	jQOld(this).parent().append('<span class="error-message">Please enter your '+labelText+'</span>').find('.error-message').width(jQOld(this).parent().width()).height(jQOld(this).parent().height()-9).hover(function(e) { jQOld(this).fadeOut(300); });
            	jQOld(this).addClass('inputError');
            	hasError = true;
            } else if(jQOld(this).hasClass('email')) {
            	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            	if(!emailReg.test(jQOld.trim(jQOld(this).val()))) {
            		jQOld(this).parent().append('<span class="error-message">Please enter a valid email address</span>').find('.error-message').width(jQOld(this).parent().width()).height(jQOld(this).parent().height()-9).hover(function(e) { jQOld(this).fadeOut(300); });
            		jQOld(this).addClass('inputError');
            		hasError = true;
            	}
            }
		});
		
		if(!hasError) {
			if (jQOld('#contact-recaptcha').hasClass('true')) {
				return true;
			} else {
			jQOld('#contactForm #contact-submit').fadeOut('normal', function() {
				jQOld(this).parent().append('<div class="wait"></div>');
			});
			var formInput = jQOld(this).serialize();
			jQOld.post(jQOld(this).attr('action'), formInput, function(data){
				jQOld('#contactForm').slideUp('fast', function() {
					jQOld('#contactForm .wait').slideUp("fast");
					jQOld(this).before('<p class="message-box no-icon green"><strong>Thanks!</strong> Your email was successfully sent. I check my email all the time, so I should be in touch soon.</p>');
				});
			});
			}
		}
		return false;
	});
});


/* widget contact form
================================================== */
jQOld(document).ready(function() {
	
	jQOld('#widget-contactForm').submit(function() {
		jQOld('#widget-contactForm .error-message').remove();
		var hasError = false;
		jQOld('#widget-contactForm .requiredField').each(function() {
            var labelText = jQOld(this).siblings('label').attr('data-rel');
			if(jQOld.trim(jQOld(this).val()) == '' || jQOld.trim(jQOld(this).val()) == labelText) {
            	jQOld(this).parent().append('<span class="error-message">Please enter your '+labelText+'</span>').find('.error-message').width(jQOld(this).width()).height(jQOld(this).height()-7).delay(1000).fadeOut(350);
            	jQOld(this).addClass('inputError');
            	hasError = true;
            } else if(jQOld(this).hasClass('email')) {
            	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            	if(!emailReg.test(jQOld.trim(jQOld(this).val()))) {
            		jQOld(this).parent().append('<span class="error-message">Please enter a valid email address</span>').find('.error-message').width(jQOld(this).width()).height(jQOld(this).height()-7).delay(1000).fadeOut(350);
            		jQOld(this).addClass('inputError');
            		hasError = true;
            	}
            }
		});
		
		if(!hasError) {
			jQOld('#widget-contactForm #widget-contact-submit').fadeOut('normal', function() {
				jQOld(this).parent().append('<div class="wait"></div>');
			});
			var formInput = jQOld(this).serialize();
			jQOld.post(jQOld(this).attr('action'), formInput, function(data){
				jQOld('#widget-contactForm').slideUp('fast', function() {
					jQOld('#widget-contactForm .wait').slideUp("fast");
					jQOld(this).before('<p class="message-box no-icon green"><strong>Thanks!</strong> Your email was successfully sent. I check my email all the time, so I should be in touch soon.</p>');
				});
			});
		}
		return false;
	});
});