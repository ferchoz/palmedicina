-- phpMyAdmin SQL Dump
-- version 4.5.0-dev
-- http://www.phpmyadmin.net
--
-- Servidor: 192.168.0.66
-- Tiempo de generación: 21-03-2016 a las 17:56:50
-- Versión del servidor: 5.5.31
-- Versión de PHP: 5.3.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `new_palmedicina`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acl_classes`
--

CREATE TABLE IF NOT EXISTS `acl_classes` (
  `id` int(10) unsigned NOT NULL,
  `class_type` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acl_entries`
--

CREATE TABLE IF NOT EXISTS `acl_entries` (
  `id` int(10) unsigned NOT NULL,
  `class_id` int(10) unsigned NOT NULL,
  `object_identity_id` int(10) unsigned DEFAULT NULL,
  `security_identity_id` int(10) unsigned NOT NULL,
  `field_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ace_order` smallint(5) unsigned NOT NULL,
  `mask` int(11) NOT NULL,
  `granting` tinyint(1) NOT NULL,
  `granting_strategy` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `audit_success` tinyint(1) NOT NULL,
  `audit_failure` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acl_object_identities`
--

CREATE TABLE IF NOT EXISTS `acl_object_identities` (
  `id` int(10) unsigned NOT NULL,
  `parent_object_identity_id` int(10) unsigned DEFAULT NULL,
  `class_id` int(10) unsigned NOT NULL,
  `object_identifier` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `entries_inheriting` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acl_object_identity_ancestors`
--

CREATE TABLE IF NOT EXISTS `acl_object_identity_ancestors` (
  `object_identity_id` int(10) unsigned NOT NULL,
  `ancestor_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acl_security_identities`
--

CREATE TABLE IF NOT EXISTS `acl_security_identities` (
  `id` int(10) unsigned NOT NULL,
  `identifier` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `username` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fos_user_group`
--

CREATE TABLE IF NOT EXISTS `fos_user_group` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `fos_user_group`
--

INSERT INTO `fos_user_group` (`id`, `name`, `roles`) VALUES
(1, 'Grupo de Administradores', 'a:3:{i:0;s:10:"ROLE_ADMIN";i:1;s:9:"ROLE_USER";i:2;s:17:"ROLE_SONATA_ADMIN";}');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fos_user_user`
--

CREATE TABLE IF NOT EXISTS `fos_user_user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `date_of_birth` datetime DEFAULT NULL,
  `firstname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `biography` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locale` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `twitter_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `gplus_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gplus_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gplus_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `two_step_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `fos_user_user`
--

INSERT INTO `fos_user_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`, `created_at`, `updated_at`, `date_of_birth`, `firstname`, `lastname`, `website`, `biography`, `gender`, `locale`, `timezone`, `phone`, `facebook_uid`, `facebook_name`, `facebook_data`, `twitter_uid`, `twitter_name`, `twitter_data`, `gplus_uid`, `gplus_name`, `gplus_data`, `token`, `two_step_code`) VALUES
(1, 'root', 'root', 'root@mail.com', 'root@mail.com', 1, 'odir9sy4ty8gwk00sw8kk8g4wggoc0c', 'aIMlQhifeAaECU+c5wJ0BvALdD81fMGJz+ic6jLxm0go8Ub/naxdOPbcM75iZW064rTNxRSbcDxSD2D/D18/nw==', '2015-03-16 09:35:14', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:16:"ROLE_SUPER_ADMIN";}', 0, NULL, '2015-03-15 15:27:26', '2015-03-16 09:35:14', NULL, NULL, NULL, NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(2, 'admin', 'admin', 'admin@admin.com', 'admin@admin.com', 1, '2sehpgg81f8kcc8oc088o4cc4c0cw08', '3NbMhp1XpuFOOM0jRf4CjILlzILxWmzH/T5nsAVI8WNw9bsac5MK4aWrgq/AunnPCT1OnHX5QMSsIv7zv6Xt4g==', '2015-09-16 12:40:53', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:10:"ROLE_ADMIN";}', 0, NULL, '2015-03-15 15:29:37', '2015-09-16 12:40:53', NULL, NULL, NULL, NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(3, 'slange', 'slange', 'slange@pathcomunicacion.com.ar', 'slange@pathcomunicacion.com.ar', 1, 'baxd8exn7f48ocss4o4kwcsowgs00oc', 'R1fItja1pGqqv1SS3daLSmDZGy357/GdeVtyUXNuhIVO8LEKvj+8LowX9EE1AdO35NVbjqK1ruTrZ+NKNDbglw==', NULL, 0, 0, NULL, NULL, NULL, 'a:2:{i:0;s:10:"ROLE_ADMIN";i:1;s:17:"ROLE_SONATA_ADMIN";}', 0, NULL, '2015-07-29 09:22:03', '2015-07-29 09:22:31', '1967-11-11 00:00:00', 'Sergio', 'Lange', NULL, NULL, 'm', 'es_AR', 'America/Argentina/Buenos_Aires', NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fos_user_user_group`
--

CREATE TABLE IF NOT EXISTS `fos_user_user_group` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `media__gallery`
--

CREATE TABLE IF NOT EXISTS `media__gallery` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `context` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `default_format` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `media__gallery_media`
--

CREATE TABLE IF NOT EXISTS `media__gallery_media` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `media_id` int(11) DEFAULT NULL,
  `position` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `media__media`
--

CREATE TABLE IF NOT EXISTS `media__media` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `enabled` tinyint(1) NOT NULL,
  `provider_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_status` int(11) NOT NULL,
  `provider_reference` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_metadata` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `length` decimal(10,0) DEFAULT NULL,
  `content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content_size` int(11) DEFAULT NULL,
  `copyright` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `context` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cdn_is_flushable` tinyint(1) DEFAULT NULL,
  `cdn_flush_at` datetime DEFAULT NULL,
  `cdn_status` int(11) DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `media__media`
--

INSERT INTO `media__media` (`id`, `name`, `description`, `enabled`, `provider_name`, `provider_status`, `provider_reference`, `provider_metadata`, `width`, `height`, `length`, `content_type`, `content_size`, `copyright`, `author_name`, `context`, `cdn_is_flushable`, `cdn_flush_at`, `cdn_status`, `updated_at`, `created_at`) VALUES
(1, 'slide1.jpg', NULL, 0, 'sonata.media.provider.image', 1, '6c4c7860dc942e762f6959b407218961d83eff7b.jpeg', '{"filename":"slide1.jpg"}', 1920, 550, NULL, 'image/jpeg', 1900664, NULL, NULL, 'slider_company', NULL, NULL, NULL, '2015-03-10 23:19:39', '2015-03-10 23:19:39'),
(2, 'slide2.jpg', NULL, 0, 'sonata.media.provider.image', 1, '35e559e8db7ecab63c82b3cd9c6d96eaf4401748.jpeg', '{"filename":"slide2.jpg"}', 1920, 550, NULL, 'image/jpeg', 1892857, NULL, NULL, 'slider_company', NULL, NULL, NULL, '2015-03-10 23:20:50', '2015-03-10 23:20:50'),
(3, 'slide3.jpg', NULL, 0, 'sonata.media.provider.image', 1, '9079aa4bcbe2505c21e10088a29598af206ca901.jpeg', '{"filename":"slide3.jpg"}', 1920, 550, NULL, 'image/jpeg', 1700088, NULL, NULL, 'slider_company', NULL, NULL, NULL, '2015-03-10 23:21:50', '2015-03-10 23:21:50');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ServiciosAccidenteTrabajo`
--

CREATE TABLE IF NOT EXISTS `ServiciosAccidenteTrabajo` (
  `id` int(11) NOT NULL,
  `RazonSocial` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ApellidoNombreSolicitante` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Fecha` date NOT NULL,
  `ART` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ApellidoNombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DiagnosticoPresuntivo` longtext COLLATE utf8_unicode_ci,
  `isRead` tinyint(1) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `ServiciosAccidenteTrabajo`
--

INSERT INTO `ServiciosAccidenteTrabajo` (`id`, `RazonSocial`, `ApellidoNombreSolicitante`, `Email`, `Fecha`, `ART`, `ApellidoNombre`, `DiagnosticoPresuntivo`, `isRead`, `created`) VALUES
(1, 'x', 'x', 'x@x.com', '2015-03-31', 'Prevención', 'aa', 'aa', 1, '2015-03-20 21:02:27'),
(2, 'H', 'Y', 'eba@h.com', '2016-02-08', 'Asociart', 'T', 'Y', 0, '2016-02-08 12:17:57'),
(3, 'yd', 'yd', 'yd@e.com', '2016-02-18', 'La Meridional', 'ydd', 'ydd', 0, '2016-02-18 10:57:57'),
(4, 'yd', 'yd', 'yd@e.com', '2016-02-18', 'La Meridional', 'ydd', 'ydd', 0, '2016-02-18 11:10:31'),
(5, 'EW', 'K', 'EBABOR@FIBERTEL.COM.AR', '2016-02-19', 'Asociart', 'W', 'E', 0, '2016-02-19 10:53:01'),
(6, 'd', 'i', 'gueebara@go.com', '2016-02-23', 'Asociart', 'd', 'd', 0, '2016-02-23 11:34:16'),
(7, 'PRUENA EZE', 'D', 'E@C.COM', '2016-02-23', 'Asociart', 'WE', 'E', 0, '2016-02-23 14:57:59');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ServiciosAtencionConsultorio`
--

CREATE TABLE IF NOT EXISTS `ServiciosAtencionConsultorio` (
  `id` int(11) NOT NULL,
  `RazonSocial` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ApellidoNombreSolicitante` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Fecha` date NOT NULL,
  `ApellidoNombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Observaciones` longtext COLLATE utf8_unicode_ci,
  `ConsultaClinica` tinyint(1) NOT NULL,
  `traumatologia` tinyint(1) NOT NULL,
  `cardiologia` tinyint(1) NOT NULL,
  `controlAusentismo` tinyint(1) NOT NULL,
  `dermatologia` tinyint(1) NOT NULL,
  `neurologia` tinyint(1) NOT NULL,
  `oftalmologia` tinyint(1) NOT NULL,
  `ginecologia` tinyint(1) NOT NULL,
  `otros` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isRead` tinyint(1) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `ServiciosAtencionConsultorio`
--

INSERT INTO `ServiciosAtencionConsultorio` (`id`, `RazonSocial`, `ApellidoNombreSolicitante`, `Email`, `Fecha`, `ApellidoNombre`, `Observaciones`, `ConsultaClinica`, `traumatologia`, `cardiologia`, `controlAusentismo`, `dermatologia`, `neurologia`, `oftalmologia`, `ginecologia`, `otros`, `isRead`, `created`) VALUES
(1, 'D', 'F', 'SD@O.COM', '2015-12-14', 'S', 'D', 0, 0, 0, 0, 1, 0, 0, 0, NULL, 0, '2015-12-14 11:55:29'),
(2, 'G', 'G', 'eba@h.com', '2016-02-08', 'G', 'G', 0, 0, 0, 1, 0, 0, 0, 0, NULL, 0, '2016-02-08 12:16:38');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ServiciosContactUs`
--

CREATE TABLE IF NOT EXISTS `ServiciosContactUs` (
  `id` int(11) NOT NULL,
  `RazonSocial` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ApellidoNombreSolicitante` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `consulta` longtext COLLATE utf8_unicode_ci NOT NULL,
  `isRead` tinyint(1) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ServiciosEncuestaSatisfaccion`
--

CREATE TABLE IF NOT EXISTS `ServiciosEncuestaSatisfaccion` (
  `id` int(11) NOT NULL,
  `empresa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `responde` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cargo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comoConocioElServicio` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `porQueNosEligio` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cuantoTiempo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `conQueFrecuencia` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `satisfaccionGeneral` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `haRecomendado` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `recomendariaNuestroServicio` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `atencionTelefonica` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `visitasDomicilio` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `examenesMedicos` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `art` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facturacion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `paginaWeb` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `conQueFrecuenciaUtilizaElSitio` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comoConsideraLaVisualizacion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comoEvaluaLaFacilidad` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comoCalificariaAccesoClientes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `opinion` longtext COLLATE utf8_unicode_ci,
  `experienciaSugerencias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `haTenidoAlgunProblema` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seResolvieronEsosProblemas` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentarios` longtext COLLATE utf8_unicode_ci,
  `isRead` tinyint(1) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `ServiciosEncuestaSatisfaccion`
--

INSERT INTO `ServiciosEncuestaSatisfaccion` (`id`, `empresa`, `responde`, `cargo`, `comoConocioElServicio`, `porQueNosEligio`, `cuantoTiempo`, `conQueFrecuencia`, `satisfaccionGeneral`, `haRecomendado`, `recomendariaNuestroServicio`, `atencionTelefonica`, `visitasDomicilio`, `examenesMedicos`, `art`, `facturacion`, `paginaWeb`, `conQueFrecuenciaUtilizaElSitio`, `comoConsideraLaVisualizacion`, `comoEvaluaLaFacilidad`, `comoCalificariaAccesoClientes`, `opinion`, `experienciaSugerencias`, `haTenidoAlgunProblema`, `seResolvieronEsosProblemas`, `comentarios`, `isRead`, `created`) VALUES
(1, 'G', 'G', 'T', 'Internet', 'Precio', 'Menos de un mes', 'Dos o tres veces al mes', 'Completamente satisfecho', 'Sí', 'Sí', 'Excelente', 'Excelente', 'Excelente', 'Excelente', 'Excelente', 'No', 'Dos o tres veces al mes', 'Excelente', 'Muy buena', 'Excelente', NULL, 'Sí', 'Sí', 'Sí', NULL, 0, '2016-02-08 14:57:09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ServiciosExamenMedico`
--

CREATE TABLE IF NOT EXISTS `ServiciosExamenMedico` (
  `id` int(11) NOT NULL,
  `RazonSocial` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ApellidoNombreSolicitante` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Fecha` date NOT NULL,
  `ApellidoNombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `CargoTarea` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Observaciones` longtext COLLATE utf8_unicode_ci,
  `Ingreso` tinyint(1) NOT NULL,
  `Periodico` tinyint(1) NOT NULL,
  `Egreso` tinyint(1) NOT NULL,
  `AusenciaProlongada` tinyint(1) NOT NULL,
  `CambioDeTareas` tinyint(1) NOT NULL,
  `LibretaSanitaria` tinyint(1) NOT NULL,
  `Audiometria` tinyint(1) NOT NULL,
  `Psicotecnico` tinyint(1) NOT NULL,
  `DapTest` tinyint(1) NOT NULL,
  `RxColumnaLumbarFrente` tinyint(1) NOT NULL,
  `RxColumnaFrentePerfil` tinyint(1) NOT NULL,
  `GrupoFactorSanguineo` tinyint(1) NOT NULL,
  `Otros` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isRead` tinyint(1) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `ServiciosExamenMedico`
--

INSERT INTO `ServiciosExamenMedico` (`id`, `RazonSocial`, `ApellidoNombreSolicitante`, `Email`, `Fecha`, `ApellidoNombre`, `CargoTarea`, `Observaciones`, `Ingreso`, `Periodico`, `Egreso`, `AusenciaProlongada`, `CambioDeTareas`, `LibretaSanitaria`, `Audiometria`, `Psicotecnico`, `DapTest`, `RxColumnaLumbarFrente`, `RxColumnaFrentePerfil`, `GrupoFactorSanguineo`, `Otros`, `isRead`, `created`) VALUES
(1, 'TY', 'Y', 'YQW@L.COM', '2015-12-14', 'U', 'U', 'U', 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'D', 0, '2015-12-14 11:54:45'),
(2, 'Eze', 'V', 'eba@h.com', '2016-02-08', 'T', 'Y', NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, '2016-02-08 12:16:07'),
(3, 'V', 'T', 'eba@h.com', '2016-02-08', 'Y', 'Y', 'T', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, '2016-02-08 12:35:58'),
(4, 'Y', 'G', 'eba@h.com', '2016-02-08', 'G', 'T', 'G', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, '2016-02-08 12:44:24');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ServiciosVisitaDomicilio`
--

CREATE TABLE IF NOT EXISTS `ServiciosVisitaDomicilio` (
  `id` int(11) NOT NULL,
  `RazonSocial` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ApellidoNombreSolicitante` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Fecha` date NOT NULL,
  `ApellidoNombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `domicilio` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `numero` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `piso` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `departamento` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entreCalles` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `localidad` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provincia` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `observaciones` longtext COLLATE utf8_unicode_ci,
  `isRead` tinyint(1) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `ServiciosVisitaDomicilio`
--

INSERT INTO `ServiciosVisitaDomicilio` (`id`, `RazonSocial`, `ApellidoNombreSolicitante`, `Email`, `Fecha`, `ApellidoNombre`, `domicilio`, `numero`, `piso`, `departamento`, `entreCalles`, `localidad`, `provincia`, `observaciones`, `isRead`, `created`) VALUES
(1, 'vbzb', 'ad', 's@s.com.ar', '2015-03-25', 's', 's', 's', 'a', 's', 's', 's', 's', 's', 0, '2015-03-20 20:54:40'),
(2, 'sdfsfddfsfsd', 'sdsfdsffsdsdf', 'sdsdf@sas.com', '2015-08-11', 'sdsdfsdf', 'sddfsfsdfsdfsd', 'sdffsdsdf', 'sdfdsfsfd', 'dfsfsdsdf', 'sdffsdsdfdfs', 'dsfdfsdsf', 'sddfsfsd', 'sdffsdfsd', 0, '2015-08-11 15:36:09'),
(3, 'F', 'F', 'sdsdf@sas.com', '2015-12-14', 'D', 'KI', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 0, '2015-12-14 11:56:24'),
(4, 'miguel miguelo', 'pepe peponn', 'mail@mail.com', '2016-01-14', 'dasdasdasd asdasd', 'dasdas', '12121', '3', '4', 'miguel y pepe', 'localidad', 'provincia', 'observaciones', 0, '2016-01-31 16:27:51'),
(5, 'miguel miguelo', 'pepe peponn', 'mail@mail.com', '2016-01-14', 'dasdasdasd asdasd', 'dasdas', '12121', '3', '4', 'miguel y pepe', 'localidad', 'provincia', 'observaciones', 0, '2016-01-31 16:28:07'),
(6, 'miguel miguelo', 'pepe peponn', 'mail@mail.com', '2016-01-14', 'dasdasdasd asdasd', 'dasdas', '12121', '3', '4', 'miguel y pepe', 'localidad', 'provincia', 'observaciones', 0, '2016-01-31 16:32:20'),
(7, 'PRUEBA', 'E', 'i@sd.com', '2016-02-08', 'E', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 0, '2016-02-01 11:16:56'),
(8, 'PRUEBA', 'EEEEE', 'sdsdf@sas.com', '2016-02-01', 'J', 'J', 'J', 'J', 'J', 'J', 'J', 'J', 'J', 0, '2016-02-01 12:14:38'),
(9, 'PRUEBA 23545', 'E', 'PRUEBA235@SPEY.COM', '2016-02-01', 'H', 'H', 'H', 'H', 'H', 'H', 'H', 'H', 'H', 0, '2016-02-01 13:46:32'),
(10, 'Asas', 'w', 'aaaaaaaaa2@com.com', '2016-02-01', 'u', 'u', 'uu', 'u', NULL, 'u', 'u', 'u', NULL, 0, '2016-02-01 17:05:57'),
(11, 'adsda', 'dsadas', 'dsds@mail.com', '2016-02-17', 'asdfsd', 'fasdfa', '324342', NULL, NULL, NULL, 'asfasdfdasf', 'afsdfasdfaf', 'sfasdfasdfsdf', 0, '2016-02-02 12:20:01'),
(12, 'DFFDFD', 'FDFDDFF', 'VISITASWEBNUEVA@FER.COM', '2016-02-02', 'Y', 'Y', 'Y', 'Y', 'Y', NULL, 'Y', 'Y', 'Y', 0, '2016-02-02 12:31:41'),
(13, 'FERNANDO PRUEBA', 'EIEIEI', 'sdsdf@sas.com', '2016-02-02', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 0, '2016-02-02 12:34:18'),
(14, 'test', 'es una prueab', 'frivas@prueba.com', '2016-02-11', 'adsfd sfa dsafsdfasdf', 'domicilio', '123', NULL, NULL, NULL, 'popoppop', 'prep reprep', 'observacion', 0, '2016-02-02 13:10:01'),
(15, 'prueba', 'prueba', 'prueba@prueba.com', '2016-02-02', 'pruebaprueba', 'prueba', 'prueba', NULL, NULL, NULL, 'prueba', 'prueba', 'pruebapruebapruebaprueba', 0, '2016-02-02 13:15:54'),
(16, 'prueba', 'prueba', 'prueba@prueba.com', '2016-02-02', 'pruebaprueba', 'prueba', 'prueba', NULL, NULL, NULL, 'prueba', 'prueba', 'pruebapruebapruebaprueba', 0, '2016-02-02 13:20:29'),
(17, 'pruebaaaaaaaa', 'pruebaaaaaaaa', 'prueba@prueba.com', '2016-02-02', 'pruebaaaaaaaa', 'pruebaaaaaaaa', 'pruebaaaaaaaa', NULL, NULL, NULL, 'pruebaaaaaaaa', 'pruebaaaaaaaa', 'pruebaaaaaaaapruebaaaaaaaapruebaaaaaaaapruebaaaaaaaapruebaaaaaaaa', 0, '2016-02-02 13:32:24'),
(18, 'RPEU', 'PR', 'PRUEBA235@SPEY.COM', '2016-02-02', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 0, '2016-02-02 13:41:24'),
(19, 'dfdf', 'fjdfj', 'VISITASWEBNUEVA@FER.COM', '2016-02-02', 'dt', 't', 't', 't', 't', NULL, 't', 't', 't', 0, '2016-02-02 13:52:44'),
(20, 'pruebaaaaaaaa', 'pruebaaaaaaaa', 'prueba@prueba.com', '2016-02-02', 'pruebaaaaaaaa', 'pruebaaaaaaaa', 'pruebaaaaaaaa', NULL, NULL, NULL, 'pruebaaaaaaaa', 'pruebaaaaaaaa', 'pruebaaaaaaaapruebaaaaaaaapruebaaaaaaaapruebaaaaaaaapruebaaaaaaaa', 0, '2016-02-02 14:03:04'),
(21, 'pruebaaaaaaaa', 'pruebaaaaaaaa', 'prueba@prueba.com', '2016-02-02', 'pruebaaaaaaaa', 'pruebaaaaaaaa', 'pruebaaaaaaaa', NULL, NULL, NULL, 'pruebaaaaaaaa', 'pruebaaaaaaaa', 'pruebaaaaaaaapruebaaaaaaaapruebaaaaaaaapruebaaaaaaaa', 0, '2016-02-02 14:05:18'),
(22, 'prueba', 'pruebaaaaaaaa', 'prueba@prueba.com', '2016-02-02', 'pruebaaaaaaaa', 'pruebaaaaaaaa', 'pruebaaaaaaaa', NULL, NULL, NULL, 'pruebaaaaaaaa', 'pruebaaaaaaaapruebaaaaaaaapruebaaaaaaaa', 'pruebaaaaaaaapruebaaaaaaaapruebaaaaaaaapruebaaaaaaaa', 0, '2016-02-02 14:13:46'),
(23, 'CENTRO DIAGNOSTICO LANUS', 'EZEQUIEL', 'EBABOR@FIBERTEL.COM.AR', '2016-02-02', 'R', 'R', 'R', 'R', 'R', 'R', 'R', 'E', 'R', 0, '2016-02-02 14:15:41'),
(24, 'sdfsfddfsfsd', 'J', 'EBABOR@FIBERTEL.COM.AR', '2016-02-02', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 0, '2016-02-02 15:20:03'),
(25, 'E', 'E', 'EBABOR@FIBERTEL.COM.AR', '2016-02-09', 'T', 'YTY', 'T', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 0, '2016-02-02 15:28:10'),
(26, 'w', 'u', 'gueebara@hotmail.com', '2016-02-02', 's', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 0, '2016-02-02 15:37:18'),
(27, 'R', 'Y', 'PRUEBA235@SPEY.COM', '2016-02-04', 'T', 'T', 'T', 'T', 'T', 'D', 'T', 'T', 'T', 0, '2016-02-04 12:00:42'),
(28, 'Y', 'Y', 'PRUEBA235@SPEY.COM', '2016-02-04', 'Y', 'T', 'TT', 'T', NULL, 'T', 'T', 'T', 'T', 0, '2016-02-04 12:01:52'),
(29, 'Y', 'D', 'PRUEBANUEVA@SPEY.COM', '2016-02-16', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 0, '2016-02-04 15:30:12'),
(30, 'G', 'G', 'eba@h.com', '2016-02-08', 'G', 'Y', 'G', 'G', 'G', 'G', 'G', 'G', NULL, 0, '2016-02-08 12:17:12'),
(31, 'PRUEBA 23545', 'i', 'EBABOR@FIBERTEL.COM.AR', '2016-02-19', 'ii', 'i', 'i', 'i', 'i', 'd', 'ii', 'd', 'i', 0, '2016-02-19 10:21:07'),
(32, 'aaaaaaaa', 'aaaaaaaa', 'aaaaaaaaaa@aaaaaaaa.com', '2016-02-22', 'aaaaaaa', 'aaaaaaaaaaaa', 'aaaaaaaaaa', 'aaaaaaaaa', 'aaaaaaaa', 'aaaaaaaaaa', 'aaaaaaaaa', 'aaaaaaaaaa', 'aaaaaaaaaaaaaaaaaa', 0, '2016-02-22 15:44:48'),
(33, 'RE', 'E', 'W@D.AR', '2016-02-23', 'U', 'U', 'U', 'U', 'U', 'U', 'D', 'U', 'U', 0, '2016-02-23 15:42:58'),
(34, 'aaaaa', 'aaaaa', 'aaaaaaaaaa@aaaaaaaa.com', '2016-03-02', 'aaa', 'aaaaaa', 'aaaaaaa', 'aaaaaaaa', 'aaaaaaaa', 'aaaaaaaa', 'aaaaaa', 'aaaaaaa', 'aaaaaaaaaaaaaaa', 0, '2016-03-02 10:40:09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `SlideCompany`
--

CREATE TABLE IF NOT EXISTS `SlideCompany` (
  `id` int(11) NOT NULL,
  `image_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img_order` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `SlideCompany`
--

INSERT INTO `SlideCompany` (`id`, `image_id`, `title`, `img_order`) VALUES
(1, 1, 'slide 1', 0),
(2, 2, 'slide 2', 0),
(3, 3, 'slide 3', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Usuarios`
--

CREATE TABLE IF NOT EXISTS `Usuarios` (
  `id` int(11) NOT NULL,
  `Codigopal` varchar(6) NOT NULL,
  `Nombrepal` varchar(50) NOT NULL,
  `Clave` varchar(10) NOT NULL,
  `Usuario` varchar(20) NOT NULL,
  `Activo` varchar(1) NOT NULL,
  `Correo` varchar(110) NOT NULL,
  `Domicilio` varchar(110) NOT NULL,
  `Localidad` varchar(110) NOT NULL,
  `Telefono` varchar(110) NOT NULL,
  `Contacto` varchar(110) NOT NULL,
  `Actividad` varchar(110) NOT NULL,
  `Correop` varchar(110) NOT NULL,
  `Nombart` varchar(110) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=1711 DEFAULT CHARSET=latin1 COMMENT='Tabla para login';

--
-- Volcado de datos para la tabla `Usuarios`
--

INSERT INTO `Usuarios` (`id`, `Codigopal`, `Nombrepal`, `Clave`, `Usuario`, `Activo`, `Correo`, `Domicilio`, `Localidad`, `Telefono`, `Contacto`, `Actividad`, `Correop`, `Nombart`) VALUES
(1, '8058', 'MICRO OMNIBUS 45', '8058', 'DIEGO', 'S', 'personal@mo45.com.ar,mo45mg@speedy.com.ar ', '14 DE JULIO 4100', 'R.DE ESCALADA OESTE', '4247-4834/7090', 'HECTOR CASTRO', 'SERVICIOS', 'Enviar dato faltante !! ', 'PROVINCIA'),
(2, '8061', 'TRANSP. ALTE BROWN (LINEA 33)', '8061', 'DIEGO', 'S', 'personal@mo45.com.ar,ruben.omar56@hotmail.com ', '14 DE JULIO 4100', 'R.DE ESCALADA OESTE', '4241-4351', 'Enviar, dato faltante !!', 'TRANSP.PASAJE.', 'Enviar dato faltante !! ', 'PROVINCIA'),
(3, '8060', 'TRANSPORTE 270', '8060', 'DIEGO', 'S', 'personal@mo45.com.ar,mo45mg@speedy.com.ar ', '14 DE JULIO 4100', 'R.DE ESCALADA OESTE', '4241-4351', 'HECTOR CASTRO/DIAZ', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'PROVINCIA'),
(4, '8062', 'EL URBANO', '8062', 'HECTOR', 'S', 'elurbanosrl@hotmail.com ', 'TENIENTE RANGUGNI 4059', 'R.DE ESCALADA OESTE', '4249-8754', 'CASTRO/LILIANA', 'TRANSPORTE DE PASAJEROS', 'Enviar dato faltante !! ', 'PROVINCIA'),
(5, '1878', 'GALILEO LA RIOJA', '1878', 'ANA', 'S', 'ana.vence@elster.com.ar ', 'JOSE I. RUCCI 1051', 'VALENTIN ALSINA', '4229-5600 INT 5657', 'SR PELACANACHIS', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'GALENO'),
(6, '13225', 'TRANDECAR', '13225', 'JORGE', 'S', 'rodriguez_sa@speedy.com.ar ', 'SAN LORENZO 628', 'VILLA DOMINICO', '4207-8211', 'Enviar dato faltante !!', 'TRANSPORTE', 'Enviar dato faltante !! ', 'ASOCIART'),
(7, '13480', 'TARSA LINEA 100', '13480', 'RIVAS', 'S', 'rivas-tarsa@hotmail.com ', 'PICHINCHA 1765', 'CAPITAL', '4308-0168', 'SR RIVAS', 'TRANSPORTE', 'Enviar dato faltante !! ', 'INTERACCION'),
(8, '10001', 'DZ S.A.', '10001', 'ADELQUI', 'S', 'adelqip@ciudad.com.ar,adelquip@ciudad.com.ar ', 'TTE. FRAL. JUAN D. PERON 725 4P', 'CAPITAL FEDERAL', '4202-2221', 'DARIO PEÑA-LAURA', 'REPRESENTACIONES Y MANDATOS', 'Enviar dato faltante !! ', 'MAPFRE'),
(9, '1002', 'EMBUTIDOS RUSSO SRL', '1002', 'ALEJANDRO', 'S', 'embutidosrussosrl@speedy.com.ar ', 'LAS PIEDRAS 2529', 'LANUS ESTE', '240-2248 / 241-6353', 'JORGE', 'FRIGORIFICO', 'Enviar dato faltante !! ', 'GALENO'),
(10, '10031', 'SERTECNI S.A', '10031', 'NATALIA', 'S', 'administracion@sertecni.com.ar,administracionlomas@attendance.com.ar', 'AV HIPOLITO IRIGOYEN 9228', 'LOMAS DE ZAMORA', '4239-2131', 'PABLO CEBREIRO', 'SERVICIO TECNICO', 'Enviar dato faltante !! ', 'GALENO'),
(11, '10033', 'ASOCIACION COOPERADORA ESCUELA NRO 40', '10033', 'STELLA', 'S', 'stella_3615@hotmail.com ', 'VILLEGAS 361', 'REMEDIOS DE ESCALADA OESTE', '4242-0275', 'SUSANA-STELLA', 'EDUCATIVA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(12, '10040', 'MARNOTES FRANCISCO', '10040', 'FRANCISCO', 'S', 'pap_sanflorencio@speedy.com.ar ', 'JUJUY 1968', 'LANUS OESTE', '4225-2863', 'MARIA ROSA', 'PAPELERA', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(13, '10047', 'MARKET SELF S.A', '10047', 'MARKETSELF', 'S', 'gcoll@marketself.com.ar,anaguzman@marketself.com.ar,mcastellanos@marketself.com.ar,ibonity@marketself.com.ar', 'PASEO COLON 221 PISO 6', 'CAPITAL FEDERAL', '4209-2200/4706-2232', 'ESTEFANIA FUNES', 'DISTRIBUIDORA DE DIARIOS Y REV', 'aguzman@marketself.com.ar,efunes@marketself.com.ar,gcoll@marketself.com.ar', 'GALENO'),
(14, '1005', 'ALONSO HNOS.', '1005', 'SILVANA', 'S', 'silvana.massa@alonsohnos.com ', 'SAN LORENZO 1771', 'LANUS ESTE', '4246-6869/9860', 'SILVANA MASSA', 'METALURGICA', 'rocio.alonso@alonsohnos.com ', 'BERKLEY INTERNATIONAL'),
(15, '10052', 'PANS COMPANY S.A', '10052', 'ELIZABETH', 'S', 'info@plasencia.com.ar,laura@productospozo.com.ar', 'SARMIENTO 1297', 'AVELLANEDA ESTE', '4204-1143/4204-0921', 'LAURA RRHH/LEONARDO', 'ALIMENTACION', 'pagos@productospozo.com.ar,ncrespo@productospozo.com.ar', 'LA CAJA'),
(16, '10092', 'CORRUCART S.R.L.', '10092', 'LILIANA', 'S', 'corrucart@infovia.com.ar,liliana@corrucartsrl.com.ar,mario@corrucartsrl.com.ar', 'CARLOS PELLEGRINI 124', 'VALENTIN ALSINA', '4208-7917', 'LILIANA', 'CORRUGADORA', 'Enviar dato faltante !! ', 'LA CAJA'),
(17, '10105', 'ACOSTA MARCELO JUAN', '10105', 'IVANA', 'S', 'corralonacosta@speedy.com.ar,acostamateriales@hotmail.com.ar,ivanacorralonacosta@hotmail.com', 'SANTA FE 2263 TIMBRE 2', 'AVELLANEDA OESTE', '4208-3352/4209-9806', 'IVANA/GUSTAVO', 'CONSTRUCCION', 'daiana.corralonacosta@hotmail.com ', 'PREVENCION'),
(18, '10108', 'MARROQUINERIA DOGA DE HECTOR GAMARNIK Y AIDA WAJSB', '10108', 'MIRIAM', 'S', 'mirifei@hotmail.com ', 'LAPRIDA 137', 'LOMAS DE ZAMORA', '4244-3727/43923311', 'MIRIAM', 'MARROQUINERIA', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(19, '1011', 'AGE sdh', '1011', 'MARINA', 'S', 'agesdh@hotmail.com,info@agesdeh.com ', 'ITUZAINGO 3244', 'LANUS ESTE', '230 -0580', 'AUGUSTO- KARINA', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'SMG'),
(20, '10111', 'KALPAT S.A.', '10111', 'KARINA', 'S', 'kbustelo@speedy.com.ar,info@kalpat.com.ar ', 'AV RIVADAVIA 1185', 'AVELLANEDA OESTE', '4208-3608', 'KARINA', 'EST DE SERVICIO', 'info@kalpat.com.ar ', 'SMG'),
(21, '1013', 'SUPERMERCADO AMERICA DEL NORTE', '1013', 'MYRIAM', 'S', 'miriam_almiron@hotmail.com,walteralmi@yahoo.com.ar', 'BERON DE ASTRADA 2980', 'LANUS ESTE', '4230-4296/0000/1000', 'MIRIAM', 'AUTOSERVICIO', 'Enviar dato faltante !! ', 'PREVENCION'),
(22, '10134', 'RECUBRIMIENTOS PLASTICOS LANUS S.R.L.', '10134', 'GUSTAVO', 'S', 'mcavagliatto@hotmail.com,gcavagliatto@hotmail.com', 'HEROES DE MALVINAS 2020', 'LANUS E', '4289-0311', 'Enviar dato faltante !!', 'METALURGICA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(23, '1015', 'ESTABLECIMIENTO J.A. S.R.L.', '1015', 'HECTOR', 'S', 'personal@jasrl.com.ar,jorge.alonso@jasrl.com.ar', 'BOQUERON 2860', 'LANUS OESTE', '4878-5922 .4209-8001', 'SR. JORGE', 'METALURGICA', 'compras@jasrl.com.ar ', 'QBE'),
(24, '1016', 'ALMENAR SA', '1016', 'WALTER', 'S', 'almandoz@speedy.com.ar ', 'ZAPATA 274/330 O MAYOR OLIVERO 325', 'BANFIELD OESTE', '202 - 0674', 'SILVIA-WALTER-HECTOR', 'METALURGICA', 'Enviar dato faltante !! ', 'SMG'),
(25, '1018', 'ANNIKIAN E HIJOS S.A.', '1018', 'EDUARDO', 'S', 'sarmiento@sion.com ', 'TTE GRAL PERON 2335 / 9', 'VALENTIN ALSINA', '208 - 5087 / 0451', 'EDUARDO-JUAN', 'TEXTIL', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(26, '10188', 'ANACON S.A', '10188', 'MONICA', 'S', 'ricardo@ferrocort.com ', 'AV. JUAN B. JUSTO 6502', 'CAPITAL FEDERAL', '4636-0662', 'MONICA', 'METALURGICA', 'silvina@ferrocort.com ', 'QBE'),
(27, '10196', 'ALBERTO F. VICENTE S.A.', '10196', 'ALDANA', 'S', 'administracion@filtrosmaxfil.com ', 'GRAL ARIAS 4051', 'LANUS ESTE - MONTE CHINGOLO', '4246-2048/5089-9000', 'ALBERTO VICENTE/ALDANA', 'TEXTIL', 'Enviar dato faltante !! ', 'PROVINCIA'),
(28, '10197', 'PRODMOBI S.A', '10197', 'JORGE', 'S', 'jorge.goni@archivosactivos.com,lucas.arizabalo@archivosactivos.com,federico.alonso@archivosactivos.com', 'HIPOLITO YRIGOYEN 673', 'AVELLANEDA OESTE', '4138-3011', 'JORGE', 'AMOBLAMIENTO DE OFICINA', 'mauro.pollio@archivosactivos.com ', 'PREVENCION'),
(29, '1020', 'FARMACIA SOC. TEXTIL LANUS', '1020', 'BETTY', 'S', 'socialtextil@speedy.com.ar ', 'O'' HIGGINS 2062', 'LANUS ESTE', '241 - 0305 / 6054', 'OSVALDO- ORLANDO', 'FARMACIA', 'Enviar dato faltante !! ', 'MAPFRE'),
(30, '10204', 'ISHI SA', '10204', 'GUSTAVO', 'S', 'ventas@ishi.com.ar ', 'CNEL DIAZ 668', 'AVELLANEDA OESTE', '4201-8124', 'GUSTAVO CAVALLI', 'METALURGICA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(31, '10207', 'SELPLAST', '10207', 'MARIA', 'S', 'mlbornacin@selplast-sa.com.ar,mjpalagro@selplast-sa.com.ar,pagoproveedores@selplast-sa.com.ar', 'LOMAS DE ZAMORA 130', 'WILDE', '4206-2870/4206-4408', 'MARIA LAURA', 'TEXTIL', 'pagoproveedores@selplast-sa.com.ar ', 'ART INTERACCION SA'),
(32, '10210', 'ESQUINA 4 SOLES S.R.L. EN FORMACION', '10210', 'GUILLERMO', 'S', 'petrobrasbanfield@yahoo.com.ar ', 'HIPOLITO YRIGOYEN 7108', 'BANFIELD OESTE', '4242-4234', 'MIGUEL ANGEL-GUILLER', 'EST. DE SERV.', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(33, '1025', 'LA CARDEUSE S.A.', '1025', 'SERGIO', 'S', 'evelina@lacardeuse.com.ar ', 'CARLOS PELLEGRINI 3422', 'VALENTIN ALSINA', '4209-8660/61', 'DAVID. MARIA JOSE', 'FCA. COLCHONES', 'cintia@lacardeuse.com.ar ', 'ART LIDERAR SA'),
(34, '10269', 'TRASCIERRA YESICA VANINA', '10269', 'MARIASOLEDAD', 'S', 'mariavalentina@speedy.com.ar ', 'PASO 375', 'CAPITAL', '4953-3156', 'MARIA SOLEDAD', 'COMERCIO', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(35, '1027', 'BLANCO MONTAJES S.A.', '1027', 'GUSTAVO', 'S', 'csierra@bmontajes.com.ar ', 'RIVADAVIA 954 1ER PISO', 'CAPITAL FEDERAL', '4331-4355/7234/5276', 'GUSTAVO', 'CONSTRUCCION', 'linsolia@bmontajes.com.ar,larce@bmontajes.com.ar', 'MAPFRE'),
(36, '10277', 'CREACIONES HASAKE S.A.', '10277', 'ALBERTO', 'S', 'pintorbebes@yahoo.com.ar ', 'BOLIVIA 690', 'LANUS OESTE', '4240-2802', 'ALBERTO', 'TEXTIL', 'Enviar dato faltante !! ', 'SMG'),
(37, '1028', 'BURGERT S.A.', '1028', 'CLAUDIO', 'S', 'claudio.martin@pinturasburgert.com,marcia.nemcek@pinturasburgert.com,natalia.paredes@pinturasburgert.com', 'SAN LORENZO 2162', 'LANUS ESTE', '246-4923/6295/9167', 'CLAUDIO/MARCIA', 'PINTURAS FCA', 'carlos.fernandez@pinturasburgert.com,sabrina.bianchi@pinturasburgert.com', 'QBE'),
(38, '10329', 'TRUJILLO DIEGO HERNAN', '10329', 'MARIASOLEDAD', 'S', 'mariavalentina@speedy.com ', 'Bme MITRE 2799', 'CAPITAL', '4953-3156', 'MARIA SOLEDAD', 'COMERCIO', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(39, '10344', 'COLEGIO SIDNEY SOWELL', '10344', 'SIDNEY', 'S', 'colegiosidneysowell@speedy.com ', 'EL PLUMERILLO 769', 'BANFIELD', '4286-8375/4286-4800', 'DINA', 'EDUCATIVA', 'Enviar dato faltante !! ', 'GALENO'),
(40, '10349', 'INDUSTRIA METALURGICA AOA SA', '10349', 'RODOLFO', 'S', 'industria_aoa@uolsinectis.com.ar ', 'E. CASTRO 1047', 'LANUS OESTE', '4-225-5168', 'RODOLFO ANDRIOLI', 'METALURGICA', 'Enviar dato faltante !! ', 'LA CAJA'),
(41, '10362', 'OBRA SOCIAL DE CERAMISTAS', '10362', 'NILDA', 'S', 'rodrigueznilda@fibertel.com.ar ', 'DOBLAS 629', 'CAPITAL', '15-6053-4483/4921-0906 rr', 'NILDA RODRIGUEZ', 'OBRA SOCIAL', 'tmaurin@fibertel.com.ar ', 'ART INTERACCION SA'),
(42, '10384', 'SIMONETTI LEANDRO', '10384', ' LEANDRO', 'S', 'info@enrejilladocelda.com.ar ', 'RESISTENCIA 1330', 'LANUS OESTE', '4218-2529/4241-1206', 'ENREJILLADOS CELDA', 'Enviar dato faltante !!', 'info@enrejilladocelda.com.ar,rocio@enrejilladoscelda.com.ar', 'FEDERACION PATRONAL'),
(43, '10390', 'COLEGIO BALMORAL', '10390', 'ALICIA', 'S', 'administracion@balmoral.esc.edu.ar,aceres@balmoral.esc.edu.ar,sueldos@balmoral.esc.edu.ar,mtferraro@balmoral.e', 'MANUEL CASTRO 1536', 'BANFIELD OESTE', '4248-3878/4242-0385', 'ALICIA/ARIEL/ADRIANA', 'EDUCATIVA', 'mtferraro@balmoral.esc.edu.ar ', 'SMG'),
(44, '10422', 'ELECTRONICA BS. AS. SA', '10422', 'CRISTIAN', 'S', 'triper@opcionestelmex.com.ar,admtotal2011@yahoo.com.ar,estudio@bustosmurilloyasoc.com.ar', 'FEDERICO GARCIA LORCA 255 4TO A', 'CAPITAL', '4289-3780//81', 'CRISTIAN', 'CAMIONES', 'Enviar dato faltante !! ', 'SMG'),
(45, '1043', 'CUPLAS JAC SRL', '1043', 'SILVIA', 'S', 'cuplasjacsrl@hotmail.com ', 'RAWSON 1364', 'LOMAS DE ZAMORA OESTE', '4286-3625', 'SILVIA CALARESU/MONICA', 'METALURGICA', 'Enviar dato faltante !! ', 'GALENO'),
(46, '10437', 'LOGISTICA SALAMANCA SRL', '10437', 'CINTIA', 'S', 'logisticasalamanca@hotmail.com ', 'PICHINCHA 1910', 'LANUS ESTE', '4225-0673 4225-8872', 'ANDREA / ANTONELLA', 'DISTRIBUIDORA', 'logisticasalamanca@gmail.com ', 'ART INTERACCION SA'),
(47, '10440', 'TRANSPORTE FUENTECILLA S.A.C.I.F', '10440', 'EMILIO', 'S', 'transporte@fuentecilla.com.ar ', 'GARCIA 686', 'PIÑEYRO-AVELLANEDA', '4229-8059//8060', 'EMILIO-FEDERICO', 'TRANSPORTE', 'Enviar dato faltante !! ', 'LA CAJA'),
(48, '1047', 'COLOR - PLAST S.R.L.', '1047', 'MARIA', 'S', 'maria@color-plast.com.ar,daniela@color-plast.com.ar', 'FRAY JULIAN LAGOS 2949', 'LANUS OESTE', '4240-2645/4241-7224', 'MARIA', 'VENTA PIGMENTOS', 'maria@color-plast.com.ar ', 'PREVENCION'),
(49, '10486', 'TOP SINERGIA S.A.', '10486', 'MAGALI', 'S', 'info@topsinergia.com.ar ', 'CNO. GRAL BELGRANO 4390', 'VILLA DOMINICO', '4230-4809/4115-0435', 'ROLANDO', 'MATERIALES DE CONSTRUCCION', 'compras@topsinergia.com.ar ', 'SMG'),
(50, '10499', 'SAADE CRISTIAN DANIEL', '10499', 'CRISTIAN', 'S', 'christian_saade@hotmail.com ', 'CENTENARIO URUGUAYO 2000', 'MONTE CHINGOLO', '4-230-7916', 'EX. CARNICERIA GINO (2566)', 'CARNICERIA', 'Enviar dato faltante !! ', 'QBE'),
(51, '1050', 'CAAGUAZU S.A.', '1050', 'FERNANDO', 'S', 'caaguazu@caaguazu.com.ar,administracion@caaguazu.com.ar', 'AV. EVA PERON 3260', 'LANUS ESTE', '246 - 5236', '', 'METALURGICA', 'Enviar dato faltante !! ', 'QBE'),
(52, '10518', 'TRANSPORTE ALTO CUYO SA', '10518', 'GRACIELA', 'S', 'ezequiel@transportesbardone.com.ar,info@transportesbardone.com.ar,graciela@transportesbardone.com.ar', 'CAMINO GRAL BELGRANO KM 80,500', 'QUILMES', '4270-1145', 'GRACIELA GOMEZ', 'TRANSPORTE DE CARGA', 'sebastian@transportesbardone.com.ar ', 'LA CAJA'),
(53, '1054', 'CAPULLITOS S.A.C.I.', '1054', 'ALDO', 'S', 'capullitos@speedy.com.ar ', 'YATAY 805', 'LANUS OESTE', '209 - 7012 / 4141', 'Sra.CARBALLO', 'FCA. COPOS MAIZ', 'Enviar dato faltante !! ', 'PROVINCIA'),
(54, '10546', 'ALPAMET SRL', '10546', 'MARIANA', 'S', 'afiliacionesalpa@yahoo.com.ar,alpamet@yahoo.com.ar', 'AV.SAN MARTIN 3007', 'LANUS OESTE', '4266-0492', 'MARIANA', 'TALLER DE AFILA', 'Enviar dato faltante !! ', 'SMG'),
(55, '1056', 'CONS.COOPROP. (ADM. KOZIURA)', '1056', 'MARIA', 'S', 'koziura@fibertel.com.ar ', 'MINISTRO BRIN 2839 PTA. BAJA', 'LANUS OESTE', '247 - 3897', 'Sra. MARIA- Sr.PEDRO', 'CONSORCIO', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(56, '10561', 'ALVEAR ALEM SA', '10561', 'MARTIN', 'S', 'recursoshumanos@alvearalem.com.ar ', 'ALVEAR 1250', 'BANFIELD OESTE', '4202-7238/ 1149979447', '4202-6556 MARTIN', 'CLINICA PSIQUIATRICA', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(57, '10575', 'ACCESORIOS Y AUTOPARTES 3 EME SRL', '10575', 'MARICEL', 'S', 'maricelvicente@hotmail.com,distribuidora3m@hotmail.com', 'CORONEL PRINGLES 2153', 'LANUS ESTE', '4225-8830', 'MARISEL/MARTIN', 'DISTRIBUIDORA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(58, '1058', 'FABIO HNOS S.A.C.F.A.I.', '1058', 'NORBERTO', 'S', 'fabiohnos@opcionestelmex.com.ar,sergiorenno@fabiohnos.com,proveedores@fabiohnos.com', 'CON.GRAL. BELGRANO 1971/9', 'LANUS ESTE', '4204-4511/6989 PLANTA 427', 'MARCELA/NORBERTO FABIO/VICTOR', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'PREVENCION'),
(59, '10584', 'GRUPO PYD SA', '10584', 'GISELE', 'S', 'ncarlisle@grupopyd.com,csalazar@grupopyd.com ', 'RECONQUISTA 1011 4TO PISO', 'CAPITAL', '5555-4800', 'SABRINA', 'TELECOMUNICACIONES', 'sdamiano@grupopyd.com ', 'LA CAJA'),
(60, '10585', 'CENTRO COMERCIAL E INDUSTRIAL DE AVELLANEDA', '10585', 'NORMA', 'S', 'cciaavellaneda@speedy.com.ar,lilia.cciavelllaneda@gmail.com,mtomasino@gmail.com,robertogp@hotmail.com', 'MITRE 429 1ER PISO', 'AVELLANEDA', '4201-7613/4222-6154', 'NORMA-JUAN CARLOS', 'CENTRO COMERCIAL', 'Enviar dato faltante !! ', 'QBE'),
(61, '1059', 'CLINICA SAN ROQUE SH', '1059', 'GERMAN', 'S', 'geriatricosanroque@gmail.com ', 'ALVEAR 1666 Y VIEYTES', 'BANFIELD OESTE', '4242-1646', 'GERMAN CABRERA', 'GERIATRICO', 'Enviar dato faltante !! ', 'PREVENCION'),
(62, '10602', 'BUDDHABA BS AS (888 S.R.L.)', '10602', 'VICTORIA', 'S', 'info@buddhaba.com.ar,administracion@fullmen.com.ar', 'BARTOLOME MITRE 2017 1° PISO', 'CAPITAL', '4706-2382/4953-1966', 'VICTORIA/FREDY', 'RESTAURANT', 'Enviar dato faltante !! ', 'PROVINCIA'),
(63, '10621', 'WORLD PACK S.A', '10621', 'INES', 'S', 'inesliszaj@hotmail.com,info@worldpack-ar.com ', 'SANTIAGO DEL ESTERO 1671', 'PIÑEYRO-AVELLANEDA', '4218-3555/4209-4442', 'INES', 'FABRICACION DE MAQ', 'info@worldpack-ar.com ', 'ART INTERACCION SA'),
(64, '1065', 'COTIGRAF SA', '1065', 'ROBERTO', 'S', 'info@cotigraf.com.ar,cotigraf@gmail.com,racai@cotigraf.com.ar', 'RUCCI 2533', 'LANUS OESTE', '208 - 2428 / 0837', 'BEATRIZ-MIRTA-P:ANA', 'fabrica de tinturas y diluyent', 'Enviar dato faltante !! ', 'SMG'),
(65, '10651', 'RMMB S.A.', '10651', 'ROBERTO', 'S', 'rmmbsa@speedy.com.ar,ribonifacio21@hotmail.com', 'AV 12 DE OCTUBRE 4052', 'QUILMES', '4280-7707/1752', 'MABEL', 'Enviar dato faltante !!', 'gabriel@rmmbsa.com ', 'FEDERACION PATRONAL'),
(66, '1067', 'CASTELLANO', '1067', 'SALVADOR', 'S', 'NO MAIL ', 'OYUELA 2536 - 38', 'LANUS ESTE', '246 - 2754', 'NICOLAS-SALVADOR', 'METALURGICA', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(67, '10676', 'RAMONDA MARIELA ELIZABETH', '10676', 'MARIELA', 'S', 'marielaramonda@hotmail.com ', '29 DE SEPIEMBRE 1954', 'LANUS ESTE', '1535493104 (ALEJANDRO)', 'MARIELA', 'PERFUMERIA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(68, '1068', 'COSENTINO E HIJOS S.R.L.', '1068', 'JORGE', 'S', 'jorgeacosentino@speedy.com.ar,cosentinoehijossrl@speedy.com.ar', 'GRAL. HORNOS 1561', 'RDIOS. DE ESCALADA O.', '4286-8192---4273-3417', 'MIGUEL- Sr.COSENTINO', 'MAYORIS.BEBIDAS', 'Enviar dato faltante !! ', 'QBE'),
(69, '10681', 'GEORGE TRANS S.A.', '10681', 'SUSANA', 'S', 'george_trans_s.a@hotmail.com ', 'MAMBERTI 948', 'LANUS ESTE', '4241-6949/9925', 'JORGE-SUSANA', 'TRANSPORTE', 'Enviar dato faltante !! ', 'PREVENCION'),
(70, '10688', 'SANCOR SEGUROS - INTEGRO', '10688', 'SANCOR', 'S', 'NO ', 'AV INDEPEDENCIA 333', 'SUNCHALES- SANTA FE', 'PREVENCION', 'Enviar dato faltante !!', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'SANCOR SEGUROS PERSONALES ART'),
(71, '1069', 'COLUMBIA ELECTROTECNICA', '1069', 'NICOLAS', 'S', 'columbiaelec@speedy.com.ar ', 'SARMIENTO 1662', 'LANUS', '241 - 7782 / 0148', 'MONICA-SANDRA', 'ALUMBRADO', 'monicacolumbia@speedy.com.ar ', 'QBE'),
(72, '1071', 'CAUPUR S.A.C.I.', '1071', 'SARA', 'S', 'caupur@giba.com.ar,ana@duravit.com.ar ', 'REMEDIOS DE ESCALADA DE SAN MARTIN 2870', 'LANUS OESTE', '4205-0838/4228-2220', 'Enviar dato faltante !!', 'FCA DE JUGUETES', 'Enviar dato faltante !! ', 'QBE'),
(73, '1073', 'QUIMICA RODRAN S.R.L.', '1073', 'SILVANA', 'S', 'quimica@rodran.com.ar ', 'CARLOS CASARES 1738', 'LANUS OESTE', '241 - 9898 / 7409', 'SILVANA', 'PROD. QUIMICOS', 'Enviar dato faltante !! ', 'LA CAJA'),
(74, '10736', 'DISEÑO Y MODA SRL', '10736', 'GUILLERMO', 'S', 'dymoda@gmail.com ', 'CHACO 657', 'LANUS OESTE', '4240-8855', 'GUILLERMO', 'CALZADO', 'Enviar dato faltante !! ', 'QBE'),
(75, '10743', 'DANIEL OSCAR (RIAL)', '10743', 'OSCAR', 'S', 'talleroscar@live.com ', 'WARNES 3101', 'LANUS OESTE', '4-228-3073', 'OSCAR', 'TALLER', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(76, '10745', 'FABIANO MIGUEL ANGEL', '10745', 'MIGUEL', 'S', 'miguelfabiano@hotmail.com ', 'RIVADAVIA 2790', 'LANUS OESTE', '4-262-0097/7774', 'MIGUEL', 'ARTICULOS P/EL HOGAR', 'Enviar dato faltante !! ', 'QBE'),
(77, '10769', 'IDALNO S.R.L.', '10769', 'JUAN', 'S', 'juanzx7@hotmail.com,eguaglianone@loscuatro.com.ar', 'OLAZABAL 3738', 'LANUS OESTE', '4267-3348', 'JUAN AYALA/PABLO AIELLO', 'DISTRIBUIDORA', 'mangeletti@loscuatro.com.ar ', 'PREVENCION'),
(78, '1077', 'DA CUNHA Y MUIOLA SRL', '1077', 'DACUNHA', 'S', 'dacunhaymuiola@yahoo.com.ar ', 'ALFREDO L. PALACIOS 3835', 'VALENTIN ALSINA', '208 - 4150', '.', 'METALURGICA', 'Enviar dato faltante !! ', 'PREVENCION'),
(79, '10772', 'ESCUELA N° 54', '10772', 'ELDA', 'S', 'eldaamad@yahoo.com.ar ', 'SANTIAGO PLAUL 1832', 'LANUS OESTE', '4262-5796', 'EDUCATIVA', 'EDUCATIVA', 'Enviar dato faltante !! ', 'PROVINCIA COLONIA SUIZA'),
(80, '1079', 'INDUSTRIAS DERIPLOM S.A.', '1079', 'MONICA', 'S', 'mfernandez@deriplom.com.ar,gfontana@deriplom.com.ar', 'BLANCO ENCALADA 3277', 'LANUS ESTE', '246 - 3048 / 5793', 'LUIS LARA/MONICA', 'QUIMICA', 'Enviar dato faltante !! ', 'MAPFRE'),
(81, '10797', 'RIVERA ANA MARIA', '10797', 'ANAMARIA', 'S', 'sanitarios_emede@hotmail.com ', 'BELGRANO 4267', 'VILLA DOMINICO', '4207-5825/4353-0144', 'ANA MARIA-SEBASTIAN', 'SANITARIOS', 'Enviar dato faltante !! ', 'GALENO'),
(82, '10806', 'E.P.B. NRO 7', '10806', 'KARINA', 'S', 'ep7lanus@hotmail.com ', 'MAMBERTI 1217', 'LANUS ESTE', '4241-7178', 'MERCEDES-KARINA', 'EDUCATIVA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(83, '1081', 'DOURADO JORGE', '1081', 'JORGE', 'S', 'elastico_elmono@hotmail.com,charly_shark01@hotmail.com', 'MAGDALENA 1439', 'LANUS ESTE', '220-8667part293-2030', 'SR. JORGE', 'REP. ELASTICOS', 'Enviar dato faltante !! ', 'PROVINCIA'),
(84, '1085', 'REGAN CREACIONES S.A.', '1085', 'ROSA', 'S', 'marianelaleone@hotmail.com ', 'AV. MITRE 676', 'AVELLANEDA ESTE', '4201-4777/8915', 'ROSA VIDAL-Sr.MINKAS', 'INDUMENTARIA', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(85, '1086', 'INDUSTRIAS LEP S.A.', '1086', 'GRACIELA', 'S', 'informes@industriaslep.com.ar ', 'COSTA RICA 64', 'TEMPERLEY ESTE', '4260-0083', 'Sra. ALICIA', 'PLASTICOS', 'Enviar dato faltante !! ', 'PROVINCIA'),
(86, '1087', 'LOMAS S.R.L.', '1087', 'CECILIA', 'S', 'administracion@lomas-srl.com ', 'CHICLANA 83', 'LOMAS DE ZAMORA ESTE', '244-1798/6592', 'MAITE', 'PANIFICADORA', 'Enviar dato faltante !! ', 'LA CAJA'),
(87, '1090', 'EL ROBLE S.R.L.', '1090', 'HERNAN', 'S', 'elroble@rcc.com.ar ', 'CHACO 2580', 'LANUS OESTE', '4208-8500/4218-4603', 'HERNAN', 'METALURGICA', 'normarodriguez63@hotmail.com.ar ', 'SMG'),
(88, '10906', 'JUAN CARLOS CHILLEMI SRL', '10906', 'CLAUDIA', 'S', 'info@stone-shoes.com.ar ', 'YATAY 527', 'V. ALSINA', '42289834', '1558175106 RAMIRO CHILLEMI', 'CALZADO', 'info@stone-shoes.com.ar ', 'PRODUCTORES DE FRUTAS ARGENTINAS'),
(89, '1091', 'BOMBAY S.A.', '1091', 'JOSE', 'S', 'bombay@datamarkets.com.ar,ernesto.dutch@datamarkets.com.ar', 'ALEM 207 3ER PISO', 'LOMAS DE ZAMORA OESTE+', '4245-9003/5715-2102 jose', 'DANIEL:15-5376-4655', 'FCA SANDWICH', 'Enviar dato faltante !! ', 'LA CAJA'),
(90, '10929', 'MARCHELO (CASTILLO J Y AVILA M)', '10929', 'MARCHELO', 'S', 'jujuliolio@hotmail.com ', 'M. WIELD 1896', 'LANUS ESTE', '4241-0483', 'JULIO', 'PELUQUERIA', 'Enviar dato faltante !! ', 'PREVENCION'),
(91, '1093', 'PEMCO EMELIER SA', '1093', 'CARLA', 'S', 'carla.vdovichenko@pemco-intl.com ', 'BOUCHARD 3040', 'LANUS ESTE', '4-289-2430', 'ARIEL', 'QUIMICA', 'marquez.veronica@pemco-intl.com ', 'QBE'),
(92, '1094', 'BELTRES', '1094', 'GABRIELA', 'S', 'beltres_zaranda@hotmail.com ', 'GENERAL ACHA 792', 'SARANDI', '4205-0013', 'GABRIELA/ROMINA', 'CARPINTERIA', 'Enviar dato faltante !! ', 'SMG'),
(93, '10940', 'HORIZONTE CIA ARG DE SEGUROS GENERALES DIV ART', '10940', 'HORIZONTE', 'S', 'susanafrieyro@hotmail.com,susanafrieyro@hotmail.com,areamedicabsas@horizonte.com.ar', 'MAIPU 509', 'CIUDAD AUTONOMA DE BUENOS AIRE', '4394-3970 // 02920-431746', 'SUSANA INES FRIEYRO', 'SEGUROS', 'dcevallos@horizonte.com.ar,dcambruzzi@horizonte.com.ar', 'HORIZONTE CIA ARG DE SEGUROS GENERALES D'),
(94, '10948', 'CLINICA SAN JORGE S.A.C', '10948', 'LINA', 'S', 'compras@csanjorge.com.ar,percsj@csanjorge.com.ar,luisaraujo@csanjorge.com.ar', 'AV EVA PERON 1536', 'LANUS ESTE', '4240-7136/39 INT 220 (PER', 'LINA-LUIS', 'CLINICA', 'Enviar dato faltante !! ', 'SMG'),
(95, '10950', 'VAZQUEZ MARCELO ARIEL', '10950', 'MARCELO', 'S', 'marceloavazquez@live.com.ar,annapennella@hotmail.com,marceloarielvazquez@gmail.com', 'TUYUTI 638', 'LANUS OESTE', '4241-1506', 'ARIEL- Sr.CAYETANO', 'LAVADERO PAPAS', 'Enviar dato faltante !! ', 'QBE'),
(96, '1097', 'EMANAL S.R.L.', '1097', 'CRISTINA', 'S', 'cristina@emanal.com.ar ', 'MANUEL CASTRO 3364', 'LANUS OESTE', '4262-9777', 'Sra CRISTINA', 'METALURGICA', 'Enviar dato faltante !! ', 'LA CAJA'),
(97, '1098', 'FUNDACION EDUCACIONAL CRISTIANA EVANGELICA', '1098', 'NGRADEFF', 'S', 'fundacion@ecea.edu.ar ', 'GRECIA 4175', 'LANUS OESTE', '4262-1685/7619', 'SR OCHOA DR. AMENOS', 'EDUCATIVA', 'pagos@ecea.edu.ar ', 'GALENO'),
(98, '10980', 'SIBA (ICTIOLOGICA ARGENTINA S.R.L.)', '10980', 'SOLEDADMARIA', 'S', 'mblanco2140@yahoo.com.ar,silviacandeloro@yahoo.com.ar', 'FELIPE AMOEDO 3450', 'QUILMES', '4250-4145 MALENA', 'MALENA (SIBA)', 'Enviar dato faltante !!', 'gloureiro@sibaute.com.ar ', 'GALENO'),
(99, '10982', 'ELECTROMECANICA ENVER S.R.L.', '10982', 'ALBERTO', 'S', 'albertoverardi@yahoo.com.ar ', 'J. FARREL 2280', 'LANUS OESTE', '4-209-1850', 'ALBERTO/ANGELICA', 'ELECTROMECANICA', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(100, '1099', 'ELECTRON PLAST S.A.', '1099', 'SUSANA', 'S', 'susanam@electronplast.com,descartables@electronplast.com,evelinai@electronplast.com', 'MARMOL 867', 'LANUS OESTE', '241 - 4710 247 -7718', 'SUSANA', 'ENVS. TELGOPOR', 'evelinai@electronplast.com ', 'PROVINCIA'),
(101, '1100', 'EST. MET. SAN FRANCISCO SRL', '1100', 'LORENA', 'S', 'lorena_fabio@yahoo.com.ar,info@metalurgicasanfrancisco.com,lorenafabio@metalurgicasanfrancisco.com', 'BASAVILBASO 2045', 'AVELLANEDA ESTE (LIBRETA L.E)', '4204-2712', 'LORENA', 'METALURGICA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(102, '11026', 'JARDIN DE INFANTES GRAL OLAZABAL', '11026', 'GLADYS', 'S', 'jardindeinfantes@hotmail.com ', 'OLAZABAL 4606 Y OLIDEN', 'LANUS OESTE', '4240-8607 / 20450-953', 'GLADYS', 'EDUCATIVA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(103, '1103', 'ENPLAS S.A.I.C', '1103', 'EVELYN', 'S', 'enplas@infovia.com.ar,administracion@envases-enplas.com.ar,cscigolini@envases-enplas.com.ar', 'TELEMIAN CONDIE 2020', 'BARRIO 9 DE ABRIL. M. GRANDE', '4693-4848', 'WALTER DANIELA', 'ENV. PLASTICOS', 'marieladarosa@envases-enplas.com.ar ', 'PROVINCIA'),
(104, '11036', 'KARBON ATF SRL', '11036', 'YAMILA', 'S', 'karbon@sion.com ', 'AV RABANAL 3220', 'CAPITAL FEDERAL', '4115-2962/2963', 'YAMILA/NORMA', 'METALURGICO', 'Enviar dato faltante !! ', 'QBE'),
(105, '1104', 'PRODUCTOS EL MOLINERO S.A', '1104', 'SILVIA', 'S', 'el_molinero@sion.com ', 'CNEL. SAYOS 2549', 'VALENTIN ALSINA', '4208-6761/4209-8493', 'SILVIA SALOMON', 'FCA. ESPECIAS', 'Enviar dato faltante !! ', 'QBE'),
(106, '11057', 'FRIO INTERLOGISTICA SUR S.A.', '11057', 'CLAUDIA', 'S', 'proveedoresfis@yahoo.com.ar ', 'EVA PERON 2045', 'LANUS ESTE', '4241-4867', 'CLAUDIA', 'DISTRIBUIDORA', 'Enviar dato faltante !! ', 'LA CAJA'),
(107, '1109', 'ESTEVEZ HNOS S.R.L.', '1109', 'NORBERTO', 'S', 'estevez.hnos@gmail.com ', 'SAN LORENZO 875', 'LANUS ESTE', '246-2655/8982', 'OSVALDO-JULIO-JESUS', 'SUBP. DEL CUERO', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(108, '1110', 'EST. METALURGICOS TAMEL S.R.L.', '1110', 'MARTA', 'S', 'tamel@ciudad.com.ar ', 'JUAN FARREL 1043', 'VALENTIN ALSINA', '4204-4185/4228-3820', 'MIRTA', 'METALURGICA', 'Enviar dato faltante !! ', 'PREVENCION'),
(109, '11102', 'GRANJA LOS ANDES S.A.', '11102', 'SEBASTIAN', 'S', 'sebastianzy@hotmail.com ', 'EJERCITO DE LOS ANDES 82', 'BANFIELD OESTE', '4267-1851/15-5226-8482', 'SEBASTIAN', 'GRANJA', 'Enviar dato faltante !! ', 'SMG'),
(110, '1112', 'EXOTERM S.R.L.', '1112', 'SONIA', 'S', 'sonia@exoterm.com.ar ', 'SARMIENTO 637', 'LANUS ESTE', '4241 - 0040', 'Enviar dato faltante !!', 'METALURGICA', 'Enviar dato faltante !! ', 'PREVENCION'),
(111, '11120', 'FAPYC S.R.L.', '11120', 'ANDREA', 'S', 'fapyc.srl@gmail.com ', 'PUERTO DE PALOS 834', 'VILLA DOMINICO', '4207-8668', 'ANDREA-PABLO', 'METALURGICA', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(112, '11121', 'SERVICE NORTH FULL EN FORMACION', '11121', 'GUSTAVO', 'S', 'grmere@yahoo.com.ar ', 'LAVALLOL 74', 'LANUS OESTE', '4346-4000', 'GUSTAVO (INT 11282)', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'GALENO'),
(113, '11135', 'EST. BOMECA (MELLINO LIDIAY MELLINO NORMA) S.H.', '11135', 'NORMA', 'S', 'bomeca@ciudad.com.ar,info@bomecaresortes.com.ar', 'ROQUE PEREZ 301', 'VILLA DOMINICO', '4227-5129', 'NORMA', 'FABRICA DE RESORTES', 'Enviar dato faltante !! ', 'MAPFRE'),
(114, '1114', 'ELECTRO ROAR INDUSTRIAL', '1114', 'SERGIO', 'S', 'leandro.ochoa@electroroar.com.ar ', 'M. G. BALCARCE 4144', 'RDIOS. DE ESCALADA', '276-1086', 'TITO-ARMANDO-ERNESTO', 'METALURGICA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(115, '1115', 'FABRICA LOMAS', '1115', 'FCA_LOMAS', 'S', 'info@suelaslomas.com.ar ', 'LUNA 418', 'LANUS OESTE', '208 - 9387 209 -8580', 'LAURA', 'ZAPATERIA', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(116, '11154', 'MENDICOOP SA (FIDEOS DEL SUR)', '11154', 'ALFREDO', 'S', 'diego@mendia.com.ar,diegodigiano@mendia.com.ar', 'MARGARITA WEILD 2775', 'LANUS ESTE', '4220-1421/4241-0161', 'ALFREDO', 'FABRICA DE PASTAS', 'antonio@mendia.com.ar,emanuel@mendia.com.ar ', 'BERKLEY INTERNATIONAL'),
(117, '11167', 'ACERO LOGISTICA SRL', '11167', 'ACERO', 'S', 'acerologisticasrl@yahoo.com.ar ', 'RAUCH 3691', 'REMEDIOS DE ESCALADA OESTE', '4202-9387', 'ESTELA', 'TRANSPORTE', 'Enviar dato faltante !! ', 'PROVINCIA'),
(118, '1118', 'FABEC S.R.L.', '1118', 'ARIEL', 'S', 'fabec@infovia.com.ar ', 'FREIRE 792', 'AVELLANEDA OESTE', '208 - 4528/4209-3549', 'OSVALDO CAEIRO', 'CARTONERIA', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(119, '11184', 'COOP. DE TRAB. DE TAXI DEL AEROPUERTO DE EZEIZA', '11184', 'TAXI EZEIZA', 'S', 'info@taxiezeiza.com.ar ', 'PLAYA DE EST. DEL AEROPUERTO S/N', 'EZEIZA', '5480-8894', 'Enviar dato faltante !!', 'TAXIS', 'Enviar dato faltante !! ', 'SMG'),
(120, '11191', 'GEO ALUMINIO (PASCUAL SALESE)', '11191', 'PASCUAL', 'S', 'info@geoaluminio.com.ar ', '25 DE MAYO 1590', 'LANUS OESTE', '4247-3458', 'PASCUAL', 'ABERTURAS', 'Enviar dato faltante !! ', 'LA CAJA'),
(121, '11194', 'SEPULVEDA MATIAS SEBASTIAN', '11194', 'MATIAS', 'S', 'jona_2307@hotmail.com ', 'NAZAR 456', 'LANUS ESTE', '4225-6689', 'MATIAS/ JONATAN', 'CARPINTERIA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL ACC PERSONALES'),
(122, '1120', 'WALTER BLANCO-SERGIO AVELIKIAN', '1120', 'WALTER', 'S', 'wrb645@hotmail.com ', 'JUAN D. PERON 2400', 'VALENTIN ALSINA', '208 - 7468', 'WALTER BLANCO', 'FCA. DE PASTAS', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(123, '11200', 'ANIL LAN SRL', '11200', 'RUBEN', 'S', 'ruben@anillan.com.ar,jorge@anillan.com.ar ', 'GRAL DEHEZA 1445', 'LANUS ESTE', '4225-3194', 'RUBEN', 'COLORANTES', 'emiliano@anillan.com.ar ', 'GALENO'),
(124, '11206', 'FONDEVILA JOSE MANUEL', '11206', 'NARINA', 'S', 'estudioepiscopo@fibertel.com.ar,granjalomas@hotmail.com', 'SAAVEDRA 821', 'LOMAS DE ZAMORA', '4292-5050', 'DIEGO', 'MARINA', 'Enviar dato faltante !! ', 'MAPFRE'),
(125, '11213', 'RAFED PLAST S.A.', '11213', 'ADOLFO', 'S', 'info@resolverautomacion.com.ar ', 'SALVADOR SOREDA 4151', 'SARANDI', '4353-2183/4600-6457', 'ADOLFO-SILVIA-MARCELA-EDDY', 'Enviar dato faltante !!', 'administracion@rafed.com.ar ', 'LA CAJA'),
(126, '1122', 'FADAP SA', '1122', 'EZEQUIEL', 'S', 'administracion@piazconveyor.com,piazconveyor@pianzconeyor.com', 'AV. GRAL. RODRIGUEZ 2485', 'LANUS ESTE', '4241-9555', 'EZEQUIEL', 'FCA. PLASTICOS', 'Enviar dato faltante !! ', 'PROVINCIA'),
(127, '1123', 'FEDERICO VOGT S.A.', '1123', 'MARTA', 'S', 'martamarkarian@vogtsa.com.ar,federicovogt@vogtsa.com.ar', 'MALDONADO 335', 'REMEDIOS DE ESCALADA ESTE', '202 - 1122 / 1125', 'MARTA-FEDERICO', 'METALURGICA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(128, '11240', 'COLEGIO JACARANDA', '11240', 'ELINA', 'S', 'colegio.jacaranda@yahoo.com,secretaria.jacaranda@gmail.com', 'PICO 537', 'LANUS ESTE', '4230-2281', 'ELINA-BETY', 'EDUCATIVA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(129, '11241', 'COLEGIO JACARANDA JARDIN DE INFANTE', '11241', 'ELINA', 'S', 'colegio.jacaranda@yahoo.com,secretaria.jacaranda@gmail.com', 'PICO 537', 'LANUS ESTE', '4230-2281', 'ELINA-BETY', 'EDUCATIVA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(130, '11249', 'BRODSCHI ERNESTO LUIS', '11249', 'ERNESTO', 'S', 'sandra@estudiobrodschi.com.ar,controller@estudiobrodschi.com.ar', 'MALABIA 2379 1 A', 'CAPITAL FEDERAL', '4833-6868/48', 'ERNESTO', 'ESTUDIO CONTABLE', 'administracion@estudiobrodschi.com.ar ', 'LA CAJA'),
(131, '11296', 'PROCESOS METALICOS SA', '11296', 'MATIAS', 'S', 'mduduletz@guzman-nacich.com.ar ', 'COSSETTINI OLGA 1190', 'CAP FED', '02229-443764', 'LEANDRO', 'METALAURGICA', 'leandro@procesosmetalicos.com.ar ', 'PREVENCION'),
(132, '1130', 'GUZMAN COLOMBO S.R.L.', '1130', 'COLOMBO', 'S', 'NO MAIL ', 'MATANZA 3055', 'LANUS ESTE', '289 - 0231', 'OSCAR COLOMBO', 'CONFECCIONES', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(133, '11304', 'COMPANIA ARGENTINA DE ACEITES S.A.', '11304', 'ADRIAN', 'S', 'anoe@caasa.net ', 'CORDOBA 232', 'LANUS ESTE', '4205-8888', 'ADRIAN -FERNANDO', 'ACEITERA', 'aguerrero@caasa.net ', 'SMG'),
(134, '11316', 'BAENA SABRINA', '11316', 'SABRINA', 'S', 'ce_di_o@hotmail.com ', 'GRAL PICO 1548', 'LANUS ESTE', '4289-5283', 'SUSANA/SABRINA', 'EDUCATIVA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(135, '11326', 'RUBEN DAVID AISLACIONES SRL', '11326', 'RUBEN DAVID AISLALCI', 'S', 'rubendavid@davidaislaciones.com.ar,rubenemiliodavid@hotmail.com', 'ALEJANDRO MAGARINO CERVANTES 2946 1A', 'C.A.B.A.', '4568-5331/1540684820', 'RUBEN DAVID', 'AISLACIONES', 'Enviar dato faltante !! ', 'QBE'),
(136, '11339', 'ALSIL CONSTRUCCIONES S.A.', '11339', 'GONZALO', 'S', 'alsilsa@yahoo.com.ar ', 'RINCON 1058', 'BANFIELD ESTE', '4246-4547/4248-0250', 'GONZALO-PEDRO-IGNACIO', 'CONTRUCCION', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(137, '11353', 'LA METALURGICA INDUSTRIAL LAMPE LUTZ Y CIA', '11353', 'ALBERTO', 'S', 'aanchordoqui@lmi.com.ar ', 'AV HIPOLITO YRIGOYEN 1310', 'AVELLANEDA OESTE', '4208-1637', 'ALBERTO - JORGE', 'METALURGICA', 'pagos@lmi.com.ar ', 'PREVENCION'),
(138, '11371', 'J-B LAMINADOS SH', '11371', 'EMILIANO', 'S', 'joaquinbazarra@yahoo.com.ar ', 'ARGAÑARAZ 1380', 'AVELLANEDA ESTE', '4205-3088', 'EMILIANO', 'LAMINADOS', 'Enviar dato faltante !! ', 'PROVINCIA'),
(139, '1138', 'GRAFICA RAB S.R.L.', '1138', 'ROBERTO', 'S', 'bortolottir@graficarab.com.ar,ventas@graficarab.com.ar', '25 DE MAYO 1570', 'LANUS', '241-4349 240 - 0162', 'ROBERTO/ISABEL/RUBEN', 'GRAFICA', 'Enviar dato faltante !! ', 'LA CAJA'),
(140, '11382', 'PROMASI S.A.', '11382', 'FLORENCIA', 'S', 'promasisa_quilmes@speedy.com.ar ', 'CON GRAL BELGRANO KM 10,50', 'BERNAL OESTE', '4270-1241', 'FLORENCIA', 'METALAURGICA', 'Enviar dato faltante !! ', 'RECONQUISTA ART'),
(141, '11390', 'TRANSPORTE LLORRYLIQUID SA', '11390', 'EMILIO', 'S', 'lorryliquid@yahoo.com.ar ', 'CARLOS PELLEGRINI 710', 'AVELLANEDA', '4229-8060/8059', 'EMILIO-FEDERICO', 'TRANSPORTE', 'Enviar dato faltante !! ', 'LA CAJA'),
(142, '1140', 'GERIATRICO STA RITA (GASCON Y RAPISARDA SH)', '1140', 'NORBERTO', 'S', 'ho.sa.ri@hotmail.com,guivan1@hotmail.com ', 'CAAGUAZU 1254', 'LANUS ESTE', '241 - 2254', 'ANGELITA', 'GERIATRICO', 'Enviar dato faltante !! ', 'PREVENCION'),
(143, '11403', 'JUCANI SRL', '11403', 'MICAELA', 'S', 'info@jucani.com.ar ', 'FONROUGE 3291 PQUE INDUST ALTE BROWN', 'BURZACO', '5235-9383/84', 'MICAELA', 'METALURGICA', 'micaelaciccotta@jucani.com.ar ', 'FEDERACION PATRONAL'),
(144, '11415', 'RODRIGUEZ CRISTIAN Y PARISES GUILLERMO SH', '11415', 'CRISTIAN', 'S', 'cavanelas@yahoo.com.ar,compras@lacteoscavanelas.com.ar,gestion@lacteoscavanelas.com.ar', 'TRES SARGENTOS 1626', 'AVELLANEDA', '4203-2852', 'NADIA-MARIA JOSE', 'MUZZARELLA', 'Enviar dato faltante !! ', 'GALENO'),
(145, '11448', 'MACH 5 SRL', '11448', 'MACH', 'S', 'gnbajko@hotmail.com,meteororexsh@hotmail.com,mach5srl@hotmail.com', 'CHILE 1243', 'LANUS O', '4209-4443', 'GUILLERMO BAICO', 'TRATAMIENTOS TE', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(146, '1146', 'GO - PAR S.R.L.', '1146', 'ANALIA', 'S', 'metalurgicagopar@yahoo.com.ar ', '9 DE JULIO 3443', 'LANUS ESTE', '246-6626/15-4093-4163', 'ANALIA', 'METALURGICA', 'Enviar dato faltante !! ', 'MAPFRE'),
(147, '11487', 'COSMOS ARGENTINA SRL', '11487', 'INES', 'S', 'cosmosadm@speedy.com.ar,inescosmos@speedy.com.ar,cosmosargentina@speedy.com.ar', '20 DE SEPTIEMBRE 421', 'JOSE MARMOL', '4291-2919/4236-0978', 'DANIEL', 'EQUIP. MEDICO', 'Enviar dato faltante !! ', 'PREVENCION'),
(148, '1150', 'HOME S.R.L.', '1150', 'ANALIA', 'S', 'tribunohome@tribunohome.com ', 'LORENZO GUARRACCINO 3052', 'LANUS ESTE', '240 -4629 225 -7930', 'ENC. ANALIA', 'QUIMICA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(149, '11504', 'FAINSCHTEIN MARIANO', '11504', 'ARIEL', 'S', 'flors@argentina.com,cuerosclik@gmail.com ', 'CHUBUT 2467', 'LANUS OESTE', '4262-8555', 'ARIEL-MARIANO', 'FABRICA DE CALZADO', 'cuerosclick@gmail.com ', 'ASOCIART A.R.T.'),
(150, '11528', 'SIEMPRE SUR SA', '11528', 'CLAUDIO', 'S', 'rcatalano@pastasramos.com.ar ', 'GALICIA 270', 'AVELLANEDA', '4302-0045 INT. 24', 'MARLENE MOLINA-HECTOR', 'FCA. DE PASTAS', 'compras@pastasramos.com.ar ', 'PREVENCION'),
(151, '11544', 'SUIPACHA INN S.R.L.', '11544', 'DANIEL', 'S', 'danielhiga@live.com ', 'SUIPACHA 515', 'CAPITAL', '4322-0099', 'DANIEL', 'HOTEL', 'Enviar dato faltante !! ', 'PROVINCIA'),
(152, '1156', 'PLEGA CHAP S.A.', '1156', 'ALICIA', 'S', 'plegachap@hotmail.com ', 'PASTOR FERREYRA 4126', 'R. ESCALADA', '248 - 0761', 'ALICIA- Sra.GIANNONE - MIGUEL', 'METALURGICA', 'Enviar dato faltante !! ', 'SMG'),
(153, '11567', 'DISTRI S.A.', '11567', 'EDUARDO', 'S', 'distri@speedy.com.ar,admikodama@hotmail.com ', '29 DE SEPTIEMBRE 1960 LOCAL19 Y 20', 'LANUS ESTE', '4241-7550', 'ALEJANDRO-EDUARDO', 'COMERCIO', 'Enviar dato faltante !! ', 'GALENO'),
(154, '1157', 'INDUSTRIAS QUIMICAS DARDEX SRL', '1157', 'VERONICA', 'S', 'joseluis@anilinasdardex.com,dardex@anilinasdardex.com,vero@anilinasdardex.com,galarza@anilinasdardex.com', 'ALSINA 1441 5 OF 053', 'CAP FED', '4381-7950/ 4207-3187', 'JORGE', 'QUIMICA', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(155, '11594', 'GRAVCEM SRL', '11594', 'AGUSTINA', 'S', 'apanza@villavieja.info,cbogon@villavieja.info,rboggon@villavieja.info', 'MARIANO ACOSTA 1254', 'AVELLANEDA OESTE', '4218-3533/ 4228-7589', 'AGUSTINA', 'DEPOSITO DE MERCADERIA', 'apanza@villavieja.info,cbogon@villavieja.info,rboggon@villavieja.info', 'FEDERACION PATRONAL'),
(156, '1160', 'INDUSTRIAS MIGUENS HNOS. SRL', '1160', 'ELIZABETH', 'S', 'administracion@lanss.com.ar,marianamiguens@lanss.com.ar', 'UCRANIA 463', 'VALENTIN ALSINA', '4208-7311/4228-4681', 'SR. CARLOS', 'METALURGICA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(157, '11600', 'RESINAS INDUSTRIALES S.A.', '11600', 'BLANCA', 'S', 'contadorbbm@yahoo.com.ar ', 'HUMAITA 1966', 'AVELLANEDA', '4115-0012/13/153032-8827', 'CLAUDIO', 'PLASTICO', 'Enviar dato faltante !! ', 'MAPFRE'),
(158, '1162', 'JOSIMAR S.A.', '1162', 'GRISELDA', 'S', 'griseldarivero@josimar.com.ar,anabenitez@josimar.com.ar', 'AV. MONTES DE OCA 1123', 'CAPITAL', '4239-9610 4301-5888', 'GRISELDA', 'SUPERMERCADO', 'jorgeflores@josimar.com.ar ', 'ART INTERACCION SA'),
(159, '11620', 'ARQUITECTURA MOVIL BUENOS AIRES SRL', '11620', 'NATALIA', 'S', 'info@arquimov.com.ar ', 'LORIA 2530', 'LOMAS DE ZAMORA OESTE', '2061-6200/3200', 'NATALIA', 'VENTA DE CARPAS', 'Enviar dato faltante !! ', 'QBE'),
(160, '11638', 'ELECTRICIDAD ALSINA SA', '11638', 'ALBERTO', 'S', 'beto@electricidadalsina.com.ar,carolina@electricidadalsina.com.ar,proveedores@electricidadalsina.com.ar', 'AV BELGRANO 727', 'AVELLANEDA', '4201-8162', 'ALBERTO', 'CASA DE ELECTRICIDAD', 'proveedores@electricidadalsina.com.ar ', 'GALENO'),
(161, '1165', 'JUNTAS KLOSTER S.R.L.', '1165', 'JORGE', 'S', 'juntaskloster@speedy.com.ar ', 'DE LA PE#A 1156', 'LANUS ESTE', '246 - 3832 1545308591 JOR', 'JORGE- Sr.J. KLOSTER', 'METALURGICA', 'Enviar dato faltante !! ', 'SMG'),
(162, '1166', 'JUNAR SA', '1166', 'HERNANDO', 'S', 'personal@juntasmeyro.com.ar,carlosangelucci@juntasmeyro.com.ar', 'JOSE INGENIEROS 2215 PQUE IND. BURZACO', 'BURZACO', '4238-8215/8557', 'PEDRO MARTIN CARLOS ANGELUCCI RRHH', 'METALURGICA', 'danieldebrito@juntasmeyro.com.ar,mancebo@juntasmeyro.com.ar', 'GALENO'),
(163, '11664', 'ROMEO DI PIERO', '11664', 'LIDIA', 'S', 'esrdp@speedy.com.ar ', 'AV HIPOLITO YRIGOYEN 4701', 'LANUS OESTE', '4241-0770', 'LIDIA', 'ESCRIBANIA', 'Enviar dato faltante !! ', 'MAPFRE'),
(164, '11671', 'INDUSTRIAS METALURGICA LANDER SAICIY F', '11671', 'SILVINAM', 'S', 'vivijudy@gmail.com,agostina@landersa.com.ar ', 'RIVADAVIA 621', 'AVELLANEDA', '4209-3788', 'MATIAS-SILVINA', 'METALURGICA', 'agostina@landersa.com.ar ', 'LA CAJA'),
(165, '11675', 'SITECA SRL', '11675', 'ROMINA', 'S', 'viviendasrolon@speedy.com.ar,siteca1@speedy.com.ar,faei1983@hotmail.com', 'GRAL PICO 3546', 'LANUS ESTE', '4202-9602 ROMINA', 'ROMINA/ CRISTINA', 'PREMOLDEADOS', 'Enviar dato faltante !! ', 'PROVINCIA'),
(166, '11679', 'JACQUARD TEXTILES S.A.', '11679', 'FERNANDO', 'S', 'administracion@jacquard-textiles.com,perassofer@hotmail.com,administracion2@jacquard-textiles.com', 'THOMAS GUIDO 2468 PQUE IND. BURZACO', 'BURZACO', '15-5487-8948 ,4238-2451', 'FERNANDO PERASSO, WALTER', 'TEXTIL', 'Enviar dato faltante !! ', 'QBE'),
(167, '1169', 'CASA KORUK', '1169', 'GRISELDA', 'S', 'casakoruk@hotmail.com,lake@speedy.com.ar,lakeventas@proforce.com.ar', 'HIPOLITO YRIGOYEN 4169', 'LANUS OESTE', '4241-8597 4247-4422/4848', 'SILVINA', 'ZAPATER.MAYORIS', 'Enviar dato faltante !! ', 'GALENO'),
(168, '11698', 'CABALLERO ORTIZ Y POZZI S.A', '11698', 'GLORIA', 'S', 'info@caballeroortizpozzi.com.ar,caballeroortizypozzi@gmail.com', 'AV HIPOLIO YRIGOYEN 5160', 'LANUS OESTE', '4241-3449///4834', 'GLORIA', 'PAPELERA', 'Enviar dato faltante !! ', 'QBE'),
(169, '1173', 'ARBOLEYA S.R.L.', '1173', 'ALEJANDRO', 'S', 'ladivalanus@gmail.com ', 'H. YRIGOYEN 4599', 'LANUS OESTE', '241 -6355 225 -6454', 'REY- ALEJANDRO 15-40474632', 'RESTAURANTE', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(170, '11741', 'EDICIONES COLIHUE S.R.L.', '11741', 'AURELIO', 'S', 'narvaja@colihue.com.ar,administracion@colihue.com.ar,proveedores@colihue.com.ar', 'AV DIAZ VELEZ 5125', 'CAPITAL', '4958-4442', 'AURELIO', 'EDITORIAL', 'proveedores@colihue.com.ar ', 'LA CAJA'),
(171, '11752', 'EDICIONES DEL SOL S.R.L.', '11752', 'MONICA', 'S', 'edelsol@edicionesdelsol.com.ar,proveedores@edicionesdelsol.com.ar', 'CALLAO 737', 'CAPITAL FEDERAL', '4811-6722/ 4372-8342', 'MONICA', 'EDITORIAL', 'Enviar dato faltante !! ', 'LA CAJA'),
(172, '1176', 'LAVADERO BANFIELD S.A.', '1176', 'PABLO', 'S', 'servicios@lavaderobanfield.com.ar,rrhh@lavaderobanfield.com.ar,vquiroga@lavaderobanfield.com.ar', 'AV. EVA PERON 2275 (EX. PASCO)', 'TEMPERLEY ESTE', '4264-1899', 'OSVALDO BUSOLIN', 'LAVADERO', 'Enviar dato faltante !! ', 'PREVENCION'),
(173, '11771', 'FIBOSA S.A.', '11771', 'MARCELO', 'S', 'info@fibosa.com.ar ', 'H. AZCASURI 1232', 'WILDE', '4206-9708/4227-9999', 'MARCELO-ANGELA-ALFONSO', 'METALURGICO', 'Enviar dato faltante !! ', 'PROVINCIA'),
(174, '11775', 'FRAPELAY', '11775', 'MAGA', 'S', 'anamariamato@yahoo.com.ar ', 'JOSE LEON SUAREZ 1657', 'REMEDIOS DE ESCALADA', '4276-1788', 'Enviar dato faltante !!', 'SODERIA', 'Enviar dato faltante !! ', 'QBE'),
(175, '11776', 'LOLO SUPERMERCADO', '11776', 'HERNAN', 'S', 'hernan.mendez@speedy.com.ar,dra.varacka@gmail.com,pedidos@lolosupermercado.com.ar,lolosupermercado@hotmail.com', 'CABILDO 137', 'AVELLANEDA', '4209-0044', 'HERNAN-LEONARDO', 'SUPERMERCADO', 'Enviar dato faltante !! ', 'QBE'),
(176, '1178', 'LA NATA S.A.', '1178', 'RODOLFO', 'S', 'camposarriba@hotmail.com ', 'ANATOLE FRANCE 841', 'LANUS ESTE', '241 - 0020/ 1561918549', 'VILMA-GALLO FRANCO', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(177, '1179', 'LAURUTIS HNOS S.H.', '1179', 'ELSA', 'S', 'info@vialro.com.ar ', 'DR. PATXOT 351', 'VALENTIN ALSINA', '208-8603 228-6506', 'ELSA-STELLA-YOLANDA', 'METALURGICA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(178, '11794', 'BRACALZ S.R.L.', '11794', 'FERNANDO', 'S', 'brafordcal@brafordcal.com.ar,brafordcal@yahoo.com.ar', 'SANTIAGO DEL ESTERO 1345', 'LANUS OESTE', '4241-8039', 'FERNANDO-MARIO', 'CALZADO', 'Enviar dato faltante !! ', 'LA CAJA'),
(179, '1181', 'LUMICEM', '1181', 'DANIEL', 'S', 'lumicem@lumicem.com ', 'SARMIENTO 1223', 'LANUS ESTE', '4240-2087', 'DANIEL/MARTIN', 'CARTELES', 'Enviar dato faltante !! ', 'QBE'),
(180, '11811', 'LAUCLAMAR S.A.', '11811', 'ARIEL', 'S', 'ignomeriello@live.com ', 'COLECTORA ACCESO SUD ESTE KM 12', 'AVELLANEDA ESTE', '4206-0922', 'ARIEL', 'ESTACION DE SERVICIO', 'Enviar dato faltante !! ', 'SMG'),
(181, '1182', 'LAS PALMAS 2000', '1182', 'MIGUEL ANGEL', 'S', 'NO MAIL 2 ', '9 DE JULIO 1253', 'LANUS ESTE', '4241-0359 4225-4636', 'MIGUEL ANGEL ARCE', 'PIZZERIA', 'Enviar dato faltante !! ', 'QBE'),
(182, '11832', 'TRANSPORTE RIOS', '11832', 'MARIANELA', 'S', 'riostransporte@live.com.ar ', 'LIMAY 1734 PLANTA ALTA', 'AVELLANEDA ESTE', '4204-1411/155-3089120', '-JAVIER', 'TRANSPORTE', 'Enviar dato faltante !! ', 'PREVENCION'),
(183, '11847', 'TROKUN WALTER ALBERTO', '11847', 'WALTER', 'S', 'bladicortsrl@hotmail.com,lidercort@gmail.com ', 'CONDARCO 245', 'LANUS ESTE', '4246-1392', 'WALTER', 'METALURGICA', 'Enviar dato faltante !! ', 'QBE'),
(184, '1186', 'TERMOPLASTICOS ANTARTIDA S.A.', '1186', 'ARTURO', 'S', 'arturos@termoplasticosantartida.com,personal@termoplasticosantartida.com', 'JAEN JAURES 4402', 'VALENTIN ALSINA', '4891-8154 / 8100', 'ARTURO-BOHDAN', 'PLASTICO', 'Enviar dato faltante !! ', 'LA CAJA'),
(185, '11861', 'QUIMICA GALI S.A.', '11861', 'LUCAS', 'S', 'info@quimicagali.com ', 'ENRIQUE FERNANDEZ 4045', 'LANUS OESTE', '4286-1292/1325', 'LUCAS', 'QUIMICA', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(186, '11866', 'JORGE EDUARDO Y JORGE ROBERTO SANFELIPPO', '11866', 'JORGE', 'S', 'transsanfelippo@speedy.com.ar ', 'MARGARITA WEILD 2983', 'LANUS ESTE', '4246-5328', 'JORGE', 'TRANSPORTE', 'Enviar dato faltante !! ', 'GALENO'),
(187, '1187', 'SUCESION DE LUCAS REYNOSO', '1187', 'PEDRO', 'S', 'met.reynosolucas@live.com.ar ', 'ENRIQUE VELLOSO 3349', 'R. ESCALADA O', '4267-5414', 'LUCAS REYNOSO', 'METALURGICA', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(188, '11882', 'ESTACION DE SERVICIO SAAVEDRA SRL', '11882', 'DELIA', 'S', 'deliaypf@ciudad.com.ar ', 'CRISOLOGO LARRALDE 4688', 'CAPITAL FEDERAL', '4542-9267//4541-7867', 'DELIA', 'EST. DE SERV.', 'deliaypf@ciudad.com.ar ', 'BERKLEY INTERNATIONAL'),
(189, '11905', 'OTERO MATIAS', '11905', 'DARIO', 'S', 'info@distridiet.com.ar,distrodiet@speedy.com.ar', 'PASO DE LA PATRIA 1031', 'VALENTIN ALSINA', '4218-5498/4139-4666', 'DARIO OTERO', 'VENTA DE LEGUMBRES', 'Enviar dato faltante !! ', 'QBE'),
(190, '11917', 'WEB LOGIST BUSINESS S.A.', '11917', 'FERNANDO', 'S', 'celec@sion.com ', 'ZAVALETA 205', 'CAPITAL FEDERAL', '4218-5500', 'CLAUDIO', 'LOGISTICA', 'admcelec@yahoo.com.ar ', 'GALENO'),
(191, '11919', 'SCHULZ RAUL FRANCISCO', '11919', 'HUGO', 'S', 'info@hugopompili.com.ar ', 'ALVAREZ THOMAS 399', 'LOMAS DE ZAMORA OESTE', 'Enviar dato faltante !!', 'schulz', 'STAND PUBLICITA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(192, '11936', 'DECUZZI GUILLERMO JOSE', '11936', 'CRISTINA', 'S', 'maquinas_jalley@hotmail.com ', 'ANATOLE FRANCE 1070', 'LANUS ESTE', '4241-8817', 'GUILLERMO-EDUARDO', 'METALURGICA', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(193, '11954', 'FANTOZZI MARCELO RAUL', '11954', 'MARCELO', 'S', 'puri-center@hotmail.com ', 'CHUBUT 2728', 'LANUS OESTE', '4262-9454/7425', 'MARCELO', 'DISTRIBUIDORA', 'Enviar dato faltante !! ', 'QBE'),
(194, '11964', 'TRANSPORTES DIPE S.A.', '11964', 'KARINA', 'S', 'mercedesf@transportesdipesa.com.ar, karinad@transportesdipesa.com.ar', 'HERNANDARIAS 2718', 'REMEDIOS DE ESCALADA OESTE', '4267-1235', 'MERCEDES-KARINA', 'TRANSPORTE', 'Enviar dato faltante !! ', 'ART LIDERAR SA'),
(195, '11969', 'SBORA SRL', '11969', 'CARLOS', 'S', 'info@sboramudanzas.com.ar,matisbora@hotmail.com', 'DIAZ VELEZ 4552', 'CAPITAL FEDERAL', '4982-9728', 'MATIAS SBORA', 'EMPRESA DE MUDANZAS', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(196, '1197', 'M.A.P.S.A.', '1197', 'JULIO', 'S', 'ofpersonal@mapsanet.com,greyna@mapsanet.com ', 'FERRE 373', 'LANUS ESTE', '205-3212 int 118 rosa', 'LAURA-ADRIANA-P-ROSA', 'FCA. PLASTICOS', 'slopez@mapsanet.com,greyna@mapsanet.com ', 'SMG'),
(197, '11973', 'TREFITOOL S.A.', '11973', 'SILVANA', 'S', 'trefitool@speedy.com.ar ', 'AGUILAR 2261', 'REMEDIOS DE ESCALADA ESTE', '4225-3341/155228-3170 RIC', 'CRISTINA-RICARDO', 'METALURGICA', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(198, '11975', 'PEOPLE EMPLOY SA', '11975', 'CARLA', 'S', 'estefania.karadanian@pesesa.com.ar,soledad.maldonado@pesesa.com.ar', 'STGO DEL ESTERO 366 8° PISO OF 82', 'CABA', '4383-6353/4382-9753', 'MERCEDES', 'AGENCIA DE EMPLEO', 'soledad.maldonado@pesesa.com.ar,estefania.karadanian@pesesa.com.ar', 'ASOCIART A.R.T.'),
(199, '11979', 'VALVULAS BALBOA', '11979', 'MARTIN', 'S', 'martin@valvulas-balboa.com.ar,karenlento@hotmail.com,karenlento@valvulas-balboa.com.ar', 'POSADAS 1464', 'LANUS', '4230-3208', 'KAREN-MARTIN', 'VALVULAS', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(200, '1199', 'MARTINEZ HNOS. S.R.L.', '1199', 'ALBERTO', 'S', 'info@ma-he.com.ar ', 'CAVOUR 3725', 'LANUS OESTE', '4115-8169/4115', 'CELIA- Sr. MARTINEZ', 'FCA. ESTUFAS', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(201, '11997', 'ESB NRO 28', '11997', 'RICARDO', 'S', 'Enviar dato faltante !! ', 'ITUZAINGO 1762', 'LANUS ESTE', '4225-2086 IRIS', 'EDUCATIVA', 'EDUCATIVA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(202, '12000', 'CENTRAL-TREK S.A.', '12000', 'CRISTIAN', 'S', 'triper@opcionestelmex.com.ar,admtotal2011@yahoo.com.ar,estudio@bustosmurilloyasoc.com.ar', 'URUGUAY 16 PISO 9 OF 91', 'CAPITAL', '4116-2972/9481', 'CRISTIAN pecoche 4816 - 1238/OSCAR NICOL', 'TRANSPORTE', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(203, '12001', 'DEPOTRANS S.A.', '12001', 'CRISTIAN', 'S', 'triper@opcionestelmex.com.ar,admtotal2011@yahoo.com.ar,estudio@bustosmurilloyasoc.com.ar', 'PANAMA 933 PISO 13 DTO F', 'CAPITAL', '4116-2972/9481', 'OSCAR', 'TRANSPORTE', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(204, '12005', 'SPIRAL SHOES S.A', '12005', 'ANALIA', 'S', 'info@spiralshoes.com.ar,analia@spiralshoes.com.ar', 'CHACO 2317', 'VALENTIN ALSINA', '4218-6382/3784', 'EMILIO-ANALIA', 'FABRICA DE ZAPATILLAS', 'proveedores@spiralshoes.com,info@spiralshoes.com.a', 'PROVINCIA'),
(205, '12009', 'INTANSIA S.R.L.', '12009', 'CLAUDIA', 'S', 'info@liccoons.com.ar ', 'PJE LATARICO 1571', 'BANFIELD ESTE', '4202-5400', 'CLAUDIA', 'COMERCIO', 'Enviar dato faltante !! ', 'PREVENCION'),
(206, '1203', 'IMPRESOS MACRI S.A.', '1203', 'MARTA', 'S', 'impresos-macri@ertach.com.ar ', 'SUIPACHA 3344', 'REMEDIOS DE ESCALADA', '2058-7674///1537279240(NA', 'ALEJANDRO/MARTA', 'IMPRENTA', 'Enviar dato faltante !! ', 'PROVINCIA');
INSERT INTO `Usuarios` (`id`, `Codigopal`, `Nombrepal`, `Clave`, `Usuario`, `Activo`, `Correo`, `Domicilio`, `Localidad`, `Telefono`, `Contacto`, `Actividad`, `Correop`, `Nombart`) VALUES
(207, '1204', 'METALURGICA BEMAR S.A.', '1204', 'MIRTA', 'S', 'metalurgica.bemar@speedy.com.ar ', 'BOQUERON 2075', 'VALENTIN ALSINA', '208-1519', 'MIRTA MARTINEZ', 'METALURGICA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(208, '12076', 'ZANELLO DANIEL HUMBERTO', '12076', 'VANINA', 'S', 'vaninazanello@hotmail.com ', 'MARIANO ACOSTA 1959', 'AVELLANEDA OESTE', '4209-0424', 'Enviar dato faltante !!', 'TRANSPORTE', 'Enviar dato faltante !! ', 'QBE'),
(209, '1208', 'MICRO OMNIBUS ESTE', '1208', 'MARIA', 'S', 'linea526@yahoo.com.ar ', '9 DE JULIO 3349', 'LANUS ESTE', '4246-5213/4220-2609', '15-5240-8243 STAMARIA', 'TRANSPORTE', 'info@moesa.com.ar ', 'ART INTERACCION SA'),
(210, '1210', 'MIÑOQUI S.R.L.', '1210', 'LAURA', 'S', 'joehopi@infovia.com.ar ', 'ITUZAINGO 1500', 'LANUS ESTE', '249 - 7240', 'FRANCISCO MAIALE', 'ROPA DE NI#O', 'Enviar dato faltante !! ', 'LA HOLANDO'),
(211, '1212', 'DISTRIBUIDORA LA ESTRELLA', '1212', 'DANIELA', 'S', 'rrhh@motosdle.com.ar,dlecontaduria@motosdle.com.ar', 'CATAMARCA 1097', 'LANUS OESTE', '4209-2614/4115-1448', 'ZULMA', 'FCA.REPUES.MOTO', 'elenandle@yahoo.com.ar ', 'PREVENCION'),
(212, '12122', 'MARIA EVA CARLE', '12122', 'MARIAEVA', 'S', 'carlemariaeva@yahoo.com.ar ', 'ALMEYRA 2775', 'REMEDIOS DE ESCALADA ESTE', '4230-5180', 'MARIA EVA', 'ARMADO DE RODILLO', 'Enviar dato faltante !! ', 'MAPFRE'),
(213, '1214', 'MOLYSIL ARGENTINA S.A.', '1214', 'MATIAS', 'S', 'roberto.arganaraz@molysil.com,viviana.marino@molysil.com', 'CATAMARCA 1856', 'AVELLANEDA', '4555-4800', 'MARCELA- P: LIDIA', 'DIST.LUBRICANTE', 'Enviar dato faltante !! ', 'LA SEGUNDA'),
(214, '1215', 'MOLDEMAT S.R.L.', '1215', 'JORGE', 'S', 'moldemat@speedy.com.ar,moldemat@hotmail.com.ar', 'LAS PIEDRAS 2804 - 06', 'LANUS ESTE', '246 - 2884', 'JORGE E. ROST', 'METALURGICA', 'Enviar dato faltante !! ', 'PREVENCION'),
(215, '12155', 'FRIGORIFICO ONETO Y CIA SAIYC', '12155', 'FATIMA', 'S', 'recepcion@frioneto.com.ar,flanzillotta@frioneto.com.ar', 'SOLIS 1950', 'CABA', '4306-0022/0021', 'FATIMA', 'FRIGORIFICO', 'info@frioneto.com.ar ', 'SMG'),
(216, '12158', 'MIELNIK SEBASTIAN GABRIEL', '12158', 'SEBASTIAN', 'S', 'construccioneschubut@gmail.com ', 'CHUBUT 2386', 'LANUS OESTE', '4262-2421/1538012255', 'DAVID', 'CONSTRUCCION', 'Enviar dato faltante !! ', 'MAPFRE'),
(217, '12159', 'VILOIDE SRL', '12159', 'RUBEN', 'S', 'info@emrsa.com.ar ', 'EJ.DE LOS ANDRES 521/ BOQUERON 2962', 'LOMAS DE ZAMORA OESTE', '5290-0678/0759', 'RUBEN RODRIGUEZ', 'METALURGICA', 'Enviar dato faltante !! ', 'GALENO'),
(218, '1216', 'MOLDERIL S.A.', '1216', 'ANDREA', 'S', 'soledadalvarez@molderil.com,andreak@molderil.com', 'MACHAIN 3915', 'LANUS OESTE', '247 - 2718', 'CRISTINA', 'MOLDEO DE PAPEL', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(219, '12170', 'CURTIEMBRE TORRES HNOS SA', '12170', 'FABIAN', 'S', 'curtor_fabian@hotmail.com ', 'PASO DE LA PATRIA 220', 'VALENTIN ALSINA', '4115-2971/72', 'FABIAN/ALEJANDRO', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'PROVINCIA'),
(220, '12171', 'TRANSPORTES LEVALLE', '12171', 'PAOLA', 'S', 'administracion@translevalle.com.ar ', 'IRALA 1541', 'CAPITAL FEDERAL', '4302-8740/7542', 'FLORENCIA', 'TRANSPORTE', 'Enviar dato faltante !! ', 'QBE'),
(221, '12174', 'SOCIEDAD INTREGRADA DE TRABAJOS ENERGETICOS S.A.', '12174', 'SITE', 'S', 'npenatti@gdesarrollo.com.ar,edores@site-sa.com.ar', 'AV. RIVADAVIA 1545 6° B', 'CAPITAL FEDERAL', '4381-2600 /3221-0192', 'EDGARDO/NORA', 'MANTENIMIENTO DE REDES', 'Enviar dato faltante !! ', 'NO POSEE ART'),
(222, '1218', 'MEDICA HNOS.', '1218', 'DANIEL', 'S', 'gerencia@neumaticosmedica.com.ar ', 'MADARIAGA 2054', 'LANUS ESTE', '241-1074 240-5439', 'ANGEL-RODOLFO-MARTA', 'GOMERIA', 'Enviar dato faltante !! ', 'PREVENCION'),
(223, '12200', 'TRANSPORTES E.J.W. SRL', '12200', 'ALICIA', 'S', 'wcastignano@transportesejw.com.ar,administracion@transportesejw.com.ar', 'COLON 363', 'AVELLANEDA ESTE', '4201-7300', 'ALICIA DEL RIO/ WALTER CASTIGNANO', 'TRANSPORTE', 'administracion@transportesejw.com.ar ', 'LA CAJA'),
(224, '12237', 'ASEGURADORA DE RIESGOS DEL TRABAJO LIDERAR SA', '12237', 'LIDERAR', 'S', 'evoluciones@liderarart.com.ar, autorizaciones@liderarart.com.ar,equesada@liderarart.com.ar', 'RECONQUISTA 585 PB', 'CIUDAD AUTONOMA DE BUENOS AIRE', '4696-3330', 'CLEMENTINA', 'ART', 'gcobelo@liderarart.com.ar,equesada@liderarart.com.ar', 'ART LIDERAR SA'),
(225, '12239', 'HAUSZLER EDMUNDO MARTIN', '12239', 'MARTIN', 'S', 'centralcalderas@yahoo.com.ar ', 'OLIDEN 2362', 'LANUS OESTE', '4209-2013/4116-9206', 'Enviar dato faltante !!', 'CENTRAL DE CALDERA', 'Enviar dato faltante !! ', 'NO POSEE ART'),
(226, '1224', 'LUIS ALBERTO ARIAS', '1224', 'LUIS', 'S', 'carlosfernandofabio@yahoo.com.ar,pizzafactory@speedy.com.ar', '25 DE MAYO 171', 'LANUS OESTE', '4225-0080/0173', 'LUIS-FABIO-RUBEN', 'PIZZERIA', 'Enviar dato faltante !! ', 'GALENO'),
(227, '12242', 'ADYN SRL', '12242', 'NICOLAS', 'S', 'nbassi@small-shoes.com.ar,jmatajurc@small-shoes.com.ar', 'BRASIL 960', 'LANUS OESTE', '4218-1126/4218-5125', 'NICOLAS JUAN', 'CALZADO', 'Enviar dato faltante !! ', 'PREVENCION'),
(228, '1229', 'PIMON S.R.L.', '1229', 'GUSTAVO', 'S', 'pimon_srl@hotmail.com ', 'TAGLE 3362', 'LANUS OESTE', '262 - 1690', 'Sres.LOPEZ-GONZALEZ', 'METALURGICA', 'Enviar dato faltante !! ', 'GALENO'),
(229, '12329', 'DEALER S.A.', '12329', 'VANESA', 'S', 'varaujo@dealersa.com.ar,info@dealer.com.ar,yrodriguez@dealersa.com.ar', 'CAMINO GRAL BELGRANO 10,500', 'BERNAL', '4270-0990', 'VANESA ARAUJO', 'PLASTICOS', 'Enviar dato faltante !! ', 'SMG'),
(230, '1235', 'PASTURA PASCUAL', '1235', 'PASCUAL', 'S', 'NO MAIL 2 ', 'CORONEL D''ELIA 1465', 'LANUS OESTE', '4262-5839', 'EDUARDO- P: MARCELA', 'LITOGRAFIA', 'Enviar dato faltante !! ', 'GALENO'),
(231, '12351', 'MESSINA CALZADOS SA', '12351', 'FRANCO', 'S', 'franco@messina.com.ar,gladys@messina.com.ar ', 'M. ROSAS 1340', 'BANFIELD', '4139-6100/6200-4242-7213', 'GLADYS-FRANCO', 'CALZADOS', 'Enviar dato faltante !! ', 'GALENO'),
(232, '12368', 'ESPOSITO VICENTE JOSE', '12368', 'VICENTE', 'S', 'daies_90@hotmail.com ', 'HUMBERTO PRIMO 3031', 'LANUS OESTE', '4262-5996', 'VICENTE', 'MATERIALES DE CONSTRUCCION', 'Enviar dato faltante !! ', 'PROVINCIA'),
(233, '12372', 'IC PUERTAS (AMATO MARIO JOSE)', '12372', 'MARIANO', 'S', 'icpuertas@ic-puertas.com.ar ', 'COLON 3536', 'LANUS OESTE', '4878-4004/4005', 'MARIANO', 'METALURGICA', 'Enviar dato faltante !! ', 'PREVENCION'),
(234, '12378', 'BINDI ELIZABETH GABRIELA', '12378', 'PABLO', 'S', 'paulaugus@argentina.com ', 'O HIGGINS 1923', 'LANUS ESTE', '4249-3004/1550017096', 'AGATA-ELIZABETH', 'COMERCIO', 'Enviar dato faltante !! ', 'NO POSEE ART'),
(235, '1238', 'FASATRA S.R.L.', '1238', 'MARIASOLEDAD', 'S', 'paketona@opcionestelmex.com.ar,paketona@telmex.com.ar', 'ALFREDO PALACIOS 1708', 'LANUS OESTE', '4228-7727/5372', 'SRA. NORMA', 'CONFECCIONES', 'paketona@telmex.com.ar ', 'MAPFRE'),
(236, '12380', 'ABRAHAM MARIO OSCAR', '12380', 'ABRAHAM', 'S', 'betu_capa@hotmail.com ', 'VIAMONTE 2612', 'LANUS OESTE', '4209-6737', 'ABRAHAM', 'VIDRIERIA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(237, '12389', 'CONFLICTUAR SA', '12389', 'CARMEN', 'S', 'gerenciasiroco@gmail.com,damianrubio@fibertel.com.ar,adm-hotel@hotmail.com', 'J V GONZALEZ 1421', 'LANUS OESTE', '4241-1931/4957-0865', 'CARMEN MASSINI', 'HOTEL SIROCCO', 'adm-hotel@hotmail.com ', 'SMG'),
(238, '1239', 'HUNT S.R.L.', '1239', 'ROSANA', 'S', 'administracion@huntsrl.com.ar ', 'BURELAS 1957', 'LANUS ESTE', '4240-4777', 'GUSTAVO G. HUNT', 'TORNERIA', 'Enviar dato faltante !! ', 'SMG'),
(239, '1242', 'POLHEA S.R.L.', '1242', 'FERNANDO', 'S', 'polhea@rcc.com.ar ', 'CMTE.MAMBERTI 954', 'LANUS ESTE', '240 - 3978', 'FERNANDO-ROBERTO', 'METALURGICA', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(240, '12428', 'GABIART S.R.L.', '12428', 'PATRICIA', 'S', 'pasimonti@gabiart.com.ar ', 'MACHAINT 3919', 'LANUS OESTE', '4249-7872/4225-3652', 'PATRICIA', 'METALURGICA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(241, '1243', 'PILCOMAYO S.R.L.', '1243', 'OMAR', 'S', 'compraspilcomayo@fibertel.com.ar ', 'PILCOMAYO 952', 'VALENTIN ALSINA', '4208 - 5359 / 9470', 'OMAR FERRERI- SUSANA PANUCCIO', 'PAPELERA', 'Enviar dato faltante !! ', 'LA SEGUNDA'),
(242, '12433', 'SWISS MEDICAL ART SA', '12433', 'SWISS MEDICAL SA', 'N', 'no ', 'AV CORRIENTES 1891 PISO 5', 'CIUDAD AUTONOMA DE BUENOS AIRE', '011-5239-6400', 'INES OCHOA', 'ART', 'Enviar dato faltante !! ', 'QBE'),
(243, '12438', 'CAMPO HIPICO Y DE PATO BARRACAS AL SUR', '12438', 'JOSE', 'S', 'chpbs@speedy.com.ar ', 'ALSINA 1051', 'AVELLANEDA', '4201-5196/15-5252-1610', 'OMAR SANCHEZ', 'CAMPO HIPICO', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(244, '1244', 'LAVADERO EL PUNTUAL SRL', '1244', 'LUCIANA', 'S', 'elpuntual@fullzero.com.ar,lavaderoelpuntual@hotmail.com', 'CNEL. D''ELIA 3805 Y POTOSI', 'VALENTIN ALSINA', '209 - 1399', 'JOSE CABALEIRO/ELBA', 'LAVADERO', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(245, '12456', 'MEPEX S.A.', '12456', 'GABRIELAMEPEX', 'S', 'mepex@mepex.com.ar ', 'COLOMBIA 1173', 'LANUS OESTE', '4218-4888', 'LILIANA-HECTOR', 'MONTAJES', 'mepex@mepex.com.ar ', 'GALENO'),
(246, '12466', 'GLADYS MABEL PARDO', '12466', 'VANESA', 'S', 'tentissimotemperley@yahoo.com.ar ', 'AV. H IRIGOYEN 10656 COTO 1ER PISO CAF.', 'TEMPERLEY OESTE', '4137-5210/5211', 'VANESA-GUSTAVO', 'CONFITERIA', 'Enviar dato faltante !! ', 'MAPFRE'),
(247, '12478', 'INSTALACIONES ELECTRICAS BC SRL', '12478', 'GUILLERO', 'S', 'bcsrl10@gmail.com ', 'RECUERO C. TT. CNEL 3394', 'C.A.B.A.', '15-5182-3284 (TRANSITORIO', 'BECKER GULLERMO', 'INSTALACIONES', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(248, '1248', 'REY - GOMA S.R.L.', '1248', 'MARIA', 'S', 'teresaprinc@hotmail.com,valege1@hotmail.com,proveedores@reygoma.com.ar', 'C. URUGUAYO 571', 'LANUS ESTE', '4246 -2540/ 4230 -6125', 'MARIA T. VENEGAS, VALERIA', 'CAUCHO', 'soportes@reygoma.com.ar ', 'LA HOLANDO'),
(249, '12488', 'LA CASA DEL PARQUET LOMAS DE ZAMORA S.R.L.', '12488', 'CAROLINA', 'S', 'lacasadelparquet9763@live.com ', 'AV HIPOLITO YRIGOYEN 9763', 'LOMAS DE ZAMORA', '4243-3095', 'MARTA-ELVIRA', 'PISOS', 'Enviar dato faltante !! ', 'QBE'),
(250, '1250', 'RUBIES S.C.A.', '1250', 'OMAR', 'S', 'rubies@house.com.ar ', 'PICO 346', 'LANUS ESTE', '246-8909', 'Sr. OMAR A. RUBIES', 'METALURGICA', 'Enviar dato faltante !! ', 'LA HOLANDO'),
(251, '12505', 'MASSTECH ARGENTINA S.A.', '12505', 'ARIEL', 'S', 'gsfernandez@masstech.com.ar,arielfernandez@masstech.com.ar,vsfernandez@live.com.ar', 'RANGUNI 3061', 'LANUS OESTE', '4249-9200 4247-4293', 'GISELE-ARIEL', 'SEGURIDAD E HIGIENE', 'Enviar dato faltante !! ', 'QBE'),
(252, '1251', 'OLIGRA S.A.', '1251', 'GRACIELA', 'S', 'lauraburattini@oligra.com.ar,lilianab@oligra.com.ar,diego.giordano@oligra.com.ar', '12 DE OCTUBRE 3783', 'QUILMES', '4115/6700/6801/6803', 'H.POSSE- O.ANACONDIO', 'ACEITE COMESTIB', 'mariela.gimenez@oligra.com.ar ', 'GALENO'),
(253, '1252', 'REVESIL S.R.L.', '1252', 'GUSTAVO', 'S', 'gmp@revesil.com.ar,info@revesil.com.ar,admin@revesil.com.ar', 'CENTENARIO URUGUAYO 685', 'LANUS ESTE', '246 - 5809 / 6130', 'ING. PIQUET GUSTAVO', 'PLASTICOS', 'Enviar dato faltante !! ', 'PROVINCIA'),
(254, '12528', 'CONS. DE PROP L.N.ALEM 255 LOMAS DE ZAMORA', '12528', 'DIEGO', 'S', 'admifernandez@yahoo.com.ar ', 'L.N. ALEM 255', 'LOMAS DE ZAMORA', '15-57496524/4244-2190', 'FERNANDEZ FERNANDO', 'CONSORCIO', 'Enviar dato faltante !! ', 'QBE'),
(255, '1258', 'ROTATIVOS UNICOM S.R.L.', '1258', 'GUILLERMO', 'S', 'guillermo_gutierrez@rotativosunicom.com.ar,alonso@rotativosunicom.com.ar,unicom@yahoo.com.ar', 'DINAMARCA 3489', 'TEMPERLEY ESTE', '4260-5339 o 36/0125 INT 1', 'Enviar dato faltante !!', 'IMPRENTA', 'Enviar dato faltante !! ', 'GALENO'),
(256, '12583', 'PRINOLA S.R.L. SRL', '12583', 'PRINOLA', 'S', 'prinolasel@hotmail.com ', 'RUCCI 1366', 'VALENTIN ALSINA', '4209-9599/5648-8738', 'MARCELO-LEONARDO', 'TRANSPORTE', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(257, '12584', 'RUMER LOGISTICA S.A.', '12584', 'MARCELO', 'S', 'info@rumerlogistica.com,rumerlogistica_sa@hotmail.com', 'RUCCI 1366', 'VALENTIN ALSINA', '4209-9599/5648-8738', 'MARCELO-LEONARDO', 'TRANSPORTE', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(258, '12588', 'DOMINIKA ARTESANAL SRL', '12588', 'PAOLA', 'S', 'dominikaartesanal@yahoo.com.ar ', 'JUJUY 1133', 'LANUS OESTE', '4240-3513', 'PAOLA-MARIA TERESA', 'ALIMENTOS', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(259, '12594', 'LOGISTICA Y TRANSPORTE DOBLE A', '12594', 'SILVINA', 'S', 'matibelen@fibertel.com.ar,agzsa@speedy.com.ar ', 'PEDRO DE MENDOZA 3367', 'C.A.B.A.', '4302-3658', 'VINCULADA CON ZOTTICH', 'TRANSPORTE', 'Enviar dato faltante !! ', 'PREVENCION'),
(260, '12622', 'OSCAR IBARRA', '12622', 'RAMIRO', 'S', 'ibarraoscaralberto@hotmail.com ', 'ATILIO LAVARELLO 443', 'SARANDI', '4204-1309', 'RAMIRO-CELESTE', 'TRANSPORTES', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(261, '1263', 'ALPES', '1263', 'OSCAR', 'S', 'alpessrl@yahoo.com.ar,oscar_alpessol@yahoo.com.ar,nicolas_alpessrl@yahoo.com.ar', 'M.CASTRO 5761', 'LANUS OESTE', '4208-8143', 'OSCAR SOSA', 'MEDICAR', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(262, '12644', 'RUCA PANEL S.R.L.', '12644', 'LAURA', 'S', 'amachado@rucapanel.com.ar ', 'DINAMARCA 3489', 'TEMPERLEY', '4260-3446/4192/3445', 'PABLO INT 114', 'METALURGICA', 'proveedores@rucapanel.com.ar ', 'SMG'),
(263, '1265', 'ROTALOG S.R.L.', '1265', 'SILVIA', 'S', 'rotalogsrl@yahoo.com.ar ', 'HUMAUACA 3036', 'LANUS OESTE', '1553039731 JUAN/3530-4080', 'SILVIA/HUGO', 'METALURGICA', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(264, '12651', 'PAMPA BISTRO S.A', '12651', 'FEDERICO', 'S', 'fedebela@gmail.com,mruedin@gmail.com,ayelenciccone@gmail.com', 'DEHEZA 967', 'LANUS ESTE', '4247-7949/ 4240-8550', 'MIGUEL', 'FABRICA DE ALIMENTOS', 'Enviar dato faltante !! ', 'QBE'),
(265, '12657', 'LOPEZ DIEGO', '12657', 'DIEGO', 'S', 'diegoelopez1163@hotmail.com ', 'MAGDALENA 2236', 'LANUS ESTE', '1557153368/4246-2323', 'DIEGO', 'DISTRIBUIDORA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(266, '1266', 'RIVESPA S.A.', '1266', 'RIVESPA', 'S', 'rivespa@speedy.com.ar,depositorivespa@yahoo.com.ar,r.b.canous@hotmail.com,r.b.canovas@gmail.com', 'PAMPA 1202', 'VALENTIN ALSINA', '4-228-9163/4218-0370', 'HECTOR', 'SUBPRODUCTOS', 'Enviar dato faltante !! ', 'LA CAJA'),
(267, '12672', 'PANADERIA CLAUDIA', '12672', 'CLAUDIA', 'S', 'juanfceliz@hotmail.com.ar ', 'AMENEDO 3166', 'JOSE MARMOL', '4291-0270/ 4293-8000', 'MARTA SANCHEZ', 'PANADERIA', 'juanfceliz@hotmail.com.ar ', 'ART INTERACCION SA'),
(268, '12688', 'MARTINEZ DIEZ E HIJO S.A', '12688', 'LAURA', 'S', 'info@martinezdiez.com.ar ', 'CHACABUCO 145', 'BANFIELD ESTE', '4242-2974', 'LAURA', 'FABRICA DE CARROCERIAS', 'Enviar dato faltante !! ', 'PROVINCIA'),
(269, '12690', 'ABANCUER S.R.L.', '12690', 'EDUARDO', 'S', 'cueros@abancuer.com.ar ', 'FLORIDA 1869', 'VALENTIN ALSINA', '4228-1070/4209-7005', 'ANA PAULA', 'CURTIEMBRE', 'Enviar dato faltante !! ', 'MAPFRE'),
(270, '12706', 'LA TERESA SACI', '12706', 'BELEN', 'S', 'belen@millicom.com.ar ', 'LA RIOJA 2646', 'LANUS OESTE', '4262-1049/1146', 'BELEN', 'CURTIEMBRE', 'Enviar dato faltante !! ', 'ART LIDERAR SA'),
(271, '1271', 'ROMPAL S.A.', '1271', 'ELENA', 'S', 'rrhh@motosdle.com.ar,dlecontaduria@motosdle.com.ar,ivanexito2013@gmail.com', 'REMEDIOS DE ESCALADA 1133', 'LANUS OESTE', '4218-2421/4208-1880', 'ZULMA', 'FCA. MOTOS', 'rompalrepuestos@gmail.com ', 'PREVENCION'),
(272, '12722', 'ASOC. MUTUAL 22 DE JULIO', '12722', 'JUAN', 'S', 'juanf@mutual22dejulio.com.ar,adm@bances.com.ar', 'AV. CERVIÑO 4678 PALERMO', 'CAPITAL', '5237-1164', 'JUAN FRANCO LOPRIATO', 'MUTUAL', 'carmenjimenez@mutual22dejulio.com.ar,adm@bances.com.a', 'FEDERACION PATRONAL'),
(273, '1275', 'COMENSE S.A.', '1275', 'VIRGINIA', 'S', 'personal@comense.com.ar ', 'BLANCO ENCALADA 3078', 'LANUS ESTE', '4246-7074/8473/8114/4246-', 'ROBERTO', 'TEXTIL', 'Enviar dato faltante !! ', 'SMG'),
(274, '1276', 'CARLOS SANTAMARIA S.R.L.', '1276', 'ELBA', 'S', 'guinea@escobillones.com.ar ', 'MAGDALENA 1459', 'LANUS ESTE', '4-230-9099', 'ROBERTO / ROXANA', 'DEPOSIT-CAMION', 'guinea@escobillones.com.ar ', 'PREVENCION'),
(275, '12764', 'ZAPATERIA PI (FABIANA BEATRIZ ROLDAN)', '12764', 'FABIANA', 'S', 'camiluchiss@hotmail.com ', '9 DE JULIO 1135', 'LANUS ESTE', '4241-8264', 'GABRIEL', 'ZAPATERIA', 'Enviar dato faltante !! ', 'SMG'),
(276, '12765', 'ZAPATERIA GUGGA (IRMA BEATRIZ DE CONZOLO)', '12765', 'FABIANA', 'S', 'camiluchiss@hotmail.com ', 'MAIPU 319', 'BANFIELD', '4241-8264', 'GABRIEL', 'ZAPATERIA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(277, '12769', 'CARUSI ANA MARIA', '12769', 'ANAMARIA', 'S', 'lauraarce1973@hotmail.com ', '1º DE MAYO 2528', 'REMEDIOS DE ESCALADA', '4276-4219', 'ANA MARIA', 'PANADERIA', 'Enviar dato faltante !! ', 'LA CAJA'),
(278, '12792', 'MAYORISTA PACO S.H', '12792', 'MARCELO', 'S', 'marcelodiegoguillen@hotmail.com ', 'BARBIERI 1256', 'BANFIELD', '4285-2321', 'MARCELO', 'MAYORISTA GOLOSINAS', 'Enviar dato faltante !! ', 'PROVINCIA'),
(279, '1280', 'PABLOVICH ERNESTO OSCAR', '1280', 'PABLOVICH', 'S', 'servibank@ciudad.com.ar ', 'PARAGUAY 2645', 'VALENTIN ALSINA', '4218-3497/4209-4247/4029-', 'PABLOVICH ERNESTO', 'METALURGICA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(280, '1281', 'SOLNIC S.A.', '1281', 'MARIANELA', 'S', 'info@solnic-soma.com.ar,solnicsoma@gmail.com,l.nobile@solnic-soma.com.ar', 'CENTENARIO URUGUAYO 1656', 'AVELLANEDA ESTE', '4246-9378/4246-9005', 'ROSA/LUIS', 'METALURGICA', 'Enviar dato faltante !! ', 'LA CAJA'),
(281, '12810', 'LA ALTAGRACIA S.A.', '12810', 'FEDERICO', 'S', 'laaltagracia@live.com.ar,ventas@laaltagracia-sa.com.ar', 'DE LASERNA 1049', 'GERLI', '4203-6888/4115-5477', 'FEDERICO', 'METALURGICA', 'Enviar dato faltante !! ', 'GALENO'),
(282, '12811', 'SOBRES EBENEZER S.R.L.', '12811', 'MARIO', 'S', 'roagostino@speedy.com.ar ', 'ZABALLOS 451', 'AVELLANEDA ESTE', '4222-3114', 'DANIEL-MARIO', 'FABRICA DE SOBRES', 'abenezerventas@speedy.com.ar,ebenezeradm@speedy.com.ar', 'FEDERACION PATRONAL'),
(283, '12818', 'BORDENAVE RODOLFO', '12818', 'RODOLFO', 'S', 'distribuidorabordenave@speedy.com.ar,depositoburzaco@live.com.ar,transportebardone@speedy.com.ar', 'HIPOLITO YRIGOYEN 14054', 'ADROGUE', '4214-1929', 'RODOLFO', 'DISTRIBUIDORA', 'Enviar dato faltante !! ', 'QBE'),
(284, '1282', 'SERVICIOS AL SUR S.R.L.', '1282', 'GRACIELA', 'S', 'gracielapose@hotmail.com,seviciosalsur@yahoo.com.ar', 'H. YRIGOYEN 4901', 'LANUS OESTE', '247-6994/6888/7445', 'RITCHER RODOLFO', 'EST. DE SERV.', 'Enviar dato faltante !! ', 'LA CAJA'),
(285, '12823', 'SUMDAF S.A.', '12823', 'JUAN', 'S', 'compras@sumdaf.com.ar,dtecnico@sumdaf.com.ar ', 'GUARANI 163', 'C.A.B.A.', '4911-0127 /1545776685', 'JUAN CRUZ', 'MATERIALES ELECTRICOS', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(286, '12824', 'ALBRIO SRL', '12824', 'MARCELA', 'S', 'albrioavellaneda@gmail.com,danieliervasi@gmail.com,pablogut40@hotmail.com', 'LAVALLOL 80/GALICIA 101', 'LANUS OESTE', '4218-6724/MARCELA', '4227-5350 (AVELL) 4247-8937(LANUS)', 'CAFETERIA', 'danieliervasi@gmail.com ', 'PREVENCION'),
(287, '1283', 'SAN LORENZO ILUMINACION SRL', '1283', 'VIVIANA', 'S', 'administracion@sanlorenzoilum.com.ar ', 'ALMEYRA 2789', 'RDOS ESCALADA ESTE', '246 - 0227 / 3575', 'Sr.DE LUCA- VIVIANA', 'METALURGICA', 'administracion@sanlorenzoilum.com.ar ', 'LA CAJA'),
(288, '1286', 'REY GOMA (CONSTITUCION)', '1286', 'REYGOMA', 'S', 'soportes@reygoma.com.ar ', 'PARACAS 75', 'CAPITAL FEDERAL', '306 - 5423 / 3174', 'MAURICIO', 'CAUCHO', 'Enviar dato faltante !! ', 'LA HOLANDO'),
(289, '12889', 'ESPECIAS LOCOCO S.R.L.', '12889', 'ALEJANDRO', 'S', 'alejandrolococo@hotmail.com ', 'MONTES DE OCA 426', 'AVELLANEDA', '4222-9716/4229-9501', 'ALEJANDRO', 'ALIMENTICIO', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(290, '12893', 'TRANSPORTES SOUTO SH', '12893', 'MARISOL', 'S', 'marisolsouto@hotmail.com ', 'CNEL SANTIAGO 1530', 'LANUS OESTE', '4247-1988/4342-3575', 'MARISOL', 'TRANSPORTE', 'Enviar dato faltante !! ', 'PROVINCIA'),
(291, '12897', 'TALLER METALURGICO COHEN (COHEN MIGUEL ANGEL)', '12897', 'COHEN', 'S', 'tmc98@hotmail.com ', 'MURATURE 1402', 'BANFIELD', '4286-7662', 'MIGUEL-MARTIN', 'METALURGICA', 'Enviar dato faltante !! ', 'GALENO'),
(292, '1291', 'MADERERA UNION S.R.L.', '1291', 'JORGE', 'S', 'madunion@hotmail.com ', 'CNEL. MENDEZ 2502', 'LANUS ESTE', '246 - 6488', 'Enviar dato faltante !!', 'VENTA-DEP.MADER', 'Enviar dato faltante !! ', 'SMG'),
(293, '12929', 'ANTONIO LUONGO', '12929', 'ANTONIO', 'S', 'NO MAIL ', 'SAN VLADIMIRO 5670', 'LANUS OESTE', '4209-3592', 'PASCUAL-ANTONIO', 'CALZADO', 'Enviar dato faltante !! ', 'SMG'),
(294, '12937', 'KABUL SA', '12937', 'CLAUDIO', 'S', 'blumy@speedy.com.ar,albertosalto@hotmail.com ', 'AV HIPOLITO YRIGOYEN 7498', 'BANFIELD', '4288-3133/4 4202-2034', 'ALEJANDRA CANSINI', 'INDUMENTARIA', 'Enviar dato faltante !! ', 'SMG'),
(295, '1294', 'LANUS ALAMBRES S.A.', '1294', 'LILIANA', 'S', 'lasa@infovia.com.ar,lasa-administracion@lanusalambres.com.ar', 'GUIDO SPANO 558', 'LANUS OESTE', '249 - 5566 / 8999', 'LILIANA CASARES', 'METALURGICA', 'lasa-administracion@lanusalambres.com.ar,lasa-compras@lanusalambres.com.ar', 'GALENO'),
(296, '12947', 'ALIMENTOS SUREÑOS S.A.', '12947', 'YESICA', 'S', 'neosol-produccion@speedy.com.ar ', 'LIMAY 1861', 'AVELLANEDA', '4241-0741', 'YESSICA MUÑOZ', 'ALIMENTICIO', 'tesoreria@galletitasneosol.com.ar ', 'GALENO'),
(297, '1295', 'ZOTTICH ALBERTO G. S.A.', '1295', 'SILVINA', 'S', 'agzsa@speedy.com.ar,matibelen@fibertel.com.ar ', 'PEDRO DE MENDOZA 3367', 'CAPITAL FEDERAL', '4302-3658', 'ALBERTO/MIRTA ZOTICH', 'CAMIONES', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(298, '12962', 'FRABIDEL GROUP SA', '12962', 'LAURA', 'S', 'mlpisarro@hotmail.com,lilianafrabidel@gmail.com', 'MURATURE 3350', 'LANUS OESTE', '4262-2015', 'LILIANA MARIA LAURA', 'CALZADO', 'Enviar dato faltante !! ', 'LA CAJA'),
(299, '12977', 'LIBRERÍA LA TERMINAL SRL', '12977', 'LETICIA', 'S', 'na_borioli@hotmail.com,nahyrborioli@speedy.com.ar', 'BOLIVIA 1015', 'LANUS OESTE', '4249-4223', 'LETIZIA', 'DISTRIBUIDORA', 'Enviar dato faltante !! ', 'GALENO'),
(300, '12988', 'WITTUR S.A.', '12988', 'WITTUR', 'S', 'monica.prada@wittur.com,veronica.musacchio@wittur.com,alexia.denazis@wittur.com', 'AV. BELGRANO 2445', 'SARANDI', '4138-9200/4138-9221/ 9258', 'VERONICA MUSSACHIO', 'ASCENSORES', 'maria.valverde@wittur.com,veronica.musacchio@wittur.com', 'FEDERACION PATRONAL'),
(301, '12989', 'G.N.C. CICALV', '12989', 'MARTIN', 'S', 'gnc-cicalv@speedy.com.ar ', 'BLAS PARERA 1276', 'CASTELAR', '4692-1353', 'MARTIN', 'ESTACION DE SERVICIO', 'gnc-cicalv@speedy.com.ar ', 'SMG'),
(302, '1300', 'WAIVAL S.R.L.', '1300', 'ARIEL', 'S', 'waival@arnet.com.ar,info@waivalsrl.com.ar ', 'M. AVELLANEDA 1428', 'RDIOS. DE ESCALADA', '4267-2000 4267-2023', 'ARIEL', 'METALURGICA', 'Enviar dato faltante !! ', 'GALENO'),
(303, '13000', 'BPAR S.A.', '13000', 'ANDREA', 'S', 'amachiaroli@bpar.com.ar ', 'INCLAN 2839', 'CAPITAL', '4942-0123/0001', 'ADRIAN-ANDREA', 'VENTA DE EQUIPOS', 'amachiaroli@bpar-tecnologia.com.ar ', 'PREVENCION'),
(304, '13002', 'LOXA S.A', '13002', 'MAURICIO', 'S', 'mauriciogaj@loxa.com.ar,info@loxa.com.ar ', 'SITIO DE MONTEVIDEO 3949', 'LANUS ESTE', '4289-4398//1530351044', 'MAURICIO', 'METALURGICA', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(305, '13006', 'COOP. DE TRAB. BOULOGNE LTDA', '13006', 'MONICA', 'S', 'nuevasocios@speedy.com.ar ', 'LUJAN 2757', 'CAPITAL', '4301-8005', 'MONICA-ALEJANDRA', 'DISTRIBUIDORA', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(306, '13007', 'T.P.F. S.A.', '13007', 'MONICA', 'S', 'nuevasocios@speedy.com.ar ', 'RIO LIMAY 1970', 'CAPITAL', '4301-8005 INT. 108/15-616', 'MONICA-MARTIN', 'DISTRIBUIDORA', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(307, '13008', 'COOP. DE TRAB. DIST. DIARIOS Y REV. NUEVA ERA LTDA', '13008', 'MONICA', 'S', 'nuevasocios@speedy.com.ar ', 'LUJAN 2757', 'CAPITAL', '4301-8005', 'MONICA-ALEJANDRO', 'DISTRIBUIDORA', 'Enviar dato faltante !! ', 'INTERACCION - COLONIA SUIZA'),
(308, '13039', 'MACROSER S.A.', '13039', 'LOPEZ', 'S', 'ilopezdiaz@estudiomattera.com.ar,personal@edgardovazquez.com.ar,proveedoresev@speedy.com.ar', 'RAUCH 1350', 'TANDIL', '4374-6315/155410-2086', 'INDALECIO', 'Enviar dato faltante !!', 'administracion@edgardovazquez.com.ar,proveedoresev@speedy.com.ar,indalopezdiaz@gmail.com', 'Enviar dato faltante !!'),
(309, '1304', 'VALDOPLAS S.R.L.', '1304', 'MARCELO', 'S', 'valdoplas@yahoo.com.ar ', 'J. M. MORENO 2490 Y RIVADAVIA', 'LANUS OESTE', '262 - 0395 / 0551', 'VANESA', 'PLASTICOS', 'Enviar dato faltante !! ', 'QBE'),
(310, '13058', 'G.N.C. LAS 5 ESQUINAS DE VARELA SRL', '13058', 'SILVIA', 'S', 'petrobrasbanfield@yahoo.com.ar ', 'AV. EVA PÈRON 5100', 'FLORENCIO VARELA', '4274-1300', 'TITO FERNANDEZ', 'ESTACION DE SERVICIO', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(311, '13097', 'FRANCO RODOLFO GABRIEL', '13097', 'FRANCO', 'S', 'NO MAIL 2 ', '20 DE OCTUBRE 100 (libreta Lanus Este)', 'LANUS OESTE', '4225-5820 / 4249-6195', 'RODOLFO', 'LOCUTORIO', 'Enviar dato faltante !! ', 'PROVINCIA'),
(312, '1310', 'VADEMARCO S.A.', '1310', 'JUANJOSE', 'S', 'info@vademarco.com.ar,juanjo@vademarco.com.ar ', 'DARRAGUEIRA 538 - 50', 'VALENTIN ALSINA', '208-8398/6001/3120', 'SR. BARRONE/CALDERON', 'METALURGICA', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(313, '1312', 'JORGE TRASI Y CIA S.R.L.', '1312', 'GUILLERMINA', 'S', 'age.masi@hotmail.com,age.mafi@hotmail.com ', 'MARSELLA 464', 'BANFIELD OESTE', '4276-2727', 'GUILLERMINA', 'MINERALES', 'Enviar dato faltante !! ', 'PROVINCIA'),
(314, '13125', 'SERCATEC S.A.', '13125', 'MARICEL', 'S', 'sercatecsa@gmail.com ', 'CRISOLOGO LARRALDE 2648', 'SARANDI', '4204-7557', 'MARICEL', 'TEXTIL', 'Enviar dato faltante !! ', 'MAPFRE'),
(315, '13127', 'TRANSPORTE Y LOGISTICA PUERTO DE PALOS S.R.L.', '13127', 'MARCELO', 'S', 'transpuertodepalos@hotmail.com ', 'LAS PIEDRAS 4430', 'MONTE CHINGOLO', '4260-7698/155182-8780', 'LUIS-MARCELO', 'TRANSPORTE', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(316, '13130', 'CORRUGADORA LOS ANDES S.R.L.', '13130', 'CRISTINA', 'S', 'corrugadoralosandes@gmail.com ', 'ALEGRE 1170', 'AVELLANEDA OESTE', '5290-7909', 'CRISTINA', 'CORRUGADORA', 'Enviar dato faltante !! ', 'LA CAJA'),
(317, '13136', 'MIDAS CONSTRUCCIONES SRL', '13136', 'JULIETA', 'S', 'midasconstrucciones@hotmail.com ', 'DEHEZA 1671', 'CAPITAL FEDERAL', '4693-5157/4693-1629', 'ROBERTO', 'CONST.', 'administracion@trenquemolque.com.ar,mairabazan@midasservicios.com.ar,mairabazan@midasservicios.com.ar', 'FEDERACION PATRONAL'),
(318, '13144', 'MARVIOMAN SA', '13144', 'MARCELO', 'S', 'sancayetanojms@live.com ', 'AV PASCO 3170', 'TEMPERLEY', '4264-3609', 'MARCELO', 'DISTRIBUIDORA', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(319, '1315', 'TEJEDURIA RUSPINI', '1315', 'INES', 'S', 'nuevossoles@hotmail.com ', 'DIP PEDRERA 1679', 'LANUS OESTE', '208 - 5741', 'Sra.INES', 'TEXTIL', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(320, '13168', 'SOLUCON SRL', '13168', 'NEDI', 'S', 'soluconsrl@hotmail.com ', 'GRAL HORNOS 1120', 'CAP FED', '4302-2490/1565689390', 'NEDI/CARLOS JUAN JOSE BELIZ', 'CONSTRUCCION', 'Enviar dato faltante !! ', 'PREVENCION'),
(321, '13176', 'ALMACEN NAVAL PITRE S.R.L.', '13176', 'DIEGO', 'S', 'pitre@speedy.com.ar,diego.m.rodriguez@hotmail.com', 'PEDRO DE MENDOZA 2405', 'CAPITAL', '4302-3971/4301-0605', 'MIGUEL RODRIGUEZ', 'FERRETERIA INDUSTRIAL', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(322, '13183', 'TRANSPOCAR SRL', '13183', 'MERCEDES', 'S', 'rodriguez_sa@speedy.com.ar ', 'SAN LORENZO 628', 'VILLA DOMINICO', '4207-8211/4378', 'VINCULADA CON OS.FA.MA', 'TRANSPORTE', 'rodriguez_sa@speedy.com.ar ', 'PROVINCIA'),
(323, '1319', 'TOVLENT S.A.', '1319', 'ANIBAL', 'S', 'rrhh@retov.com.ar,rrhh2@retov.com.ar ', 'HERRERA 2314', 'CAPITAL FEDERAL', '4303-1029/', 'CARLOS-P:BLANCA', 'METALURGICA', 'Enviar dato faltante !! ', 'SMG'),
(324, '13192', 'MS SERVICIOS GLOBALES S.R.L.', '13192', 'NESTOR', 'S', 'mnunez@msserviciosglobales.com.ar ', '1 DE MAYO 2879', 'LANUS OESTE', '4267-9959/1551642001', 'MIRTA NUÑEZ/ NESTOR BRITEZ', 'CONSTRUCCION', 'Enviar dato faltante !! ', 'PREVENCION'),
(325, '13207', 'SASSONE JOSE ALBERTO', '13207', 'JOSE', 'S', 'josesassoneinsumos@hotmail.com ', 'PILCOMAYO 2264', 'LANUS OESTE', '4262-2576', 'JOSE SASSONE', 'FABRICA DE CALZADOS', 'Enviar dato faltante !! ', 'SMG'),
(326, '13224', 'TRANSPORTE IGLESIAS SILVA S.R.L.', '13224', 'MABEL', 'S', 'iglesiassilvasrl@yahoo.com.ar,vjunco@silvasrl.com.ar', 'ALFONSINA STORNI 3275', 'VALENTIN ALSINA', '4209-8509', 'MABEL-CARLOS', 'TRANSPORTE', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(327, '13227', 'HUGO MENDIETA', '13227', 'LUCAS', 'S', 'info@bubisa.com.ar ', 'LUJAN Y 9 DE JULIO', 'LANUS ESTE', '4-247-5128/5134', 'CARLOS', 'EST. DE SERV.', 'Enviar dato faltante !! ', 'SMG'),
(328, '1326', 'T Y T SA', '1326', 'NAZARENA', 'S', 'rrhh@tytsa.com.ar ', 'ALCORTA 52', 'LANUS OESTE', '4241 - 6807 / 0297', 'NAZARENA-DANIEL-FLORENCIA-ARIEL F', 'VENTA DE ACCESORIOS Y FUNDAS P', 'proveedores@tytsa.com.ar ', 'GALENO'),
(329, '13268', 'FONSECA S.A.', '13268', 'MARCELA', 'S', 'administracion@conductoresfonseca.com.ar ', 'A. MAGALDI 1930', 'CAPITAL FEDERAL', '4303-8800', 'MARCELA PAOLUCCI', 'FAB. DE CONDUCTORES ELECT', 'administracion@conductoresfonseca.com.ar ', 'LA CAJA'),
(330, '13283', 'TECMI INDUSTRIAL SA', '13283', 'MAURO', 'S', 'ricardodelavega@tecmi.com.ar,ismaelzenon@tecmi.com.ar,proveedores@tecmi.com.ar', 'GALICIA 1257', 'AVELLANEDA OESTE', '4228-8877', 'MAURO', 'METALURGICA', 'Enviar dato faltante !! ', 'LA CAJA'),
(331, '13327', 'BOHERDI SRL', '13327', 'YANINA', 'S', 'info@fundicionboherdi.com.ar,rrhh@fundicionboherdi.com.ar', 'JUNIN 1100', 'BURZACO', '4238-2004/8438/8498', 'LUIS-MARCELA-MONICA-MARIO (RRHH)', 'FUNDICION', 'compras@fundicionboherdi.com.ar ', 'ART INTERACCION SA'),
(332, '1333', 'COLEGIO DEL NIÑO JESUS', '1333', 'ANA MARIA', 'S', 'ninojesus_adm@hotmail.com ', 'ITUZAINGO 1129', 'LANUS ESTE', '241 -1465', 'ANA MARIA- ETHEL', 'EDUCATIVA', 'Enviar dato faltante !! ', 'GALENO'),
(333, '13333', 'FARMACIA UOM GERLI', '13333', 'UOM', 'S', 'acomandatore@uomlanus.com.ar,pgonta@uomlanus.com.ar,jgoldberg@uomlanus.com.ar,tsciscivo@uomlanus.com.ar', 'LACARRA 1556', 'GERLI', '4357-9058', 'ALEJANDRA', 'FARMACIA', 'administracion@uomlanus.com.ar ', 'ART LIDERAR SA'),
(334, '1334', 'INDUSTRIAL VARELA SRL', '1334', 'ALEJANDRO', 'S', 'adriana.cricelli@industrialvarela.com.ar,alejandro.giordano@industrialvarela.com.ar', 'RUTA 2 KM 31800', 'FCIO VARELA', '0222-9493629/9496232', '15-5561-4210/1138803660 ADRIANA', 'RECUPERAC.PLOMO', 'Enviar dato faltante !! ', 'LA SEGUNDA'),
(335, '13382', 'LAMBDA SISTEMAS S.R.L.', '13382', 'GUILLERMO', 'S', 'proveedores@fierro-soft.com.ar ', 'LAVALLEJA 519 1ER PISO', 'CAPITAL FEDERAL', '4139-0493/94 4857-6662', 'GUILLERMO', 'SISTEMAS', 'vanesa.ruizdiaz@fierro-soft.com.ar ', 'LA CAJA'),
(336, '1339', 'QUIMANT S.R.L.', '1339', 'QUIMANT', 'S', 'info@quimant.com.ar,ventas@quimant.com.ar ', 'CMDTE. MAMBERTI 1358', 'LANUS ESTE', '241 -8728 240 -1775', 'Sa.LIDIA VAGHI', 'QUIMICA', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(337, '13390', 'KRANEVITTER LUIS ATILIO', '13390', 'CRISTINA', 'S', 'friccionsur@uolsinectis.com.ar,mcristina.gaspari@gmail.com', 'LITUANIA 2836', 'REMEDIOS DE ESCALADA OESTE', '4276-3111/3334', 'MARIA CRISTINA', 'RESPUESTOS', 'Enviar dato faltante !! ', 'LA CAJA'),
(338, '1340', 'NEOSOL S.A.C.I', '1340', 'YESICA', 'S', 'neosol-produccion@speedy.com.ar,rrhh@galletitasneosol.com.ar', 'PICHINCHA 1865', 'LANUS ESTE', '4241-0741', 'YESICA MUÑOZ', 'FCA. GALLETITAS', 'tesoreria@galletitasneosol.com.ar ', 'QBE'),
(339, '1343', 'I.P.A.B. S.R.L.', '1343', 'ZULMA', 'S', 'zferrara@ipab.com.ar,mcoriza@ipab.com.ar ', 'AYACUCHO 2625', 'LANUS ESTE', '4246-3119/4220-4632', 'ZULMA- MARIEL', 'FCA.DE MUEBLES', 'Enviar dato faltante !! ', 'GALENO'),
(340, '13431', 'NOVOCAP S.A.', '13431', 'DAIANA', 'S', 'lorena.biazzi@novocap.com,adp@novocap.com ', 'TORCUATO DI TELLA 911', 'AVELLANEDA OESTE', '4208-2975,15-40293685 ( L', 'VALERIA', 'LABORATORIO', 'administracion@novocap.com,pago.proveedores@novocap.com', 'SMG'),
(341, '13433', 'COOPERSOL S.A.', '13433', 'VIVIANA', 'S', 'info@coopersol.com.ar,ventas@coopersol.com.ar ', 'AV. CAMINO GRAL BELGRANO 1245', 'GERLI', '4205-2014/6191', 'VIVIANA', 'METALURGICA', 'compras@coopersol.com.ar ', 'GALENO'),
(342, '13437', 'CONSORCIO QUINTANA 376', '13437', 'VANESA', 'S', 'administracionsrl@lasaprop.com ', 'QUINTANA 376', 'LANUS', '4249-7790', 'SRTA. VANESA', 'ADMINISTRACION', 'administracionsrl@lasaprop.com ', 'QBE'),
(343, '13439', 'CONS. M. WIELD 1522, 1526, 1532 Y 1536', '13439', 'VANESA', 'S', 'lasapropiedades@hotmail.com ', 'M. WIELD 1522, 1526, 1532 Y 1536', 'LANUS ESTE', '4249-7790', 'VANESA', 'ADMINISTACION', 'administracionsrl@lasaprop.com ', 'QBE'),
(344, '1344', 'EXE S.A.', '1344', 'DARIO', 'S', 'exesa2000@hotmail.com ', 'GRAL. GUEMES 700', 'AVELLANEDA', '4205-8756/57/8704', 'DOMINGO ANGELINI TIT/YULIA', 'CARGAS-DESCARGA', 'Enviar dato faltante !! ', 'LA CAJA'),
(345, '13442', 'CEREMINATI LUIS ALBERTO Y JORGE ENRIQUE', '13442', 'LUIS', 'S', 'info@lacer.com.ar,luis@lacer.com.ar ', 'DEL VALLE IBERLUCEA 4364', 'REMEDIOS DE ESCALADA', '4241-8822', 'GILDA-LUIS', 'ENTRETENIMIENTO', 'Enviar dato faltante !! ', 'PREVENCION'),
(346, '13444', 'INDUSTRIAS PLARTEC S.A.', '13444', 'LIDIA', 'S', 'plartec@plartec.com.ar,lidiamast@plartec.com.ar', 'BUERAS 234', 'LANUS ESTE', '4240-5510', 'LIDIA-OMAR', 'PLASTICO', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(347, '1345', 'REFRATEX DE LEANDRO A. SCHWARZ', '1345', 'LEANDRO', 'S', 'refratex@speedy.com.ar ', 'BOLA#OS 2573', 'LANUS ESTE', '4241-0412', 'EUGENIO', 'REFRACTARIOS', 'Enviar dato faltante !! ', 'LA CAJA'),
(348, '13450', 'WHEELPAM SA', '13450', 'SAMANTA', 'S', 'wheelpam@gmail.com ', 'JOSE M FREYRE 532', 'AVELLANEDA', '4218-3366/3322', 'SAMANTA/VERONICA PARDO', 'ELABORADORA DE CAUCHO', 'Enviar dato faltante !! ', 'GALENO'),
(349, '13453', 'RECUFIBRA SA', '13453', 'HECTOR', 'S', 'hferrari@celulosacampana.com.ar ', 'DARRAGUEIRA 1261', 'VALENTIN ALSINA', '4208-4831', 'HECTOR FERRARI/ OSCAR MUIÑA', 'TRANSPORTE', 'proveedeores@celulosacampana.com.ar ', 'SMG'),
(350, '13472', 'GIOIA MARIO ANTONIO', '13472', 'MARIANA', 'S', 'gioiamariana@gmail.com ', 'PORTELA 1143', 'LOMAS DE ZAMORA OESTE', '4292-5189', 'CRISTINA-MARIA', 'CALZADO', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(351, '13474', 'CONS.SITIO DE MONTEVIDEO 1517/21', '13474', 'VANESA', 'S', 'vanesawolker@hotmail.com ', 'CONS. SITIO DE MONTEVIDEO 1517/21', 'LANUS ESTE', 'SE HABLO CON PUCHI', 'EL ADM.(LAZA PROPIEDADES)', 'ADMINISTRACION', 'Enviar dato faltante !! ', 'QBE'),
(352, '13501', 'CURTIEMBRE RANCHOS SRL', '13501', 'ANDRES', 'S', 'info@thepoplar.com.ar,info@curtiembreranchos.com.ar', 'MANUEL OCAMPO 2865', 'V. ALSINA', '4139-6809', 'ANDRES', 'CURTIEMBRE', 'Enviar dato faltante !! ', 'GALENO'),
(353, '13505', 'TORNEART S.A.', '13505', 'MARISA', 'S', 'metalurgicamonserrat@hotmail.com,torneart@hotmail.com,torneart@hotmail.com.ar', 'HIPOLITO YRIGOYEN 1922', 'AVELLANEDA OESTE', '4208-7714', 'MARISA', 'METALURGICA', 'Enviar dato faltante !! ', 'ART LIDERAR SA'),
(354, '13508', 'DOMINGUEZ ALEJANDRO FABIO', '13508', 'MELINA', 'S', 'impactodevida@hotmail.es ', 'BOULOGNE SUR MER 2308 ESQ. GALLO', 'BANFIELD', '4248-7426', 'MELINA- ALEJANDRO', 'DISTRIBUIDORA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(355, '13512', 'PG HIDRAULICA S.R.L.', '13512', 'MARIO', 'S', 'pg2@ciudad.com.ar ', 'RICO 24', 'LANUS OESTE', '4249-6890', 'PABLO-HERNAN', 'TALLER', 'pablo@pghidraulica.com.ar ', 'SMG'),
(356, '13515', 'PILOTES DEL SUR S.R.L.', '13515', 'ANALIA', 'S', 'pilotesdelsur@hotmail.com.ar ', 'TUCUMAN 3161', 'LANUS ESTE', '4220-5867', 'ANALIA', 'CONSTRUCCION', 'Enviar dato faltante !! ', 'GALENO'),
(357, '13539', 'ROMANO CARLOS GUSTAVO Y ROMANO ALEJANDRO IVAN', '13539', 'GLADIS', 'S', 'romano.hnos@hotmail.com,romanohnos@speedy.com.ar,estudiognr@hotmail.com', 'HIPOLITO YRIGOYEN 13866', 'BURZACO', '4293-5430/', 'ROXANA', 'METALURGICA', 'Enviar dato faltante !! ', 'RECONQUISTA ART'),
(358, '13560', 'IKE ASISTENCIA ARGENTINA SA', '13560', 'SEBASTIAN', 'S', 'scastro@ikeasistencia.com,mlcastro@ikeasistencia.com,fulloa@ikeasistencia.com,ddacampo@ikeasistencia.com', 'UGARTE 1674 4TO PISO', 'CAPITAL FEDERAL', '4136-0672', 'SEBASTIAN CASTRO', 'CALL CENTER', 'nraposo@ikeasistencia.com,scastro@ikeasistencia.com,mlcastro@ikeasistencia.com,nvogt@ikeasistencia.com', 'QBE'),
(359, '1357', 'LA ZULEMITA S.A.', '1357', 'ZULEMITA', 'S', 'moriatishnos@infovia.com.ar ', 'SITIO DE MONTEVIDEO 1150', 'LANUS', '4241-1243 4225-3795/8403', 'VICTOR MORIATIS', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(360, '1359', 'SILDAM S.A.', '1359', 'SILVINA', 'S', 'agzsa@speedy.com.ar,sildamsa@speedy.com.ar ', 'PEDRO DE MENDOZA 3367', 'CAPITAL FEDERAL', '4301-6998', 'MIRTA- P:SILVINA', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'PREVENCION'),
(361, '13592', 'H YRIGOYEN 26501 S.A.', '13592', 'ALEJANDRO', 'S', 'menagaita@hotmail.com,sanchezmanuelenrique@hotmail.com', 'HIPOLITO YRIGOYEN 26501', 'GUERNICA', '02225-424071', 'MANUEL', 'HOTEL', 'Enviar dato faltante !! ', 'QBE'),
(362, '13601', 'CENTRO COMUNITARIO GUESHER', '13601', 'MIRTA', 'S', 'ccguesher@guesher.org.ar,administracion@guesher.org.ar', 'TTE.GRAL. JUAN D. PERON 1878', 'C.A.B.A.', '4372-2800/4372-7525', 'SRA. MIRTA', 'CENTRO COMUNITARIO', 'Enviar dato faltante !! ', 'GALENO'),
(363, '13652', 'VINOVISKI CARLOS ANTONIO', '13652', 'ALBERTO', 'S', 'albertogarciaar@yahoo.com ', 'PEDERNERA 1321', 'LANUS ESTE', '4241-7698', 'ALBERTO', 'CALZADO', 'Enviar dato faltante !! ', 'NO POSEE ART'),
(364, '13657', 'LAVALLEN CARLOS ALBERTO', '13657', 'CARLOS', 'S', 'plavallen@carloslavallen.com.ar ', 'LACARRA 630', 'LANUS ESTE', '15-6209-1246/4240-1925', 'CARLOS-PRISCILLA', 'CONSTRUCCION', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(365, '13660', 'NUT ARGENTINA ICSA', '13660', 'MARTA', 'S', 'nut-arg@nut-arg.com.ar,info@nutargentina.com,administracion@nut-arg.com.ar', 'CORDOBA 1319', 'LANUS ESTE', '4241-1140', 'MARTA-SOFIA', 'METALURGICA', 'Enviar dato faltante !! ', 'GALENO'),
(366, '13665', 'TREVIPLAST S.A.', '13665', 'KARINA', 'S', 'karinasanfilipo@treviplast.com,adriandellamattia@treviplast.com', 'HEREDIA 2375', 'AVELLANEDA ESTE', '4205-6169', 'KARINA-ADRIAN', 'PLASTICO', 'wascher@treviplast.com ', 'SMG'),
(367, '13682', 'HIGH FASHION SA', '13682', 'IVANA', 'S', 'ivana.merida@gmail.com,ndjorgecazon@gmail.com,rrhh@novadenim.com.ar', 'LAVALLE 2444', 'CAPITAL FEDERAL', '4918-0041', 'IVANA NOVA DENIM', 'TEXTIL', 'Enviar dato faltante !! ', 'LA CAJA'),
(368, '13699', 'TRANSPORTE GARAY', '13699', 'MARIA', 'S', 'las_americas440@hotmail.com,martpe99@hotmail.com,administracion@tgtransportes.com.ar', 'JUAN DOMINGO PERON 2176', 'BANFIELD', '4285-6502', 'YPF LAS AMERICAS (MARIA)', 'TRANSPORTE COMBUSTILE', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(369, '13705', 'RAUL GATTI S.R.L.', '13705', 'SUSANA', 'S', 'lauragatti@speedy.com.ar,analiagatti@speedy.com.ar', 'POSADAS 1442', 'LANUS ESTE', '4220-0801/2491/4246-3833', 'ANALIA-LAURA', 'METALURGICA', 'gattisrl@speedy.com.ar ', 'QBE'),
(370, '13709', 'AMSTORK SA', '13709', 'GONZALO', 'S', 'g.avalos@amstork.com.ar,rrhh@amstork.com.ar,v.escala@amstork.com.ar', 'PERU 440 3ER PISO DTO H', 'CABA', '4203-6030 INT 109', 'GONZALO/ALEJANDRO MASERATI', 'CONSTRUCTORA', 'g.aguirre@amstork.com.ar ', 'LA CAJA'),
(371, '13718', 'SUR-CAM S.A.', '13718', 'LORENA', 'S', 'analia@sur-cam.com.ar,gabrielbianchi@sur-cam.com.ar,lorena@sur-cam.com.ar', 'RIVADAVIA 664', 'AVELANEDA', '4209-4209', 'ANALIA', 'CONSECIONARIA', 'Enviar dato faltante !! ', 'SMG'),
(372, '1372', 'RESIDENCIA GERIATRICA DAIG SRL', '1372', 'DANIEL', 'S', 'da.ig18@hotmail.com,sydgarcia@hotmail.com ', 'MAXIMO PAZ 1060', 'VALENTIN ALSINA', '240-9092', 'CLAUDIA VALLEJOS', 'GERIATRICO', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(373, '13734', 'LOIS RIO FRANCISCO MARTIN Y PAZOS MOURO RAUL A. SH', '13734', 'MARCELO', 'S', 'martinloisrio@hotmail.com ', 'AV. M. BELGRANO 4099', 'V. DOMINICO AVELLANEDA', '4227-7788', 'SR. MARCELO LOTVIN', 'PANADERIA', 'Enviar dato faltante !! ', 'GALENO'),
(374, '13741', 'DISTRIBUIDORA SEGOD S.R.L.', '13741', 'FACUNDO', 'S', 'consultas@segod.com.ar ', 'TALCAHUANO 2847', 'LANUS OESTE', '4218-1746', 'FACUNDO', 'DISTRIBUIDORA', 'recepcion@segod.com.ar ', 'PROVINCIA'),
(375, '13758', 'PATRONE HNOS', '13758', 'CARLOS', 'S', 'transportespatronesa@hotmail.com ', 'LEVALLE 1051', 'AVELLENEDA ESTE', '4201-0311', 'CARLOS', 'TRANSPORTE', 'Enviar dato faltante !! ', 'QBE'),
(376, '13761', 'TRANSPORTE PATRONE S.A.', '13761', 'CARLOS', 'S', 'transportespatronesa@hotmail.com ', 'VIAMONTE 1660 3RO C', 'CAPITAL', '4201-0311', 'CARLOS PATRONE', 'TRANSPORTE', 'Enviar dato faltante !! ', 'QBE'),
(377, '13781', 'PRONTO PIZZA', '13781', 'FRANCISCO', 'S', 'NO MAIL ', 'SITIO DE MONTEVIDEO 2701', 'LANUS ESTE', '4220-4397/15-3052-1839', 'FRANCISCO', 'PIZZERIA', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(378, '13809', 'ELEVAR SRL', '13809', 'GUSTAVO', 'S', 'autoelevadores@elevarsrl.com.ar ', 'MACHAINT 4040', 'LANUS OESTE', '4241-7817', 'ANTONIO ESPOSITO///GUSTAVO', 'REPARACION DE AUTOELEVADORES', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(379, '1381', 'DISTRIB. ALIMENTICIA LA PERLA S.A.', '1381', 'LEANDRO', 'S', 'felipepaolini@laperlasa.com.ar,info@laperlasa.com.ar', 'C.URUGUAYO 1886', 'AVELLANEDA ESTE', '4220-8444', 'GASTON-LEANDRO', 'DISTRIBUIDORA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(380, '1384', 'METALURGICA OLIVA HNOS. S.A.', '1384', 'RAUL', 'S', 'metalurgicaoliva@speedy.com.ar,laurita_mendez2@hotmail.com', 'ALTE. CORDERO 198', 'AVELLANEDA OESTE', '4222-9900', 'JUAN C SALINAS/OSCAR', 'METALURGICA', 'Enviar dato faltante !! ', 'LA HOLANDO'),
(381, '1385', 'PAINT SRL', '1385', 'CARLOS', 'S', 'paint@sinectis.com.ar,administracion@paint-srl.com.ar', 'ARITOBULO DEL VALLE 2080', 'LANUS OESTE', '262-2838/6570', 'DOMINGUEZ CARLOS', 'METALURGICA', 'administracion@paint-srl.com.ar ', 'GALENO'),
(382, '13858', 'BENETIC ANDRES JAVIER', '13858', 'SILVANA', 'S', 'enveco_ajb@yahoo.com.ar,silvana_benetic@yahoo.com.ar', 'CAAGUAZU 458 E BALTAZAR Y LAS CASUARINAS', 'TEMPERLEY', '4260-6170/1524201392 albe', 'ANDRES- SILVANA', 'METALURGICA', 'Enviar dato faltante !! ', 'PREVENCION'),
(383, '13944', 'NOVA DENIM SA', '13944', 'IVANA', 'S', 'rrhh@novadenim.com.ar,ndjorgecazon@gmail.com ', 'CRESPO 3330', 'CAPITAL FEDERAL', '4918-0041/4953-0090 INT 2', 'IVANA', 'LOGISTICA', 'pago.proveedores2013@gmail.com ', 'GALENO'),
(384, '13955', 'CONSORCIO DR. MELO 2971/77', '13955', 'MELO', 'S', 'admfuentes@hotmail.com ', 'DR MELO 2971/77', 'LANUS OESTE', '42499629', 'Enviar dato faltante !!', 'CONSORCIO', 'Enviar dato faltante !! ', 'GALENO'),
(385, '13963', 'DEL YESSO PABLO', '13963', 'PABLO', 'S', 'paulaugus@argentina.com ', '9 DE JULIO 1439', 'LANUS ESTE', '4249-6006', 'PABLO', 'COMERCIO', 'Enviar dato faltante !! ', 'QBE'),
(386, '13989', 'MAURICIO GONCALVES SRL', '13989', 'MAURICIO', 'S', 'administracion@fademax-srl.com.ar ', 'CNO. GRAL BELGRANO 3933/4', 'LANUS ESTE', '4246-9566', 'RICARDO', 'MUEBLERIA', 'facturacion@fademax-srl.com.ar ', 'LA CAJA'),
(387, '14001', 'SUCESION DE DE RUVO JOSE NESTOR', '14001', 'CARMELO', 'S', 'laurinchu_15@hotmail.com,lauvilardo@hotmail.com', 'ALTTE BROWN 3222', 'TEMPERLEY', '4392-0046', 'CARMELO BILARDO', 'DISRIBUIDORA', 'Enviar dato faltante !! ', 'GALENO'),
(388, '14002', 'DE MITO EMILIANO SEBASTIAN', '14002', 'EMILIANO', 'S', 'gemhap@hotmail.com,info@gemhap.com.ar ', 'MILAN 1320', 'BANFIELD', '4286-3419', 'MARTA', 'METALURGICA', 'Enviar dato faltante !! ', 'PREVENCION'),
(389, '14008', 'CLUB SOCIAL Y DEPORTIVO DEFENSORES DE BANFIELD', '14008', 'MARINA', 'S', 'marinalesci@hotmail.com,marina.lesci@estudio-monastero.com.ar', 'JOSE M. PENNA 1610', 'BANFIELD', '4242-7994', 'MARINA', 'CLUB', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(390, '14010', 'SEIJO ROBERTO Y SEIJO GUILLERMO S.H.', '14010', 'PAULA', 'S', 'inmeplas@hotmail.com ', 'GRANADEROS 1260', 'BANFIELD OESTE', '4248-2845', 'PAULA', 'PLASTICOS', 'Enviar dato faltante !! ', 'PROVINCIA'),
(391, '14036', 'CLASIPEL S.R.L.', '14036', 'ALEJANDRA', 'S', 'clasipel@hotmail.com ', 'PILCOMAYO 1017', 'LANUS OESTE', '4208-3688/15-4055-5844', 'ALEJANDRA', 'RECORTERA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(392, '14066', 'CERAMICA DOÑA MARIA', '14066', 'PABLO', 'S', 'ceramicas_d.maria@hotmail.com ', 'EVA PERON 3071/51 (CORRALON)', 'LANUS ESTE', '4220-2278', 'DANIEL- PABLO', 'CONSTRUCCION', 'Enviar dato faltante !! ', 'LA SEGUNDA'),
(393, '14080', 'DE MAIO ANTONIO CARLOS', '14080', 'ANTONIO', 'S', 'curtiembredemaio@yahoo.com ', 'RICARDO BALBIN 245', 'VALENTIN ALSINA', '4208-4759', 'ANTONIO-NICOLAS', 'CURTIEMBRE', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(394, '14118', 'GASPAR JOSE ROQUE', '14118', 'GAMAR', 'S', 'gamar1991@yahoo.com.ar ', 'AV. PASCO 3161/55', 'TEMPERLEY', '4264-6704', 'LIDIA-GABRIELA-MARTIN', 'COTILLON', 'Enviar dato faltante !! ', 'QBE'),
(395, '14149', 'EMBALAJES Y ACCESORIOS DE CARTON SA', '14149', 'ERIKA', 'S', 'info@eyacsa.com,erikavillalba_23@yahoo.com.ar ', 'UCRANIA 1524', 'VALENTIN ALSINA', '4209-0009', 'CINTIA', 'TUBOS DE CARTON', 'cintiusly@hotmail.com ', 'ART LIDERAR SA'),
(396, '14162', 'DANIEL ALFONSO LOPEZ SRL', '14162', 'DANIEL', 'S', 'daniel_alopez@hotmail.com.ar ', 'CHICLANA 1889 (PORTON GRIS)', 'R. DE ESCALADA OESTE', '4267-6667', 'DANIEL', 'SERVICIO', 'Enviar dato faltante !! ', 'GALENO'),
(397, '14183', 'SORBETTO SA', '14183', 'ANALIA', 'S', 'analia_kre@hotmail.com,pablogut40@hotmail.com ', 'AV GALICIA 101', 'AVELLANEDA', '4218-6724/PABLO1567040609', 'ANALIA', 'CONFITERIA', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(398, '14210', 'LA SEGUNDA COMPANIA DE SEGUROS DE PERSONAS SA', '14210', 'LA SEGUNDA COMPANIA', 'N', 'no ', 'MANUEL DE ROSAS 957', 'MANUEL DE ROSAS 957', 'Enviar dato faltante !!', 'Enviar dato faltante !!', 'ART', 'Enviar dato faltante !! ', 'Enviar dato faltante !!'),
(399, '14219', 'FARMACIA UOM ESCALADA S.C.S.', '14219', 'UOM', 'S', 'acomandatore@uomlanus.com.ar,pgonta@uomlanus.com.ar,jgoldberg@uomlanus.com.ar,tsciscivo@uomlanus.com.ar', 'H. IRIGOYEN 6401', 'R. DE ESCALADA', '4242-0090/ 4357-9058 ALEJ', 'ALEJANDRA', 'FARMACIA', 'administracion@uomlanus.com.ar ', 'QBE'),
(400, '14235', 'STECK ELECTRIC SA', '14235', 'CAROLINA', 'S', 'marta.herrera@steckgroup.com,carolina.centurion@steckgroup.com', 'BELISARIO HUEYO 165', 'AVELLANEDA ESTE', '4201-1489/1562887993', 'CAROLINA-MARTA', 'EQUIPAMIENTOS ELECT', 'viviana.soto@steckgroup.com ', 'LA CAJA'),
(401, '14244', 'SMG ART', '14244', 'SWISSMEDICAL', 'S', 'autorizacionesart@smg.com.ar ', 'CORRIENTES 1826', 'CIUDAD AUTONOMA DE BUENOS AIRE', '011-5239-6400', 'INES OCHOA', 'ART', 'juanpablo.casoni@smg.com.ar ', 'SMG'),
(402, '14253', 'CEBALLOS TERESA EMMA', '14253', 'MARIANA', 'S', 'marianazbonellia@hotmail.com,teresapons@hotmail.com.ar', 'ESPAÑA 56', 'LOMAS DE ZOMARA', '4292-3442/4244-1007', 'DRA. CASTELLI 1560439098', 'PELUQUERIA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(403, '14254', 'PONS JUAN PEDRO', '14254', 'MARIANA', 'S', 'marianazbonellia@hotmail.com,teresapons@hotmail.com.ar', 'LAPRIDA 244', 'LOMAS DE ZAMORA', '4292-3442', 'MARIANA', 'PELUQUERIA', 'Enviar dato faltante !! ', 'NO POSEE ART'),
(404, '14256', 'PONS DEBORA', '14256', 'MARIANA', 'S', 'marianazbonellia@hotmail.com,teresapons@hotmail.com.ar', 'ESPAÑA 431', 'LOMAS DE ZAMORA', '4244-1007/4244-7070', 'MARIANA', 'PELUQUERIA', 'Enviar dato faltante !! ', 'NO POSEE ART'),
(405, '14263', 'TEMPLADOS SUPER S.A.', '14263', 'SILVINA', 'S', 'templadossuper@gmail.com ', 'HECTOR GUIDI 855', 'LANUS ESTE', '4249-8625', 'CINTYA/SILVINA', 'VIDRIERIA', 'Enviar dato faltante !! ', 'GALENO'),
(406, '14264', 'MARTA SANCHEZ', '14264', 'MARTA', 'S', 'alejandra-sanchez@speedy.com.ar ', 'SUIPACHA 3740', 'REMEDIOS DE ESCALADA ESTE', '4288-0059', 'FERNANDO', 'PLASTICO', 'Enviar dato faltante !! ', 'QBE'),
(407, '14290', 'PAUPLYS SRL', '14290', 'SANTIAGO', 'S', 'info@pauplys.com,pauplys@hotmail.com ', 'WARNES 2744', 'LANUS OESTE', '4209-5821/4208-1776', 'SANTIAGO', 'METALURGICA', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(408, '14298', 'UGALDE JORGE RAUL', '14298', 'VALERIA', 'S', 'valeria-ugalde@hotmail.com ', 'SAN MARTIN 4100', 'LANUS OESTE', '4267-1354', 'VALERIA-JORGE', 'COMERCIO', 'Enviar dato faltante !! ', 'SMG'),
(409, '14299', 'SANTORO DOMINGO', '14299', 'CLAUDIO', 'S', 'amoblamientos2001@speedy.com.ar ', 'PICO 264', 'LANUS ESTE', '4246-7993', 'CLAUDIO', 'COMERCIO', 'Enviar dato faltante !! ', 'LA SEGUNDA'),
(410, '14322', 'DE RUVO NORA', '14322', 'NORA', 'S', 'calzadosmorelli@gmail.com ', 'AV GRAL SAN MARTIN 2619', 'LANUS OESTE', '4262-9948', 'KARINA', 'CALZADO', 'Enviar dato faltante !! ', 'LA HOLANDO'),
(411, '14336', 'VASCELLO SERGIO OSCAR', '14336', 'SERGIO', 'S', 'sergiovascello@ciudad.com.ar ', 'MARGARITA WEILD 2541', 'LANUS ESTE', '4240-2296/15-6455-8242', 'SERGIO', 'METALURGICA', 'Enviar dato faltante !! ', 'MAPFRE'),
(412, '14350', 'CONS. A.DEL VALLE 462', '14350', 'CRISTINA', 'S', 'cisenlinea@datamarkets.com.ar ', 'A.DEL VALLE 462', 'LANUS OESTE', '4241-0369', 'CAMPASSI', 'CONSORCIO', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(413, '14351', 'CONS. DE PROP J.V. GONZALEZ 2793', '14351', 'CRISTINA', 'S', 'cristinavera@yahoo.com ', 'J.V. GONZALEZ 2793', 'LANUS OESTE', '4141-1881', 'CRISTINA', 'CONSORCIO', 'Enviar dato faltante !! ', 'INTERACCION - COLONIA SUIZA'),
(414, '14402', 'FARMA BANFIELD SCS', '14402', 'ELIZABETH', 'S', 'farmalanus@ciudad.com.ar,elizabethcifuentes33@gmail.com', 'AV. ALSINA 470', 'BANFIELD', '4242-1244,4208-1944', 'MARIANA', 'FARMACIA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL');
INSERT INTO `Usuarios` (`id`, `Codigopal`, `Nombrepal`, `Clave`, `Usuario`, `Activo`, `Correo`, `Domicilio`, `Localidad`, `Telefono`, `Contacto`, `Actividad`, `Correop`, `Nombart`) VALUES
(415, '14403', 'NESTORE HNOS SCS', '14403', 'ELIZABETH', 'S', 'farmalanus@ciudad.com.ar,elizabethcifuentes33@gmail.com,farmalanus.alsina@gmail.com', 'PASO DE LA PATRIA 1555', 'VALENTIN ALSINA', '4218-2649', 'ELIZABETH-JOSE', 'FARMACIA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(416, '14404', 'NEST FARMA SOC. EN COMANDITA SIMPLE', '14404', 'ELIZABETH', 'S', 'farmalanus@ciudad.com.ar,elizabethcifuentes33@gmail.com', '9 DE JULIO 2501', 'LANUS ESTE', '4241-9285', 'ROXANA', 'FARMACIA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(417, '14405', 'NUEVA GISPERT SCS', '14405', 'ELIZABETH', 'S', 'farmalanus@ciudad.com.ar,elizabethcifuentes33@gmail.com', 'MAIPU 401', 'BANFIELD', '4242-0020', 'VERONICA', 'FARMACIA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(418, '14406', 'TACUN PRODUCTOS SRL', '14406', 'ENRIQUE', 'S', 'tacunprod@speedy.com.ar,melgaleo@yahoo.com.ar ', 'PERON 1916', 'VALENTIN ALSINA', '4218-2718/2481', 'CARRIZO', 'TEXTIL', 'Enviar dato faltante !! ', 'PROVINCIA'),
(419, '14456', 'HOTEL SATELITE', '14456', 'ALEJANDRA', 'S', 'costaalejandra@homail.com ', 'COSQUIN 1810', 'LOMAS DE ZAMORA', '4285-7553', 'ALEJANDRA', 'HOTEL', 'Enviar dato faltante !! ', 'MAPFRE'),
(420, '14462', 'TORFRES S.R.L.', '14462', 'MIGUEL.', 'S', 'info@torfres.com.ar ', 'SUIPACHA 1288', 'AVELLANEDA ESTE', '4227-8201/4139-7134/5', 'MIGUEL-MARIA ROSA', 'METALURGICA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(421, '14489', 'ZETALUZ S.R.L.', '14489', 'CRISTINA', 'S', 'cristinadeidos@hotmail.com,zetaluz@speedy.com.ar', 'SERRANO 1580', 'BANFIELD ESTE', '4202-7533', 'CRISTINA', 'METALURGICA', 'Enviar dato faltante !! ', 'LA CAJA'),
(422, '14549', 'ESTISOL SACIF', '14549', 'NICOLAS', 'S', 'inforrhhbuenosaires@grupoestisol.com ', 'IGUAZU 911', 'CAPITAL FEDERAL', '4911-8111 INT 233 DAMIAN', 'NORBERTO SERRANO/ LAURA/ NICOLAS', 'EMBALAJES', 'pagoproveedores@grupoestisol.com,administracion@grupoestisol.com', 'SMG'),
(423, '14565', 'EME TOYS SRL', '14565', 'CECILIA', 'S', 'emetoyssrl@gmail.com ', 'ACHAVAL 4435', 'LANUS ESTE', '1553088956/4220-0442', 'DE ALMEIDA MIGUEL/ CECILIA', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'GALENO'),
(424, '1457', 'BERCAM S.R.L.', '1457', 'OLGA', 'S', 'cobranzas@bercam.com.ar,miriam.flores@bercam.com.ar', 'BELGRANO 3740', 'SARANDI', '4207-0880', 'MIRIAM', 'FABRICA DE CUBIERTAS', 'miriam.flores@bercam.com.ar ', 'SMG'),
(425, '14584', 'AVELINO VEIRAS E HIJOS S.R.L.', '14584', 'GABRIEL', 'S', 'info@avelinoveiras.com.ar ', 'BOLAÑOS 759', 'LANUS ESTE', '4225-8024', 'ELIO-GABRIEL', 'CARPINTERIA', 'Enviar dato faltante !! ', 'LA CAJA'),
(426, '14585', 'CONS. ITALIA 2150 GERLI', '14585', 'CONS. ITALIA 2150 GE', 'N', 'mrech_adm@yahoo.com.ar ', 'ITALIA 2150', 'GERLI', '3528-4932/15-5515-3977', 'MARIA RECH', 'CONSORCIO', 'Enviar dato faltante !! ', 'QBE'),
(427, '14594', 'ELECTROMECANICA ALEBA SRL', '14594', 'ADALBERTO', 'S', 'info@aleba.com.ar ', 'AV. ALVEAR 1815', 'BANFIELD', '4202-5229', 'adalberto-alesandro-maria fernanda', 'METALAURGICA', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(428, '14632', 'REY MARCELO HERNAN', '14632', 'MARCELO', 'S', 'mrey@indeltec.com.ar ', 'BELELLI 1150', 'BANFIELD OESTE', '4202-6852 15-5-429-4487', 'MARCELO', 'TORNERIA', 'Enviar dato faltante !! ', 'PREVENCION'),
(429, '14638', 'PASCUAL JOSE MARCELINO', '14638', 'JOSE', 'S', 'franvick_srl@hotmail.com,jocpascual@hotmail.com', 'CARLOS TEJEDOR 1672', 'LANUS OESTE', '4262.5859', 'JOSE - LEANDRO', 'CALZADO', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(430, '1467', 'ESTAB. ALIM. EL CENTENARIO SRL', '1467', 'CENTENARIO', 'S', 'vanina_diaz@hotmail.com ', 'MAXIMO PAZ 443', 'LANUS OESTE', '249-8361', 'DIAZ JORGE', 'PANADERIA', 'Enviar dato faltante !! ', 'SMG'),
(431, '14674', 'DISTRIBUIDORA TRES CAMINOS', '14674', 'MARTIN', 'S', 'trescaminos@speedy.com.ar ', 'LUJAN 2312', 'CAPITAL FEDERAL', '4303-0060', 'MARTIN/MARIA/FERNANDO', 'DISTRIBUIDORA DE DIARIOS Y REV', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(432, '14675', 'CNC TRANSPORTES S.R.L.', '14675', 'MARTIN', 'S', 'cnctransportes@speedy.com.ar ', 'LUJAN 2312', 'CAPITAL FEDERAL', '4303-0060', 'MARTIN/MARIA FERNANDA', 'TRANSPORTE', 'mfls28@yahoo.com.ar ', 'ART INTERACCION SA'),
(433, '14676', 'MAYORISTA SAN TELMO S.R.L.', '14676', 'MARTIN', 'S', 'cnctransportes@speedy.com.ar ', 'LUJAN 2312', 'CAPITAL FEDERAL', '4303-0060', 'MARTIN/MARIA FERNANDA', 'DISTRIBUIDORA DE BEBIDAS', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(434, '1469', 'ENREJILLADOS CELDA S.R.L.', '1469', 'CELDA', 'S', 'info@enrejilladocelda.com.ar ', 'JEAN JAURES 770', 'LANUS OESTE', '225-4206 241-4625', 'SR. RICARDO CIGNETTI/JORGE', 'MEDICAR', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(435, '14723', 'PAIDELLA S.A.', '14723', 'CLAUDIA', 'S', 'admicse@gmail.com ', 'IBERA 168', 'AVELLANEDA', '3531-5125/3980-8937', 'CLAUDIA', 'EDUCATIVA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(436, '14728', 'STAHL BRASIL SA SUCUSAL ARGENTINA', '14728', 'CLAUDIA', 'S', 'claudia.rhoden@stahl.com ', 'VERACRUZ 2543', 'VALENTIN ALSINA', '4208-5755///4208-5715', 'CLAUDIA', 'IMPORTACION', 'Enviar dato faltante !! ', 'GALENO'),
(437, '14740', 'AVAL S.R.L.', '14740', 'KARINA', 'S', 'personal@seguridadaval.com.ar,daniela.parodi@seguridadaval.com.ar', 'AZOPARDO 185 AL 6700 DE PAVON', 'REMEDIOS DE ESCALADA OESTE', '4242-6662/6664', 'KARINA RRHH 104, PAG PROV INT102', 'SEGURIDAD', 'Enviar dato faltante !! ', 'PROVINCIA'),
(438, '14743', 'EPISCOPO RICARDO ALBERTO', '14743', 'RICARDO', 'S', 'ricalepi@hotmail.com,episcopo_9@hotmail.com,juniorpoker@hotmail.com.ar', 'AV. R. OBLIGADO 4151 (KOTROCOS)', 'CAPITAL FEDERAL', '4773-8408', 'MARTIN', 'GASTRONOMICO', 'Enviar dato faltante !! ', 'QBE'),
(439, '14774', 'CELFER SRL', '14774', 'ALBERTO', 'S', 'lilium508@hotmail.com,lilium@hotmail.com ', '9 DE JULIO 1301', 'LANUS ESTE', '4241-7109', 'ALBERTO RAMOS 1530680545/ FLAVIA', 'TEXTIL', 'Enviar dato faltante !! ', 'GALENO'),
(440, '14782', 'INTERSUL SRL', '14782', 'FERNANDO', 'S', 'fer_compras@abastecedoradelsur.com.ar ', 'MOLINEDO 1042', 'LANUS OESTE', '4208 - 9137', 'FERNANDO/CAMILA', 'DISTRIBUIDORA', 'compras@abastecedoradelsur.com.ar ', 'PROVINCIA'),
(441, '14817', 'CASA PESQUEIRA S.A.C.I.', '14817', 'SILVIO', 'S', 'info@casapesqueira.com.ar,pablo.muniz@casapesqueira.com.ar', 'AV CORDOBA 3458', 'CAPITAL FEDERAL', '4862-5656', 'VALENTIN-SILVIO', 'GOMERIA', 'info@casapesqueira.com.ar ', 'LA CAJA'),
(442, '1482', 'COINPLAST S.R.L.', '1482', 'MARIA', 'S', 'r.buldo@coinplastinyeccion.com.ar,administracion@coinplastinyeccion.com.ar', 'PEDRERA 1640', 'LANUS OESTE', '4116-1993/1994', 'JOSE BULDO/GUILLERMO', 'PLASTICOS INYEC', 'Enviar dato faltante !! ', 'GALENO'),
(443, '14824', 'ANA GOMEZ VARGAS (LA CABAÑA)', '14824', 'ANA', 'S', 'luisa2013argentina@hotmail.com ', 'CAAGUAZU 11', 'BANFIELD', '4260-6196', 'GENARO', 'CARNICERIA', 'Enviar dato faltante !! ', 'VICTORIA'),
(444, '14863', 'CASTRO EZEQUIEL FERNANDO', '14863', 'EZEQUIEL', 'S', 'ezequiel.f.castro@hotmail.com ', 'BOEDO 525', 'CAPITAL FEDERAL', '4240-8314/15-3153-5609', 'EZEQUIEL CASTRO', 'FIAMBRERIA', 'Enviar dato faltante !! ', 'LA CAJA'),
(445, '1487', 'TOLDER S.A.', '1487', 'CINTIA', 'S', 'administracion@tolder.com.ar,personal@tolder.com.ar', 'VERACRUZ 518', 'LANUS OESTE', '4807-7000 int 13', 'NORBERTO/MONICA/CINTIA', 'FABRICA TOLDOS', 'contaduria@tolder.com.ar ', 'ART INTERACCION SA'),
(446, '14881', 'ABRELIM S.R.L.', '14881', 'MIRIAM', 'S', 'abrelimsrl@gmail.com,myriabrelim@gmail.com ', 'BOULEVAR SAN MARTIN 3136 OF 5', 'EL PALOMAR', '4751-8897', 'MIRIAM', 'LIMPIEZA', 'myriabrelim@gmail.com ', 'FEDERACION PATRONAL'),
(447, '14886', 'ELIDA CECILIA SAMAMES', '14886', 'EDUARDO', 'S', 'eddye_ar@hotmail.com ', 'VETERE 535', 'LOMAS DE ZAMORA OESTE', '4282-8333', 'EDUARDO-ADRIANA', 'PANIFICADORA', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(448, '14907', 'HIJOS Y NIETOS DE MARIA ASUNCION CORNEJO SRL', '14907', 'CARLOS', 'S', 'sures@speedy.com.ar,sures-ventas@speedy.com.ar', 'A. DEL VALLE 2522', 'LANUS OESTE', '4209-5771', 'CARLOS', 'REFRIGERACION', 'Enviar dato faltante !! ', 'PREVENCION'),
(449, '14919', 'SUAREZ EUGENIO ALBERTO', '14919', 'GABRIEL', 'S', 'servitapsrl@yahoo.com.ar ', 'RDIO DE ESCALADA 2217', 'LANUS OESTE', '1161136445 /15-5183-5142', 'GABRIEL SUAREZ', 'TAPICERIA', 'servitapsrl@yahoo.com.ar ', 'GALENO'),
(450, '1492', 'FIBON S.R.L.', '1492', 'NICOLAS', 'S', 'info@fibonsrl.com.ar ', 'AGUILAR 3921', 'LANUS ESTE', '246-8665', 'SR. NICOLAS RASCHELLA', 'DEP.SANITARIOS', 'Enviar dato faltante !! ', 'LA HOLANDO'),
(451, '1493', 'VAGLIO S.R.L.', '1493', 'CLAUDIO', 'S', 'administracion@vaglio.com.ar ', '2 DE MAYO 4325', 'REMEDIOS DE ESCALADA OESTE', '4357-0029/30', 'VAGLIO', 'METALURGICA', 'cotizaciones@vaglio.com.ar ', 'GALENO'),
(452, '14932', 'DEL SUR DEPORTES SA', '14932', 'ADRIAN', 'S', 'adrianf@delsurdeportes.com.ar ', 'SALTA 164 B. SAN JOSE', 'TEMPERLEY ESTE', '4264-1235/2306/8855 /PABL', 'ADRIAN', 'DEPORTIVAS', 'andreav@delsurdeportes.com.ar ', 'PROVINCIA'),
(453, '1494', 'DELTA LADRO SRL', '1494', 'GISELA', 'S', 'gvillavieja@villavieja.info,apanza@villavieja.info,consultas@villavieja.info', 'BURELA 39', 'LANUS OESTE', '4225-3146', 'ANALIA/AGUSTINA/CRISTIAN FARIAS', 'TRANSPORTE', 'administracion@villavieja.info ', 'PROVINCIA'),
(454, '1496', 'SOGEFI FILTRATION ARGENTINA S.A.', '1496', 'RICARDO', 'S', 'antonio.amabile@sogefigroup.com ', 'CNEL. AGUILAR 3003', 'REMEDIOS DE ESCALADA ESTE', '4220-4600', 'ING.ESTRADA/ADRIANA', 'FILTROS', 'marianao@sogefi.com.ar ', 'LA CAJA'),
(455, '14987', 'MAPAROSA S.A.', '14987', 'MARTIN', 'S', 'maparosasa@gmail.com ', 'URUGUAY 246', 'AVELLANEDA', '4208-5961', 'DAVID-MARTIN', 'METALURGICA', 'Enviar dato faltante !! ', 'SMG'),
(456, '15011', 'ARCE CARLOS DANIEL', '15011', 'CARLOS', 'S', 'silviabtesta@gmail.com ', 'GUIDO 4780', 'MONTE CHINGOLO', '15-5103-5922/4230-2954', 'CARLOS', 'CONSTRUCCION', 'Enviar dato faltante !! ', 'PREVENCION'),
(457, '15024', 'PIENI (GUIALEGOR S.A.)', '15024', 'RICARDO', 'S', 'ricardo_goro@yahoo.com.ar,vero_ale@hotmail.com,anibal_ale@hotmail.com,pieni_pizza.libre@hotmail.com.ar', 'HIPOLITO IROGOYEN 8100', 'BANFIELD', '4242-2762', 'RICARDO-VERONICA// MARIA LAURA', 'PIZZERIA', 'Enviar dato faltante !! ', 'PREVENCION'),
(458, '15054', 'ARDISPLAC SRL', '15054', 'LUIS', 'S', 'contacto@ardisplac.com.ar ', '25 DE MAYO 1680', 'LANUS OESTE', '4262-8634', 'GARCIA CARLOS', 'METALURGICA', 'Enviar dato faltante !! ', 'MAPFRE'),
(459, '15072', 'CONSORCIO MINISTRO BRIN 2980', '15072', 'JORGE', 'S', 'ingjorgegomez@gmail.com ', 'MINISTRO BRIN 2980', 'LANUS OESTE', '4240-6249', 'GOMEZ SIRIMARCO', 'CONSORCIO', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(460, '15073', 'PAYO Y CIA. S.R.L.', '15073', 'DANIELA', 'S', 'daniela@payoycia.com.ar,ventas@payo.com.ar ', 'TRAFUL 3842', 'CAPITAL FEDERAL', '4911-0408/2546 INT 124', 'DANIELA-GUILLERMINA', 'COMERCIO', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(461, '15093', 'INTERGOM RODILLOS DE FRANCISCO NORBERTO DOS REIS', '15093', 'ELIZABETH', 'S', 'compras@intergom.net ', 'JEAN JAURES 1045', 'LANUS OESTE', '4218-4498/3436', 'ELIZABETH', 'REVESTIMIENTO EN CAUCHO', 'Enviar dato faltante !! ', 'LA SEGUNDA'),
(462, '15094', 'CASCOS ANITA', '15094', 'ANDREA', 'S', 'administracion@allmailing.info ', '3 DE FEBRERO 4060', 'REMEDIOS DE ESCALADA', '4267-0728/15-5588-3531', 'ANDREA DOS REIS', 'LOGISTICA', 'Enviar dato faltante !! ', 'LA SEGUNDA'),
(463, '15098', 'RAE TRANSMISIONES', '15098', 'LILIANA', 'S', 'raetransmisiones29@yahoo.com.ar ', 'CALLAO 2327', 'VALENTIN ALSINA', '4209-0626', 'LILIANA', 'REPARACION DE TRANSMICIONES', 'raetransmisiones29@yahoo.com.ar ', 'PROVINCIA'),
(464, '15106', 'SURMEN SA', '15106', 'PATRICIA', 'S', 'floridamens686@gmail.com.ar ', 'AV. MITRE 668', 'AVELLANEDA ESTE', '4201-4464/5044', 'PARICIA-Sr.MINKAS', 'INDUMENTARIA', 'Enviar dato faltante !! ', 'LA CAJA'),
(465, '15114', 'VICTOR SZULGACZ', '15114', 'VICTOR', 'S', 'sergiofleitas@yahoo.com.ar,victorszulgacz@hotmail.com', 'PAVON 1400', 'AVELLANEDA', '4209-7717', 'VICTOR -MARTIN', 'GIMNASIO', 'Enviar dato faltante !! ', 'QBE'),
(466, '15119', 'GARCIA LEONEL ANIBAL', '15119', 'VERONICA', 'S', 'administracion@fumigacionessanam.com.ar,lgarcia@fumigacionessanam.com.ar', 'DEL VALLE IBERLUCEA 3364 6A', 'LANUS OESTE', '3965-1180/4241-6400', 'GARCIA LEONEL/VERONICA DE ALESANDRE', 'COMERCIO', 'Enviar dato faltante !! ', 'PREVENCION'),
(467, '15139', 'ASSISTANCE CRISTAL S.A.', '15139', 'ADRIAN', 'S', 'assistance_cristal@hotmail.com ', 'PASCO 708', 'CAPITAL FEDERAL', '4942-8000', 'ADRIAN', 'CRISTALERIA', 'Enviar dato faltante !! ', 'QBE'),
(468, '15154', 'CONS. DEL VALLE IBERLUCEA 2913', '15154', 'CONS. DEL VALLE IBER', 'S', 'ingjorgegomez@gmail.com ', 'DEL VALLE IBERLUCEA 2913', 'LANUS OESTE', '4240-6249', 'JORGE SIRIMARCO', 'CONSORCIO', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(469, '15193', 'L.F.K. SRL', '15193', 'HERNAN', 'S', 'hjaneiro@lefkas.com.ar ', 'TUCUMAN 3333', 'LANUS ESTE', '4246-2090', 'HERNAN', 'MATERIALES ELECTRICOS', 'Enviar dato faltante !! ', 'GALENO'),
(470, '15243', 'CAMINOS (DE CRISTINA GUERRA)', '15243', 'CRISTINA', 'S', 'liccmguerra@hotmail.com,direccion@cetcaminos.com', 'MANUEL CASTRO 750', 'REMEDIOS DE ESCALADA', '4288-2873', 'CRISTINA-HUGO', 'CENTRO DE DIA', 'Enviar dato faltante !! ', 'GALENO'),
(471, '15251', 'GUSCOL S.A.', '15251', 'GUSTAVO', 'S', 'gustavo@aromashop.com.ar ', 'HIPOLITO IRIGOYEN 4549', 'LANUS OESTE', '4240-6375/1554524931', 'GUSTAVO', 'PERFUMERIA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(472, '15261', 'BONBAR S.R.L.', '15261', 'MICAELA.', 'S', 'bonbar.srl@hotmail.com,bonbar.srl@gmail.com ', 'PITAGORAS 5368', 'LANUS ESTE', '/4246-7092 (PART', 'MICAELA BAREIRO', 'SERVICIOS NAVALES Y TERRESTRES', 'Enviar dato faltante !! ', 'PREVENCION'),
(473, '15269', 'FABINSA S.R.L', '15269', 'ROBERTO', 'S', 'fabinsasrl@gmail.com ', 'AV RIVADAVIA 1562', 'LANUS OESTE', '15-4046-3626', 'ROBERTO', 'METALURGICA', 'Enviar dato faltante !! ', 'PREVENCION'),
(474, '15280', 'L.C. COMUNICACIÓN VISUAL S.R.L.', '15280', 'DINA', 'S', 'info@camilot.com.ar ', 'OLIDEN 2639', 'LANUS OESTE', '4209-9941', 'DINA-ENRIQUE', 'PUBLICIDAD', 'Enviar dato faltante !! ', 'LA CAJA'),
(475, '15298', 'CONS. DEL VALLE IBERLUCEA 2906', '15298', 'JORGE', 'S', 'Enviar dato faltante !! ', 'DEL VALLE IBERLUCEA 2906', 'LANUS OESTE', '15-57213113', 'DANIEL/ADMINISTRADOR', 'CONSORCIO', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(476, '15313', 'NINA SRL', '15313', 'ANA', 'S', 'hcaraccio@yahoo.com.ar ', 'FABIAN ONSARI 265', 'WILDE', '4249-0841', 'ANA HUMBERTO', 'VETERINARIA', 'Enviar dato faltante !! ', 'QBE'),
(477, '15317', 'COLEGIO JOSE MANUEL ESTRADA', '15317', 'KARINA', 'S', 'colejmestrada@yahoo.com.ar ', '2 DE MAYO 2932', 'LANUS OESTE', '4240-6323/ 4225-4367', 'KARINA', 'EDUCATIVA', 'Enviar dato faltante !! ', 'GALENO'),
(478, '15319', 'SCALISE CARLOS ALBERTO', '15319', 'carlos', 'S', 'NO MAIL ', 'CONDARCO 2719', 'MONTE CHINGOLO', '4289-1047', 'CARLOS', 'ALIMENTOS', 'Enviar dato faltante !! ', 'SMG'),
(479, '15348', 'RODRIGUEZ ALBERTO JACINTO', '15348', 'ALBERTO', 'S', 'beto32@hotmail.com ', 'BLANCO ENCALADA 2277 2DO B', 'CAPITAL FEDERAL', '4787-4785/15-5182-9215', 'ALBERTO', 'TRANSPORTE', 'Enviar dato faltante !! ', 'LA SEGUNDA'),
(480, '15352', 'HELLER NORTEÑA SA', '15352', 'TOMAS', 'S', 'recepcion@heller.com.ar ', 'VIAMONTE 4141', 'VALENTIN ALSINA', '4208-5278/4208-7796', 'TOMAS HELLER/ ESTELA', 'TEXTIL', 'Enviar dato faltante !! ', 'PREVENCION'),
(481, '15375', 'E.P. NRO 29', '15375', 'PATRICIA', 'S', 'gablanco2010@hotmail.com ', 'RAMOS 64', 'LANUS OESTE', '4241-2715', 'CLAUDIA-MONICA', 'EDUCATIVA', 'Enviar dato faltante !! ', 'PROVINCIA COLONIA SUIZA'),
(482, '15387', 'FARMACIA UOM AVELLANEDA', '15387', 'UOM', 'S', 'acomandatore@uomlanus.com.ar,pgonta@uomlanus.com.ar,jgoldberg@uomlanus.com.ar,tsciscivo@uomlanus.com.ar', 'AV HIPOLITO IRIGOYEN 386', 'AVELLANEDA', '4357-9058', 'ALEJANDRA', 'FARMACIA', 'administracion@uomlanus.com.ar ', 'QBE'),
(483, '15433', 'CENTRO LOGISTICO SUR S.R.L.', '15433', 'CERVECERIA', 'S', 'cris32_k@hotmail.com ', 'ALEM 1170', 'BANFIELD', '4242-5015', 'CRISTINA', 'DISTRIBUIDORA', 'Enviar dato faltante !! ', 'GALENO'),
(484, '15448', 'RODRIGUEZ Y PARISE SRL', '15448', 'CRISTIAN', 'S', 'ventas@lacteoscavanelas.com.ar,compras@lacteoscavanelas.com.ar,gestion@lacteoscavanelas.com.ar', 'TRES SARGENTOS 1626', 'AVELLANEDA', '4203-2852', 'NADIA-MARIA JOSE', 'MUZZARELLA', 'Enviar dato faltante !! ', 'GALENO'),
(485, '15481', 'CARRIVALE ANGEL', '15481', 'ANGEL', 'S', 'acarriva@litograf.com.ar,claudia@litograf.com.ar', 'GUIDO 3466', 'LANUS ESTE', '4246-2240/154-9354779', 'ANGEL', 'GRAFICA', 'Enviar dato faltante !! ', 'GALENO'),
(486, '1549', 'R.I.D.R.U. IND. Y COM. S.R.L.', '1549', 'JOSE', 'S', 'indridru@yahoo.com.ar ', '9 DE JULIO 2574', 'LANUS ESTE', '241-6523 - 247-8587', 'SRA. MARTA/LAURA', 'METALURGICA', 'Enviar dato faltante !! ', 'LA SEGUNDA'),
(487, '15492', 'X- URBAN SA', '15492', 'PAULA', 'S', 'paula@x-urban.com.ar,info@x-urban.com.ar,mario@x-urban.com.ar', 'BLANCO ESCALADA 3037', 'LANUS ESTE', '4230-0070', 'PAULA ZICARELLI/MARIO', 'METALURGICA TEXTIL', 'paula@x-urban.com.a ', 'ART INTERACCION SA'),
(488, '15507', 'CONSORCIO LLAVALLOL 33', '15507', 'JORGE', 'S', 'ingjorgegomez@gmail.com ', 'LLAVALLOL 33', 'LANUS OESTE', '4240-6249/4247-6332', 'GOMEZ SIRIMARCO', 'CONSORCIO', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(489, '15559', 'GARAVAGLIA ALEJANDRO', '15559', 'MARIANO', 'S', 'alejandro@agconsorcios.com.ar,edificios@agconsorcios.com.ar', '29 DE SEPTIEMBRE 1570', 'LANUS ESTE', '4241-4760', 'MARIANO', 'CONSORCIO', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(490, '15583', 'HERRERA VILLALAIN JUAN C Y HERRERA VILLALAIN LUIS', '15583', 'HERNANDO', 'S', 'admin@asfaltos-faa.com.ar,marysosa@asfaltos-faa.com.ar', 'BUERAS 3026', 'LANUS ESTE', '4246-1316', 'HERNANDO', 'FABRICA DE ASFALTOS', 'fluna@asfaltos-faa.com.ar ', 'FEDERACION PATRONAL'),
(491, '15586', 'DE VINCENTI EMILIANO', '15586', 'EMILIANO', 'S', 'supervol2871@yahoo.com.ar ', 'AV REMEDIOS DE ESCALADA 2871', 'VALENTIN ALSINA', '4209-7824 4368-6660', 'ANDREA', 'METALURGICA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(492, '1561', 'TARSA LINEA 100 (PASAJEROS)', '1561', 'RIVAS', 'S', 'tarsa_monsi@yahoo.com.ar,rivas-tarsa@hotmail.com.ar', 'PICHINCHA 1765', 'CAPITAL FEDERAL', '4308-0168', 'SOLO ACC.PASAJEROS', 'TRANSPORTE DE PASAJEROS', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(493, '15615', 'TERMOFORM SA', '15615', 'CECILIA', 'S', 'termoform@speedy.com.ar ', 'WARNES 2431', 'LANUS OESTE', '4262-1079/9035', 'CECILIA-MARCELA', 'TEXTIL', 'Enviar dato faltante !! ', 'SMG'),
(494, '15620', 'TRASHVILLE SA', '15620', 'VANINA', 'S', 'administracion@mar-vic.com.ar ', 'EL PARTENON 1318', 'MONTE GRANDE', '4228-8441/4228-8898', 'VANINA', 'FABRICA DE CABLES', 'Enviar dato faltante !! ', 'QBE'),
(495, '15661', 'LASSTER SRL', '15661', 'VANESA', 'S', 'info@lasster.com.ar,nadia@lasster.com.ar,vanesa@lasster.com.ar', 'O`GORMAN 3173', 'POMPEYA', '4919-9334', 'Enviar dato faltante !!', 'PAPELERA', 'Enviar dato faltante !! ', 'PREVENCION'),
(496, '1567', 'SUPRA S.R.L.', '1567', 'MARCELOS', 'S', 'suprasrl@speedy.com.ar ', 'LAVALLOL 1555 (FRENTE AL PORTON)', 'LANUS OESTE', '262-7073/7107', 'GERENTE STUR GERARDO', 'REFINACION DE A', 'Enviar dato faltante !! ', 'GALENO'),
(497, '15676', 'RUSSO GISELA CARINA', '15676', 'GISELLA', 'S', 'NO MAIL ', 'CAVOUR 3030', 'LANUS OESTE', '4241-1955', 'LUIS', 'COTILLON', 'Enviar dato faltante !! ', 'MAPFRE'),
(498, '15685', 'CONSORCIO RIOBAMBA 125', '15685', 'CONSORCIO RIOBAMBA 1', 'N', 'sisenlinea@datamarkets.com.ar ', 'RIOBAMBA 125', 'LANUS OESTE', '4241-0369', 'CAMPASSI EDUARDO', 'CONSORCIO', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(499, '15697', 'CORVALAN GUSTAVO JAVIER Y CORVALAN HERNAN', '15697', 'GUSTAVO', 'S', 'hrcorvalan@hotmail.es ', 'BELTRAN 114', 'REMEDIOS DE ESCALADA', '4242-3754', 'HERNAN CORVALAN', 'ALIMENTOS', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(500, '15743', 'SPEED CUSTOMS SRL', '15743', 'HORACIO', 'S', 'h.beltransimo@nhrecursoshumanos.com.ar ', 'ESMERALDA 155 2DTO 11', 'CAPITAL FEDERAL', '4328-8100/5761', 'HORACIO BELTRANSIMO', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'LA CAJA'),
(501, '1578', 'MANUEL LITWAK', '1578', 'BEATRIZ', 'S', 'admin@leonardifoulard.com.ar ', 'PASTEUR 525', 'CAPITAL FEDERAL', '4952-8702 4951-9068', 'LITO', 'TEXTIL', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(502, '1579', 'PEPI INC. S.A.', '1579', 'BEATRIZ', 'S', 'admin@leonardifoulard.com.ar ', 'PASTEUR 439', 'CAPITAL FEDERAL', '4952-9105/4953-5867', 'SUSANA', 'COMERCIO', 'Enviar dato faltante !! ', 'MAPFRE ARGENTINA SEG VIDA SA'),
(503, '15804', 'AMATO OSCAR DARIO', '15804', 'ANALIA', 'S', 'contacto@transporteamato.com.ar,carlos@transporteamato.com.ar', 'FERRE 1945 DPTO 1', 'LANUS ESTE', '3972-7313', 'ANALIA (TRANSP. DANIEL AMATO)', 'TRANSPORTE', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(504, '15847', 'SIMEX INTERNACIONAL SA', '15847', 'CECILIA', 'S', 'cecilia@simex.com.ar,paula@simex.com.ar,luis@simex.com.ar,asesorcontable@speedy.com.ar', 'SAN MARTIN 536 4TO PISO', 'CAPITAL FEDERAL', '4310-7212', 'CECILIA/PAULA', 'DESPACHANTE DE ADUANA', 'cecilia@simex.com.ar,paula@simex.com.ar ', 'PREVENCION'),
(505, '15866', 'ENVASES ZENITH SRL', '15866', 'RODRIGO', 'S', 'aconcaguasrl@hotmail.com ', 'ITAPIRU 2537', 'LANUS OESTE', '4209-5229', 'RODRIGO- GUSTAVO', 'DISTRIBUIDORA', 'Enviar dato faltante !! ', 'LA CAJA'),
(506, '15874', 'AGRO COMBUSTIBLE SRL', '15874', 'LUCAS', 'S', 'aspro_bubi@hotmail.com ', 'MITRE 2099', 'BERAZATEGUI', '4216-9223', 'SAMANTHA', 'EST. DE SERV.', 'Enviar dato faltante !! ', 'QBE'),
(507, '15877', 'METALURGICA FERNANDEZ SRL', '15877', 'FERMIN', 'S', 'info@metalfernandez.com.ar ', 'MAXIMO PAZ 1452', 'LANUS OESTE', '4262-7281', 'FERMIN VILLARROEL', 'METALURGICA', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(508, '1591', 'I.M.E.I. S.R.L.', '1591', 'ALEJANDRO', 'S', 'admi@modernschool.edu.ar,ruggerinatalia@yahoo.com.ar,torresrubenr@yahoo.com.ar', 'AV.H.IRIGOYEN 5302', 'LANUS OESTE', '4240-5054', 'AREA ALE/DORA INT129', 'DOCENCIA', 'Enviar dato faltante !! ', 'GALENO'),
(509, '15910', 'ROBERTO MARTINEZ ACCESORIOS SA', '15910', 'KARINA', 'S', 'karina@rmaccesorios.com.ar,administracion@rmaccesorios.com.ar', 'SARMIENTO 574', 'LANUS ESTE', '4249-4000', 'MARCELA GONZALEZ RRHH/KARINA', 'ACCESORIOS', 'proveedores@rmaccesorios.com.ar ', 'GALENO'),
(510, '15922', 'ROSELLI MIGUEL', '15922', 'MARISA', 'S', 'dangraf@speedy.com.ar ', 'CNO GRAL BELGRANO 831', 'LANUS E', '4204-0245', 'MIGUEL ROSELI', 'GRAFICA SERIGR.', 'Enviar dato faltante !! ', 'GALENO'),
(511, '1594', 'TRANSPORTE ROBERTO', '1594', 'ROBERTO', 'S', 'raftransporte@yahoo.com.ar ', 'CASTELLI 2049', 'LOMAS DE ZAMORA OESTE', '4282-8222', 'LUISA', 'TRANSPORTE', 'Enviar dato faltante !! ', 'QBE'),
(512, '1596', 'COPISOL S.R.L.', '1596', 'ALEJANDRO', 'S', 'copisol@copisolsrl.com.ar ', 'QUIRNO COSTA 2053', 'REMEDIOS DE ESCALADA OESTE', '4267-5053/5060', 'CAMILO CANAL', 'PAPELERA', 'Enviar dato faltante !! ', 'GALENO'),
(513, '15966', 'CARPEL FIBRA SRL', '15966', 'ANDREA', 'S', 'savamilau@hotmail.com ', 'MENDOZA 4329 Y MAGALLANES', 'LANUS OESTE', '42863644//4241-9645(conta', 'ANDREA RIVERA', 'PAPELERA', 'savamilau@hotmail.com ', 'GALENO'),
(514, '16022', 'LINEA M SA', '16022', 'RODOLFO', 'S', 'administracion@linea-m.com.ar ', 'AV SEGUI 1275', 'LAVALLOL', '4214-6364', 'DIEGO', 'METALURGICA', 'flavia@linea-m.com.ar ', 'PREVENCION'),
(515, '16025', 'BUSTAMANTE 2275 SA', '16025', 'PABLO', 'S', 'pperegal@casasurhotel.com,administracion@casasurhotel.com', 'AV QUINTANA 337 3ER PISO', 'CAPITAL FEDERAL', '4515-0085/4807-4848 int 1', 'NICOLAS/GUIDO', 'HOTEL', 'administracion@casasurhotel.com ', 'QBE'),
(516, '1610', 'JORGE ALBERTO GIACCHINO', '1610', 'JORGE', 'S', 'lefontaine@fibertel.com.ar ', '25 DE MAYO 677', 'LANUS OESTE', '241-9018', 'SR. JORGE', 'PANADERIA', 'Enviar dato faltante !! ', 'PREVENCION'),
(517, '16158', 'MOULOUHI MIGUEL', '16158', 'MIGUEL', 'S', 'dra.miriamalonso@yahoo.com.ar ', 'LAS PIEDRAS 2131', 'LANUS ESTE', '4247-0752', 'MIRIAM', 'LIMPIEZA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(518, '16173', 'TRANSPORTE HACHA DE PIEDRA SRL', '16173', 'ROBERTO', 'S', 'hdpbsas@hotmail.com.ar ', 'AMANCIO ALCORTA 3207', 'CAPITAL FEDERAL', '4912-0471/1569626442', 'ROBERTO ALONSO', 'TRANSPORTE', 'Enviar dato faltante !! ', 'NO POSEE ART'),
(519, '16184', 'AYALA ROBERTO ADRIAN', '16184', 'MARTIN', 'S', 'calzadosrave@hotmail.com ', 'MURGIONDO 2431', 'VALENTIN ALSINA', '4209-3216', 'MARTIN COCCIOLI- ALICIA', 'CALZADO', 'calzadosrave@hotmail.com ', 'PROVINCIA'),
(520, '16198', 'PRE PRE SRL', '16198', 'MAXIMILIANO', 'S', 'maximilianopellegrino@preimpregnados.com.ar ', 'FOURNIER 764 EX 394', 'QUILMES', '4115-0645/0606', 'MARIANO/ MAXI', 'PLASTICO', 'Enviar dato faltante !! ', 'QBE'),
(521, '16222', 'MIERIÑO RODRIGUEZ SARA', '16222', 'SARA', 'S', 'sanfacundo4@hotmail.com ', '9 DE JULIO 1847', 'LANUS ESTE', '4225-1768 4241-4716', 'GUSTAVO /15-5247-8462', 'PIZZERIA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(522, '1624', 'SUAREZ TORRICO FLORENCIO', '1624', 'FLORENCIO', 'S', 'stconstrucciones@yahoo.com.ar,info@stconstrucciones.com.ar', 'LAVALLOL 66 PB', 'LANUS OESTE', '4247-0346', 'Enviar dato faltante !!', 'CONSTRUCCION', 'info@stcosntrucciones.com.ar ', 'MAPFRE'),
(523, '16240', 'CONS. DE PROPIETARIOS GRAL ARIAS 1488/92', '16240', 'VANESA', 'S', 'mrech_adm@yahoo.com.ar ', 'GRAL ARIAS 1488', 'LANUS ESTE', '3528-4932/1555153977', 'MARISA REACH', 'CONSORCIO', 'Enviar dato faltante !! ', 'QBE'),
(524, '16244', 'BAIRES IMPORT SA', '16244', 'CRISTIAN', 'S', 'triper@opcionestelmex.com.ar,admtotal2011@yahoo.com.ar,estudio@bustosmurilloyasoc.com.ar', 'MARTINEZ ROSAS 999', 'CAPITAL FEDERAL', '4289-3780/1', 'CRISTIAN///RODOLFO', 'TRANSPORTE', 'Enviar dato faltante !! ', 'SMG'),
(525, '16259', 'LUZ DE LUNA SRL', '16259', 'SILVINA', 'S', 'susyarena@yahoo.com.ar ', '2 DE MAYO 2819', 'LANUS OESTE', '4225-8711', 'SILVINA', 'COLEGIO', 'Enviar dato faltante !! ', 'PROVINCIA'),
(526, '16270', 'ACYMAT S.A.', '16270', 'NESTOR', 'S', 'nestorscalise@gmail.com ', 'DR MELO 4015', 'REMEDIOS DE ESCALADA', '4241-8282', 'CARLA,NESTOR(TITULAR)', 'SEGURIDAD', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(527, '16286', 'CALIBRA SUR S.A', '16286', 'SANDRA', 'S', 'monicaraes@hotmail.com,info@raes.com.ar ', 'MARCO AVELLANEDA 1065', 'REMEDIOS DE ESCALADA', '4248-1508', '', 'METALURGICA', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(528, '16321', 'QUAMAQ SRL', '16321', 'MATIAS', 'S', 'administracion@quamaq.com.ar,matiasdelgado@quamaq.com.ar', 'MENDOZA 4026', 'LANUS OESTE', '4115-3785/90', 'MATIAS DELGADO PRUDA', 'FAB.MAQUINAS PARA ENVASADOS', 'Enviar dato faltante !! ', 'GALENO'),
(529, '16323', 'BOLAÑO CARLOS JACINTO', '16323', 'CARLOS', 'S', 'NO MAIL ', 'PLUMERILLO 1951', 'LOMAS DE ZAMORA', '4286-1403', 'CARLOS', 'METALURGICA', 'Enviar dato faltante !! ', 'QBE'),
(530, '1633', 'ARYES', '1633', 'ROXANA', 'S', 'info@aryesargentina.com.ar ', 'BERUTI 1853', 'BANFIELD OESTE', '4242-6837/4248-9628', 'ROXANA-SR SPIRAKIS', 'INSTALACIONES', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(531, '16339', 'ROLANPLAST', '16339', 'PAULA', 'S', 'info@rolanplast.com.ar,rrhh@rolanplast.com.ar,omontero@rolanplast.com.ar,svalenzuela@rolanplast.com.ar', 'MARIO BRAVO 959', 'AVELLANEDA', '4208-0376/4208-6384', 'PAULA- OMAR', 'PLASTICO', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(532, '16341', 'CROMAQUIM SRL', '16341', 'HORACIO', 'S', 'admcontable@cromaquimsrl.com.ar ', 'REPUBLICA ARGENTINA 2815', 'VALENTIN ALSINA', '4228-5706', 'HORACIO', 'LABORATORIO', 'Enviar dato faltante !! ', 'QBE'),
(533, '16373', 'SEBALEN SRL', '16373', 'SILVINA', 'S', 'silvina@sebalen.com.ar,dparada@sebalen.com.ar,silvina.sebalen@gmail.com', 'MARIO BRAVO 972/78', 'AVELLANEDA', '4209-5962', 'FERNANDO- SILVINA', 'DISTRIBUIDORA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(534, '16395', 'MAPFRE ARGENTINA SEGUROS DE VIDA SA', '16395', 'MAPFREVIDA', 'S', 'bidalov@mapfre.com.ar,veromm@mapfre.com.ar,tramitaciondesiniestros@mapfre.com.ar', 'LAVALLE 348 PB', 'CIUDAD AUTONOMA DE BUENOS AIRE', '4320-6700 IT 3132', 'JORGELINA AFILIACION', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'MAPFRE ARGENTINA SEG VIDA SA'),
(535, '1640', 'TECNOENVASE S.A.', '1640', 'MARIO', 'S', 'tecnoenvase@house.com.ar,proveedores@tecnoenvase.com.ar', 'GABRIELA MISTRAL 140', 'BANFIELD OESTE', '4248-3784 4242-8889', 'MARIO', 'PLASTICOS', 'proveedores@tecnonenvase.com.ar ', 'PROVINCIA'),
(536, '16402', 'INSTALANUS SA', '16402', 'ROMINA', 'S', 'jorge_arias401@hotmail.com,romi_listo@hotmail.com', 'HIPOLITO IRIGOYEN 4981', 'LANUS OESTE', '4240-8611', 'SR. JORGE ARIAS/ROMINA', 'INSTALACION DE CABLE', 'Enviar dato faltante !! ', 'GALENO'),
(537, '16411', 'VICSIL ( ULFE SILVIA CRISTINA)', '16411', 'SILVIA', 'S', 's.c.u.silvia@hotmail.com.ar ', 'ITAPIRU 481', 'VALENTIN ALSINA', '4368-7028/4262-1852', 'SILVIA', 'CALZADO', 'Enviar dato faltante !! ', 'SMG'),
(538, '16435', 'LA ALDEA ARTES DISEÑO Y DECORACION SRL', '16435', 'BETINA', 'S', 'betinabal@gmail.com ', 'ANATOLE FRANCE 1958', 'LANUS ESTE', '4249-0615', 'BETINA', 'CARPINTERIA', 'Enviar dato faltante !! ', 'SMG'),
(539, '16469', 'CRUPO MATERIALES ELECTRICOS SA', '16469', 'DIEGO', 'S', 'cruposa@hotmail.com ', 'MURGUIONDO 3373', 'LANUS OESTE', '4228-8935', 'DIEGO/ NORBERTO', 'ELECTROMECANICA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(540, '16476', 'AMERICAN BOX S.A.', '16476', 'ABOX', 'S', 'celia@abox.com.ar,info@abox.com.ar ', 'CRESPO 3049', 'CAPITAL FEDERAL', '4918-2562', 'CELIA/VANINA', 'ENVASE', 'Enviar dato faltante !! ', 'QBE'),
(541, '16510', 'CIPRIJ SA', '16510', 'MIRTA', 'S', 'cetciprijsa@gmail.com,cetciprijenfe@gmail.com ', 'AYACUCHO 1151', 'LANUS ESTE', '4247-1207/ 2070-5194', 'MIRTA- ROMINA', 'COMUNIDAD TERAPEUTICA', 'Enviar dato faltante !! ', 'SMG'),
(542, '16514', 'ECAMSA SRL', '16514', 'PAULA', 'S', 'gerencia@ecamsa.com.ar ', 'PERGAMINO 778', 'LANUS ESTE', '4240-8215', 'PAULA', 'FERRETERIA INDUSTRIAL', 'gerencia@ecamsa.com.ar ', 'QBE'),
(543, '16526', 'BACKEN FOODS SA', '16526', 'LEONARDO', 'S', 'cfernandez@backen.com.ar ', 'CARLOS TEJEDOR 868', 'LANUS OESTE', '4249-0661/1124', 'LEONARDO', 'PASTELERIA', 'proveedores@backen.com.ar ', 'LA CAJA'),
(544, '16529', 'ARQUETIPO PRODUCCIONES SRL', '16529', 'JUAN PABLO', 'S', 'info@arquetipoweb.com.ar,sebastianmorandi@arquetipoweb.com.ar', 'AZOPARDO 68', 'REMEDIOS DE ESCALADA OESTE', '4202-2186', 'SEBASTIAN MORANDI TITULAR/ JUAN PABLO', 'CARPINTERIA', 'Enviar dato faltante !! ', 'PREVENCION'),
(545, '16534', 'KRASNOBRODA ANGEL', '16534', 'CARLOS', 'S', 'ckrasno@hotmail.com ', 'LORIA 367', 'LOMAS DE ZAMORA', '4245-0065/ 4292-3993', 'CARLOS', 'TURISMO', 'Enviar dato faltante !! ', 'PREVENCION'),
(546, '16545', 'ALVAREZ CARBALLAL DIEGO', '16545', 'FABIAN', 'S', 'info@liccoons.com.ar ', 'AV HIPOLITO YRIGOYEN 7750', 'LOMAS DE ZAMORA', '4202-5400', 'FABIAN', 'COMERCIO', 'Enviar dato faltante !! ', 'PREVENCION'),
(547, '16551', '4 PL S.A.', '16551', 'MIRIAM', 'S', '4plsa@speedy.com.ar ', 'MANUEL ESTEVEZ 1471', 'DOCK SUD', '4222-7272 / 4201-3898', 'MARCELO/MIRIAM ALVAREZ', 'TRANSPORTE', '4plsa@speedy.com.ar,alicia_alfonso@speedy.com.ar', 'LA CAJA'),
(548, '16552', 'COLFAR S.A.', '16552', 'GISELA', 'S', 'administracion@colfarargentina.com.ar,compras@colfarargentina.com.ar', 'EL PARTENON 1192', '9 DE ABRIL', '4693-1768 INT. 106 /119 H', 'SILVIA JEHLE///GISELLE/ HUMBERTO', 'FAB. ESPUMA DE POLIURETANO', 'pagoaproveedores@colfarargentina.com.ar ', 'GALENO'),
(549, '16623', 'ANCALPHIA SA', '16623', 'ALDANA', 'S', 'ancalphia@speedy.com.ar ', 'SPEGAZZINI 3446', 'REMEDIOS DE ESCALADA', '4276-0161/4286-2869', 'ALDANA-ZULEMA', 'PLASTICO', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL ACC PERSONALES'),
(550, '16630', 'FIORELLA DEPORTES S.R.L', '16630', 'SILVANA', 'S', 'silvanavenezia@yahoo.com.ar,fiorellapersonal@yahoo.com.ar', 'AV JUAN D PERON 2758', 'VALENTIN ALSINA', '4228-6733', 'SILVANA VENEZIA/ BARBARA VALERIO', 'CASA DE ART.DEPORTIVOS', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(551, '16678', 'PROMO FIESTA AVELLANEDA SRL', '16678', 'ALEJANDRA', 'S', 'promofiestaavellaneda2000@yahoo.com.ar ', 'AVDA MITRE 2504', 'SARANDI', '4204-7080/ 4205-1585', 'ALEJANDRA (ENCARGADA)/ GABRIEL', 'VENTA DE PROD. CONGELADOS', 'Enviar dato faltante !! ', 'SMG'),
(552, '16689', 'SOCPES SRL', '16689', 'SILVIO', 'S', 'info@casapesqueira.com.ar ', 'AV CORDOBA 3458', 'CAPITAL FEDERAL', '4862-5656', 'SILVIO PESQUEIRA', 'GOMERIA', 'Enviar dato faltante !! ', 'MAPFRE'),
(553, '16691', 'ARBIT DANIEL ALBERTO', '16691', 'DANIEL', 'S', 'darbit@daequipamientos.com.ar ', 'VIRREY LINIERS 641', 'CAPITAL FEDERAL', '4931-8395/4932-3797', 'DANIEL ARBIT/SUSANA', 'EQUIPAMIENTOS PARA OFICINA', 'facturacion@daequipamientos.com.ar ', 'LA HOLANDO'),
(554, '16719', 'DESIMONE SERGIO F Y PASZCZUK CHRISTIAN A. SH', '16719', 'CHRISTIAN', 'S', 'fundicion_argentina@yahoo.com.ar ', 'CAMINO GRAL BELGRANO km10.5 calle 1 galp', 'BERNAL', '1561571869/1559918970', 'CHRISTIAN', 'FUNDICION', 'Enviar dato faltante !! ', 'SMG'),
(555, '1672', 'DISTRINAR S.A.', '1672', 'ADRIANA', 'S', 'nel_@distrinarsa.com.ar ', 'PSJE. LAUTARO 3841', 'VILLA DOMINICO', '4289-1339/0381', 'ING.SIMONE/NELIDA', 'METALURGICA', 'nel_@distrinarsa.com.ar ', 'PROVINCIA'),
(556, '1673', 'ELECTROMECANICA CONELEC', '1673', 'MARIANA', 'S', 'administracion@conelec.com.ar ', 'E. FERNANDEZ 257', 'AVELLANEDA OESTE', '4201-9698', 'MARIANA', '', 'vivianaledesma@conelec.com.ar ', 'GALENO'),
(557, '16792', 'CONSORCIO DR MELO 2860', '16792', 'MARISA', 'S', 'cristinafvera@yahoo.com.ar ', 'DR MELO 2860', 'LANUS OESTE', '4241-1881', 'CRISTINA', 'CONSORCIO', 'Enviar dato faltante !! ', 'MAPFRE'),
(558, '16819', 'LEATHER GROUP ARGENTINA SA', '16819', 'CLAUDIA', 'S', 'info@stone-shoes.com.ar ', '9 DE JULIO 1448', 'LANUS ESTE', '4228-9834//4228-9835', 'CLAUDIA', 'CALZADO', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(559, '16839', 'ATENIESE MARIA ANTONIA', '16839', 'DANIEL', 'S', 'casavilchez@speedy.com.ar,vilelectro@speedy.com.ar', 'GOBERNADOR IRIGOYEN 571', 'LANUS OESTE', '4249-5858/ 1556357715', 'SERGIO/DANIEL', 'VENTA DE PROD. ELECT', 'Enviar dato faltante !! ', 'LA CAJA'),
(560, '16859', 'LADDEN SA', '16859', 'PABLO', 'S', 'alvarez_hnos@hotmail.com,f.astein@jamonescumbrenios.com.ar', 'ONCATIVO 271', 'LANUS ESTE', '5290-7130 /7131', 'PABLO ALVAREZ', 'FRIGORIFICO', 'Enviar dato faltante !! ', 'LA CAJA'),
(561, '16877', 'IMISA S.A', '16877', 'SOLEDAD', 'S', 'sbrichetto@imisasa.com.ar,wbrichetto@imisasa.com.ar', 'BUERAS 3005', 'LANUS ESTE', '4246-4080/9224', 'SOLEDAD/MARTIN/WALTER', 'METALURGICA', 'sbrichetto@imisasa.com.ar ', 'PREVENCION'),
(562, '16880', 'HERRAMIENTAS CHAYTO S.R.L', '16880', 'BEATRIZ', 'S', 'b.feu@chayto.com.ar,info@chayto.com.ar ', 'JUAN FARREL 153', 'VALENTIN ALSINA', '4208-8457/ 4228-5675', 'BEATRIZ', 'METALURGICA', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(563, '16884', 'CRE-VAN EQUIPAMIENTOS SRL', '16884', 'PABLO', 'S', 'crevanpvc@hotmail.com,info@crevant.com.ar ', 'MENDOZA 2449', 'LANUS OESTE', '4262-4760', 'PABLO', 'CARPINTERIA', 'Enviar dato faltante !! ', 'GALENO'),
(564, '16910', 'NUÑEZ HECTOR OSVALDO Y NUÑEZ WALTER CESAR', '16910', 'WALTER', 'S', 'walnun@fibertel.com.ar,honz@speedy.com.ar ', 'ESTADOS UNIDOS 3100', 'LANUS OESTE', '4262-1688', 'WALTER NUÑEZ', 'RESTAURANT', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(565, '16933', 'DISTRIBUCION DE AVILA SRL', '16933', 'EZEQUIEL', 'S', 'eze_colo2@hotmail.com,distribuidoradeavilasrl@hotmail.com', 'EVA PERON 3031', 'LANUS ESTE', '4289-4027', 'EZEQUIEL', 'DISTRIBUIDORA', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(566, '16935', 'TELTRON BAIRES SA', '16935', 'VERONICA', 'S', 'v.glauser@teltron.com.ar,verodaru0805@hotmail.com', 'AV HIPOLITO YRIGOYEN 4111', 'LANUS OESTE', '4249-4136', 'VERONICA', 'FABRICA DE CD', 'hugo.sanchez@teltron.com.ar ', 'GALENO'),
(567, '16936', 'TELTRON SA', '16936', 'VERONICA', 'S', 'v.glauser@teltron.com.ar,verudaru0805@hotmail.com', 'AV HIPOLITO YRIGOYEN 4111', 'LANUS OESTE', '4249-4136', 'VERONICA', 'FABRICA DE CD', 'hugo.sanchez@teltron.com.ar ', 'MAPFRE'),
(568, '16993', 'TOSCANO VICENTE', '16993', 'VICENTE', 'S', 'tosqui_v@hotmail.com ', 'ROMA 2569', 'REMEDIOS DE ESCALADA', '4202-1821', 'VICENTE', 'FABRICA DE CARBONES', 'Enviar dato faltante !! ', 'PROVINCIA'),
(569, '17006', 'MUEBLES BOUTIQUE SRL', '17006', 'JUAN IGNACIO', 'S', 'info@epecuenmuebles.com.ar ', 'CORONEL BRANDSEN 3569', 'AVELLANEDA ESTE', '4115-9002/1565686037', 'JUAN IGNACIO 1562479999', 'MUEBLERIA', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(570, '1703', 'FARMACIA UOM MODERNA S.C.S.', '1703', 'UOM', 'S', 'acomandatore@uomlanus.com.ar,jgoldberg@uomlanus.com.ar,pgonta@uomlanus.com.ar,tsciscivo@uomlanus.com.ar', '9 DE JULIO 1352', 'LANUS ESTE', '4241-8243/7769', 'LUCY/NESTOR', 'FARMACIA', 'administracion@uomlanus.com.ar,jmontes@uomlanus.com.ar', 'QBE'),
(571, '17039', 'LOPATA MARIANO GABRIEL', '17039', 'MARIANO', 'S', 'mglopata@gmail.com ', '29 DE SEPTIEMBRE 1885', 'LANUS ESTE', '4225-2676/ 4245-9526 (LOM', 'MARIANO', 'SANTERIA', 'Enviar dato faltante !! ', 'QBE'),
(572, '17052', 'TTB S.A', '17052', 'OLGA', 'S', 'ttbsa@speedy.com.ar ', 'ALMAFUERTE 473/83', 'AVELLANEDA', '4201-8597/4229-8238', 'OLGA-FABIAN', 'MATRICERIA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(573, '17054', 'INDUSTRIA DERLUZ SRL', '17054', 'ALBERTO', 'S', 'cablesderluz@hotmail.com ', 'MANUEL CASTRO 4962', 'LANUS OESTE', '4208-8924', 'ALBERTO-LIDIA-DIEGO', 'FABRICA DE CABLES', 'Enviar dato faltante !! ', 'QBE'),
(574, '17069', 'DISTRISUMA SRL', '17069', 'WALTER', 'S', 'walterkozel@hotmail.com,distriteo@hotmail.com ', 'SALTA 151', 'TEMPERLEY', '4264-5847// 1562958370', 'WALTER KOZEL', 'COMERCIO', 'Enviar dato faltante !! ', 'QBE'),
(575, '17116', 'EY WAIS SRL', '17116', 'ELIZABETH', 'S', 'ey_wais@hotmail.com ', 'COLON 1061', 'REMEDIOS DE ESCALADA', '4262-6572/ 1558365467', 'ELIZABETH', 'CENTRO DE DIA', 'Enviar dato faltante !! ', 'NO POSEE ART'),
(576, '17128', 'BOTRUGNO LEONARDO LUIS ANTONIO', '17128', 'GUILLERMO', 'S', 'guillermo@adraelectronica.com.ar,administracion@adraelectronica.com.ar', 'ACHAVAL 4354', 'LANUS ESTE', '4220-4001/4206', 'GUILLERMO/ CAROLINA', 'ELECTRONICA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(577, '17133', 'ESCUELA NRO 5', '17133', 'FATIMA', 'S', 'marcelayfatima@yahoo.com.ar ', 'RIVADAVIA 1847', 'LANUS OESTE', '4228-4301', 'FATIMA-CLAUDIA', 'EDUCATIVA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(578, '17162', 'BASHOES S.A.', '17162', 'DONANFER', 'S', 'falmeyra@heyday.com.ar,jbalekian@heyday.com.ar', 'YATAY 845', 'VALENTIN ALSINA', '4208-9688 /4208-0100', 'ALEJANDRO-JAQUELINE', 'CALZADOS', 'Enviar dato faltante !! ', 'GALENO'),
(579, '1718', 'LOPEZ ALBERTO', '1718', 'ANALIA', 'S', 'falavital@hotmail.com ', 'CAMINO GRAL. BELGRANO 1587', 'LANUS ESTE', '204-8119', 'SR. LOPEZ', 'EST. DE SERV.', 'shell230@live.com ', 'GALENO'),
(580, '17187', 'INST. GRUTA NTRA SRA DE LOURDES(DIOCS DE AVELL/LAN', '17187', 'PAULA', 'S', 'paulalenticchia@yahoo.com.ar ', 'GUIDO SPANO 1038', 'LANUS OESTE', '4241-9257', 'MARTA PELLEGRINI', 'EDUCATIVA', 'paulalenticchia@yahoo.com.ar ', 'GALENO'),
(581, '17197', 'SASEM LOGICS SRL', '17197', 'CLAUDIA', 'S', 'proveedoresfis@yahoo.com.ar ', 'GUIDO 2136', 'LANUS ESTE', 'Enviar dato faltante !!', 'Enviar dato faltante !!', 'DISTRIBUIDORA', 'Enviar dato faltante !! ', 'LA CAJA'),
(582, '17211', 'AUTOSERVICIO SCHAUMI SA', '17211', 'GABRIELA', 'S', 'schaumi-sa@hotmail.com ', 'UCRANIA 2385', 'VALENTIN ALSINA', '4208-3227', 'GRACIELA', 'AUTOSERVICIO', 'Enviar dato faltante !! ', 'LA CAJA'),
(583, '17226', 'DOMINGUEZ NORMA BEATRIZ', '17226', 'NORMA', 'S', 'tubospola@hotmail.com ', 'EST. ECHEVERRIA 2040', 'WILDE', '4220-0913/1698', 'HECTOR BONOVIAS / NORMA DOMINGUEZ', 'ELAB.TUBOS DE CARTON', 'Enviar dato faltante !! ', 'LA CAJA'),
(584, '17247', 'EP 12', '17247', 'GRACIELA', 'S', 'graciesi@yahoo.com.ar ', 'R.DE ESCALADA DE SAN MARTIN 1480', 'LANUS OESTE', '4208-3223/1560258811', 'EDUCATIVA', 'EDUCATIVA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(585, '1729', 'CARPAS EL TREBOL (PIOMBO NATALIA VERONICA)', '1729', 'VERONICA', 'S', 'eltrebol@sion.com,info@carpaseltrebol.com.ar ', 'CORDOBA 1550', 'LANUS ESTE', '241-0356', 'VERONICA', 'EVENTOS', 'Enviar dato faltante !! ', 'SMG'),
(586, '17322', 'DE SANTO SRL', '17322', 'NATALIA', 'S', 'desantosrl@speedy.com.ar ', 'PAMPA 1012', 'VALENTIN ALSINA', '4208-0810/4208-0280', 'NATALIA PAGGANO', 'DISTRIBUIDORA', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(587, '17324', 'TOP SERVICE ELECTRODOMESTICOS SA', '17324', 'IGNACIO', 'S', 'r.leva@topservicesa.com.ar ', 'BASAVILBASO 1597', 'LANUS ESTE', '4812-2285/4249-0955', 'ARIEL/IGNACIO', 'SERVICE ELCTRO', 'martineznet@ciudad.com.ar ', 'ART INTERACCION SA'),
(588, '17328', 'GODOY JUAN CARLOS', '17328', 'JUAN CARLOS', 'S', 'silviabtesta@gmail.com ', 'PRINGLES 4852', 'LANUS ESTE', '15-51376828', 'GODOY JUAN CARLOS', 'PERFORACIONES', 'silviabtesta@gmail.com ', 'GALENO'),
(589, '17369', 'MONTAÑA HERMANOS CONDUCTORES ELECT. SRL', '17369', 'CECILIA', 'S', 'neutroluz@fibertel.com.ar ', 'AV.CASTRO BARROS 980 7 PISO DTO A', 'CAPITAL FEDERAL', '42402550', 'CECILIA/RUBEN MONTAÑA', 'FAB. DE CONDUCTORES ELECT', 'Enviar dato faltante !! ', 'PREVENCION'),
(590, '17376', 'INDUSTRIAS MECPOL S.A.', '17376', 'LIDIA', 'S', 'info@mecpol.com ', 'CHOELE CHOEL 1752', 'AVELLANEDA OESTE', '4208-3578/4209-4884', 'C.SCARRONE- VIVIANA', 'METALURGICA', 'Enviar dato faltante !! ', 'GALENO'),
(591, '17382', 'S.I.P.E.F SRL', '17382', 'ROXANA', 'S', 'sipef@sipef.com.ar,rdopazo@sipef.com.ar ', 'AV CORRIENTES 524 PISO 3', 'CAPITAL FEDERAL', '4394-1388', 'ROXANA', 'SERV.PARA EMP.FINANCIERAS', 'Enviar dato faltante !! ', 'LA CAJA'),
(592, '1739', 'METALURGICA F.H.D. S.A.', '1739', 'CARLOS', 'S', 'ventasfhd@speedy.com.ar ', 'J.M. MORENO 1052', 'LANUS OESTE', '247-3655/3673', 'SR. LUIS FAIJA/CARLOS', 'METALURGICA', 'administracion@metsh.com.ar ', 'ART INTERACCION SA'),
(593, '17406', 'MARIA JOSEFA GARRIDO', '17406', 'VERONICA', 'S', 'mveronicalopez@yahoo.com.ar,gemona@live.com.ar', 'PTE. PERON 3349', 'V. ALSINA', '209-8543/1535763963', 'CLAUDIO LEPORE', 'RESTAURANTE', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(594, '17455', 'PANADERIA SAN CAYETANO (ANTONIO BOSCO,ELENA BOSCO', '17455', 'ELENA', 'S', 'leandroriso@hotmail.com.ar ', 'JUAN DOMINGO PERON 1654', 'VALENTIN ALSINA', '4208-0313', 'ELENA', 'CONFITERIA', 'Enviar dato faltante !! ', 'SMG'),
(595, '17473', 'EL CAPRICHO SA', '17473', 'MERCEDES', 'S', 'rodriguez_sa@speedy.com.ar ', 'STURLA 3950', 'SARANDI', '4207-8211', 'MERCEDES', 'TRANSPORTE', 'Enviar dato faltante !! ', 'PROVINCIA'),
(596, '17482', 'DONDOW SA', '17482', 'DANIEL', 'S', 'info@cieloytierra-web.com.ar ', 'AV HIPOLITO YRIGOYEN 1312', 'AVELLANEDA OESTE', '4209-9900', 'DANIEL', 'FAB. ROPA DE CUERO', 'Enviar dato faltante !! ', 'PROVINCIA'),
(597, '17492', 'NEUMATICOS RUEDASUR SRL', '17492', 'MARIO', 'S', 'neumaticosruedasur@hotmail.com ', 'AV HIPOLITO YRIGOYEN 7064', 'BANFIELD OESTE', '4202-6379', 'DANIEL-MARIO-NICOLAS', 'NEUMATICOS', 'neumaticosruedasur@hotmail.com ', 'PROVINCIA'),
(598, '17496', 'CORRICAL SRL', '17496', 'NOEMI', 'S', 'patriciam1958@yahoo.com.ar,noemisilva@hotmail.com', 'AV.CALLAO 368', 'CAPITAL FEDERAL', '4372-2765', 'PATRICIA KIPERMAN/ NOEMI SILVA', 'BAR- BUFFET', 'noemisilva71@hotmail.com ', 'PREVENCION'),
(599, '17513', 'FRANCISCO OSCAR TROISI', '17513', 'FRANCISCO', 'S', 'cuerosfrancisco@gmail.com ', 'MANUELA PEDRAZA 3146', 'LANUS OESTE', '4228-6594/ 1558-255228', 'FRANCISO-FERNANDO', 'CURTIEMBRE', 'Enviar dato faltante !! ', 'GALENO'),
(600, '1752', 'SOCOTHERM AMERICAS S.A.', '1752', 'GISELA', 'S', 'gisela.escobar@socotherm.com.ar,pedro.streeton@socotherm.com.ar', 'DR CROTTI 200', 'VALENTIN ALSINA', '4208-7814/03484433595pago', 'DIEGO FABRI - PATRICIA BERNANDO', 'Enviar dato faltante !!', 'abel.cafruni@socotherm.com.ar,atencionaproveedores@socotherm.com.ar', 'GALENO'),
(601, '17520', 'PORCO DIEGO EZEQUIEL', '17520', 'DIEGO', 'S', 'diegoe@kupallogistica.com.ar,belmontefiambreria@gmail.com', 'JUAN HIPOLITO VIEYTES 1092', 'BANFIELD', '4202-8243/4225-5115', 'PORCO DIEGO', 'DISTRIB. FABRIC DE ALIMENTOS', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(602, '17548', 'PANADERIA MISKY', '17548', 'MARA', 'S', 'marabacigalupi@hotmail.com ', 'KLOOSTERMAN 1575', 'MONTE CHINGOLO', '4246-7717/1557301430', 'FREDY-MARA', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'SMG'),
(603, '17552', 'GERMAN BRUNETTA Y SEBASTIAN CAPUZZI SH', '17552', 'LORENA', 'S', 'lorena.brucap@gmail.com ', 'UNAMUNO 123', 'BANFIELD OESTE', '5290-2903 INT 30', 'GERMAN BRUNETTA/LORENA RUIZ', 'CALZADO', 'Enviar dato faltante !! ', 'SMG'),
(604, '17583', 'TALLERES LUGONES SRL', '17583', 'MARICEL', 'S', 'tallereslugonessrl@ciudad.com.ar ', 'MONTEAGUDO 1260', 'BANFIELD OESTE', '4202-5000/4248-7178/4202-', 'MARICEL', 'REPARAC.DE AUTOMOVILES', 'Enviar dato faltante !! ', 'PREVENCION'),
(605, '17584', 'RICARDO SERAFIN DOMINGUEZ', '17584', 'RUBEN', 'S', 'avenidaautoelevadores@gmail.com ', 'AV HIPOLITO YRIGOYEN 3168', 'LANUS OESTE', '4247-2580', 'RUBEN/RICARDO', 'AUTOELEVADORES', 'Enviar dato faltante !! ', 'SMG'),
(606, '17592', 'CHAPIGRAF SA', '17592', 'MARIA FLORENCIA', 'S', 'compras@chapigrafsa.com.ar ', 'MANUELA PEDRAZA 2741', 'LANUS OESTE', '4228-2263/4208-9590', 'MARIA FLORENCIA/ MATIAS/ELIDA', 'GRAFICA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(607, '1762', 'CONS. DE PROPIET. TUCUMAN 1638', '1762', 'ANAMARIA', 'S', 'estudiomozzone@hotmail.com ', 'TUCUMAN 1638', 'LANUS ESTE', '4240-3566', 'DRA ANA MARIA MOZZONE', 'CONSORCIO', 'Enviar dato faltante !! ', 'PROVINCIA'),
(608, '17644', 'RECORTERA AJ SH', '17644', 'ADRIAN', 'S', 'NO MAIL ', 'DR A.LAVARELLO 1162', 'SARANDI', '4204-6495/1537272988', 'ADRIAN BURRIEZA', 'RECICLADORA', 'Enviar dato faltante !! ', 'NO POSEE ART'),
(609, '17668', 'REP ART SA', '17668', 'ANDRES', 'S', 'andres.ok@gmail.com,info@sr60.com.ar ', 'ECHAURI 1721', 'CAPITAL FEDERAL', '4918-2382/4490', 'ANDRES/DIANA PEDEMONTE', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'PREVENCION'),
(610, '17675', 'TUTINO SERGIO MIGUEL', '17675', 'SERGIO', 'S', 'sergiotutino@ciudad.com.ar ', 'LISANDO DE LA TORRE 3969', 'CAPITAL FEDERAL', '4602-4655', 'SERGIO TITULAR', 'CAUCHO', 'Enviar dato faltante !! ', 'LA SEGUNDA'),
(611, '1768', 'GUAPAS SA.', '1768', 'MIRTA', 'S', 'ventas@gracielagrin.com.ar ', 'ACOYTE 1678', 'CAPITAL', '855-3777', 'GRACIELA', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'PREVENCION'),
(612, '17708', 'INDUSTRIAS C Y K S.A', '17708', 'JUAN', 'S', 'ventas@yali.com.ar ', 'SALTA 567', 'LANUS ESTE', '4240-8566 / 4247-1898', 'LA GENTE DE YALI S.A.', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'GALENO'),
(613, '17774', 'GABRIEL DAMIAN SPARECHE ( MULTICLEAN)', '17774', 'GABRIEL', 'S', 'gestion@multi-clean.com.ar ', 'MALABIA 859', 'BANFIELD ESTE', '20504500', 'GABRIEL', 'LIMPIEZA', 'GESTION@MULTI-CLEAN.COM.AR ', 'PREVENCION'),
(614, '17786', 'SAINT ELOI SRL', '17786', 'MONICA', 'S', 'saint_eloisrl@yahoo.com.ar ', 'C. TEJEDOR 1563', 'LANUS OESTE', '4262-8159', 'MONICA', 'TALLER DE PINTURA', 'Enviar dato faltante !! ', 'LA CAJA'),
(615, '17788', 'DE CLERCK LEILA MIRTA', '17788', 'LEILA', 'S', 'natanjo@hotmail.com ', 'O HIGGINS 1933 1ER PISO', 'LANUS ESTE', '4241-0247', 'LEILA', 'REG. AUTOMOTOR', 'Enviar dato faltante !! ', 'LA CAJA'),
(616, '17792', 'RADIOLOGIA DIAGNOSTICA ARGENTINA SA', '17792', 'LAURA', 'S', 'relacionesinstitucionales@rda.com.ar,araffo@rda.com.ar,raffo@rda.com.ar', 'HIPOLITO YRIGOYEN 8400 ESQ. CASTELLI', 'LOMAS DE ZAMORA OESTE', '4225-5526/ 4292-9464', 'LAURA RAFFO 1565112211', 'CENTRO DIAG POR IMÁGENES', 'Enviar dato faltante !! ', 'PROVINCIA'),
(617, '17826', 'PREMEC SRL', '17826', 'HUGO', 'S', 'premec_ascensores@fullzero.com.ar,hugo-bustos@speedy.com.ar,info@ascensorespremec.com.ar', 'CAFERATA 5147', 'R. ESC. OESTE', '4202-6942/1556014435', 'ENRIQUE O HUGO', 'METALURGICA', 'Enviar dato faltante !! ', 'GALENO'),
(618, '17875', 'VN GLOBAL BPO SA', '17875', 'LEONARDO', 'S', 'lcapeletti@v-n.com.ar,deromero@v-n.com.ar ', 'SARMIENTO 643 PISO 6 OF 618', 'CAPITAL FEDERAL', '4323-0129/0127//4323-0100', 'LEONARDO CAPELETTI/FEBE SUAREZ', 'CALL CENTER', 'factproveedores@v-n.com.ar ', 'PROVINCIA COLONIA SUIZA'),
(619, '17877', 'ROVERO SRL', '17877', 'MARECO', 'S', 'gabisimms@gmail.com,rocio19-05-78@hotmail.com ', 'RIVADAVIA 1220', 'AVELLANEDA OESTE', '4209-6606/4208-4458', 'SOLEDAD/LORENA', 'RESTAURANT', 'Enviar dato faltante !! ', 'LA CAJA'),
(620, '1788', 'TALLERES BANFIELD S.A.', '1788', 'ESTEBAN', 'S', 'info@talleresbanfield.com.ar,eghiraldo@talleresbanfield.com.ar,talleresbanfield@yahoo.com.ar', 'QUINTANA 152', 'BANFIELD ESTE', '242-8280/8380', 'ING.QUEIROLO/DANIEL', 'TALLER', 'Enviar dato faltante !! ', 'MAPFRE'),
(621, '17884', 'POLIFRONE ANTONELA Y POLIFRONE ANTONIO SH', '17884', 'ANTONELA', 'S', 'polifrone_tda@hotmail.com ', 'MURGUIONDO 166', 'VALENTIN ALSINA', '4228-8676/1552260432', 'ANTONELA', 'TRANSPORTE', 'Enviar dato faltante !! ', 'QBE'),
(622, '17906', 'FRIGORIFICO DOMINICO DE RASO ANDREA MARIANA', '17906', 'MARIANO', 'S', 'frigorificodominico@yahoo.com.ar ', 'CENTENARIO URUGUAYO 1035', 'AVELLANEDA', '4207-8951', 'MARIANO', 'FRIGORIFICO', 'Enviar dato faltante !! ', 'LA CAJA'),
(623, '1791', 'CONFECCIONES GABE S.R.L.', '1791', 'FLAVIA', 'S', 'no ', 'AV. RIVADAVIA 551', 'V. ALSINA', '4208-2842/6574', 'SR. GAUTO GUILLERMO', 'TEXTIL', 'Enviar dato faltante !! ', 'PREVENCION'),
(624, '17926', 'LOLAC SRL', '17926', 'SANDRA', 'S', 'sandraazzio@freschezza.com.ar ', 'AV. EVA PERON 1450', 'TEMPERLEY ESTE', '4245-9143', 'SANDRA', 'FAB. DE HELADOS', 'Enviar dato faltante !! ', 'SMG'),
(625, '1795', 'LA BOTEYA S.R.L.', '1795', 'MONICA', 'S', 'laboteya@speedy.com.ar ', 'SALTA 1574', 'LANUS ESTE', '4249-8463', 'MONICA-ROBERTO', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL');
INSERT INTO `Usuarios` (`id`, `Codigopal`, `Nombrepal`, `Clave`, `Usuario`, `Activo`, `Correo`, `Domicilio`, `Localidad`, `Telefono`, `Contacto`, `Actividad`, `Correop`, `Nombart`) VALUES
(626, '17956', 'SIBA ( CONSTRUCTORA GARRYS SA)', '17956', 'SOLEDADMARIA', 'S', 'mblanco2140@yahoo.com.ar ', 'FELIPE AMOEDO 3450', 'QUILMES', '4250-4145', 'MALENA', 'ZANJEO- INST ELECT', 'gloureiro@sibaute.com.ar ', 'SMG'),
(627, '18007', 'FINGER EDUARDO ENRIQUE', '18007', 'GRACIELA', 'S', 'gcaruso@fingerplast.com.ar,info@fingerplast.com.ar', 'FRANCIA 1968', 'BENAVIDEZ', '03327-472379', 'GRACIELA', 'PLASTICO', 'Enviar dato faltante !! ', 'GALENO'),
(628, '18008', 'HOJAL LAT DE LUIS RUA', '18008', 'LUIS', 'S', 'estudioapex@hotmail.com ', 'WARNES 3211', 'LANUS OESTE', '4228-3152//4368-6086 (TEL', 'LUIS-DEBORA', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'PREVENCION'),
(629, '18019', 'CRYSLAN SA', '18019', 'FLORENCIA', 'S', 'administracion@cryslan.com,alemontini@cryslan.com', 'GAEBELER 1720', 'LANUS ESTE', '4249-0533', 'FLORENCIA', 'CRISTALES PARA AUTOS', 'Enviar dato faltante !! ', 'MAPFRE'),
(630, '18025', 'MASTER DISTRIBUTION SA', '18025', 'MARIA.JOSE', 'S', 'mariajose.garcia@masterdi.com.ar ', 'VILLA DE LUJAN 1491', 'SARANDI', '4230-1406', 'MARIA JOSE', 'DISTRIBUIDORA', 'Enviar dato faltante !! ', 'GALENO'),
(631, '18030', 'POBLADORA SRL', '18030', 'EMILIO', 'S', 'clsanta40@yahoo.com.ar,panapobladora@yahoo.com.ar', 'URUGUAY 971', 'AVELLANEDA OESTE', '4209-4395/15-5698-5716', 'EMILIO/CLAUDIA', 'CONFITERIA', 'panapobladora@yahoo.com.ar ', 'QBE'),
(632, '18059', 'QUIMICA COTTON FIELDS DE ROMERA WALTER', '18059', 'WALTER', 'S', 'quimicacottonfields@hotmail.com ', 'CNEL MENDEZ 1908', 'WILDE', '4230-9399', 'WALTER', 'VENTA PROD DE LIMPIEZA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(633, '1809', 'COBUS S.R.L.', '1809', 'MARIO', 'S', 'cobus@cobussrl.com.ar,malberico@cobussrl.com.ar', 'BUSTAMANTE 1829', 'LANUS ESTE', '4116-3458/59-4203-9589', 'DOMINGO ALBERICO', 'METALURGICA', 'cobus@cobussrl.com.ar,malberico@cobussrl.com.ar', 'PREVENCION'),
(634, '18134', 'E P Nº 17', '18134', 'MARIA ALEJANDRA', 'S', 'eprimaria17@hotmail.com.ar ', 'MINISTRO BRIN 3024', 'LANUS OESTE', '4241-2124', 'MARIA ALEJANDRA - BETINA- ALICIA', 'EDUCATIVA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(635, '18144', 'BFB AUTOLUBE SAMICIF Y A', '18144', 'JULIO', 'S', 'ventas@bfbautolube.com.ar,tecnica@bfbautolube.com.ar,facturacion@bfbautolube.com.ar', 'TRIUNVIRATO 1135', 'TEMPERLEY ESTE', '4244-4639', 'JULIO', 'METALURGICA', 'Enviar dato faltante !! ', 'ART LIDERAR SA'),
(636, '18152', 'INSTITUTO STELLA MARIS', '18152', 'GUILLERMO', 'S', 'guillermoariel.martin@hotmail.com ', 'JUNCAL 1762', 'LANUS ESTE', '4241-2223', 'GUILLERMO', 'EDUCATIVA', 'Enviar dato faltante !! ', 'GALENO'),
(637, '18153', 'JARDIN DE INFANTES 912', '18153', 'ANALIA', 'S', 'moraanalia@yahoo.com.ar ', 'TERRANOVA 345', 'LANUS OESTE', '4225-4241', 'ANALIA', 'EDUCATIVA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(638, '1816', 'FRIGORIFICO ESCALADA S.R.L.', '1816', 'ESCALADA', 'S', 'NO MAIL ', 'CAFERATA 2187', 'R. ESCALADA OESTE', '242-1531', 'OSVALDO', 'FRIGORIFICO', 'Enviar dato faltante !! ', 'PROVINCIA'),
(639, '1817', 'FLORES SIMO HNOS. S.A', '1817', 'NESTOR', 'S', 'sjnestor@speedy.com.ar ', 'TUCUMAN 2338', 'LANUS ESTE', '241-9008 / 240-6699', 'RAFAEL', 'FLORERIA', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(640, '18184', 'GLM LOGISTICA SA', '18184', 'SILVIA', 'S', 'transportesluaces@gmail.com ', 'HERNAN CORTES 425', 'SARANDI', '4205-5322', 'CARLOS-ADOLFO', 'TRANSPORTE', 'Enviar dato faltante !! ', 'PROVINCIA'),
(641, '1819', 'ITAL NAF S.A.', '1819', 'SILVINA', 'S', 'italnaf@hotmail.com ', '25 DE MAYO 2073', 'LANUS OESTE', '262-9049/4880-0580', '- SILVINA', 'EXP. DE NAFTA', 'Enviar dato faltante !! ', 'GALENO'),
(642, '18193', 'INSTITUTO CRISTO REY', '18193', 'LILIANA', 'S', 'cristorey@yahoo.com.ar ', 'VILLA DE LUJAN 1538', 'LANUS ESTE', '4240-3044', 'LILIANA', 'EDUCATIVA', 'Enviar dato faltante !! ', 'SMG'),
(643, '18196', 'JARDIN DE INFANTES N° 911', '18196', 'CLAUDIA', 'S', 'jardindeinfantesalmafuerte@gmail.com ', 'CARLOS TEJEDOR 1061', 'LANUS OESTE', '4240-6410', 'CLAUDIA', 'EDUCATIVA', 'Enviar dato faltante !! ', 'LA SEGUNDA'),
(644, '18201', 'VN COBRANZAS SA', '18201', 'LEONARDO', 'S', 'lcapeletti@vn.com.ar,fsuarez@vn.com.ar,deromero@v-n.com.ar', 'SARMIENTO 643 PISO 6 OF 618', 'CAPITAL FEDERAL', '4323-0129', 'LEONARDO', 'CALL CENTER', 'factproveedores@v-n.com.ar ', 'ASOCIART A.R.T.'),
(645, '18216', 'JARDIN N° 923', '18216', 'VERONICA', 'S', 'NO MAIL ', 'AV HIPOLITO YRIGOYEN 5005', 'LANUS OESTE', '4241-8210', 'VERONICA', 'EDUCATIVA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(646, '18225', 'MATAFUEGOS TORCEL', '18225', 'LEONARDO', 'S', 'lhceliz@matafuegostorcel.com.ar,matafuegostorcel@fibertel.com.ar', 'SUPISICHE 196', 'SARANDI', '4116-0665', 'VANESA- LEONARDO', 'MATAFUEGOS', 'Enviar dato faltante !! ', 'QBE'),
(647, '18226', 'DELGADO NESTOR ADRIAN', '18226', 'NESTOR', 'S', 'adr_bar@live.com.ar ', 'CALLE 5 N° 140', 'GUERNICA', '02224473154/02225482723', 'NESTOR', 'COMERCIO', 'Enviar dato faltante !! ', 'SMG'),
(648, '18234', 'DUSHA TEXTIL SRL', '18234', 'BERNARDA', 'S', 'melanie.breque@hotmail.es ', 'FRANCISCO GARCIA ROMERO 3911', 'REMEDIOS DE ESCALADA OESTE', '4267-1106', 'BERNARDA', 'Enviar dato faltante !!', 'dusha.textil@gmail.com ', 'FEDERACION PATRONAL'),
(649, '18264', 'JARDIN N° 905', '18264', 'UBALDO', 'S', 'NO MAIL ', 'MORENO 32', 'LANUS OESTE', '4249-1358', 'VIVIANA-MARIANA', 'EDUCATIVA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(650, '18266', 'SOL MONSERRAT SRL', '18266', 'NOEMI', 'S', 'patricia1958@yahoo.com.ar,noemisilva71@hotmail.com', 'MEXICO 1149', 'CAPITAL FEDERAL', '4381-2548', 'NOEMI SILVA', 'HOTEL', 'Enviar dato faltante !! ', 'PREVENCION'),
(651, '18285', 'PH BASES SA', '18285', 'PABLO', 'S', 'phbases@hotmail.com ', 'A. DEL VALLE 2668', 'LANUS OESTE', '4228-7027 4218-4482', 'PABLO', 'CALZADOS', 'Enviar dato faltante !! ', 'PREVENCION'),
(652, '1829', 'ESCUELA NRO. 6 EGB LANUS', '1829', 'SILVIA', 'S', 'epb6_lanus@yahoo.com.ar ', 'ITUZAINGO 1762', 'LANUS', '241-0536', 'AREA PROTEGIDA/ SILVIA GOMEZ DIRECTORA', 'EDUCATIVA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(653, '18292', 'ALTRUI CARLOS NORBERTO', '18292', 'CARLOS', 'S', 'anymarando@yahoo.com.ar ', 'JUNCAL 2330', 'LANUS ESTE', '4249-5078', 'CARLOS', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'MAPFRE'),
(654, '18309', 'APARICIO ANGEL', '18309', 'ANGEL', 'S', 'angaparicio@yahoo.com.ar ', 'LITUANIA 3269', 'LANUS OESTE', '4262-1518', 'ANGEL', 'METALURGICA', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(655, '18332', 'INDUSTRIAS TECNOLOGICAS ARGENTINA SA', '18332', 'JORGE', 'S', 'patricia@itargsa.com.ar,silvina@itargsa.com.ar', 'GRAL. DEHEZA 991', 'AVELLANEDA', '4207-9567', 'PATRICIA', 'METALURGICA', 'silvina@itargsa.com.ar ', 'GALENO'),
(656, '18342', 'KIERO SRL', '18342', 'ROBERTO', 'S', 'rnieto@aquafloat.com.ar,mirta@aquafloat.com.ar', 'CAMACUA 5055', 'VILLA DOMINICO', '42206666', 'ROBERTO NIETO', 'Enviar dato faltante !!', 'rnieto@aquafloat.com.ar ', 'LA CAJA'),
(657, '18351', 'PROTEKIP SACI', '18351', 'ANDRES', 'S', 'info@protekip.com.ar,andrestournier@hotmail.com', 'TRIUNVIRATO 1927', 'AVELLANEDA OESTE', '4209-0433', 'ANDRES', 'SEGURIDAD INDUSTRIAL', 'Enviar dato faltante !! ', 'GALENO'),
(658, '1837', 'CONFECOR S.A.', '1837', 'MANUELA', 'S', 'personal@confecor.com.ar,agencias@confecor.com.ar,confecor@confecor.com.ar', 'REPUBLICA ARGENTINA 2573', 'VALENTIN ALSINA', '4218-1500/4208-7593', 'GUSTAVO FELDMAN', 'TEXTIL', 'proveedores@confecor.com.ar ', 'LA CAJA'),
(659, '18370', 'CONS.DE COPROP.EDIF.TTE.CNEL.GIUFRA 827', '18370', 'MARCELO', 'S', 'estudiofontanan@yahoo.com,mfontana@consejo.org.ar,estudiofontanan@yahoo.com.ar', 'PATXOT 827', 'VALENTIN ALSINA', '15 4147-5886 4209-7183', 'MARCELO FONTANA', 'CONSORCIO', 'Enviar dato faltante !! ', 'PREVENCION'),
(660, '18398', 'DE RUCCI SA', '18398', 'DERUCCI', 'S', 'calzadosfierros@hotmail.com ', 'FLORIDA 2446', 'VALENTIN ALSINA', '42089489', 'SRA TERESA', 'CALZADO', 'teresa_albornoz@hotmail.com ', 'MAPFRE'),
(661, '1840', 'INDUSTRIAS M.H. S.R.L.', '1840', 'JAVIER', 'S', 'rrhh@industriasmh.com.ar,martarrhh@industriasmh.com.ar', 'CNEL. MAURE 1628', 'LANUS ESTE', '249-5585 241-3520', 'SRA. SILVANA, JAVIER (RR.HH)', 'FCA.DE CONDENSA', 'compras@industriasmh.com.ar ', 'PREVENCION'),
(662, '1841', 'PLASTICOS LANUS S.A.', '1841', 'WALTER', 'S', 'plasticos_lanus@yahoo.com.ar ', 'EVA PERON 1834', 'LANUS ESTE', '249-5217', 'WALTER/MONICA', 'FCA.DE POLIETIL', 'Enviar dato faltante !! ', 'PROVINCIA'),
(663, '18416', 'ANTIGUOS TALLERES RAGOR S.A', '18416', 'ANIBAL', 'S', 'ragor@hotmail.com.ar ', 'ESTEBAN ECHEVARRIA 335', 'WILDE', '4207-6918', 'ANIBAL/MANUEL', 'GRAFICA', 'admin@ragor.com.ar ', 'QBE'),
(664, '18440', 'EL ANGEL TOURS SRL', '18440', 'NORMA', 'S', 'elangeltours@hotmail.com ', 'TUCUMAN 4230', 'LANUS ESTE', '4246-2616', 'NORMA', 'TRANSPORTE', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(665, '18455', 'TRADE SEEDING S.A.', '18455', 'GRACIELA', 'S', 'lauraburattini@oligra.com.ar,lilianab@oligra.com.ar', 'ARMENIA 62', 'VILLA DOMINICO', '4115-6700/6801/6802', 'LAURA', 'DISTRIBUIDORA', 'mariela.gimenez@oligra.com.ar ', 'SMG'),
(666, '18465', 'FARMACIA UOM MITRE SCS', '18465', 'UOM', 'S', 'acomandatore@uomlanus.com.ar,pgonta@uomlanus.com.ar,jgoldberg@uomlanus.com.ar,tsciscivo@uomlanus.com.ar', 'AV MITRE 1973', 'AVELLANEDA', '4357-9058', 'ALEJANDRA', 'FARMACIA', 'administracion@uomlanus.com.ar,jmontes@uomlanus.com.ar', 'QBE'),
(667, '18466', 'FARMACIA UOM DEL PUENTE SCS', '18466', 'UOM', 'S', 'acomandatore@uomlanus.com.ar,pgonta@uomlanus.com.ar,jgoldberg@uomlanus.com.ar,tsciscivo@uomlanus.com.ar', 'PTE JUAN D PERON 3212', 'VALENTIN ALSINA', '4357-9058', 'ALEJANDRA', 'FARMACIA', 'administracion@uomlanus.com.ar ', 'QBE'),
(668, '18475', 'VIL ELECTRO SRL', '18475', 'DANIEL', 'S', 'casavilchez@speedy.com.ar,vilelectro@speedy.com.ar', 'ARISTOBULO DEL VALLE 1347', 'LANUS OESTE', '4249-5858', 'SERGIO/DANIEL', 'VENTA DE PROD. ELECT', 'Enviar dato faltante !! ', 'LA CAJA'),
(669, '18489', 'CONSORCIO GARZON 153', '18489', 'ANA MARIA', 'S', 'estudiomozzone@hotmail.com ', 'GARZON 153', 'MONTE GRANDE', 'Enviar dato faltante !!', 'ESTUDIO MOZZONE ANA MARIA', 'CONSORCIO', 'Enviar dato faltante !! ', 'MAPFRE'),
(670, '18511', 'MARCELO DANIEL MAZZINI', '18511', 'MARCELO', 'S', 'mdmazzini@gmail.com ', 'BUENOS AIRES 169', 'BANFIELD ESTE', '4202-3100 INT 45', 'MARCELO', 'TEXTIL', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(671, '18537', 'SM3 S.A.', '18537', 'ALBERTO', 'S', 'orzanco@speedy.com.ar ', 'LORIA 398', 'LOMAS DE ZAMORA', '4262-1055', 'ALBERTO', 'CONSORCIO', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(672, '18546', 'PEREYRA ELENA MARINA', '18546', 'ALEJANDRA', 'S', 'aletoth@yahoo.com ', 'BERON DE ASTRADA 2072', 'CAPITAL FEDERAL', '4918-9872', 'ALEJANDRA', 'METALURGICA', 'Enviar dato faltante !! ', 'SMG'),
(673, '18553', 'GROSSO NORBERTO OSCAR', '18553', 'NORBERTO', 'S', 'transportesgrosso@gmail.com ', 'CALLE 30 nro 4554', 'SAN MARTIN', '1552483831', 'NORBERTO', 'TRANSPORTE', 'Enviar dato faltante !! ', 'SMG'),
(674, '18565', 'DOFRAN SA', '18565', 'JULIAN', 'S', 'jtomaro@dofran.com.ar,ptomaro@dofran.com.ar ', 'ARREDONDO 3171', 'SARANDI', '4205-9068 1555888908', 'JULIAN TOMARO', 'FAB. DE ENVASES PLASTICOS', 'mariluz@dofran.com.ar ', 'LA CAJA'),
(675, '1857', 'MULTICRISTAL SRL EN FORMACION', '1857', 'CRISTAL', 'S', 'multicristal@hotmail.com ', 'MAURE 2043', 'LANUS ESTE', '247-2685 249-0702', 'DARIO', 'FCA.DE VIDRIOS', 'Enviar dato faltante !! ', 'GALENO'),
(676, '18571', 'METALURGICA FERRARI SRL', '18571', 'SERGIO', 'S', 'graferrari@yahoo.com.ar ', 'ITUZAINGO 2515', 'LANUS E', '4241-6961', 'GRACIELA SERGIO', 'METALURGICA', 'Enviar dato faltante !! ', 'LA CAJA'),
(677, '18575', 'SCHIAVONE FABIAN ALEJANDRO', '18575', 'CLAUDIA', 'S', 'info@fseweb.com.ar,administracion@fsweb.com.ar', 'HONDURAS 1867', 'VALENTIN ALSINA', '4209-6800', 'ANDREA/CLAUDIA', 'FAB.IND.INCENDIOS Y SEG IND', 'Enviar dato faltante !! ', 'GALENO'),
(678, '18577', 'JPG CARRERA S.A.', '18577', 'ALEJANDRO', 'S', 'admin@jpracing.com.ar ', 'VIEYTES 1448', 'BANFIELD', '4202-0558', 'GABRIEL', 'Enviar dato faltante !!', 'admin@jpracing.com.ar ', 'LA CAJA'),
(679, '18582', 'VUCASSOVICH NICOLAS', '18582', 'MARIANA', 'S', 'administracion@intartornillos.com ', 'ANATOLE FRANCE 461', 'AVELLANEDA ESTE', '4204-9343', 'MARIANA', 'METALURGICA', 'Enviar dato faltante !! ', 'PREVENCION'),
(680, '1859', 'FUTURO S.A.', '1859', 'RAUL', 'S', 'np@pepasfuturo.com.ar,rlozada@pepasfuturo.com.ar', 'SALTA 456', 'LANUS ESTE', '4241-9905', 'DRA.NELLY', 'ALIMENTICIA', 'Enviar dato faltante !! ', 'GALENO'),
(681, '18602', 'BREDA SABRINA ALEJANDRA', '18602', 'SABRINA', 'S', 's.ale-79@hotmail.com ', 'JUNCAL 3844', 'LANUS ESTE', '4220-2233', 'SABRINA', 'DIST. ART PARA GOMERIA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(682, '1862', 'WUNJO S.R.L.', '1862', 'ISABEL', 'S', 'linacondino@gmail.com,mii.wunjocd@gmail.com ', 'CASTRO BARROS 53/55', 'LANUS OESTE', '4240-9609', 'LINA/ISABEL', 'EDUCATIVA', 'linacondino@gmail.com ', 'SMG'),
(683, '18625', 'KUMARAL SRL', '18625', 'DIGNA', 'S', 'kumaral_srl@hotmail.com ', 'HUMBERTO PRIMO 1851', 'AVELLANEDA OESTE', '4209-8162', 'DIGNA-JOSE', 'ELABORACION DE MATAMBRES', 'kumaral_srl@hotmail.com ', 'PREVENCION'),
(684, '18630', 'EL SOL DEL SUR SRL', '18630', 'MARTIN', 'S', 'aalem-sa@speedy.com.ar,lacmano@gmail.com ', 'PERON 2017', 'CAPITAL FEDERAL', '4298-2100', 'VIVIANA', 'CLINICA PSIQUIATRICA', 'alemserver@speedy.com ', 'ART INTERACCION SA'),
(685, '18642', 'PALU CORPORATIVA SRL', '18642', 'ALE', 'S', 'info@paluhilados.com.ar ', 'CAMINO GRAL BELGRANO KM 10.5 LOTE 12 GAL', 'QUILMES', '4208-9576/2076-5053/2236', 'Enviar dato faltante !!', 'TEXTIL', 'info@paluhilados.com.ar ', 'RECONQUISTA ART'),
(686, '18646', 'CITYMAKERS SA', '18646', 'VANESA', 'S', 'administracionsrl@lasaprop.com ', 'DR MELO 3343', 'LANUS OESTE', '4249-7790', 'VANESA', 'CONSTRUCCION', 'administracionsrl@lasaprop.com ', 'NO POSEE ART'),
(687, '18658', 'LARRACHE JOSE MARIA', '18658', 'NATALIA', 'S', 'lanusdeposito@hotmail.com ', 'PILCOMAYO 2501', 'LANUS OESTE', '4262-5926', 'DIEGO-NATALIA', 'TRANSPORTE', 'Enviar dato faltante !! ', 'PREVENCION'),
(688, '18659', 'LARRACHE DIEGO SEBASTIAN', '18659', 'NATALIA', 'S', 'lanusdeposito@hotmail.com ', 'PILCOMAYO 2501', 'LANUS OESTE', '4262-5926', 'DIEGO-NATALIA', 'TRANSPORTE', 'Enviar dato faltante !! ', 'PREVENCION'),
(689, '18688', 'BARUTTA FABIAN GUILLERMO-OLMO GRACIELA INES SH', '18688', 'MARIA', 'S', 'divina-11@hotmail.com.ar ', 'LAPRIDA 183', 'LOMAS DE ZAMORA', '4292-3293', 'MARIA', 'COMERCIO', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(690, '18715', 'CONSORCIO ALSINA 80', '18715', 'CONSORCIO ALSINA 80', 'N', 'civeira_romero@yahoo.com.ar,estudioromero@yahoo.com.ar', 'ALSINA 80', 'BANFIELD', '4240-7856', 'ANA ROMERO', 'CONSORCIO', 'Enviar dato faltante !! ', 'NO POSEE ART'),
(691, '18729', 'PIRAMYDES GLOBAL SRL', '18729', 'MARCELO', 'S', 'info@piramydesglobal.com.ar ', 'SOMMARUGA 166', 'BANFIELD OESTE', '4878-9323', 'ING MARCELO PRADO', 'FAB. ART.DEPORTIVOS', 'Enviar dato faltante !! ', 'GALENO'),
(692, '18738', 'ADMINISTRA-LASA SRL', '18738', 'VANESA', 'S', 'lasapropiedades@hotmail.com ', 'MARGARITA WEILD 1493', 'LANUS ESTE', '4249-7790/4241-2289', 'VANESA', 'CONSORCIO', 'administracionsrl@lasaprop.com ', 'BERKLEY INTERNATIONAL'),
(693, '18756', 'ALERGOM SRL', '18756', 'GABRIELA', 'S', 'info@alergom.com.ar ', 'ENTRE RIOS 244', 'AVELLANEDA OESTE', '4208-6610/4209-8003', 'SANSOGNE GABRIELA', 'CAUCHO', 'Enviar dato faltante !! ', 'PREVENCION'),
(694, '18789', 'CONSORCIO VELEZ SARFIELD 2765', '18789', 'JORGE', 'S', 'ingjorgegomez@gmail.com ', 'VELEZ SARFIELD 2765', 'LANUS OESTE', '4240-6249', 'Enviar dato faltante !!', 'CONSORCIO', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(695, '18793', 'PEDELHEZ ZULEMA', '18793', ' ZULEMA', 'S', 'pedidos@mercadoalsina.com.ar ', 'JUAN DOMINGO PERON 3242', 'VALENTIN ALSINA', '4218-2549/4208-8138', 'ZULEMA', 'VENTA DE POLLOS', 'Enviar dato faltante !! ', 'NO POSEE ART'),
(696, '18799', 'CAZAUX HNOS SH DE RAUL CAZAUX Y EDUARDO CAZAUX', '18799', 'RAUL', 'S', 'cazauxhnos@hotmail.com ', 'SANTIAGO DEL ESTERO 1648', 'LANUS OESTE', '4241-9828/ 2060-9828', 'RAUL CAZAUX/', 'METALURGICA', 'Enviar dato faltante !! ', 'GALENO'),
(697, '1880', 'DACOBA EDGARDO LEONEL', '1880', 'NANCY', 'S', 'administracion@edgard-plast.com.ar ', 'DORA FLEITAS 840', 'PQUE INDUSTRIAL LUIS GUILLON', '4290-8285/8309', 'EDGARDO', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'QBE'),
(698, '18802', 'IMAGING GROUP SRL', '18802', 'GUSTAVO', 'S', 'gustavo@imagingroup.com.ar ', 'GRAL HORNOS 1078', 'CAPITAL FEDERAL', '4302-1015', 'GUSTAVO-DANIEL', 'INSUMOS-IMPRESIÓN', 'admin@imagingroup.com.ar,gustavo@imagingroup.com.ar', 'GALENO'),
(699, '18815', 'CONS.MARGARITA WEILD 1478', '18815', 'VANESA', 'S', 'lasapropiedades@hotmail.com ', 'MARGARITA WEILD 1478', 'LANUS ESTE', '4249-7790', 'VANESA', 'CONSORCIO', 'administracionsrl@lasaprop.com ', 'BERKLEY INTERNATIONAL'),
(700, '18851', 'CONEXIA', '18851', 'CONEXIA', 'N', ' ', '', '', '', '', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'Enviar dato faltante !!'),
(701, '1886', 'FADER PLASTICS', '1886', 'FADER', 'S', 'sercar@faderplastics.com.ar ', 'SUIPACHA 1844', 'LANUS ESTE', '220-2079', 'ADRIAN', 'PLASTICOS', 'Enviar dato faltante !! ', 'SMG'),
(702, '18888', 'ACERPLAST-ACEROS Y PLASTICOS SA', '18888', 'CRISTIAN', 'S', 'cgtaboada@arielsa.com.ar,gcouso@arielsa.com.ar', 'USPALLATA 2913', 'CAPITAL FEDERAL', '4911-2777', 'CRISTIAN/GUSTAVO', 'ACEROS-PLASTICOS', 'lbarcelo@arielsa.com.ar ', 'MAPFRE'),
(703, '18915', 'WEINBAUR NESTOR GABRIEL', '18915', 'NESTOR', 'S', 'neswein@hotmail.com,administracion@fustroctel.com.ar', 'MANUEL OCAMPO 2542', 'VALENTIN ALSINA', '4208-7382', 'NESTOR-WALTER', 'TEXTIL', 'Enviar dato faltante !! ', 'SMG'),
(704, '18923', 'LUCIANO AGUSTIN CAPRISTO', '18923', 'LUCIANO', 'S', 'latinvende@gmail.com ', '9 DE JULIO 1602', 'LANUS ESTE', '1544974309', 'LUCIANO', 'JUGUETERIA', 'Enviar dato faltante !! ', 'NO POSEE ART'),
(705, '18925', 'COLEGIO LAUSANNE', '18925', 'MARIA', 'S', 'maperez_93@hotmail.com,administracionlaussane@speedy.com.ar,lucasagustin-@hotmail.com', '9 DE JULIO 1760', 'LANUS ESTE', '4249-3069', 'MARIA DE LOS ANGELES', 'COLEGIO', 'Enviar dato faltante !! ', 'GALENO'),
(706, '1893', 'MARCELINO MAYO E HIJOS S.R.L.', '1893', 'MARCELINO', 'S', 'carloslandi22@hotmail.com,valeriamayo@hotmail.com', 'CHACO 1866', 'LANUS OESTE', '228-8595', 'MARCELO MAYO', 'METALURGICA', 'Enviar dato faltante !! ', 'LA HOLANDO'),
(707, '18946', 'YALVE CLAUDIO DANIEL', '18946', 'CLAUDIO', 'S', 'yyalve@yahoo.com.ar ', 'GONZALEZ BALCARCE 3349', 'LANUS OESTE', '1557599675', 'CLAUDIO', 'MONTAJES', 'Enviar dato faltante !! ', 'NO POSEE ART'),
(708, '18949', 'PALLETS FEGIAN SA', '18949', 'MARIA', 'S', 'info@induspallets.com ', 'LAS HIGUERITAS 548', 'LANUS ESTE', '4220-9600', 'YAMILA', 'MADERERA', 'yamila.santos@induspallets.com ', 'BERKLEY INTERNATIONAL'),
(709, '18971', 'HERRERIA BUNKER SRL', '18971', 'DANIEL', 'S', 'herreriabunker@yahoo.com.ar ', 'BALBIN 67', 'VALENTIN ALSINA', '156-381-2795', 'DANIEL/ESTEFANIA', 'HERRERIA', 'Enviar dato faltante !! ', 'PREVENCION'),
(710, '18972', 'PILISAR SA', '18972', 'PABLO', 'S', 'melina.dassieu@pilisar.com.ar,daniela.raveglia@siam.com.ar,ailenlis.risso@siam.com.ar', 'CERRITO 1070 PISO 4 DTO 74', 'CAPITAL FEDERAL', '4208-1323/4218-2626/2420', 'PABLO MONTALTO', 'ELECTRONICA', 'nahuelignacio.fuentes@newsan.com.ar,cuentasapagar@newsan.com.ar', 'QBE'),
(711, '19003', 'FAITH GEOLAND SA', '19003', 'MONICA', 'S', 'info@geolandsa.com.ar,michelle@geolandsa.com.ar,contable@geolansd.com.ar,tesoreria@geolandsa.com.ar', 'MARGARITA WEILD 2976', 'LANUS ESTE', '4220-1716', 'VANESA/MICHELLE', 'RECICLADORA', 'proveedores@geolandsa.com.ar ', 'NO POSEE ART'),
(712, '1901', 'PRIMER ROUND S.R.L.', '1901', 'ALEJANDRA', 'S', 'patriciamontonetti@hotmail.com,patriciaolivelli@hotmail.com', 'PAMPA 981', 'VALENTIN ALSINA', '5-544-0999 4218-2871', 'AREA PROTEG. SOLAMEN', 'ZAPATERIA', 'romijaguar@hotmail.com ', 'GALENO'),
(713, '19023', 'ACERTUBO SRL', '19023', 'MARIAELENA', 'S', 'proveedores@acertubo.com,administracion@acertubo.com', 'NAZAR 1435', 'AVELLANEDA ESTE', '42047776 /4205-4319', 'MORALES ISIDRO/MARIA ELENA/ELIZABETH', 'METALURGICA', 'administracion@acertubo.com ', 'QBE'),
(714, '19050', 'TARANTINO DELMA LUISA', '19050', 'LAURA', 'S', 'hidalgo.lau2011@gmail.com,delma_t@hotmail.com ', 'ARTURO ILLIA 1054', 'LANUS ESTE', '4240-7231', 'LAURA HIDALGO', 'REG. AUTOMOTOR', 'Enviar dato faltante !! ', 'SMG'),
(715, '19059', 'NAVE REINALDO', '19059', 'MARA', 'S', 'lavaderoalsina@gmail.com ', 'MADARIAGA 740', 'LANUS ESTE', '4247-8309', 'MARA/REINALDO', 'LAVADERO', 'Enviar dato faltante !! ', 'QBE'),
(716, '19088', 'BUTACO SRL', '19088', 'GRACIELA', 'S', 'butaco_srl@yahoo.com.ar,info@butacosrl.com.ar ', 'SALTA 242', 'LANUS ESTE', '4246-6660', 'GRACIELA- SHIRLI', 'RECUP. DE ENVASES', 'butaco_srl@yahoo.com.ar,info@butacosrl.com.ar ', 'MAPFRE'),
(717, '19090', 'PLANET EXPRESS SRL', '19090', 'CRISTIAN', 'S', 'mensajeriaplanet@gmail.com,ccorbalan@planetexpress.com.ar', 'LUIS S PEÑA 352 10P DTO A', 'CAPITAL FEDERAL', '4383-1655', 'CRISTIAN CORBALAN', 'MENSAJERIA', 'mensajeriaplanet@gmail.com ', 'GALENO'),
(718, '19095', 'TED SH DE DISTEFANO BERNARDO D. Y TRUFFINI CLAUDIO', '19095', 'CLAUDIO', 'S', 'tedsh@hotmail.com ', 'FRANCISCO AMERO 158', 'LOMAS DE ZAMORA ESTE', '4392-0822', 'CLAUDIO TRUFFINI', 'MATERIALES DE CONSTRUCCION', 'tedsh@hotmail.com ', 'PROVINCIA'),
(719, '19107', 'LLANTAS FLOGOEZ SRL', '19107', 'MIRTA', 'S', 'gomerialosamigos@hotmail.com ', 'POSADAS 1521', 'LANUS ESTE', '4220-0383', 'MIRTA', 'GOMERIA', 'gomerialosamigos@hotmail.com ', 'LA CAJA'),
(720, '19108', 'NEUMATICOS SAAVEDRA SRL', '19108', 'MIRTA', 'S', 'gomerialosamigos@hotmail.com ', 'EVA PERON 3203', 'LANUS ESTE', '4220-0383', 'MIRTA', 'GOMERIA', 'gomerialosamigos@hotmail.com ', 'LA CAJA'),
(721, '19113', 'METRO SAFE DE ARGENTINA SRL', '19113', 'GISELA', 'S', 'gisela@fc-group.com.ar,sse@msaarg.com.ar ', 'AV MAIPU 3278', 'OLIVOS', '4242-7067/4202-6052', 'FABIO CUPPARI-GISELA', 'SEGURIDAD', 'gisela@fc-group.com.ar ', 'SMG'),
(722, '19127', 'SUCESION DE SANZ OSCAR', '19127', 'RICARDO', 'S', 'sanz_oscar@ciudad.com.ar ', 'AV HIPOLITO YRIGOYEN 3600', 'LANUS OESTE', '4247-5242/4241-2723', 'RICARDO', 'GOMERIA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(723, '19156', 'WAL-MAR TOYS S.A.', '19156', 'WALTER', 'S', 'walfaz@live.com.ar ', 'ITUZAINGO 1195', 'LANUS ESTE', '4247-6025', 'WALTER', 'JUGUETERIA', 'Enviar dato faltante !! ', 'SMG'),
(724, '19163', 'VIDAL DIEGO OSCAR', '19163', 'DIEGO', 'S', 'fabinet_3000@yahoo.com.ar ', 'VERNET 165', 'BANFIELD OESTE', '4202-6659', 'HERNAN-DIEGO', 'ATAUDES', 'Enviar dato faltante !! ', 'PREVENCION'),
(725, '19174', 'CONSORCIO SALTA 1936', '19174', 'JORGE', 'S', 'ingjorgegomez@gmail.com ', 'SALTA 1936', 'LANUS ESTE', '4240-6249', 'GOMEZ SIRIMARCO', 'CONSORCIO', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(726, '19183', 'JUAN ANGEL MILESI E HIJOS SA', '19183', 'MATIAS', 'S', 'matiasmuebles@hotmail.com,juan@matiasmuebles.com.ar', 'LAS FLORES 1600 T 10 2A', 'WILDE', '4220-0681', 'JUAN-MATIAS', 'ART.PARA EL HOGAR', 'Enviar dato faltante !! ', 'PREVENCION'),
(727, '19187', 'ASCENSORES CONTESSI SRL', '19187', 'GABRIELA', 'S', 'info@ascensorescontessi.com,gabriela.contessi@ascensorescontessi.com', 'ACHAVAL 2548', 'REMEDIOS DE ESCALADA ESTE', '4246-0740', '4246-0740', 'ASCENSORES', 'info@ascensorescontessi.com ', 'FEDERACION PATRONAL'),
(728, '19193', 'TALLERES CHAPET SRL', '19193', 'ERNESTO', 'S', 'talleres.chapet@speedy.com.ar ', 'ROMERO 2343', 'LANUS OESTE', '262-0543', 'EDUARDOI', 'METALURGICA', 'Enviar dato faltante !! ', 'GALENO'),
(729, '19194', 'CONFECCIONES TRIPLE ESE SRL', '19194', 'LAURA', 'S', 'nuevasastreria@hotmail.com ', 'B SUR MER 274 y PEDERNERA', 'LOMAS DE ZAMORA ESTE', '4264-9959//4264-9960', 'VANESA-LAURA', 'SASTRERIA', 'Enviar dato faltante !! ', 'SMG'),
(730, '19197', 'ITECSA SH DE NESTOR Y CLAUDIO DA CRUZ', '19197', 'MACARENA', 'S', 'ventasitecsash@gmail.com,itecsa@speedy.com.ar ', 'VILLEGAS 413', 'REMEDIOS DE ESCALADA', '4202-0180/4242-6022', 'MACARENA/SILVIA', 'METALURGICA', 'Enviar dato faltante !! ', 'LA SEGUNDA'),
(731, '19212', 'DISTRIBUIDORA GYBI ROMANO SRL', '19212', 'GLADIS', 'S', 'romano.hnos@hotmail.com,romanohnos@speedy.com.ar,estudiognr@hotmail.com', 'CENTENARIO DE MAYO 67', 'BURZACO', '4214-0908', 'GLADYS', 'METALURGICA', 'Enviar dato faltante !! ', 'RECONQUISTA ART'),
(732, '19232', 'ZOAMIP SA', '19232', 'LEONARDO', 'S', 'leonardomiraglia@gmail.com,zoamip@gmail.com ', 'ESQUIU 1947', 'LANUS ESTE', '4225-9591', 'LEONARDO-NANCY', 'CONSTRUCCION', 'Enviar dato faltante !! ', 'PREVENCION'),
(733, '19234', 'MOVIGRUAS SA', '19234', 'JUAN CARLOS', 'S', 'movigruassa@gmail.com ', 'BUSTAMANTE 2027', 'LANUS ESTE', '204 -7313 205 -4805', 'JUAN CARLOS', 'ALQ. DE GRUAS', 'movigruassa@gmail.com ', 'FEDERACION PATRONAL'),
(734, '1924', 'SOLDADORAS ROJO S.R.L.', '1924', 'JORGE', 'S', 'soldrojo@speedy.com.ar ', 'AV ROSALES 1393', 'REM. ESCALADA OESTE', '249-3813/247-0928/4240-25', 'JORGE TELO', 'SOLD.ELECTRICAS', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(735, '19260', 'ECOTOP SA', '19260', 'MAGALI', 'S', 'magali@topsinergia.com.ar ', 'CAMINO GRAL BELGRANO 4390', 'VILLA DOMINICO', '4115-0435', 'ROLANDO', 'VIVERO', 'compras@nuevohanasono.com.ar ', 'SMG'),
(736, '19291', 'MAXAL ( ALESANDRIA JOSE LUIS)', '19291', 'JOSE LUIS', 'S', 'alesandria@hotmail.com ', 'SEGUROLA 1453', 'LANUS OESTE', '4240-8087/1544135985', 'JOSE', 'REFRIGERACION', 'Enviar dato faltante !! ', 'NO POSEE ART'),
(737, '19330', 'CONS.DE COPROPIETARIOS FINCA AV PUEYRREDON 127', '19330', 'DIANA', 'S', 'botbul@speedy.com.ar ', 'AV PUEYRREDON 127', 'CAPITAL FEDERAL', '4865-8860', 'MIRTA-DIANA', 'CONSORCIO', 'Enviar dato faltante !! ', 'LA CAJA'),
(738, '19355', 'ADVANCED PLANT SERVICES SA', '19355', 'SILVIO', 'S', 'apsadvanced@gmail.com ', 'CAMINO DE CINTURA 7285', '9 DE ABRIL', '4693-6614///1557121761', 'SILVIO', 'FUNDICION', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(739, '19356', 'LEON MARCELO GUSTAVO', '19356', 'GUSTAVO', 'S', 'nugagus@hotmail.com.ar ', 'AV HIPOLITO YRIGOYEN 4820', 'LANUS OESTE', '4225-8829/1561241612', 'GUSTAVO/LEON', 'FAB. DE CAMILLAS TERMOMASAJEAD', 'Enviar dato faltante !! ', 'MAPFRE'),
(740, '19365', 'TECAPLAS SA', '19365', 'SILVIA', 'S', 'tecaplas@hotmail.com ', 'ALMAFUERTE 481', 'AVELLANEDA ESTE', '4247-4857', 'SILVIA-GUSTAVO', 'PLASTICO', 'Enviar dato faltante !! ', 'PREVENCION'),
(741, '19367', 'FASUR SA', '19367', 'ANALIA', 'S', 'fasursa@hotmail.com ', 'UCRANIA 945', 'VALENTIN ALSINA', '4228-0400', 'KARINA-ANALIA', 'CALZADO', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(742, '19368', 'TERRITORIO SUR SA', '19368', 'WALTER', 'S', 'waltermartinez@bloodsouth.com.ar,marcelomartinez@bloodsouth.com.ar', 'TUYUTI 1869', 'VALENTIN ALSINA', '4218-6641/ 6696', 'WALTER-MARCELO', 'CALZADO', 'cobranzas@bloodsouth.com.ar ', 'FEDERACION PATRONAL'),
(743, '1937', 'CONSORCIO ANATOLE FRANCE 1824', '1937', 'CONSORCIO ANATOLE FR', 'N', 'NO MAIL ', 'ANATOLE FRANCE 1824 1ER PISO', 'LANUS ESTE', '4249-0931', 'ANALIA', 'CONSORCIO', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(744, '19378', 'HS WATER SOLUTIONS SRL', '19378', 'SERGIO', 'S', 'sanon@hsws.com.ar,contacto@hsws.com.ar ', 'WARNES 1407', 'LANUS OESTE', '4262-1135', 'SERGIO AÑON', 'BUCEO', 'Enviar dato faltante !! ', 'MAPFRE'),
(745, '19389', 'ESTECHE Y CASTILLO SRL', '19389', 'JOSE', 'S', 'estecheycastillosrl@hotmail.com.ar ', 'EVARISTO CARRIEGO 473', 'BANFIELD OESTE', '1154179937/2002-3954', 'JOSE ESTECHE/ KAREN', 'CONSTRUCCION', 'Enviar dato faltante !! ', 'LA SEGUNDA'),
(746, '1939', 'RICCHIUTI RUBEN Y RICCHIUTI SILVIA', '1939', 'RUBEN', 'S', 'tmricchiuti@gmail.com ', 'PTE. AVELLANEDA 626', 'LANUS OESTE', '4225-2315', 'LEONARDO', 'TORNERIA', 'Enviar dato faltante !! ', 'PREVENCION'),
(747, '19418', 'SEGURA ELBA ALICIA (HARMONY PILATES)', '19418', 'ALICIA', 'S', 'ali_36segura@hotmail.com ', 'AV HIPOLITO YRIGOYEN 18064', 'LONGCHAMPS', '1559247721', 'SEGURA ALICIA', 'GIMNASIO', 'Enviar dato faltante !! ', 'NO POSEE ART'),
(748, '19427', 'CZENIS VICENTE OMAR', '19427', 'VICENTE', 'S', 'cvicenteomar@yahoo.com.ar ', 'LACARRA 1606', 'GERLI', '5293-2235', 'VICENTE-MARTA-NOELIA', 'BAZAR', 'Enviar dato faltante !! ', 'LA CAJA'),
(749, '19445', 'CARY HARD SA', '19445', 'MARIANO', 'S', 'm.rodriguez@caryplas.com.ar,osvaldo@caryplas.com.ar', 'CARLOS PELLEGRINI 3488', 'LANUS OESTE', '42284429/42289191', 'MARIANO', 'PLASTICOS', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(750, '19446', 'ALEMAX CORRUGADOS SRL', '19446', 'MARIA', 'S', 'maria.alemax@hotmail.com ', 'GIBRALTAR 1770', 'VILLA DOMINICO', '4204-0533', 'GUSTAVO/MARY(ESPOSA)', 'FCA. CARTON', 'Enviar dato faltante !! ', 'GALENO'),
(751, '1945', 'WEISZ INSTRUMENTOS S.A.', '1945', 'SILVINA', 'S', 'roxana.bogado@weisz.com,beatriz@weisz.com,silvina@weisz.com', 'OLIDEN 2540', 'VALENTIN ALSINA', '208-1928/2508', 'ROXANA BOGADO', 'METALURGICA', 'rodolfo@weisz.com,beatriz@weisz.com,jorge@weisz.com', 'ASOCIART A.R.T.'),
(752, '19460', 'DEKMAK ALEX ELIAS Y DEKMAK NICOLAS MARTIN SH', '19460', 'MARIA', 'S', 'divina-11@hotmail.com.ar ', 'ESPAÑA 218', 'LOMAS DE ZAMORA', '4292-3293', 'MARIA', 'COMERCIO', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(753, '19471', 'JC GAL SA', '19471', 'LAURA', 'S', 'tesoreria@perfilesyherrajes.com.ar ', 'PIO COLLIVADINO 540', 'TEMPERLEY ESTE', '4359-2295', 'LAURA/ERICA', 'FAB.MOSQUITEROS Y PERSIANAS', 'tesoreria@perfilesyherrajes.com.ar ', 'ART INTERACCION SA'),
(754, '1948', 'CARROCERIAS CHAMULA SRL', '1948', 'MIRTA', 'S', 'mirta@carroceriaschamula.com.ar,info@carroceriaschamula.com.ar', 'MURATURE 2540', 'LANUS OESTE', '4262-2000', 'Enviar dato faltante !!', 'FAB CARROCERIAS', 'Enviar dato faltante !! ', 'GALENO'),
(755, '19539', 'HB SEGURIDAD Y CUSTODIA SRL', '19539', 'HECTOR', 'S', 'hbseguridadycustodiasrl@hotmail.com ', 'RONDEAU 3566', 'CAPITAL FEDERAL', '4911-9042/1154082825 SUP.', 'HECTOR-GABRIELA-GUSTAVO DOMINGUEZ', 'SEGURIDAD', 'Enviar dato faltante !! ', 'SMG'),
(756, '1954', 'EMPRESARIOS ASOC. DEL SUR S.A.', '1954', 'SANDRA', 'S', 'info@crematoriodelanus.com.ar ', 'AGUILAR 3302', 'LANUS ESTE', '246-9814/15', 'MARISOL', 'CREMATORIO', 'info@crematoriodelanus.com.ar ', 'ART INTERACCION SA'),
(757, '19556', 'PLANETA PUERT SRL', '19556', 'GASTON', 'S', 'gastonf@outlook.com,administracion@newpuert.com.ar', 'UCRANIA 2659', 'LANUS OESTE', '4228-7581', 'GASTON', 'METALURGICA', 'gastonf@outlook.com,administracion@newpuert.com.ar', 'FEDERACION PATRONAL'),
(758, '1956', 'CONS. ANATOLE FRANCE 1724', '1956', 'CLAUDIA', 'S', 'gastonf@outlook.com,administracion@newpuert.com.ar', 'ANATOLE FRANCE 1724', 'LANUS ESTE', '4249-1877', 'GABRIEL/ FEDERICO', 'CONSORCIO', 'Enviar dato faltante !! ', 'QBE'),
(759, '19595', 'PYTEL LEONARDO FABIAN', '19595', 'LEONARDO', 'S', 'leonardopytel@yahoo.com.ar ', 'CAMPICHUELO 253', 'AVELLANEDA OESTE', '4209-4504/1540731547', 'LEONARDO', 'TRANSPORTE', 'Enviar dato faltante !! ', 'GALENO'),
(760, '19596', 'ARIEL O WALSH Y WALTER J WALSH S.H', '19596', 'WALTER', 'S', 'artesaniawalsh@live.com.ar ', 'EVA PERON 3736', 'TEMPERLEY ESTE', '4260-2705', 'ARIEL/WALTER', 'HERRERIA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(761, '19631', 'NUEVO AL AN S.A', '19631', 'JORGE', 'S', 'talleresalan@yahoo.com.ar ', 'PICHINCHA 2228', 'LANUS ESTE', '4247-1264', 'JORGE', 'METALURGICA', 'Enviar dato faltante !! ', 'GALENO'),
(762, '19632', 'HELADERIAS RAIMON SRL', '19632', 'MARIELA', 'S', 'patrusanchez@hotmail.com ', 'DEL VALLE IBERLUCEA 2904', 'LANUS OESTE', '4225-0920', 'MARIELA PEZZUTO', 'HELADERIA', 'Enviar dato faltante !! ', 'QBE'),
(763, '19633', 'CAMINOS PROTEGIDOS ASEGURADORA DE RIESGOS DEL TRAB', '19633', 'CAMINOS', 'S', 'no ', 'BERNARDO DE IRIGOYEN 682', 'CAPITAL FEDERAL', '5288-7600', 'Astarito Silvana', 'ART', 'NO ', 'CAMINOS PROTEGIDOS ART SA'),
(764, '19637', 'ASSALONE ANTONIO, ASSALONE FRANCISCO', '19637', 'ANTONIO', 'S', 'sodalasorpresa@yahoo.com.ar ', 'LOS PATOS 4231', 'REMEDIOS DE ESCALADA OESTE', '4276-1281', 'ANTONIO-FRANCISCO', 'FABRICA DE SODA', 'Enviar dato faltante !! ', 'PREVENCION'),
(765, '19673', 'CONMAN ARGENTINA SA', '19673', 'DAVID', 'S', 'info@conman.com.ar,daboscariol@conman.com.ar ', 'CALLAO 2151', 'LANUS OESTE', '4208-5568', 'RICARDO', 'METALURGICA', 'administracion@conman.com.ar ', 'LA CAJA'),
(766, '19715', 'CONSORCIO MAIPU 180', '19715', 'MARISA', 'S', 'mrech_adm@yahoo.com.ar ', 'MAIPU 180', 'BANFIELD', '3528-4932', 'MARISA RECH', 'CONSORCIO', 'Enviar dato faltante !! ', 'PREVENCION'),
(767, '19724', 'FERNANDO LUIS GARBAGNATI', '19724', 'FERNANDO', 'S', 'fabricaeverhouse@gmail.com,viviendasmelsur@gmail.com', 'AV HIPOLITO YRIGOYEN 11826', 'TURDERA', '4231-3915/4231-9442', 'ROSI- PATRICIO', 'FAB.VIVIENAS PREMOLDEADAS', 'Enviar dato faltante !! ', 'LA CAJA'),
(768, '19734', 'MASTRONARDI NICOLAS', '19734', 'GABRIELA', 'S', 'reinalolalomas@hotmail.com ', 'AV HIPOLITO YRIGOYEN 8266', 'LOMAS DE ZAMORA OESTE', '4292-4361', 'GABRIELA', 'COMERCIO', 'Enviar dato faltante !! ', 'NO POSEE ART'),
(769, '19788', 'SORALESO SA', '19788', 'GUSTAVO', 'S', 'soraleso@live.com.ar ', 'LOS EUCALIPTOS 401', 'TEMPERLEY ESTE', '4264-3403', 'GUSTAVO GONZALEZ', 'FABRICA DE PASTAS', 'Enviar dato faltante !! ', 'SMG'),
(770, '19794', 'GUARDERIA INFANTIL CIELO AZUL', '19794', 'ANDREA', 'S', 'cieloazulmaternal@yahoo.com.ar ', 'CNEL FOREST 2140 E/USHUAIA Y CABRAL', 'AVELLANEDA OESTE', '4209-6034', 'ANDREA-ELISA', 'GUARDERIA', 'Enviar dato faltante !! ', 'GALENO'),
(771, '19814', 'GRANJA OLINDO SRL', '19814', 'MARIANO', 'S', 'granjaolindo@hotmail.com ', 'ZULOAGA 127', 'LANUS OESTE', '4249-5570', 'MARIANO', 'DISTRIBUIDORA', 'Enviar dato faltante !! ', 'PREVENCION'),
(772, '19839', 'VIAGELATO SA', '19839', 'NATALIA', 'S', 'administracion@helamontsa.com.ar,gabrielastranges@hotmail.com', 'LIMA 1125 6D', 'CAPITAL FEDERAL', '4230-1300', 'NATALIA', 'DISTRIB.DE HELADOS', 'gabriela@helamontsa.com.ar,natalia@helamontsa.com.ar', 'QBE'),
(773, '19843', 'UNION DE RECTIFICADORES SRL', '19843', 'ANA', 'S', 'unionderectificadores@fibertel.com.ar ', 'COCHABAMBA 2774', 'CAPITAL FEDERAL', '4941-6666/4942-9063', 'ANA SAMUSKWICZ/ MARCELO BARBON', 'RECTIFICADORA', 'Enviar dato faltante !! ', 'QBE'),
(774, '19870', 'SIBAU ANDREA Y DORREGO DIEGO SH', '19870', 'ANDREA', 'S', 'andreav.sibau-72@hotmail.com ', 'AV 9 DE JULIO 1588', 'LANUS ESTE', '4240-4465/1550070958', 'ANDREA', 'COMERCIO', 'Enviar dato faltante !! ', 'QBE'),
(775, '19876', 'STAFF-FIRE SRL', '19876', 'BRUNO', 'S', 'bfrontini@staff-fire.com,rvairetta@staff-fire.com', 'DAMONTE 902', 'LANUS ESTE', '3974-7466', 'BRUNO', 'SERVICIOS', 'Enviar dato faltante !! ', 'LA SEGUNDA'),
(776, '19895', 'CALZADOS GRANDINETTI SRL', '19895', 'ALBERTO', 'S', 'ammaatuspies@hotmail.com,info@lilianapalumboprop.com.ar', '9 DE JULIO 1116', 'LANUS ESTE', '4241-9240/1563982992', 'ALBERTO', 'CALZADO', 'Enviar dato faltante !! ', 'MAPFRE'),
(777, '19904', 'TEXTIL SUPPLIES SRL', '19904', 'MAXIMILIANO', 'S', 'maximilianoleone@todomerchandising.com,info@todomerchandising.com', 'HUMBERTO 1RO 857', 'VALENTIN ALSINA', '4218-5200/15-44065216', 'MAXIMILIANO/MARTA', 'TEXTIL', 'info@remeras.net,noelia@remeras.net,maximiliano@remeras.net', 'FEDERACION PATRONAL'),
(778, '19941', 'CONSORCIO DE PROPIETARIOS MANUEL ESTEVEZ 147', '19941', 'GABRIEL', 'S', 'glaborde@bmasbadm.com.ar ', '25 DE MAYO 574 1ER OF 6', 'SAN ISIDRO', '4747-7027/ 4732-4911', 'GABRIEL LABORDE', 'CONSORCIO', 'pueyrredon@bmasbadm.com.ar ', 'PREVENCION'),
(779, '19942', 'LCP ENVASES SRL', '19942', 'DAIANA', 'S', 'lcpenvases@hotmail.com ', 'MENDOZA 1961', 'AVELLANEDA OESTE', '4208-2583', 'DAIANA', 'ENVASE', 'Enviar dato faltante !! ', 'PROVINCIA'),
(780, '1995', 'SAN MARTIN HECTOR OSVALDO', '1995', 'HECTOR', 'S', 'NO MAIL 2 ', 'TUCUMAN 2762', 'LANUS', '230-3387', 'WALTER SAN MARTIN', 'SODERIA', 'Enviar dato faltante !! ', 'GALENO'),
(781, '19969', 'COLAFELLA LUCIANA MARIEL', '19969', 'LUCIANA', 'S', 'cartograficaoeste@gmail.com ', 'ISLANDIA 3744', 'LANUS OESTE', '4262-6696/8196', 'COLAFELLA LUCIANA', 'GRAFIC.PAPELERA', 'Enviar dato faltante !! ', 'QBE'),
(782, '19990', 'PREVENCION SALUD SA', '19990', 'PREVENCION SALUD SA', 'N', 'mgaranzini@prevencionart.com.ar ', 'AV INDEPENDENCIA 333', 'SANTA FE', '03493-428600 int. 2860', 'MAXIMILIANO GARANZINI', 'Enviar dato faltante !!', 'mgaranzini@prevencionart.com.ar ', 'PREVENCION'),
(783, '20010', 'INTERSERVICE SERVICIOS EMPRESARIOS SA (L3)', '20010', 'LORENA', 'S', 'csierra@bmontajes.com.ar,lvillar@bmontajes.com.ar,lvillar@bmontajes.com.ar,ise-sa@hotmail.com.ar', 'AV RIVADAVIA 954 1 PISO', 'CAPITAL FEDERAL', '4331-4355 (BLANCO MONTAJE', 'BLANCO MONTAJE LORENA INT 126', 'CONSTRUCCION', 'Enviar dato faltante !! ', 'PREVENCION'),
(784, '20011', 'INTERSERVICE SERVICIOS EMPRESARIOS SA (L2)', '20011', 'LORENA', 'S', 'csierra@bmontajes.com.ar,lvillar@bmontajes.com.ar,lvillar@bmontajes.com.ar,ise-sa@hotmail.com.ar', 'AV RIVADAVIA 954 1 PISO', 'CAPITAL FEDERAL', '4331-4355 (BLANCO MONTAJE', 'BLANCO MONTAJE LORENA INT 126', 'CONSTRUCCION', 'Enviar dato faltante !! ', 'PREVENCION'),
(785, '20048', 'CONSORCIO DE PROP MINISTRO BRIN 2970 (EDIF.BLEST)', '20048', 'MARIA', 'S', 'ad.consorcio.abril@gmail.com ', 'MINISTRO BRIN 2970', 'LANUS OESTE', '240-8409', 'MARIA DONATA GRAZIANO', 'CONSORCIO', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(786, '20050', 'PYTEL CESAR ADRIAN (TRANSPORTE VYC)', '20050', 'CESAR', 'S', 'cesarpytel@hotmail.es ', 'MARIANO ACOSTA 1475', 'AVELLANEDA', '1551830948/4208-6313', 'CESAR', 'TRANSPORTE', 'Enviar dato faltante !! ', 'GALENO'),
(787, '20051', 'CALSUR S.A', '20051', 'NORALI', 'S', 'norali@couce.com.ar ', 'PASO DE BURGOS 554', 'VALENTIN ALSINA', '4209-3575/15-60330493', 'NORALY/GASTON', 'FAB. ART.DEPORTIVOS', 'info@couce.com.ar ', 'QBE'),
(788, '20054', 'CONS.SITIO DE MONTEVIDEO 1344', '20054', 'MARISA', 'S', 'mrech_adm@yahoo.com.ar ', 'S. DE MONTEVIDEO 1344', 'LANUS ESTE', '3528-4932', 'MARISA RECH', 'CONSORCIO', 'Enviar dato faltante !! ', 'NO POSEE ART'),
(789, '2024', 'DISTRIBUIDORA A -M S.R.L.', '2024', 'MARIEL', 'S', 'distamsrl@yahoo.com.ar ', 'ZULUAGA 561', 'LANUS O', '247-6780/4247-7210', 'MENDEZ, ADOLFO', 'DIST.ALIMENTOS', 'Enviar dato faltante !! ', 'GALENO'),
(790, '20247', 'GASMER SA', '20247', 'CECILIA', 'S', 'mazzucac@audes.com.ar,gasmer@audes.com.ar,gasmeradm@audes.com.ar', 'AV ALSINA 820', 'LOMAS DE ZAMORA ESTE', '1169688967 NATALIA', 'CECILIA', 'ESTACION DE SERVICIO', 'Enviar dato faltante !! ', 'PREVENCION'),
(791, '20248', 'HOUSE PAINT (LUIS CESAR VILLEGAS)', '20248', 'LUIS', 'S', 'lvillegas2001@yahoo.com.ar ', 'BUERAS 3271', 'LANUS ESTE', '/1521712297', 'LUIS-JORGE', 'REVESTIMIENTOS', 'Enviar dato faltante !! ', 'NO POSEE ART'),
(792, '2025', 'INDUSTRIA METALURG.SOUSA SRL', '2025', 'CLAUDIO', 'S', 'marianosousa@yahoo.com.ar ', 'De la Cruz 645', 'LANUS O', '276-0286', 'MARIANO', 'METALURGICA', 'Enviar dato faltante !! ', 'GALENO'),
(793, '20267', 'GAE CONSTRUCCIONES SRL', '20267', 'ADRIANA', 'S', 'gaeconstrucciones@gmail.com ', 'ITUZAINGO 1405 6TO PISO', 'LANUS ESTE', '4247-3160/1550629406', 'ADRIANA PALANICA YANKOWEY/ OMAR', 'CONSTRUCCION', 'Enviar dato faltante !! ', 'GALENO'),
(794, '20292', 'HERRAMETAL SA', '20292', 'LILIANA', 'S', 'info@herrametalsa.com.ar ', 'FLORENCIO VARELA 1236 (LIBRETAS LE)', 'LANUS OESTE', '4241-6881', 'LILIANA DE SIMONE', 'DISTRIB.DE HERRAJES', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(795, '20306', 'SETRINT (SERVICIO DE TRANSPORTE INTEGRAL) SA', '20306', 'MARIANA', 'S', 'rodriguez_sa@speedy.com.ar ', 'PARAGUAY 1446', 'CAPITAL FEDERAL', '4207-8211', 'MELISA', 'TRANSPORTE', 'Enviar dato faltante !! ', 'PROVINCIA'),
(796, '20353', 'TRANSPORTE VADIS SRL', '20353', 'SUSANA', 'S', 'vadis_srl@fibertel.com.ar ', 'MANSILLA 3179', 'SARANDI', '4205-9955', 'SUSANA', 'TRANSPORTE', 'Enviar dato faltante !! ', 'SMG'),
(797, '20358', 'BERETTA GASTON LUIS NATALIO', '20358', 'DORA', 'S', 'sepeliobelettas@hotmail.com ', 'URIARTE 1045', 'REMEDIOS DE ESCALADA OESTE', '4202-4656 4248-8168', 'DORA', 'SERVICIOS', 'sepeliobeletta@hotmail.com ', 'SMG'),
(798, '20422', 'ESTAMINOX SRL', '20422', 'PABLO', 'S', 'aiannicello@autoperforantes.com,pciapa@autoperforantes.com', 'COLON 3067', 'LOMAS DEL MIRADOR', '4240-1166', 'PABLO CIAPA', 'METALURGICA', 'Enviar dato faltante !! ', 'GALENO'),
(799, '20438', 'LARES SEGURIDAD SRL', '20438', 'LARES SEGURIDAD SRL', 'N', 'pegasoseg@yahoo.com.ar,pegasoseg@pegasoseguridad.com.ar,diazc@pegasoseguridad.com.ar', 'HIPOLITO YRIGOYEN 5609', 'LANUS OESTE', '0800-666-5273', 'CARLOS', 'VIGILANCIA', 'Enviar dato faltante !! ', 'PREVENCION'),
(800, '20450', 'COOPERATIVA DE CREDITO FINARG LIMITADA', '20450', 'CRISTIAN', 'S', 'pablog@finarg.com.ar,pc@tarjetazero.com.ar ', 'LAPRIDA 156', 'LOMAS DE ZAMORA', '5277-7090', 'MARIANO', 'FINANCIERA', 'proveedores@finarg.com.ar ', 'PREVENCION'),
(801, '20465', 'ARIOTTI SANTORO JUAN PABLO', '20465', 'JUAN PABLO', 'S', 'sp@live.com.ar ', 'SAN LORENZO 560', 'AVELLANEDA ESTE', '5430-7473', 'JUAN PABLO', 'FAB.DIST ACC AUTOMOVILES', 'NO ', 'NO POSEE ART'),
(802, '20482', 'GUSTAVO FRANCOMAGRO & PABLO FRANCOMAGRO SH', '20482', 'GUSTAVO', 'S', 'gjfran@yahoo.com.ar,distribuidorahelasur@gmail.com', 'AV RIVADAVIA 865', 'VALENTIN ALSINA', '4228-0111', 'GUSTAVO-PABLO', 'DISTRIB.DE HELADOS', 'Enviar dato faltante !! ', 'MAPFRE'),
(803, '20494', 'LOGISTICA 4PL SRL', '20494', 'ALICIA', 'S', 'info@logistica4pl.com.ar,4plsa@speedy.com.ar,4plalicia_alfonso@speedy.com.ar', 'MANUEL ESTEVEZ 1459', 'DOCK SUD', '4201-2132', 'ALICIA-MIRIAM', 'Enviar dato faltante !!', '4plalicia_alfonso@speedy.com.ar ', 'LA CAJA'),
(804, '20497', 'TUBOSUD SA', '20497', 'ROBERTO', 'S', 'info@tubosur.com.ar ', 'SARGENTO CABRAL 1565', 'LANUS ESTE', '4204-1191/1168 /113-57500', 'ZUNINO ALBERTO', 'FABRICA', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(805, '2052', 'PINTURAS CITTA S.R.L.', '2052', 'LUIS', 'S', 'pinturascitta@hotmail.com ', 'AVDA GRAL MADARIAGA 1929', 'SARANDI', '4137-7320', 'LUIS FUSCO/MARIANA', 'PINTURERIA', 'pinturascitta@hotmail.com ', 'LA SEGUNDA'),
(806, '2053', 'CUCCI F. HUGO Y CUCCI DANIEL B', '2053', 'FERNANDO', 'S', 'marmoleriaescalada@yahoo.com.ar,cuccihugo@hotmail.com', 'PASTOR FERREIRA 3960', 'REMEDIOS DE ESCALADA', '4202-1858', 'SR HUGO/DANIEL', 'MARMOLERIA', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(807, '20601', 'MELLI PABLO', '20601', 'ROMINA', 'S', 'lyt@live.com.ar,rominalu79@gmail.com ', 'AV RIVADAVIA 1440 E/ PARAGUAY Y BRASIL', 'AVELLANEDA OESTE', '42088963/4218-6151', 'PABLO-ROMINA', 'METALURGICA', 'Enviar dato faltante !! ', 'SMG'),
(808, '20605', 'MATAFUEGOS FIRE SRL', '20605', 'DAMIAN', 'S', 'matafuegosfire@speedy.com.ar ', 'AV HIPOLITO YRIGOYEN 9827', 'LOMAS DE ZAMORA', '4292-4285/4243-9354', 'DAMIAN/RICARDO', 'MATAFUEGOS', 'Enviar dato faltante !! ', 'SMG'),
(809, '20662', 'ACM SERVICIOS (OCAMPO MIRTA GLORIA)', '20662', 'CYNTIA', 'S', 'delhugui06@hotmail.com, alexis.del_pozo@hotmail.com,cyntiadelpozo77@gmail.com', 'MARTIN FIERRO 215', 'WILDE', '4206-5251/150546343', 'CYNTIA-ALEXIS', 'SERVICIOS', 'Enviar dato faltante !! ', 'PREVENCION'),
(810, '20675', 'AMADO ANTONIA FERNANDA', '20675', 'DIEGO', 'S', 'distri.los.lecheros@gmail.com ', 'AV HIPOLITO YRIGOYEN 12047', 'LOMAS DE ZAMORA', '4298-6105', 'DIEGO-SILVANA', 'DISTRIB. FABRIC DE ALIMENTOS', 'Enviar dato faltante !! ', 'LA CAJA'),
(811, '20688', 'LORBEL NEUMATICOS( MIGUEL ANGEL LORENZO)', '20688', 'MIGUEL ANGEL', 'S', 'lorbel_neumaticos@yahoo.com.ar ', 'EVA PERON 2598', 'LANUS ESTE', '42495783', 'MIGUEL ANGEL LORENZO', 'NEUMATICOS', 'Enviar dato faltante !! ', 'QBE'),
(812, '20704', 'GRUPO ALPLAS PERFILES SRL', '20704', 'TULIA', 'S', 'tuliavegezzi@hotmail.com,ventasalplas@gmail.com,administracion@alplasperfiles.com.ar,tuliavegezzi@hotmail.com', 'GUARRACCINO 3053', 'LANUS ESTE', '4240-6553/6316/4247-1111', '4225-6681 antonio (noche)', 'PLASTICO', 'pedrofratini@yahoo.com.ar ', 'PROVINCIA'),
(813, '20751', 'MGV SEGURIDAD SRL', '20751', 'MARIA', 'S', 'administracion@jcacontrol.com,info@mgvseguridad.com.ar', 'ARISTOBULO DEL VALLE 190 1ER PISO', 'LANUS OESTE', '4241-7750', 'MARIA', 'SEGURIDAD', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(814, '2076', 'COELPLA SUDAMERICANA S.A.', '2076', 'SARA', 'S', 'info@coelpla.com.ar ', 'AYACUCHO 840', 'LANUS ESTE', '4247-1506', 'SARA', 'MATERIALES ELECTRICOS', 'Enviar dato faltante !! ', 'PREVENCION'),
(815, '20764', 'MARTI EDUARDO RAUL (FARMACIA MARIA DEL ROSARIO)', '20764', 'ALEJANDRA', 'S', 'st@ultrafarma.com.ar ', 'AV H. YRIGOYEN 3080', 'LANUS OESTE', '4241-0306/1541710214', 'ALEJANDRA COLS', 'FARMACIA', 'Enviar dato faltante !! ', 'LA CAJA'),
(816, '20778', 'SERVICIOS IBARRA SRL', '20778', 'SERVICIOS IBARRA SRL', 'N', 'ibarraoscaralberto@hotmail.com ', 'AGÜERO Y PIERRES CASA 15', 'VILLA DOMINICO', '4204-1309', 'RAMIRO-JUAN MANUEL', 'TRANSPORTES', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(817, '2079', 'LOM-DI DE LOMBA A. Y DIAZ J.C.', '2079', 'MARINA', 'S', 'lomdi_sh@yahoo.com.ar ', 'EVA PERON 2945', 'LANUS ESTE', '4246-5188', 'JUAN C. DIAZ', 'METALURGICA', 'Enviar dato faltante !! ', 'GALENO'),
(818, '20803', 'FLOWAR ARGENTINA SA', '20803', 'FLORENCIA', 'S', 'mariafserratore@flow-ar.com ', 'DEL VALLE IBERLUCEA 2983 2A', 'LANUS OESTE', '4225-8846', 'FLORENCIA', 'CONSTRUCCION', 'mariafserratore@flow-ar.com ', 'ART INTERACCION SA'),
(819, '20839', 'SANITARIOS VIAMONTE SA', '20839', 'SEBASTIAN', 'S', 'sanitariosviamonte@escape.com.ar,mlperasin@hotmail.com', 'VIAMONTE 1069', 'LANUS OESTE', '4115-9591', 'LAURA-SEBASTIAN', 'VENTA DE SANITARIOS', 'Enviar dato faltante !! ', 'PROVINCIA'),
(820, '20848', 'FERRETERIA EL DETALLE ( MARIA CRISTINA PAGNANELLI)', '20848', 'LORENA', 'S', 'lorena_furno33@yahoo.com.ar ', 'SARMIENTO 1299', 'CAPITAL FEDERAL', '3221-3206', 'LORENA', 'FERRETERIA', 'Enviar dato faltante !! ', 'GALENO'),
(821, '20866', 'TEXTIL HILDA SRL', '20866', 'LEONARDO', 'S', 'info@textilhilda.com.ar ', 'ESPORA 3173', 'BURZACO', '4299-9615 1549977165 CARL', 'CARLOS A. RODRIGUEZ', 'TEXTIL', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(822, '20868', 'VISUAL COMUNICATION S.A.', '20868', 'ROMINA', 'S', 'recepcion@dms.com.ar,administracion@dms.com.ar', 'SAN MARTIN 439 13 PISO OF. B', 'CAPITAL FEDERAL', '4302-4484', 'VERONICA FLLLONIER - DMS - ROMINA MAMANI', 'SERVICIOS', 'administracion@dms.com.ar ', 'FEDERACION PATRONAL'),
(823, '20903', 'GAJ WALTER DANIEL', '20903', 'MAURICIO', 'S', 'mauriciogaj@loxa.com.ar ', '9 DE JULIO 2291', 'LANUS ESTE', '4289-4398/1530351044/2', 'MAURICIO', 'CARNICERIA', 'mauriciogaj@loxa.com.ar ', 'PREVENCION'),
(824, '20924', 'DAO EMBALAJES SRL', '20924', 'DANIEL', 'S', 'dao@sion.com ', 'JUAN DOMINGO PERON 640', 'GERLI', '4247-2751 4247-8708', 'DANIEL ,ROLANDO ,CRISTIAN', 'EMBALAJES', 'dao@sion.com ', 'LA HOLANDO'),
(825, '20931', 'ESCUELA PRIMARIA GENERAL OLAZABAL', '20931', 'ESCUELA PRIMARIA GEN', 'N', 'jardindeinfantes@hotmail.com ', 'OLAZABAL 4606 Y OLIDEN', 'LANUS OESTE', '4240-8607 / 20450-953', 'GLADYS', 'EDUCATIVA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(826, '20935', 'LUZ DE LUNA SRL (EMERGENCIAS L)', '20935', 'LUZ DE LUNA SRL (EME', 'N', 'susyarena@yahoo.com.ar,jardindeluna@yahoo.com.ar', '2 DE MAYO 2819', 'LANUS OESTE', '4225-8711', 'SUSANA', 'COLEGIO', 'Enviar dato faltante !! ', 'PROVINCIA'),
(827, '20936', 'LUZ DE LUNA SRL (EMERGENCIAS A)', '20936', 'LUZ DE LUNA SRL (EME', 'N', 'susyarena@yahoo.com.ar,jardindeluna@yahoo.com.ar', 'ALSINA 273', 'AVELLANEDA', '4225-8711', 'CLAUDIA', 'COLEGIO', 'Enviar dato faltante !! ', 'PROVINCIA'),
(828, '20940', 'PANADERIA Y PANIFICADORA MIANCO SRL', '20940', 'PAOLA', 'S', 'paolana1981@yahoo.com.ar ', 'AV HIPOLITO YRIGOYEN 4499', 'LANUS OESTE', '4241-1002', 'PAOLA ACOSTA', 'CONFITERIA', 'paolana1981@yahoo.com.ar ', 'GALENO'),
(829, '20942', 'ORIANA (OHANA NATALIA EMILIA)', '20942', 'NATALIA', 'S', 'nataliaohana@hotmail.com ', 'ESMERALDA 597', 'CAPITAL FEDERAL', '4328-4911/4326-1316', 'NATALIA - CLAUDIO', 'COMERCIO', 'nataliaohana@hotmail.com ', 'NO POSEE ART'),
(830, '2096', 'PORPORA HNOS. S.R.L.', '2096', 'SABRINA', 'S', 'sabrinap@porpora.com.ar,sbaruch@porpora.biz,info@porpora.com.ar', '25 DE MAYO 1224/40', 'LANUS OESTE', '4239-9000', 'CECILIA/SABRINA', 'AUTOPARTES', 'info@porpora.com.ar,ceciliap@porpora.biz ', 'PREVENCION'),
(831, '20961', 'INNOVATOR S.R.L.', '20961', 'EZEQUIEL', 'S', 'ezequiel@forpont.com,forpontpapas@gmail.com ', 'JUNCAL 1732', 'LANUS ESTE', '4225-5238', 'EZEQUIEL', 'PROC. VEGETALES', 'Enviar dato faltante !! ', 'SMG'),
(832, '2100', 'INDUSTRIAS MADEPAR S.R.L.', '2100', 'JOSE', 'S', 'ventas@indumadepar.com.ar ', 'ANATOLE FRANCE 851', 'LANUS ESTE', '4249-1655', 'Enviar dato faltante !!', 'METALURGICA', 'Enviar dato faltante !! ', 'GALENO'),
(833, '21008', 'PETRIZZO HECTOR MARCOS', '21008', 'HECTOR', 'S', 'petrizzo@ciudad.com.ar ', 'AZCUENAGA 267', 'TURDERA', '4231-2822/ 1544790217', 'HECTOR', 'ESTUDIO', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(834, '21033', 'CONSORCIO DE PROPIETARIOS AV MAIPU 1064/66/70', '21033', 'IVANA', 'S', 'info@bmasbadm.com.ar ', '25 DE MAYO 574 1ER PISO OF 6', 'SAN ISIDRO', '4747-7027/7432-4911', 'ROMINA', 'CONSORCIO', 'Enviar dato faltante !! ', 'NO POSEE ART'),
(835, '210426', 'METALURGICA INGENIERIA Y DECORACION SRL', '210426', 'PATRICIA', 'S', 'devizziop@yahoo.com.es ', 'OMBU 1790', 'BURZACO', '5235-9379/9386', 'PATRICIA', 'METALURGICA', 'mailen@midsrl.com.ar ', 'INTERACCION - COLONIA SUIZA'),
(836, '210432', 'AGU ANA MARIA', '210432', 'ANA MARIA', 'S', 'mga_marin@hotmail.com ', 'GOB UGARTE 3631', 'REMEDIOS DE ESCALADA OESTE', '4276-3310', 'ALBIN MARINA', 'ODONTOLOGIA', 'Enviar dato faltante !! ', 'NO POSEE ART'),
(837, '21051', 'SPITAL HERMANOS SRL', '21051', 'GLADYS', 'S', 'ssande@spitalhnos.com,gmurillo@spitalhnos.com,jinsenga@spitalhnos.com', 'CORONEL LUNA 607', 'VALENTIN ALSINA', '4208-0664', 'SELVA-GISELLE', 'TRANSPORTE', 'administracion@spitalhnos.com ', 'CAMINOS PROTEGIDOS ART SA');
INSERT INTO `Usuarios` (`id`, `Codigopal`, `Nombrepal`, `Clave`, `Usuario`, `Activo`, `Correo`, `Domicilio`, `Localidad`, `Telefono`, `Contacto`, `Actividad`, `Correop`, `Nombart`) VALUES
(838, '2109', 'CIA DISTRIBUIDORA ACONCAGUA SR', '2109', 'NICOLAS', 'S', 'aconcaguasrl@hotmail.com ', 'BALBIN 2161', 'VALENTIN ALSINA', '4209-5229', 'GERONIMO/IGNACIO', 'DIST. DE ART.EM', 'Enviar dato faltante !! ', 'LA CAJA'),
(839, '21096', 'E E MEDIA NRO 7', '21096', 'FABIAN', 'S', 'ees7lanus@gmail.com,media7lanus@yahoo.com ', 'JUNCAL 2436', 'LANUS ESTE', '4225-7880', 'FABIAN-VERONICA', 'EDUCATIVA', 'Enviar dato faltante !! ', 'Enviar dato faltante !!'),
(840, '21099', 'TRANSPORTES HP', '21099', 'HERMINIA', 'S', 'transporteshp@hotmail.com ', 'RIVADAVIA 4935', 'RAFAEL CALZADA', '4211-3426', 'HERMINIA PAEZ', 'TRANSPORTE', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(841, '21108', 'THE NEWPORT SA', '21108', 'GLADYS', 'S', 'ssande@spitalhnos.com ', 'GRAL LUNA 607', 'VALENTIN ALSINA', '4208-0664', 'SELVA-GISELLE', 'TRANSPORTE', 'administracion@spitalhnos.com ', 'CAMINOS PROTEGIDOS ART SA'),
(842, '21109', 'TRANS CRIO SRL', '21109', 'GLADYS', 'S', 'ssande@spitalhnos.com,gmurillo@spitalhnos.com ', 'CNEL LUNA 607', 'VALENTIN ALSINA', '4208-0664', 'SELVA-GISELLE', 'TRANSPORTE', 'administracion@spitalhnos.com ', 'CAMINOS PROTEGIDOS ART SA'),
(843, '2111', 'TRANSPORTES HORIZONTES SRL', '2111', 'OSCAR', 'S', 'thorizontes@gmail.com ', 'GAEBELER 1070', 'LANUS ESTE', '4-240-5332', 'PAILOS, OSCAR', 'TRANSPORTE', 'Enviar dato faltante !! ', 'QBE'),
(844, '21122', 'PIAZZON MODESTO JOSE', '21122', 'MODESTO', 'S', 'modespiazzon@yahoo.com.ar ', 'GODOY CRUZ 118 2A', 'BANFIELD', '4202-9569/ 1569867001', 'MODESTO', 'TRANSPORTE', 'Enviar dato faltante !! ', 'GALENO'),
(845, '21133', 'BATISTA ANDREA', '21133', 'ANDREA', 'S', 'south_clean@hotmail.com ', 'AV MITRE 686 PISO 4 OF 41', 'AVELLANEDA ESTE', '4201-0614/4201-0596', 'BATISTA ANDREA', 'LIMPIEZA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(846, '21139', 'SOLARBASIC SRL', '21139', 'SOLARBASIC SRL', 'N', 'mfarias@generac.com.ar,puchuh@generac.com.ar ', 'CRISOLOGO LARRALDE 1801', 'AVELLANEDA ESTE', '4203-1722', 'HUGO', 'METALURGICA', 'Enviar dato faltante !! ', 'SMG'),
(847, '2116', 'SOCOROMA S.A.', '2116', 'LORENA', 'S', 'ssocoroma@hotmail.com ', 'CENTENARIO URUGUAYO 979', 'LANUS ESTE', '5648-7835//1565860631', 'LORENA/FRANCISCO', 'EST. DE SERV.', 'Enviar dato faltante !! ', 'LA CAJA'),
(848, '21184', 'SAPIA BIELER FABIANA', '21184', 'GRACIELA', 'S', 'graestmed@yahoo.com.ar ', 'MEEKS 32 PLANTA ALTA', 'LOMAS DE ZAMORA', '1563681034', 'GRACIELA', 'ENSEÑANZA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(849, '21185', 'SAPIA CLAUDIA NANCY', '21185', 'GRACIELA', 'S', 'graestmed@yahoo.com.ar ', '9 DE JULIO 1208', 'LANUS ESTE', '1563681034', 'GRACIELA', 'ENSEÑANZA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(850, '21187', 'MEDINA GRACIELA ESTELA', '21187', 'GRACIELA', 'S', 'graestmed@yahoo.com.ar ', 'AV HIPOLITO YRIGOYEN 419', 'QUILMES', '1563681034', 'GRACIELA', 'ENSEÑANZA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(851, '21190', 'CONCEPTO EDUCATIVO SRL', '21190', 'GRACIELA', 'S', 'graestmed@yahoo.com.ar ', 'AV HIPOLITO YRIGOYEN 4603', 'LANUS OESTE', '1563081034', 'GRACIELA MEDINA/ CLAUDIA', 'ENSEÑANZA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(852, '21229', 'CLAUDIA LILIANA VAZQUEZ', '21229', 'CLAUDIA LILIANA VAZQ', 'N', 'jcvsuelas@ciudad.com.ar,jcvsuelas@speedy.com.ar', 'PILCOMAYO 2357', 'VALENTIN ALSINA', '4262-2876/15-41821892', 'JOSE LUIS', 'FABRICA DE SUELAS', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(853, '21243', 'NEOLOGISTICA SRL', '21243', 'NORMA', 'S', 'neologisticasrl@gmail.com ', 'SALVADOR DEBENEDETI 636', 'SARANDI', '4265-0162', 'NORMA', 'TRANSPORTE', 'Enviar dato faltante !! ', 'PROVINCIA'),
(854, '2125', 'MELINKA SRL', '2125', 'DANIEL', 'S', 'melinka373@hotmail.com ', 'CNO.GRAL.BELGRANO 6602', 'WILDE', '5291-3053', 'LORENA RIVERO', 'EST. DE SERV.', 'Enviar dato faltante !! ', 'SMG'),
(855, '21268', 'DANPO SRL', '21268', 'ALBERTO', 'S', 'sounio@live.com.ar ', 'AV SAN MARTIN 945', 'LANUS OESTE', '4208-8570/ 1530680545', 'ALBERTO RAMOS/ FERNANDO-ADRIANA', 'TEXTIL', 'Enviar dato faltante !! ', 'GALENO'),
(856, '21283', 'CONSORCIO DE PROPIETARIOS AUSTRAL XVI', '21283', 'ALEJANDRA', 'S', 'libra_administracion@hotmail.com ', 'IBERLUCEA 2927', 'LANUS OESTE', '4241-6665/ 1532827230', 'ALEJANDRA CARRAL', 'CONSORCIO', 'Enviar dato faltante !! ', 'NO POSEE ART'),
(857, '21299', 'TINARI SA', '21299', 'MARIANELA', 'S', 'marianelatinari@hotmail.com,tpftinari@hotmail.com', 'URIARTE 1089', 'REMEDIOS DE ESCALADA', '4242-4351', 'MARIANELA', 'ESTACION DE SERVICIO', 'Enviar dato faltante !! ', 'LA CAJA'),
(858, '21309', 'SCHWARZ MARIANA ALEJANDRA', '21309', 'MARIANA', 'S', 'marianaaschwarz@hotmail.com ', 'MAIPU 224', 'BANFIELD ESTE', '4242-0682/ 1566817146', 'MARIANA', 'COMERCIO', 'Enviar dato faltante !! ', 'PROVINCIA'),
(859, '21323', 'TRANSFORMADORES DEL SUR SRL', '21323', 'LEANDRO', 'S', 'transformadoresdelsur@gmail.com ', 'C. URUGUAYO 1599', 'VILLA DOMINICO', '42463002- 42469911', 'CLAUDIA-LEANDRO', 'METALURGICA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(860, '21358', 'COMBUSTIBLES GODOY CRUZ SRL', '21358', 'GUILLERMO', 'S', 'petrobrasbanfield@yahoo.com.ar ', 'GODOY CRUZ 198', 'BANFIELD', '4242-4234', 'MIGUEL ANGEL-GUILLERMO', 'ESTACION DE SERVICIO', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(861, '21422', 'ROMEMI MATERIALES ELECTRICOS SRL', '21422', 'ROMINA', 'S', 'romemi@speedy.com.ar ', 'GABRIEL MIRO 897', 'LOMAS DE ZAMORA OESTE', '4267-0058', 'ROMINA-MICAELA- JORGE', 'VENTA PROD.ELECT', 'Enviar dato faltante !! ', 'LA CAJA'),
(862, '21437', 'ARGUMAL SA', '21437', 'CAROLINA', 'S', 'argumal@gncplus.com ', '25 DE MAYO 2692', 'LANUS OESTE', '4978-2325/1569960036', 'MARCOS', 'ESTACION DE SERVICIO', 'Enviar dato faltante !! ', 'SMG'),
(863, '21492', 'COLEGIO SIDNEY SOWELL (MEDICINA LABORAL)', '21492', 'ROSANA', 'S', 'escuelasidneysowell@speedy.com.ar,rosanafp@hotmail.com,nuevomps@gmail.com,carballo.h.david@gmail.com', 'EL PLUMERILLO 769', 'BANFIELD OESTE', '4286-8375', 'ROSANA', 'EDUCATIVA', 'Enviar dato faltante !! ', 'GALENO'),
(864, '21498', 'MAHEROGA ALLEDAFE SRL', '21498', 'JORGE', 'S', 'jlopardo@maheroga.com.ar,nlopardo@maheroga.com.ar', 'R.ESCALADA DE SAN MARTIN 3686', 'VALENTIN ALSINA', '4228-6360/4878-2148', 'JORGE-NATALIA', 'METALURGICA', 'Enviar dato faltante !! ', 'LA CAJA'),
(865, '21503', 'MOL & BAND SA', '21503', 'MARTIN', 'S', 'ventasmolband@gmail.com ', 'CHILE 361', 'AVELLANEDA OESTE', '4208-0220', 'MARTIN- NICOLAS', 'INSUMOS PARA PANADERIA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(866, '2151', 'OFIPEL S.A.', '2151', 'RUBEN', 'S', 'ofipel@uolsinectis.com.ar ', '25 DE MAYO 1571', 'LANUS O', '4-249-7879/4225-1803', 'RUBEN PASCUALINO', 'IMPRENTA', 'ofipel@uolsinectis.com.ar ', 'BERKLEY INTERNATIONAL'),
(867, '21512', 'CONSORCIO CORDOBA 2031', '21512', 'JORGE', 'S', 'ingjorgegomez@gmail.com ', 'CORDOBA 2031', 'LANUS ESTE', 'Enviar dato faltante !!', 'JORGE GOMEZ SIRIMARCO', 'CONSORCIO', 'Enviar dato faltante !! ', 'NO POSEE ART'),
(868, '21523', 'UNIKE GROUP SA', '21523', 'HERNAN', 'S', 'info@waterplast.com.ar, hernan@waterplast.com.ar', 'AV SAN MARTIN 2768', 'LANUS OESTE', '4225-1531', 'GABRIELA-HERNAN', 'METALURGICA', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(869, '2154', 'LUIS FEIJOO', '2154', 'LUIS', 'S', 'papelera_feijoo@fibertel.com.ar ', 'RESISTENCIA 1112', 'LANUS O', '4-240-8502', 'LUIS FEIJOO', 'PAPELERA', 'Enviar dato faltante !! ', 'SMG'),
(870, '21557', 'UNION 24 HORAS SA', '21557', 'ROXANA', 'S', 'roxanabelsito@union24hs.com,mariano5848@hotmail.com', 'AV FERNANDEZ DE LA CRUZ 3570/80', 'CAPITAL FEDERAL', '5291-4893/4919-4393', 'ROXANA BELSITO', 'AUXILIO', 'Enviar dato faltante !! ', 'PROVINCIA'),
(871, '21628', 'MYKINES SA', '21628', 'MARTA', 'S', 'impresos-macri@ertach.com.ar ', 'SUIPACHA 3368', 'REMEDIOS DE ESCALADA ESTE', '2058-7674', 'MARTA', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'PROVINCIA'),
(872, '21634', 'ALDO FRANCISCO MARTINEZ', '21634', 'DIANA', 'S', 'arte_diana48@yahoo.com.ar ', 'BLANCO ESCALADA 2384', 'LANUS ESTE', '4225-6606/1124236060', 'ALDO-DIANA', 'METALURGICA', 'Enviar dato faltante !! ', 'LA SEGUNDA'),
(873, '21666', 'FARMACIA UOM BUSTAMANTE SCS', '21666', 'UOM', 'S', 'acomandatore@uomlanus.com.ar,pgonta@uomlanus.com.ar', 'AV HIPOLITO YRIGOYEN 2331', 'GERLI', '4357-9050', 'ALEJANDRA COMANDATORE', 'FARMACIA', 'administracion@uomlanus.com.ar ', 'QBE'),
(874, '2167', 'LUCONFUE S.A.', '2167', 'MIRTA', 'S', 'info@protectpersons.com.ar ', 'CNO. GRAL. BELGRANO 1735', 'LANUS ESTE', '4204-8133/5391', 'ANDRES BRESCIANI', 'FCA. MATAFUEGOS', 'Enviar dato faltante !! ', 'GALENO'),
(875, '21714', 'POGENZO SA', '21714', 'PATRICIA', 'S', 'claudiobelloar@yahoo.com.ar,pogenzo@hotmail.com', 'AV MEEKS 53 LOCAL 1', 'LOMAS DE ZAMORA OESTE', '4292-8574/ 4288-3133/24 C', 'PATRICIA', 'COMERCIO', 'Enviar dato faltante !! ', 'LA CAJA'),
(876, '2173', 'DIETETICA CIENTIFICA SACIFI', '2173', 'MARIEL', 'S', 'administracion@dieteticacientifica.com.ar,guillermomartin@dieteticacientifica.com.ar', 'MURATURE 564', 'R. DE ESCALADA ESTE', '4288-1700/0799/0686', 'MARIEL-HUGO MARTIN', 'MOVIMED', 'comprasdc@dieteticacientifica.com.ar ', 'ART INTERACCION SA'),
(877, '2174', 'MIGUEL ABAD S.A.', '2174', 'DAVID', 'S', 'rrhhabad@miguelabad.com.ar,proveedores@miguelabad.com.ar,abad@miguelabad.com.ar', 'TIMBO 2882 PQUE INDUSTRIAL BURZACO', 'BURZACO', '4233-0050', 'MIGUEL Y HUGO ABAD', 'METALURGICA', 'proveedores@miguelabad.com.ar ', 'ART INTERACCION SA'),
(878, '2181', 'ARGENPACK CORRUGADOS S.A.', '2181', 'VIVIANA', 'S', 'admplatanos@argenpack.com,silvia@argenpack.com,adm@argenpack.com', 'FLORENCIO VARELA 1170', 'LANUS ESTE', '4241-6501-225-0762', 'VIVIANA', 'CARTONES CORR.', 'silvia@argenpack.com,recepcion@argenpack.com ', 'PREVENCION'),
(879, '2182', 'LUEY GAS S.A.', '2182', 'LUEYGAS', 'S', 'lueygas@hotmail.com ', 'CNO. GRAL. BELGRANO Y 838', 'SOLANO', '4280-1671 4250-6854', 'ANDRES MORENO/ANDREA', 'EST. DE SERV.', 'Enviar dato faltante !! ', 'LA CAJA'),
(880, '21831', 'CONSORCIO DEL VALLE IBERLUCEA 3022/26', '21831', 'CONSORCIO DEL VALLE', 'N', 'lasapropiedades@hotmail.com ', 'DEL VALLE IBERLUCEA 3022', 'LANUS OESTE', '4249-7790', 'Enviar dato faltante !!', 'CONSORCIO', 'administracionsrl@lasaprop.com ', 'BERKLEY INTERNATIONAL'),
(881, '21851', 'GRUPO CODIGO ROJO SRL', '21851', 'LUCAS', 'S', 'lucas.abraham@migaclub.com.ar ', 'BERNARDINO RIVADAVIA 830', 'AVELLANEDA OESTE', '4218-3717', 'LUCAS ABRAHAM', 'SANDWICHERIA', 'Enviar dato faltante !! ', 'NO POSEE ART'),
(882, '21885', 'EDILOT PAIS SOCIEDAD DE RESPONSABILIDAD LIMITADA', '21885', 'MAXIMILIANO', 'S', 'mracciatti@edilot.com.ar,marioracciatti@edilot.com.ar', 'MARCELO T DE ALVEAR 445 4', 'CAPITAL FEDERAL', '5031-4505', 'MAXIMILIANO RACCIATTI', 'LOGISTICA', 'Enviar dato faltante !! ', 'QBE'),
(883, '21889', 'CONS.DE PROP.AV HIPOLITO YRIGOYEN 6661/67 R ESCALA', '21889', 'FAVIO', 'S', 'adm.azcui@gmail.com ', 'AV HIPOLITO YRIGOYEN 6661', 'REMEDIOS DE ESCALADA', '4249-6479', 'PEDRO FAVIO AZCUI', 'CONSORCIO', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(884, '21919', 'HOGAR PARA ANCIANOS SAN ROQUE', '21919', 'ROQUE', 'S', 'hogarsroque@hotmail.com ', 'MONTEAGUDO 1823', 'BANFIELD OESTE', '4242-2901', 'DR CESAR ESTEVEZ/IVANA', 'HOGAR DE ANCIANOS', 'Enviar dato faltante !! ', 'QBE'),
(885, '21926', 'PRECINTOS BERKO SA', '21926', 'NOEMI', 'S', 'noesa@berko.com.ar ', 'URUGUAY 271', 'AVELLANEDA OESTE', '4228-0053', 'NOEMI SAVONE', 'METALURGICA', 'Enviar dato faltante !! ', 'NO POSEE ART'),
(886, '21958', 'RUBEN ANASTACIO TEVEZ', '21958', 'MONICA', 'S', 'r.a.tevez@hotmail.com ', 'CAZON 1321', 'LANUS ESTE', '4220-7705', 'MONICA', 'CONSTRUCCION', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(887, '21966', 'INDUSTRIAS WASSER KRAFT SA', '21966', 'MARIO', 'S', 'proveedores@tecnoenvase.com.ar,tecnoenvase@house.com.ar', 'PINTO 2200', 'BURZACO', '4248-3784 4242-8889', 'MARIO', 'PLASTICO', 'proveedores@tecnoenvase.com.ar ', 'PROVINCIA'),
(888, '21997', 'JENIC SEGURIDAD SRL', '21997', 'JENNIFER', 'S', 'jenicseguridad@yahoo.com.ar ', 'AV LOS QUILMES 604', 'BERNAL', '4251-2581', 'JENNIFER', 'SEGURIDAD', 'Enviar dato faltante !! ', 'SMG'),
(889, '22003', 'GALLARDO GABRIEL DARIO', '22003', 'GABRIEL', 'S', 'gallardodario@fibertel.com.ar,claudia_rosanarodriguez@yahoo.com.ar', 'MANUEL CASTRO 733', 'LOMAS DE ZAMORA', '4220-5563', 'GABRIEL-CLAUDIA', 'DISTRIB. FABRIC DE ALIMENTOS', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(890, '22016', 'LOS FAMOSOS 1 SRL', '22016', 'LOS FAMOSOS 1 SRL', 'N', 'sandrytom@hotmail.com ', 'AV HIPOLITO YRIGOYEN 4215', 'LANUS OESTE', '15-54517173', 'RICARDO ECHEVERRY/ SANDRA', 'MAY/GOLOSINAS', 'Enviar dato faltante !! ', 'LA SEGUNDA'),
(891, '22023', 'OLIVERA MARTIN LUIS', '22023', 'ELIANA', 'S', 'ela_05@hotmail.com ', '3 DE FEBRERO 3746', 'REMEDIOS DE ESCALADA OESTE', '20658885', 'MARTIN LUIS OLIVERA', 'FAB.MAQUINAS PARA ENVASADOS', 'Enviar dato faltante !! ', 'LA CAJA'),
(892, '2204', 'SIS INGENIERIA SRL(ALARMAS X28', '2204', 'ALBERTO', 'S', 'azabala@sisingenieria.com.ar,administracion@sisingenieria.com.ar', 'HIPOLITO IRIGOYEN 2802', 'LANUS OESTE', '4225-7500/1165186006', 'ING ZABALA/ ANALIA ZABALA', 'SEGURIDAD', 'administración@sisingenieria.com.ar ', 'FEDERACION PATRONAL'),
(893, '22069', 'AVICOLA LA GAUCHA SA', '22069', 'MARIANA', 'S', 'avicolalagaucha@yahoo.com.ar ', 'ALTE SOLIER 4567', 'VILLA DOMINICO', '4207-8041 1549980324', 'PABLO-MARIANA', 'AVICOLA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(894, '22095', 'ARCE CRISTIAN ARIEL', '22095', 'CRISTIAN', 'S', 'perforaciones_arce@yahoo.com.ar ', 'CALLE 809 NRO 1934', 'QUILMES O', '4212-2669/156274-0104', 'ARCE CARLOS DANIEL', 'PERFORACIONES', 'Enviar dato faltante !! ', 'PREVENCION'),
(895, '22098', 'LEFRAMA SA', '22098', 'LEFRAMA SA', 'N', 'carlosbustamante@transporteslu-na.com.ar,fabianlucero@transporteslu-na.com.ar', 'SARGENTO CABRAL 681', 'GERLI ESTE', '4203-9889', 'CARLOS BUSTAMANTE', 'TRANSPORTE', 'Enviar dato faltante !! ', 'QBE'),
(896, '22152', 'REHOBOT SRL', '22152', 'MAGALI', 'S', 'magalisosa35@gmail.com ', 'MARRASPIN 4149', 'REMEDIOS DE ESCALADA OESTE', '4248-9141', 'MAGALI SOSA', '16509', 'magalisosa35@gmail.com ', 'ASOCIART A.R.T.'),
(897, '2222', 'JAU SOL SRL EN FORMACION', '2222', 'ALEJANDRO', 'S', 'javsolsrl@yahoo.com.ar ', 'YERBAL 3650', 'LANUS OESTE', '4262-7071', 'LUIS/ANDREA', 'METALURGICA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(898, '22231', 'LOVE THIS SRL', '22231', 'SABRINA', 'S', 'administracion@lovethis.com.ar ', 'AVELLANEDA 3104', 'CAPITAL FEDERAL', '4672-1358 int 207', 'SABRINA', 'TEXTIL', 'Enviar dato faltante !! ', 'LA CAJA'),
(899, '22232', 'B2B SRL', '22232', 'SABRINA', 'S', 'administracion@lovethis.com.ar ', 'BAUNES 2150 7A', 'CAPITAL FEDERAL', '4372-1358 INT 207', 'SABRINA', 'TEXTIL', 'Enviar dato faltante !! ', 'LA CAJA'),
(900, '22233', 'L&L GROUP SRL', '22233', 'SABRINA', 'S', 'administracion@lovethis.com.ar ', 'BAUNES 2150 7A', 'CAPITAL FEDERAL', '4672-1358 INT 207', 'SABRINA', 'TEXTIL', 'Enviar dato faltante !! ', 'LA CAJA'),
(901, '22278', 'INDUSTRIAS RONDEAU SRL', '22278', 'MARCELO', 'S', 'iprosrl@hotmail.com ', 'RONDEAU 2168', 'LANUS ESTE', '4230-2389', 'SERGIO/FERNANDO', 'PLASTICO', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(902, '22308', 'BARUTTA FABIAN GUILLERMO', '22308', 'MARIA', 'S', 'divina-11@hotmail.com.ar ', '9 DE JULIO 1186', 'LANUS ESTE', '4292-3293', 'MARIA', 'COMERCIO', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(903, '22309', 'CIACCIARELLI DELIA ANTONIA', '22309', 'MARIA', 'S', 'divina-11@hotmail.com.ar ', 'LAPRIDA 105', 'LOMAS DE ZAMORA', '4292-3293', 'MARIA', 'COMERCIO', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(904, '22310', 'DEKMAK ALEX ELIAS-PIAGGIO PEDRO LUIS SH', '22310', 'MARIA', 'S', 'divina-11@hotmail.com.ar ', 'ESPAÑA 218', 'LOMAS DE ZAMORA', '4292-3293', 'MARIA', 'COMERCIO', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(905, '22316', 'VITUCA TRANSPORTE S.R.L', '22316', 'YAMILA', 'S', 'vitucasrl@hotmail.com,monti_yamila@hotmail.com', 'MANUELA PEDRAZA 2839', 'LANUS OESTE', '4218-2813', 'YAMILA MONTI', 'TRANSPORTE', 'Enviar dato faltante !! ', 'PREVENCION'),
(906, '22326', 'GC FABRICANTES SRL', '22326', 'GUSTAVO', 'S', 'gcfabricantes@fibertel.com.ar ', 'PARAGUAY 716', 'AVELLANEDA OESTE', '4209-4040', 'GUSTAVO GRECO', 'METALURGICA', 'gcfabricantes@fibertel.com.ar ', 'GALENO'),
(907, '22371', 'GRUPO SAMAGA SA', '22371', 'CRISTINA', 'S', 'alsaga@yahoo.com.ar ', '25 DE MAYO 502', 'LANUS OESTE', '4240-6272/4225-0049', 'CRISTINA', 'LUBRICENTRO', 'Enviar dato faltante !! ', 'LA CAJA'),
(908, '22372', 'PARAMIDANO SRL', '22372', 'GABRIELA', 'S', 'grivada2006@yahoo.com.ar ', 'GUTEMBERG 2022', 'AVELLANEDA', '4208-7803-1550395927', 'GABRIELA RIVADA', 'PLASTICO', 'Enviar dato faltante !! ', 'QBE'),
(909, '22401', 'PEREZ ELBIO BENEDICTO', '22401', 'ELBIO', 'S', 'renaultsanalberto@hotmail.es ', 'PTE PERON 2354', 'BANFIELD OESTE', '4285-2404', 'SILVIA-SEBASTIAN', 'REPUESTOS AUTOS', 'Enviar dato faltante !! ', 'GALENO'),
(910, '2244', 'HELAMONT. SA', '2244', 'NATALIA', 'S', 'administracion@helamontsa.com.ar,natalia@helamontsa.com.ar', 'EVA PERON 3050', 'LANUS ESTE', '4230-1300', 'NATALIA', 'DIST.DE HELADOS', 'natalia@helamontsa.com.ar ', 'QBE'),
(911, '22448', 'JAIDIB SA', '22448', 'IVANA', 'S', 'ivana.merida@gmail.com, ndjorgecazon@gmail.com, rrhh@novadenim.com.ar', 'LIBERTAD 434 PISO 11 DTO 118', 'CAPITAL FEDERAL', '4918-0041', 'IVANA', 'TEXTIL', 'Enviar dato faltante !! ', 'LA CAJA'),
(912, '2245', 'ITAL PAVON SA', '2245', 'ADRIANA', 'S', 'italpavon@hotmail.com ', 'HIPOLITO IRIGOYEN 345', 'AVELLANEDA', '4222-7074', 'JOSE CARDILLO', 'EST. DE SERV.', 'Enviar dato faltante !! ', 'GALENO'),
(913, '22481', 'OSCAR NATALIO LEFOSSE', '22481', 'OSCAR NATALIO LEFOSS', 'N', 'megaguard@hotmail.com,admmegaguardseguridad@gmail.com', 'EVA PERON 1072', 'LANUS ESTE', '4240-5995/15-4991-8052', 'OSCAR-LAURA', 'SEGURIDAD', 'Enviar dato faltante !! ', 'MAPFRE'),
(914, '2251', 'AMICO CARLOS', '2251', ' CARLOS', 'S', 'carlosa64@live.com.ar ', 'BELGRANO 1516', 'BANFIELD', '4248-2167/155809-9488', 'CARLOS AMICO', 'BOUTIQUE', 'Enviar dato faltante !! ', 'SMG'),
(915, '22513', 'LA HOLANDO PLENUS', '22513', 'LA HOLANDO PLENUS', 'N', 'artmedcasos@holansud.com.ar ', 'SARMIENTO 309 5TO PISO', 'CIUDAD AUTONOMA DE BUENOS AIRE', '4321-7600', '', 'ART', 'Enviar dato faltante !! ', 'PLENUS ART'),
(916, '22524', 'COMALINDO SA', '22524', 'MARCELO', 'S', 'info@comalindo.com.ar,piabartalini@hotmail.com', 'AV ENTRE RIOS 1164', 'AVELLANEDA OESTE', '4368-7337', 'MARCELO SCHAPIRA', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'NO POSEE ART'),
(917, '22530', 'GRANJA LOS HERMANOS (SARRAILH RODRIGO)', '22530', 'RODRIGO', 'S', 'rsarrailh01@hotmail.com ', 'MURGIONDO 667', 'VALENTIN ALSINA', '4209-3543', 'RODRIGO', 'CARNICERIA', 'Enviar dato faltante !! ', 'PREVENCION'),
(918, '22560', 'LEBEBOT S.R.L', '22560', 'YAIR', 'S', 'a.laje@lebebot.com.ar,proveedores@lebebot.com.ar', 'CHACO 1341', 'LANUS OESTE', '4262-3526', 'ALEJANDRO LAJE', 'FAB. DE ENVASES PLASTICOS', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(919, '2257', 'SPERNANZONI DANIEL ENRIQUE', '2257', ' DANIEL', 'S', 'spernanzoni@gmail.com ', 'SAN CARLOS 2250', 'WILDE', '4220-7474/4246-6182', 'MONICA/DANIEL', 'DISTRIBUIDORA', 'Enviar dato faltante !! ', 'PREVENCION'),
(920, '22571', 'GUTIERREZ PABLO DAMIAN', '22571', 'PABLO', 'S', 'drlimpio@hotmail.com.ar ', 'AV HIPOLITO YRIGOYEN 4374', 'LANUS OESTE', '1556101456/1565058172', 'GUTIERREZ PABLO', 'PERFUMERIA', 'Enviar dato faltante !! ', 'NO POSEE ART'),
(921, '22583', 'PLASTICOS ESPARTILLAR SA', '22583', 'RAUL', 'S', 'estruplas@speedy.com.ar ', 'PERU 1937', 'AVELLANEDA OESTE', '4218-0885', 'RAUL', 'PLASTICO', 'Enviar dato faltante !! ', 'QBE'),
(922, '22588', 'RUBIN SONIA SUSANA', '22588', 'SONIA', 'S', 'estudiosanchezquian@gmail.com ', '9 DE JULIO 1899', 'LANUS ESTE', '4225-1867/4249-5779', 'MARIANO-SONIA', 'HELADERIA', 'Enviar dato faltante !! ', 'PREVENCION'),
(923, '2259', 'MICRO OMNIBUS 45 SA', '2259', 'LUCIANA', 'S', 'personal@mo45.com.ar,linea45@mo45.com.ar,control@mo45.com.ar,siniestros@mo45.com.ar', '14 DE JULIO 4100', 'R.DE ESCALADA OESTE', '4247-4834/7090', 'HECTOR CASTRO', 'TRANSPORTE DE PASAJEROS', 'Enviar dato faltante !! ', 'PROVINCIA'),
(924, '22601', 'MEIRE SUSANA MARIEL', '22601', 'SUSANA', 'S', 'administracion@exhibidoresbertone.com.ar,ventas@exhibidoresbertone.com.ar,bertonesrl@hotmail.com', 'JOSE MARMOL 1033', 'LANUS OESTE', '4-249-9680', 'JULIO BERTONE', 'METALURGICA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(925, '22622', 'SALINA HECTOR OMAR', '22622', 'SALINA HECTOR OMAR', 'N', 'hectoryliliana@yahoo.com.ar ', 'GRAL. MADARIAGA 2195', 'AVELLANEDA', '4204-7158', 'LILIANA', 'BATERIAS', 'Enviar dato faltante !! ', 'SMG'),
(926, '22645', 'FLAMY PERFORRAP SRL', '22645', 'FLAMY PERFORRAP SRL', 'N', 'silviabtesta@gmail.com ', 'CALLE 77 ENTRE 125 Y 126 NRO 2554', 'BERAZATEGUI', '02229-455780/153634-0752/', 'JAVIER-ROQUE', 'PERFORACIONES', 'Enviar dato faltante !! ', 'GALENO'),
(927, '22658', 'SUELAS SANXENXO SA', '22658', 'SUELAS SANXENXO SA', 'N', 'gastonaguin@hotmail.com ', 'MURGIONDO 3395 LANUS O', 'LANUS OESTE', '4228-7169', 'INES-GASTON', 'BASES DE CALZADO', 'Enviar dato faltante !! ', 'GALENO'),
(928, '22665', 'CONSORCIO DE PROP. 2 DE MAYO', '22665', 'CONSORCIO DE PROP. 2', 'N', 'libra_administracion@hotmail.com ', '2 DE MAYO 2812', 'LANUS OESTE', '1532827230/4241-6665', 'ALEJANDRA ADMINISTRACION', 'CONSORCIO', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(929, '22688', 'MICELI JORGE JAVIER (TECNOELECTRICA)', '22688', 'SONIA', 'S', 'administracion@tecno-electrica.com.ar ', 'CERVANTES SAAVEDRA 1165', 'ADROGUE', '4293-7256', 'SONIA', 'ELECTRICIDAD-MONTAJES', 'Enviar dato faltante !! ', 'PROVINCIA'),
(930, '22698', 'KROM ARGENTINA SA', '22698', 'EZEQUIEL', 'S', 'administracion@piazconveyor.com ', 'VILLA DE LUJAN 947', 'LANUS ESTE', '4225-9691', 'EZEQUIEL', 'FAB. DE ENVASES PLASTICOS', 'Enviar dato faltante !! ', 'PROVINCIA'),
(931, '22710', 'MD REPARACIONES SRL', '22710', 'ARIEL', 'S', 'info.mdreparaciones@gmail.com ', 'VIAMONTE 2776', 'LANUS OESTE', '4208-9302', 'JUAN CARLOS TENCONI-ARIEL NEIRA', 'MECANICA NAVAL', 'Enviar dato faltante !! ', 'PROVINCIA'),
(932, '22711', 'COMERCIAL AMERICANA SA', '22711', 'VERONICA', 'S', 'juancarlos@comercialamericana.com.ar ', 'PASAJE ALDAZ 327 E/CHACABUCO Y MONTES DE', 'AVELLANEDA ESTE', '4222-1910/1532931766', 'VERONICA', 'ALUMINIO', 'Enviar dato faltante !! ', 'SMG'),
(933, '22727', 'DE MITO ALBERTO HORACIO', '22727', 'DE MITO ALBERTO HORA', 'N', 'hierrosaldemi@hotmail.com ', 'EJERCITO DE LOS ANDES 835', 'BANFIELD', '4286-3419', 'MARTA', 'METALURGICA', 'hierrosaldemi@hotmail.com ', 'PREVENCION'),
(934, '22753', 'GAMARGENIS S.R.L', '22753', 'DARIO', 'S', 'aquarinesrl@hotmail.com ', 'FRENCH 97 PISO 4 DTO C', 'AVELLANEDA', '4247-8200', 'DARIO ALARCON', 'DISTRIB. FABRIC DE ALIMENTOS', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(935, '2276', 'PROVINCIA ART S.A.', '2276', 'PROVART', 'S', 'region6@provart.com.ar,lmossetto@provart.com.ar,ggallo@provart.com.ar,ediaz@provart.com.ar,avisos@provart.com.', 'CARLOS PELLEGRINI 91 PB', 'CIUDAD AUTONOMA DE BUENOS AIRE', 'Enviar dato faltante !!', 'Enviar dato faltante !!', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'PROVINCIA COLONIA SUIZA'),
(936, '2277', 'ASOCIART ART S.A.', '2277', 'ASOCIART', 'S', 'autorizacionesbsas@asociart.com.ar,nabatte@asociart.com.ar,gaglione@asociart.com.ar', 'AV. L.N. ALEM 609 PB', 'CIUDAD AUTONOMA DE BUENOS AIRE', '4317-7400', 'Enviar dato faltante !!', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(937, '2278', 'BERKLEY INTERNATIONAL ART S.A.', '2278', 'BERKLEY', 'S', 'mledesma@berkley.com.ar,azanabria@berkley.com.ar', 'C.PELLEGRINI 1023 PISO 3', 'CIUDAD AUTONOMA DE BUENOS AIRE', '378-8000', 'Enviar dato faltante !!', 'Enviar dato faltante !!', 'fbarassi@berkley.com.ar ', 'BERKLEY INTERNATIONAL'),
(938, '2279', 'GALENO ART S.A.', '2279', 'GALENO', 'S', 'mendezma@consolidar.com.ar,espinolag@consolidar.com,penam@consolidar.com,', 'PIEDRAS 22 3° PISO', 'CIUDAD AUTONOMA DE BUENOS AIRE', '4348-5901/1240', '', 'ART', 'Enviar dato faltante !! ', 'GALENO'),
(939, '22790', 'AYB INTERNACIONAL SRL', '22790', 'MERCEDES', 'S', 'moterovieites@hotmail.com.ar ', 'MONTEVIDEO 248', 'CAPITAL FEDERAL', '4382-4118', 'MERCEDES-MANUEL', 'HOTEL', 'Enviar dato faltante !! ', 'LA CAJA'),
(940, '2280', 'QBE ART S.A.', '2280', 'QBE', 'S', 'no ', 'VERA 559', 'CIUDAD AUTONOMA DE BUENOS AIRE', '', '', 'ART', 'Enviar dato faltante !! ', 'NO POSEE ART'),
(941, '22800', 'TRAPAL SRL', '22800', 'CRISTINA', 'S', 'trapal@hotmail.com.ar ', 'HELGUERA 2194 PISO 6 DTO A', 'CAPITAL FEDERAL', '4286-3478 (FINCA SANTA FI', 'CLAUDIA', 'DISTRIBUIDORA DE BEBIDAS', 'compras@fincasantafilomena.com ', 'LA CAJA'),
(942, '2281', 'ASEGURADORA DE RIESGOS DE TRABAJO INTERACCION SA', '2281', 'INTERACCION', 'S', 'compras@fincasantafilomena.com ', 'SARMIENTO 2038', 'CIUDAD AUTONOMA DE BUENOS AIRE', 'Enviar dato faltante !!', 'Enviar dato faltante !!', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(943, '22815', 'PARDA MARIS SRL', '22815', 'ARIEL', 'S', 'eleusesh@hotmail.com ', 'FLORIDA 473', 'VALENTIN ALSINA', '4878-0951', 'ARIEL', 'CALZADOS', 'Enviar dato faltante !! ', 'LA CAJA'),
(944, '2282', 'LA CAJA ART A.R.T.', '2282', 'LA CAJA', 'S', 'erobles@sml.com.ar,spolidura@sml.com.ar,abritez@sml.com.ar,dllanos@sml.com.ar', 'ESMERALDA 1080 PISO 9', 'CIUDAD AUTONOMA DE BUENOS AIRE', '4854-8118', 'FITZ ROY 957 11-15', 'ART', 'pzabalza@sml.com.ar,abalmaceda.com.ar ', 'LA CAJA'),
(945, '2283', 'LIBERTY ART S.A.', '2283', 'LIBERTY', 'S', 'informacion_prestadores@libertyart.com.ar,mvillares@libertyart.com.ar', 'CORRIENTES 1826', 'CIUDAD AUTONOMA DE BUENOS AIRE', '4346 - 0550', '', 'ART', 'julieta.carranza@swissmedical.com ', 'SMG'),
(946, '2284', 'MAPFRE ACONCAGUA ART S.A.', '2284', 'MAPFREART', 'S', 'bidalov@mapfre.com.ar,veromm@mapfre.com.ar,tramitaciondesiniestros@mapfre.com.ar,arielrg@mapfre.com.ar', 'PIEDRAS 22 PB', 'CIUDAD AUTONOMA DE BUENOS AIRE', '4320-8800/8809 4320-6700', 'JORGELINA AFILIACION', 'ART', 'Enviar dato faltante !! ', 'MAPFRE'),
(947, '2285', 'QBE ARGENTINA ART', '2285', 'CNA', 'S', 'no ', 'AV LIBERTADOR 6350', 'CIUDAD AUTONOMA DE BUENOS AIRE', '4324-6700', 'SR. ALBERTO', 'ART', 'sdurante@qbe.com.ar ', 'QBE'),
(948, '22851', 'PANIFICADOS DE LA BOCA SRL', '22851', 'ESTELA', 'S', 'silvana_g_r@hotmail.com,nahuelbrito@hotmail.com', 'NECOCHEA 1122', 'CAPITAL FEDERAL', '4878-3029/28', 'ESTELA', 'PANIFICADORA', 'Enviar dato faltante !! ', 'LA HOLANDO'),
(949, '22862', 'MARIA DE LOS ANGELES LUONGO Y DANIELA LUONGO SH', '22862', 'DANIELA', 'S', 'casaluongo@hotmail.com ', 'A DEL VALLE 3360', 'LANUS OESTE', '4209-2699', 'MARIA DE LOS ANGELES', 'CORRALON MATER.', 'Enviar dato faltante !! ', 'QBE'),
(950, '2287', 'PREVENCION ART S.A.', '2287', 'PREVENCION', 'S', 'vvilloldo@prevencionart.com.ar,llefebre@prevencionart.com.ar', 'CORDOBA 1776', 'CIUDAD AUTONOMA DE BUENOS AIRE', '03493-428600', '', 'ART', 'ebussi@prevencionart.com.ar ', 'PREVENCION'),
(951, '2288', 'RESPONSAB. PATRONAL ART S.A.', '2288', 'RESPONSAB. PATRONAL', 'N', 'no ', 'CASTELLI 213', 'BAHIA BLANCA', '4342-7374/7801', 'MORENO 794 5 PISO', 'ART', 'Enviar dato faltante !! ', 'Enviar dato faltante !!'),
(952, '22888', 'CET VIDA SA', '22888', 'CET VIDA SA', 'N', 'cetcriprijenfe@gmail.com ', 'ENRIQUE FERNANDEZ 2274', 'LANUS OESTE', '2070-5194', 'PAULA- DIEGO', 'COMUNIDAD TERAPEUTICA', 'Enviar dato faltante !! ', 'SMG'),
(953, '22892', 'PUNTO PACK SRL', '22892', 'PUNTO PACK SRL', 'N', 'info@liccoons.com.ar ', 'GODOY CRUZ 1390', 'BANFIELD', 'Enviar dato faltante !!', 'INTANSIA', 'COMERCIO', 'Enviar dato faltante !! ', 'NO POSEE ART'),
(954, '2290', 'BOSTON CIA. ARG. DE SEGUROS SA', '2290', 'BOSTON CIA. ARG. DE', 'N', 'no ', 'SUIPACHA 268 PISO 3 Y 4', 'CIUDAD AUTONOMA DE BUENOS AIRE', '4324-5555', 'Enviar dato faltante !!', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'Enviar dato faltante !!'),
(955, '22958', 'KRAVETS OLESYA', '22958', 'JUAN', 'S', 'drdanielrovetta@gmail.com ', 'SARMIENTO 1701', 'AVELLANEDA ESTE', '1556068149 (JUAN TITULAR)', 'OLESYA-JUAN', 'PANADERIA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(956, '22978', 'OMINT ART SA', '22978', 'OMINT ART SA', 'N', ' ', 'CARLOS PELLEGRINI 1363 PISO 10', 'CAPITAL FEDERAL', '', '', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'Enviar dato faltante !!'),
(957, '2303', 'COREY S.A.', '2303', 'VALERIA', 'S', 'solcorey@yahoo.com.ar ', 'AV. HIPOLITO IRIGOYEN 18519', 'LONGCHAMPS', '4233-5529', 'ANIBAL MOR/VALERIA', 'EST. DE SERV.', 'Enviar dato faltante !! ', 'GALENO'),
(958, '2304', 'SOCIEDAD ESCOLAR Y DEP.ALEMANA', '2304', 'MANUEL', 'S', 'administracion@sedalo.com.ar,torrado@sedalo.com.ar,info@sedalo.com.ar,rrhh@sedalo.com.ar', 'OLIDEN 2444', 'LANUS OESTE', '4209-5111 4228-8974', 'ORLANDO/LIDIA', 'EDUCATIVA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(959, '2305', 'LA ESQUINA DE LAS FLORES SRL', '2305', 'REBECA', 'S', 'administracion@esquinadelasflores.com.ar,angelita@esquinadelasflores.com.ar', 'GURRUCHAGA 1630', 'CAPITAL FEDERAL', '4228-5000', 'REBECCA/CONTADORA CRISTINA', 'ALIMENT.', 'Enviar dato faltante !! ', 'LA CAJA'),
(960, '23060', 'PUENTES DIEGO OSCAR', '23060', 'PUENTES DIEGO OSCAR', 'N', 'tecnopack@speedy.com.ar ', 'FARRELL 2118', 'FARRELL 2118', '4208-3097/4228-7787', 'DIEGO/EVANGELINA', 'EMBALAJES', 'Enviar dato faltante !! ', 'QBE'),
(961, '23095', 'REPS COMERCIAL HOTELERA SA', '23095', 'FLORENCIA', 'S', 'florencia@rturistica.com ', 'ESMERALDA 320 8A', 'CAPITAL FEDERAL', '5276-0894', 'FLORENCIA-GASTON', 'TURISMO', 'Enviar dato faltante !! ', 'PROVINCIA'),
(962, '23098', 'PACO MAYORISTA ( DE FONTELA NILDA MARIA)', '23098', 'NILDA', 'S', 'pacolanoria@hotmail.com ', 'REANO 1640', 'LOMAS DE ZAMORA OESTE', '4285-0293', 'HERNAN', 'DISTRIBUIDORA', 'Enviar dato faltante !! ', 'GALENO'),
(963, '23102', 'GRUPPO MG SA', '23102', 'YANINA', 'S', 'info@metalglassargentina.com.ar,mariana@metalglassargentina.com.ar', 'CASACUBERTA 750', 'AVELLANEDA ESTE', '4204-2121 4265-2122', '4265-2110 MARIANA', 'INDUSTRIA', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(964, '23136', 'RUA SANTIAGO MATIAS OSVALDO', '23136', 'PAULA', 'S', 'microrua@gmail.com ', 'JEAN JAURES 3165', 'VALENTIN ALSINA', '4208-5511', 'PAULA', 'ELAB.TUBOS DE CARTON', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(965, '23178', 'LOG CORP SA', '23178', 'HERNAN', 'S', 'logcorpsa@gmail.com ', 'ONCATIVO 768', 'LANUS ESTE', '4240-1238/1553083040', 'MARTIN-JACQUELINE', 'TRANSPORTE', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(966, '23179', 'CIA DE INVERSIONES BAIRES SA', '23179', 'TITO', 'S', 'gerencia@tritonehotel.com.ar ', 'MAIPU 657', 'CAPITAL FEDERAL', '4325-8955', 'TITO', 'HOTEL', 'Enviar dato faltante !! ', 'NO POSEE ART'),
(967, '2323', 'CNA ART S.A.', '2323', 'CNA ART S.A.', 'N', 'siniestros.prestadores@cnaart.com.ar,cecilia.zelada@cnaart.com.ar', 'ALSINA 665', 'CIUDAD AUTONOMA DE BUENOS AIRE', '324-6700/08006663366', 'FERNANDA', '08006663355FAX', 'Enviar dato faltante !! ', 'Enviar dato faltante !!'),
(968, '23270', 'FALCON HECTOR OSCAR', '23270', 'FALCON HECTOR OSCAR', 'N', 'matriceriafalcon@hotmail.com,falconmet@live.com.ar', 'JOSE INGENIERO 654', 'AVELLANEDA ESTE', '4204-3378', 'HECTOR', 'METALURGICA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(969, '23271', 'PLAY SPORTS SRL', '23271', 'LUCIANA', 'S', 'karinakuc1970@yahoo.com.ar,playsport.pareto@hotmail.com', '20 DE SEPTIEMBRE 1795', 'RAFAEL CALZADA', '4219-3481', 'LUCIANA-KARINA- ALFREDO', 'CASA DE ART.DEPORTIVOS', 'Enviar dato faltante !! ', 'QBE'),
(970, '23289', 'CONS. AV MEEKS 1313', '23289', 'CONS. AV MEEKS 1313', 'N', 'admbarreiro@admbarreiro.com.ar ', 'CONS. AV MEEKS 1313', 'LOMAS DE ZAMORA OESTE', '4139-0181/ 1550008213', 'ROSA CORDOBA', 'CONSORCIO', 'Enviar dato faltante !! ', 'NO POSEE ART'),
(971, '2334', 'PRODUCTOS AFER S.A. IMP EXP', '2334', 'ALICIA', 'S', 'mabel@ebiahoney.com ', 'CARLOS CASARES 2173', 'LANUS OESTE', '4241-4935 / 5331', 'MABEL', 'ALIMENTOS', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(972, '23348', 'PARTICULARES ESTUDIOS CDL', '23348', 'PARTICULARES ESTUDIO', 'N', 'NO ', 'BASAVILBASO 1859', 'LANUS ESTE', '4247-7093', 'Enviar dato faltante !!', 'AUTONOMO', 'Enviar dato faltante !! ', 'CENTRO DIAGNOSTICO LANUS'),
(973, '2335', 'PRODUCTOS LIPO S.A.', '2335', 'JORGE', 'S', 'dspoltore@productoslipo.net,jvega@productoslipo.net', 'GUARRACINO 2328', 'LANUS ESTE', '4225-3700 int 141', 'DIEGO SPOLTORE/JORGE VEGA', 'CARAMELOS', 'eulzurrun@productoslipo.net,dspoltore@productoslipo.net', 'SMG'),
(974, '23372', 'COMERCIAL DASDASUR SRL', '23372', 'JUAN', 'S', 'juanzx7@hotmail.com,eguaglianone@loscuatro.com.ar', 'OLAZABAL 3738', 'LANUS OESTE', '4267-3348', 'JUAN AYALA/PABLO AIELLO', 'DISTRIBUIDORA', 'mangeletti@loscuatro.com.ar ', 'NO POSEE ART'),
(975, '23373', 'TRANSPORTE DENEXT SRL', '23373', 'JUAN', 'S', 'juanzx7@hotmail.com,eguaglianone@loscuatro.com.ar', 'DON ORIONE 3777', 'LANUS OESTE', '4267-3348', 'Enviar dato faltante !!', 'DISTRIBUIDORA', 'mangeletti@loscuatro.com.ar ', 'Enviar dato faltante !!'),
(976, '23424', 'SUBWAY (CLAUS HORACIO JOSE)', '23424', 'HORACIO', 'S', 'horacioclaus@hotmail.com,horacioclaus@gmail.com', 'SARGENTO CABRAL 955', 'LANUS ESTE', '4203-9709/1554171725', 'JIMENA- NICOLAS', 'ALIMENTOS', 'Enviar dato faltante !! ', 'NO POSEE ART'),
(977, '23456', 'PARONZINI LEONARDO GASTON', '23456', 'LEONARDO', 'S', 'ferrolom@yahoo.com.ar ', 'LAPRIDA 1145', 'LOMAS DE ZAMORA OESTE', '4243-3662//4244-0558', 'LEONARDO', 'FERRETERIA INDUSTRIAL', 'ferrolom@yahoo.com.ar ', 'FEDERACION PATRONAL'),
(978, '23513', 'SOC. DE EDUC. Y BIBLIOTECA POPULAR ANTONIO MEN', '23513', 'EVELYN', 'S', 'bibliotecamentruyt@yahoo.com.ar ', 'ITALIA 44', 'LOMAS DE ZAMORA', '4244-0837', 'EVELYN', 'EDUCATIVA', 'bibliotecamentruyt@yahoo.com.ar ', 'NO POSEE ART'),
(979, '23527', 'TERMOPLASTICOS DEL SUR SA', '23527', 'JULIETA', 'S', 'julieta.pangaro@pringlessanluis.com.ar,rrhh@pringlessanluis.com.ar', 'FITTE MARCELO 1755 17 DTO H', 'CAPITAL FEDERAL', '4693-4693 INT 226', 'JULIETA PANGARO', 'Enviar dato faltante !!', 'juan@pringlessanluis.com.ar ', 'NO POSEE ART'),
(980, '23542', 'CONSORCIO DE PROP. ITUZAINGO 1573', '23542', 'CONSORCIO DE PROP. I', 'N', 'admfuentes@hotmail.com ', 'ITUZAINGO 1573', 'LANUS ESTE', '4249-9629', 'MARIA DANIELA FUENTES', 'CONSORCIO', 'Enviar dato faltante !! ', 'NO POSEE ART'),
(981, '2355', 'TARANDIN S.R.L.', '2355', 'TARANDIN', 'S', 'NO MAIL 2 ', 'AV. HIPOLITO IRIGOYEN 4498', 'LANUS OESTE', '4241-9658/8890', 'MIGUEL CASTRO', 'PIZZERIA', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(982, '23583', 'HELADOS LANUS SRL', '23583', 'ALBERTO', 'S', 'freddolanus@gmail.com ', '25 DE MAYO 18', 'LANUS OESTE', '4241-1127', 'ALBERTO', 'HELADERIA', 'Enviar dato faltante !! ', 'NO POSEE ART'),
(983, '23584', 'GOMEZ CARLOS ALBERTO', '23584', 'CARLOS', 'S', 'carlos7248albertogomez@hotmail.com ', 'SOFIA T. SANTAMARINA 587 1ER PISO OF 12', 'MONTE GRANDE', '4281-7248/1559346751', 'GOMEZ CARLOS', 'LIQUIDACIONES DE SUELDOS', 'Enviar dato faltante !! ', 'NO POSEE ART'),
(984, '23599', 'HARVEST ENERGY SRL', '23599', 'LORENA', 'S', 'figueira.lorena@gmail.com,montajeselectricosmendez@yahoo.com.ar', 'RIO TUEL 1147', 'BELLA VISTA', '1130324431/1168837233', 'LORENA-NATALIA', 'ELECTRICIDAD-MONTAJES', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(985, '23620', 'PA MA DEI SCS', '23620', 'ROBERTO', 'S', 'farmaciapamadei@gmail.com ', 'CATAMARCA 3484', 'LANUS OESTE', '4262-5134/1855', 'ROBERTO E PALACIO', 'FARMACIA', 'Enviar dato faltante !! ', 'PREVENCION'),
(986, '23648', 'DISTRISUR PROFESIONAL SRL', '23648', 'LAURA', 'S', 'g_kamalian@hotmail.com,laura.distrisur@gmail.com', 'SEGUI 674', 'ADROGUE', '4244-8035', 'NORA-GABRIEL', 'PERFUMERIA', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(987, '23672', '0,1 DIGITAL SRL', '23672', 'SUSANA', 'S', 'svadministracion@fibertel.com.ar ', 'CARRANZA ANGEL J 955', 'CAPITAL FEDERAL', '4855-2282', 'MARTA VAZQUEZ', 'CONSTRUCCION', 'svadministracion@fibertel.com.ar ', 'PROVINCIA'),
(988, '2371', 'JOSE GOMEZ Y CIA S.R.L.', '2371', 'JOSE', 'S', 'curtiembregomez@yahoo.com.ar ', 'FLORIDA 2352', 'VALENTIN ALSINA', '4209-2674', 'JOSE GOMEZ', 'CURTIEMBRE', 'Enviar dato faltante !! ', 'MAPFRE'),
(989, '23751', 'MAXI KIO SRL', '23751', 'NOELIA', 'S', 'kiomax.noelia@gmail.com ', 'MORENO 850 PISO 12 DTO D', 'CAPITAL FEDERAL', '4342-0546', 'NOELIA', 'MAXIKIOSCO', 'Enviar dato faltante !! ', 'GALENO'),
(990, '2376', 'CUPPARI FRANCISCO', '2376', 'GISELA', 'S', 'gisela@fc-group.com.ar ', 'ROCA 482', 'R.DE ESCALADA OESTE', '4242-7067/4202-6052', 'FABIO CUPPARI', 'CRISTALERIA', 'gisela@fc-group.com.ar ', 'GALENO'),
(991, '2377', 'SISTEMAS INDUST. DE PESAJE SRL', '2377', 'NORBERTO', 'S', 'admin-sip@infostar.com.ar,sipsrl@speedy.com.ar', 'GRAL. ACHA 574', 'SARANDI', '4204-7082', 'NOEMI', 'FCA. PESAJE', 'Enviar dato faltante !! ', 'QBE'),
(992, '2384', 'CAAGUAZU GAS S.A.', '2384', 'DIEGO', 'S', 'caaguazu@audes.com.ar,michelinij@gncplus.com.ar,mazzucac@audes.com.ar', 'EVA PERON 5395', 'LANUS ESTE', '4230-7831', 'CAMAÑO-MARCELO', 'EST. DE SERV.', 'Enviar dato faltante !! ', 'LA CAJA'),
(993, '2387', 'LAOF S.A.', '2387', 'GRACIELA', 'S', 'graciela@laof.com.ar,jpatamia@laof.com.ar ', 'YATAY 820', 'VALENTIN ALSINA', '4208-8470/7825', 'AREA P Y ML HORACIO', 'MONTAJE CABLES', 'fernando@laof.com.ar ', 'GALENO'),
(994, '2403', 'EL POCO A POCO', '2403', 'MATILDE', 'S', 'elpocoapoco@ciudad.com.ar ', 'GRAL. CAMPOS 1106', 'BANFIELD ESTE', '4242-4182 4242-5959', 'MARCELO/MATILDE', 'EMP. MUDANZA', 'Enviar dato faltante !! ', 'QBE'),
(995, '2413', 'LA GENOVESA SUPERMERCADOS S.A.', '2413', 'CEPI', 'S', 'genovesalanus@fibertel.com.ar,ser@lagenovesasuper.com.ar', 'ONCATIVO 1942', 'LANUS ESTE', '4240-8711', 'SERGIO FRAC/MANUEL', 'SUPERMERCADO', 'patricia@lagenovesasuper.com.ar ', 'SMG'),
(996, '2422', 'GHISOLFO JUAN C (SHELL JUANCHO', '2422', 'GHISOLFO', 'S', 'juancaros1956@hotmail.com ', 'EVA PERON 2534', 'LANUS ESTE', '4247-4135', 'JUAN CARLOS', 'EST. DE SERV.', 'Enviar dato faltante !! ', 'GALENO'),
(997, '2430', 'SCIPEMA S.A.', '2430', 'SILVIAK', 'S', 'scipema@scipema.com.ar ', 'ARMENIA 2412', 'CAPITAL FEDERAL', '4833-5463/4831-9416', 'SILVIA KRAVETZ', 'CONTROL DE PLAGAS', 'Enviar dato faltante !! ', 'QBE'),
(998, '2432', 'COARDEL S.A.C.I.F.I.A', '2432', 'CECILIA', 'S', 'marina@coardel.com.ar,compras@coardel.com.ar ', 'NICOLAS MILAZOS 3251', 'PARQUE INDUSTRIAL PLATANOS (BE', '4215-4432/1556461420 ANGE', 'DAVID VAZQUEZ', 'CAUCHO', 'administracion@coardel.com.ar ', 'PREVENCION'),
(999, '2437', 'GELATO E PASTA S.R.L.', '2437', 'GUILLERMO', 'S', 'gfdominguez@ciudad.com.ar ', 'AV. EVA PERON 4280', 'LANUS ESTE', '4246-5219', 'ROBERTO/ALEJANDRA', 'FCA. DE PASTAS', 'Enviar dato faltante !! ', 'LA CAJA'),
(1000, '2442', 'MEDINTH S.A.', '2442', 'MEDINTH', 'S', 'art_medinth@fibertel.com.ar,proveedores_medinth@fibertel.com.ar', 'AV. JUAN B. ALBERDI 452', 'CIUDAD AUTONOMA DE BUENOS AIRE', '4925-0066 4924-1325', 'PATRICIA', 'Enviar dato faltante !!', 'proveedores_medinth@fibertel.com.ar ', 'GALENO'),
(1001, '2445', 'FARMACIA REX S.C.S', '2445', 'JULIAN', 'S', 'rexlanus@speedy.com.ar ', 'ITUZAINGO 1072', 'LANUS ESTE', '4241-1253 4225-2173', 'JUAN C. HERNANDEZ', 'FARMACIA', 'Enviar dato faltante !! ', 'GALENO'),
(1002, '2448', 'SIMKEVICIUS CLAUDIO JUAN', '2448', 'CLAUDIO', 'S', 'metalcsimkevicius@arnet.com.ar ', 'CARLOS TEJEDOR 170', 'LANUS OESTE', '4241-3316', 'CLAUDIO', 'METALURGICA', 'Enviar dato faltante !! ', 'PREVENCION'),
(1003, '2455', 'CELESTINO SPAHN SA (LANUS )', '2455', 'DIEGO', 'S', 'swingregalos@hotmail.com,swing-lanus@hotmail.com', '9 DE JULIO 1401', 'LANUS ESTE', '4249-5462', 'BIMESTRAL', 'BAZAR', 'Enviar dato faltante !! ', 'LA SEGUNDA'),
(1004, '2463', 'SOME S.R.L.', '2463', 'DANIEL', 'S', 'some@dipra.com.ar,mariana@dipra.com.ar,elsa@dipra.com.ar', 'CHUBUT 2581', 'LANUS OESTE', '4262-9800', 'AMADEO PAMARIT', 'METALURGICA', 'some@dipra.com.ar ', 'LA HOLANDO'),
(1005, '2466', 'PLUS SALUD S.A.', '2466', 'PLUS SALUD S.A.', 'N', 'no ', 'PTE JUAN DOMINGO PERON 1885', 'CIUDAD AUTONOMA DE BUENOS AIRE', '53546800', 'DR. OLIVERA', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'Enviar dato faltante !!'),
(1006, '2483', 'BLANCA PRESS S.A', '2483', 'GUSTAVO', 'S', 'blancapress@blancapress.com.ar ', 'URUGUAY 263', 'PI#EIRO - AVELLANEDA', '4208-3290', 'GUSTAVO INFANTINO', 'METALURGICA', 'blancapress@blancapress.com.ar ', 'MAPFRE'),
(1007, '2487', 'LAGO ELECTROMECANICA S.A', '2487', 'PAOLA', 'S', 'paola.revelli@lagoelectromecanica.com ', 'HECTOR NOYA 1578', 'LANUS OESTE', '4249-1009', 'PAOLA', 'METALURGICA', 'natalia.masdeu@lagoeletromecanica.com ', 'QBE'),
(1008, '2488', 'CONS. ITUZAINGO 1113', '2488', 'GABRIEL', 'S', 'gabrielcosentino@infovia.com.ar ', 'ANATOLE FRANCE 1617', 'LANUS ESTE', '4249-1877', 'GABRIEL', 'CONSORCIO', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(1009, '2496', 'SERVICIO GASTRONOMICO SRL', '2496', 'PABLO', 'S', 'restaurantcalanus@yahoo.com.ar ', '9 DE JULIO 1680', 'LANUS ESTE', '4241-2604 4247-2322', 'JUAN CARLOS MORAN', 'RESTAURANTE', 'Enviar dato faltante !! ', 'PREVENCION'),
(1010, '2502', 'LA GENOVESA SUP. S.A. B.', '2502', 'SOCRATES', 'S', 'ser@lagenovesasuper.com.ar ', 'BELGRANO 1560', 'BANFIELD', '4248-3306', 'SERGIO AREAL', 'Enviar dato faltante !!', 'patricia@lagenovesasuper.com.ar ', 'SMG'),
(1011, '2504', 'SARRO HNOS S.A.', '2504', 'VANESA', 'S', 'vsarro@ciudad.com.ar ', 'DR PIO COLLIVADINO 57', 'TEMPERLEY', '4264-4848/0194/3889', 'SARRO JOSE', 'MATERIALES', 'Enviar dato faltante !! ', 'PREVENCION'),
(1012, '2511', 'MADERMETAL SA', '2511', 'CLAUDIO', 'S', 'sayosn@infovia.com.ar,jdv_88@hotmail.com ', 'MADERO 1935', 'LANUS OESTE', '4208-5698/4931-7969', 'SR. LUIS', 'CARPINTERIA MET', 'Enviar dato faltante !! ', 'PREVENCION'),
(1013, '2514', 'MARPEL S.A.', '2514', 'MARIA', 'S', 'recepcion@marpel.com.ar,romina@marpel.com.ar,mtraversaro@marpel.com.ar', 'BUSTAMANTE 97', 'LANUS O (DISPENSARIO LANUS E)', '4208-9385/2198', 'MARINO', 'ENVASES FCA', 'Enviar dato faltante !! ', 'LA CAJA'),
(1014, '2515', 'COL. DE FARMAC. DE LA PCIA DE BS AS FIL.LOMAS', '2515', 'MARCELO', 'S', 'farmarossi@telecentro.com.ar ', 'PIAGGIO 553', 'LOMAS DE ZAMORA OESTE', '4245-4397', 'DE CARIA MARCELA/ANTONIEWICZ MARCELO', 'COLEGIO DE FARMACEUTICOS', 'coleglomas@yahoo.com.ar ', 'FEDERACION PATRONAL'),
(1015, '2516', 'EXPRESION GRAFICA S.R.L.', '2516', 'NORMA', 'S', 'expresion@ciudad.com.ar,info@expresiongraficasrl.com.ar,facturacion@expresiongraficasrl.com.ar', 'PRINGLES 1751', 'LANUS E', '4249-1619/1620', 'NORMA/MIGUEL RUDY', 'GRAFICA', 'expresion@ciudad.com.ar ', 'GALENO'),
(1016, '2517', 'VAZQUEZ HNOS DE JORGE Y JOSE SRL', '2517', 'MARIO', 'S', 'vazquez_srl@hotmail.com ', 'GRAL RODRIGUEZ 2580', 'LANUS E', '4247-0320/4240-6989', 'ANA MARIA / MARIO', 'EST. DE SERV.', 'Enviar dato faltante !! ', 'SMG'),
(1017, '2520', 'YALI INTI S.A.', '2520', 'CARLA', 'S', 'yali_inti@speedy.com.ar ', 'SGO DEL ESTERO 536', 'LANUS OESTE', '4208-6834/4228-7154/7036', 'LILIAN ACEVEDO', 'PLASTICOS', 'Enviar dato faltante !! ', 'GALENO'),
(1018, '2542', 'ELECTROCOL S.A.', '2542', 'JOSE', 'S', 'electrocol_sa@hotmail.com ', 'TALCAHUANO 2758', 'LANUS O', '4209-1752/7097', 'JOSE M. ANDINO', 'PINTURA DE MET.', 'Enviar dato faltante !! ', 'GALENO'),
(1019, '2545', 'METALURGICA NEIKE', '2545', 'NEIKE', 'S', 'metalurgicaneike@yahoo.com.ar ', 'PURITA 2937', 'LANUS', '4230-3310', 'JOSE BERNARDO', 'METALURGICA', 'Enviar dato faltante !! ', 'PREVENCION'),
(1020, '2548', 'C. MARINO E HIJOS S.R.L.', '2548', 'BEATRIZ', 'S', 'beatriz@cmarino.com.ar ', 'SARMIENTO 331', 'LANUS ESTE', '4205-1694/3358', 'beatriz', 'METALAURGICA', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(1021, '2550', 'VALIPARRI S.A.', '2550', 'SERGIO', 'S', 'info@parrillasvaliparri.com.ar ', 'AV.R.DE ESCALADA DE SAN MARTIN 768', 'LANUS O', '4247-7887', 'SERGIO', 'METALURGICA', 'rgomez@parrillasvaliparri.com.ar ', 'PREVENCION'),
(1022, '2553', 'APPART GERIATRICO LANUS S.A.', '2553', 'MARCOS', 'S', 'marcosbraier@yahoo.com.ar ', 'BASAVILBASO 2048', 'LANUS S.A.', '4241-7113', 'CLARISA', 'GERIATRICO', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1023, '2557', 'TECNIPLAT S.R.L.', '2557', 'TECNIPLAT', 'S', 'accesoriosfz@yahoo.com.ar ', 'TUCUMAN 3060', 'LANUS E', '4246-5177', 'ERIKA FRANCISCO ZAZA', 'PLASTICOS', 'Enviar dato faltante !! ', 'SMG'),
(1024, '2567', 'BOTANMOL S.A.', '2567', 'HECTOR', 'S', 'botanmol@botanmol.com,botanmol@speedy.com.ar ', 'JUJUY 852', 'LANUS O', '4225-2705/2706', 'HECTOR VALES', 'PLASTICOS', 'Enviar dato faltante !! ', 'PREVENCION'),
(1025, '2569', 'FCIA SAN MARTIN 3300 S.C.S.', '2569', 'WALTER', 'S', 'dpernico@puntofarma.com,hesteban@puntofarma.com,personal@puntofarma.com', 'SAN MARTIN 3300', 'LANUS OESTE', '4240-4340/2', 'LILIANA SOUZA/EDUARD', 'FARMACIA', 'Enviar dato faltante !! ', 'QBE'),
(1026, '2577', 'PAPELERA SUR S.R.L.', '2577', 'PAPELERA', 'S', 'papelerasur@live.com.ar ', 'TACUARI 740', 'LANUS O', '4247-5971', 'LEONARDO CALVOSA', 'PAPELERA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1027, '2585', 'CURTIEMBRE JOSE CONDE', '2585', 'MONICA', 'S', 'condecuer@hotmail.com ', 'HEROES DE MALVINAS 971', 'LANUS E', '4246-3492', 'JOSE CONDE', 'CURTIEMBRE', 'Enviar dato faltante !! ', 'SMG'),
(1028, '2590', 'C.E.P.P. S.R.L.', '2590', 'HAASE', 'S', 'escuelacepp@yahoo.com.ar ', 'EMILIO MITRE 1144', 'LANUS E', '4249-3662/225-5785', 'AREA P.GUILLERMO', 'EDUCATIVA', 'Enviar dato faltante !! ', 'QBE'),
(1029, '2609', 'PRO TUBO S.A.', '2609', 'DANIEL', 'S', 'protubo@protubo.com.ar ', 'CARLOS PELLEGRINI 3582', 'VALENTIN ALSINA', '4208-5789/4228-3502', 'Enviar dato faltante !!', 'METALURGICA', 'Enviar dato faltante !! ', 'QBE'),
(1030, '2615', 'AUFKOCHEN SACIFI EST. SERV YPF', '2615', 'VIOLETA', 'S', 'estacion406@hotmail.com ', 'AV. SAN MARTIN Y VIAMONTE', 'LANUS OESTE', '4225-4476', 'LUIS MARISSI/VIOLETA', 'EST. DE SERV.', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(1031, '2619', 'CONVIVIR', '2619', 'ALEJANDRA', 'S', 'administracion@convivir.com,marianaarcusa@convivir.com', 'CORDOBA 1006', 'LANUS ESTE', '4240-6938/4357-2174', 'PAOLA/PATRICIA/PABLO', 'COLEGIO', 'administracion@convivir.com,laurafernandez@convivir.com', 'SMG'),
(1032, '2626', 'ESTMAR SH DE BUCCERONI Y OTROS', '2626', 'MARCELO', 'S', 'matriceria@estmarsh.com.ar ', 'ALVEAR 2083', 'BANFIELD', '4242-5068', 'SANDRA BUCCERONI', 'METALURGICA', 'Enviar dato faltante !! ', 'GALENO'),
(1033, '2627', 'MADERLAM S.R.L.', '2627', 'ALEJANDRO', 'S', 'maderlam@speedy.com.ar ', 'EVA PERON 2751', 'LANUS E', '4246-3638/2095', 'ALEJANDRO / HECTOR', 'MADERERA', 'Enviar dato faltante !! ', 'PREVENCION'),
(1034, '2633', 'GRAU-HEREDIA S.A.', '2633', 'RUBEN', 'S', 'curtiembregrauheredia@yahoo.com.ar ', 'ITAPIRU 2229', 'VALENTIN ALSINA', '4209-1269', 'FABIANA-RUBEN-MIGUEL', 'CURTIEMBRE', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1035, '2637', 'REPRESENTACIONES AL.SA.GA S.R.L.', '2637', 'CRISTINA', 'S', 'alsagalub@yahoo.com.ar ', 'JOSE M. MORENO 1302', 'LANUS O', '4240-6272-4225-0049', 'CRISTINA-ERNESTO VAZQUEZ', 'LUBRICENTRO', 'Enviar dato faltante !! ', 'LA CAJA'),
(1036, '2642', 'DIFEL S.A.', '2642', 'ROBERTO', 'S', 'ypfdifelsa@gmail.com,difel@speedy.com.ar ', 'R. DE ESC. DE SAN MARTIN 3710', 'VALENTIN ALSINA', '4228-8224', 'Enviar dato faltante !!', 'EST. DE SERV.', 'Enviar dato faltante !! ', 'SMG'),
(1037, '2660', 'LOPES JOSE', '2660', 'JOSELOPES', 'S', 'rrhh@gaelle.com.ar,personal@gaelle.com.ar ', 'MARIO BRAVO 1937/65', 'AVELLANEDA OESTE', '4208-9833 INT 101-103', 'ADRIAN GOMEZ-KARINA PENNINO', 'CALZADOS', 'pagos@gaelle.com.ar ', 'PROVINCIA'),
(1038, '2661', 'LA TERCERA HISPANA S.R.L.', '2661', 'ANTONIO', 'S', 'NO MAIL 2 ', 'CENTENARIO URUGUAYO 1554', 'VILLA DOMINICO', '4207-2481', 'ANTONIO ROSA/DOLORES', 'PANADERIA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1039, '2670', 'NEW STADIUM (1)', '2670', 'GUSTAVO', 'S', 'arenagustavo@fibertel.com.ar ', 'HIPOLITO IRIGOYEN 4158', 'LANUS OESTE', '4247-2598/4249-5200', 'MARCELO ARENA', 'GIMNASIO', 'Enviar dato faltante !! ', 'SMG'),
(1040, '2677', 'COLOR CLAB S.R.L.', '2677', 'FRANCISCO', 'S', 'fotoclaudio1@yahoo.com.ar ', 'MAIPU 314 -170', 'BANFIELD', '4242-8371 4202-5981', 'FRANCISCO BRAVO', 'POLIRUBRO', 'Enviar dato faltante !! ', 'QBE'),
(1041, '2680', 'MADERERA BUSTAMANTE S.R.L.', '2680', 'SOLEDAD', 'S', 'maderart@infovia.com.ar ', 'BUSTAMANTE 2085', 'LANUS ESTE', '4204-7155 4203-8059', 'ESTELA', 'MADERERA', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(1042, '2684', 'DOMINGO A. CAPRIA S.A.', '2684', 'LUCIA', 'S', 'administracion@domingocapria.com ', 'HAENDEL S/N CENT. IND.GARIN', 'GARIN', '03327-414470/1/2/3', 'PABLO FORESTIER', 'METALURGICA', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(1043, '2686', 'BAZAR LOS NENES', '2686', 'LOSNENES', 'S', 'bazarlosnenes@fibertel.com.ar ', 'ANATOLE FRANCE 1855', 'LANUS E', '4240-4466', 'BEATRIZ', 'COMERCIO BAZAR', 'Enviar dato faltante !! ', 'QBE'),
(1044, '2702', 'ITAL GAS S.A.', '2702', 'NANCY', 'S', 'italgas@speedy.com.ar ', 'SAN MARTIN 1016', 'LANUS OESTE', '4228-7786', 'JOSE CARDILLO/SILVINA', 'EST. DE SERV.', 'italgas@speedy.com.ar ', 'GALENO'),
(1045, '2707', 'AUTOMOTRIZ CARIBO S.A.', '2707', 'SILVIA', 'S', 'gnccaribo@yahoo.com.ar ', 'HIPOLITO YRIGOYEN 5401', 'REM. DE ESCALADA OESTE', '4241-2675/4249-3378', 'ROBERTO / SILVIA', 'EST. DE SERV.', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1046, '2715', 'ALAMBRES RUMBOS S.A.', '2715', 'ROMINA', 'S', 'rumbos@arnet.com.ar,rumbos@alambresrumbos.com ', 'AYACUCHO 3232', 'LANUS E', '4246-5932', 'ROMINA', 'METALURGICA', 'Enviar dato faltante !! ', 'SMG'),
(1047, '2717', 'WALTER GREGORUTTI S.A.', '2717', 'KELLY', 'S', 'gregorutti@w-gregorutti.com.ar,info@w-gregorutti.com.ar', 'BOLAÑOS 2802', 'LANUS ESTE', '4230-1736', 'GLADYS-MANUEL', 'CARTELES FCA', 'Enviar dato faltante !! ', 'GALENO'),
(1048, '2739', 'ARPLAS S.R.L.', '2739', 'ANA', 'S', 'arplassrl@hotmail.com ', 'BOUCHARD 2917', 'LANUS E.', '4246-4666', 'ANA MARIA', 'PLASTICOS', 'Enviar dato faltante !! ', 'SMG');
INSERT INTO `Usuarios` (`id`, `Codigopal`, `Nombrepal`, `Clave`, `Usuario`, `Activo`, `Correo`, `Domicilio`, `Localidad`, `Telefono`, `Contacto`, `Actividad`, `Correop`, `Nombart`) VALUES
(1049, '2741', 'GAMA MOLD S.R.L.', '2741', 'FRANCISCO', 'S', 'consultas@gamamold.com.ar,administracion@gama-mold.com.ar', 'A. DEL VALLE 2585', 'LANUS O', '4218-4848', 'MARTELLO', 'PLASTICOS', 'Enviar dato faltante !! ', 'LA CAJA'),
(1050, '2742', 'FERNANDEZ CARLOS ALBERTO', '2742', 'CARLOS', 'S', 'NO MAIL ', 'HERNANDARIAS 3466', 'LANUS OESTE', '4286-9301/ 154-1470683', 'CARLOS A FERNANDEZ', 'FUNDICION', 'Enviar dato faltante !! ', 'GALENO'),
(1051, '2743', 'MONO II EXPRESS DE GARCIA C.', '2743', 'HUGO', 'S', 'NO MAIL 2 ', 'CENTENARIO URUGUAYO 1002', 'LANUS E', '4289-0200/4249-4444', 'HUGO', 'PIZZERIA', 'Enviar dato faltante !! ', 'LA SEGUNDA'),
(1052, '2752', 'CARTA SUR S.A.', '2752', 'DEBORA', 'S', 'personal@cartasur.com.ar,dcastello@cartasur.com.ar', 'SIXTO FERNANDEZ 124 E/ ESPAÑA Y MEEKS', 'LOMAS DE ZAMORA OESTE', '4239-3200', 'TIT.:DR.ELGUER /ENC. DE PERS:RAQUEL COLQ', 'FINANCIERA', 'ldiez@cartasur.com.ar ', 'PREVENCION'),
(1053, '2755', 'TRES CORONAS S.A.', '2755', 'TRESCORONAS', 'S', 'trescoronas@trescoronas.com.ar ', 'VILLA DE LUJAN 1634', 'SARANDI ESTE', '4246-8139', 'ALEJANDRA', 'LACTEOS', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1054, '2763', 'CETER S.A.', '2763', 'CELESTE', 'S', 'luciana.cruz@diaverum.com ', 'AV. EVA PERON 4798', 'TEMPERLEY', '4260-3633/4602', 'MIRIAM', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(1055, '2773', 'FOLGOSO BARDULLAS S.A.', '2773', 'MONICA', 'S', 'folgoso_bardullas@hotmail.com,folgoso.bardullas@yahoo.com.ar', 'MAXIMO PAZ 870', 'LANUS O', '4249-9889/3889', 'MONICA', 'ALIMENTICIA', 'Enviar dato faltante !! ', 'QBE'),
(1056, '2776', 'ALSINA 602 S.R.L.', '2776', 'ROSANA', 'S', 'danigali77@hotmail.com,tiara456@hotmail.com ', 'ALSINA 602', 'BANFIELD ESTE', '4242-1381/0617', 'ROXANA', 'PIZZERIA', 'Enviar dato faltante !! ', 'GALENO'),
(1057, '2778', 'DIAZ GABRIEL ALBERTO', '2778', 'GABRIEL', 'S', 'NO MAIL 2 ', 'BUSTOS 181', 'LOMAS DE ZAMORA', '4282-3723/153-9188346 JAV', 'MARTIN DIAZ', 'METALURGICA', 'Enviar dato faltante !! ', 'GALENO'),
(1058, '2780', 'IVALDI NORBERTO', '2780', 'NORBERTO', 'S', 'tallermetalurgicoivaldi@gmail.com,ivaldiguillermo@hotmail.com', 'ALEGRE 1191', 'AVELLANEDA', '4208-5988', 'RUBEN', 'METALURGICA', 'Enviar dato faltante !! ', 'LA CAJA'),
(1059, '2781', 'GUARDERIA NTRA SRA DE LA CONSOLACION', '2781', 'RITA', 'S', 'ritafabio2@hotmail.com,gyinsdelaconsolacion@hotmail.com', 'RONDEAU 509', 'LANUS', '4246-7216', 'VIVIANA-RITA', 'GUARDERIA', 'Enviar dato faltante !! ', 'GALENO'),
(1060, '2785', 'CONSTRUCCIONES ELECT. ZPM SRL.', '2785', 'HAYDEE', 'S', 'zpm@ciudad.com.ar,zpmsrl@yahoo.com.ar ', 'CRISOLOGO LARRALDE 3745', 'AVELLANEDA ESTE', '4217-3023', 'HAYDEE RICHI', 'MANTENIMIENTO', 'Enviar dato faltante !! ', 'LA CAJA'),
(1061, '2787', 'IND. MAR - VIC S.R.L.', '2787', 'VANINA', 'S', 'administracion@mar-vic.com.ar ', 'MURGUIONDO 3343', 'LANUS O', '4228-8441/8898', 'OSCAR/VANINA', 'CABLES FCA', 'Enviar dato faltante !! ', 'QBE'),
(1062, '2790', 'BERKLEY INTERNATIONAL SEG. S.A', '2790', 'BERKLEY INTERNATIONA', 'N', 'no ', 'CARLOS PELLEGRINI 1023 1er SUB', 'CIUDAD AUTONOMA DE BUENOS AIRE', '4378-8028/51', 'VICTORIA FUGAZZOTTO', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'Enviar dato faltante !!'),
(1063, '2796', 'MUEBLES MADARIAGA S.A.', '2796', 'PATRICIA', 'S', 'mjbruzzone@yahoo.com.ar,futonyfuton@hotmail.com,bruzzonancy@hotmail.com', 'GRAL MADARIAGA 1790', 'AVELLANEDA', '4204-3175/2796', 'NANCY/MARIO/ADRIAN', 'MUEBLES', 'Enviar dato faltante !! ', 'ART LIDERAR SA'),
(1064, '2818', 'EMBASSY', '2818', 'SILVIA', 'S', 'info_puntoplata@yahoo.com.ar ', 'ITUZAINGO 1180', 'LANUS E', '4247-5982', 'ISIDORO/MARCELO', 'JOYERIA', 'Enviar dato faltante !! ', 'NO POSEE ART'),
(1065, '2824', 'HECTOR PEDRO ARES S.A.', '2824', 'HECTOR', 'S', 'hectorpedroares@ciudad.com.ar,mellys6666@hotmail.com,hectoraressa@hotmail.com', '25 DE MAYO 1517', 'LANUS O', '4241-9359', 'DANIEL', 'METALURGICA', 'Enviar dato faltante !! ', 'LA CAJA'),
(1066, '2833', 'INTACO S.R.L.', '2833', 'MARIANO', 'S', 'intaco@sinectis.com.ar ', 'JUNCAL 2244', 'LANUS E', '4240-6788', 'MARIANO MORA', 'METALURGICA', 'Enviar dato faltante !! ', 'SMG'),
(1067, '2846', 'DI SA DE NORBERTO GIARRATANA', '2846', 'VALERIA', 'S', 'di-sa@speedy.com.ar ', 'DIPUTADO PEDRERA 1858', 'LANUS O', '4228-2249/4228-3668', 'VALERIA-NORBERTO', 'METALURGICA', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(1068, '2857', 'CENTRO EDUCATIVO TERAPEUTICO', '2857', 'MIRTA', 'S', 'surgiendo@sinectis.com.ar,surgiendo@autismoinfantil.org.ar', '20 DE SEPTIEMBRE 3665', 'LANUS O', '4240-9455', 'MIRTA', 'EDUCATIVA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1069, '2866', 'CONS. GOB.IRIGOYEN 236', '2866', 'ANA MARIA', 'S', 'estudiomozzone@hotmail.com ', 'GOBERNADOR IRIGOYEN 236', 'LANUS O', '4240-3566:1550948', 'ANA MARIA - EDUARDO', 'CONSORCIO', 'Enviar dato faltante !! ', 'GALENO'),
(1070, '2873', 'CONS. SARMIENTO 1785', '2873', 'MAIRA', 'S', 'graciela@estudiodhb.com.ar,paula@estudiodhb.com.ar', 'ARIAS 1497', 'LANUS ESTE', '4240-9151', 'MAIRA', 'CONSORCIO', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(1071, '2878', 'CONS. ANATOLE FRANCE 2112', '2878', 'MARIA', 'S', 'admfuentes@hotmail.com ', 'ANATOLE FRANCE 2112', 'LANUS ESTE', '4249-9629', 'DANIELA', 'CONSORCIO', 'Enviar dato faltante !! ', 'GALENO'),
(1072, '2902', 'C.B.C. METALURGICA S.R.L.', '2902', 'OMAR', 'S', 'administracion@cbcmetalurgica.com.ar ', 'RAMALLO 245', 'SARANDI', '4206-9690', 'EDUARDO / OMAR', 'METALURGICA', 'Enviar dato faltante !! ', 'LA CAJA'),
(1073, '2906', 'PINAS S.R.L.', '2906', 'DIEGO', 'S', 'pinas@pinas.com.ar ', 'PEDERNERA 3464', 'REMEDIOS DE ESCALADA E.', '4242-5583', 'DIEGO FERNANDEZ', 'METALURGICA', 'Enviar dato faltante !! ', 'PREVENCION'),
(1074, '2911', 'CONS. GOB.IRIGOYEN 135', '2911', 'ANA', 'S', 'civeira_romero@yahoo.com.ar,estudioromero@yahoo.com.ar', 'GOBERNADOR YRIGOYEN 135', 'LANUS OESTE', '4247-6870/4241-9117', 'ROMERO ANA KARINA', 'CONSORCIO', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(1075, '2916', 'DUCANPACK SRL', '2916', 'MARIA', 'S', 'recepcion@ducanpack.com,abastecimiento@ducanpack.com', 'SALTA 593', 'LANUS ESTE', '4225-5172', 'SERGIO NOSIGLIA/MARIA JOSE', 'METALURGICA', 'contaduria@ducanpack.com ', 'GALENO'),
(1076, '2927', 'CLAN SRL', '2927', 'GUSTAVO', 'S', 'gme_clansrl@ertach.com.ar,clansrl@ertach.com.ar,gustavoetchevers@gmail.com,walterdieci@hotmail.com', 'OMBU 1852 PARQ.INDUST', 'BURZACO', '4138-0800/4138-0819', 'Enviar dato faltante !!', 'MAY/GOLOSINAS', 'softalndclan@gmail.com,walterdieci@hotmail.com', 'CAMINOS PROTEGIDOS ART SA'),
(1077, '2930', 'FUNAL SRL', '2930', 'SILVIA', 'S', 'miguel@funalsrl.com.ar,funalsrl@sinectis.com.ar', 'LUJAN 1887', 'LAVALLOL', '4298-2274', 'SILVIA MIGUEL', 'METALURGICA', 'Enviar dato faltante !! ', 'PREVENCION'),
(1078, '2936', 'CLEMENTE EDUARDO VICENTE', '2936', 'NATALIA', 'S', 'info@eduardoclemente.com.ar ', 'CNO. GRAL BELGRANO 1902', 'AVELLANEDA', '5291-8661/8662', 'NATALIA CLEMENTE', 'METALURGICA', 'Enviar dato faltante !! ', 'QBE'),
(1079, '2943', 'GONZALEZ CAROLA', '2943', 'CAROLA', 'S', 'carola_gon@hotmail.com ', 'HELGUERA 6541', 'WILDE', '4227-6325', 'RODOLFO / CAROLA', 'CHACINADOS', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1080, '2960', 'CONS.TUYUTI 2745', '2960', 'MARCELO', 'S', 'mfontana@cponline.org.ar,mfontana@consejo.org.ar,estudiofontanam@yahoo.com.ar', 'UCRANIA 1220', 'LANUS O.', '15 4147-5886 4209-7183', 'MARCELO FONTANA', 'CONSORCIO', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(1081, '2962', 'OMAR VETRANO SACIFI', '2962', 'VETRANO', 'S', 'vetrano@vetrano.com.ar,fvetrano@vetrano.com.ar', 'CABILDO 692', 'AVELLANEDA OESTE', '4208-1393/6531/4203', 'HECTOR/ANABELLA', 'METALURGICA', 'vetrano@vetrano.com.ar ', 'LA CAJA'),
(1082, '2983', 'DUREX CROM SRL', '2983', 'ERNESTO', 'S', 'durexcrom@gmail.com ', 'E.FERNANDEZ 2355', 'LANUS OESTE', '4262-6026', 'ERNESTO BONGIOANNI', 'METALURGICA', 'Enviar dato faltante !! ', 'LA CAJA'),
(1083, '2984', 'RAFIAS PRI-SIM TEXTIL S.R.L.', '2984', 'MIRTA', 'S', 'rafias@house.com.ar ', 'FLORIDA 51', 'VALENTIN ALSINA', '4-208-9295', 'GRISELDA', 'TEXTIL', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1084, '3002', 'ANTONIUK LEONARDO DANIEL', '3002', 'LEONARDO', 'S', 'mg.estudio@hotmail.com,leonardoantoniuk@hotmail.com', '2 DE MAYO 2890', 'LANUS OESTE', '4241-0799', 'CONTADOR SAN JOSE', 'ALIMENTICIA', 'Enviar dato faltante !! ', 'GALENO'),
(1085, '3005', 'EL FORTUNA (JOSE S.SANTUNTUN)', '3005', 'ELFORTUNA', 'S', 'metelfortuna@gmail.com ', 'SAN CARLOS 1312', 'WILDE', '4227-9134', 'LAURA O JOSE', 'METALURGICA', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(1086, '3019', 'PERFIGOM SRL', '3019', 'JULIO', 'S', 'perfigomsrl@yahoo.com.ar,perfigom@speedy.com.ar,produccionperfigom@speedy.com.ar', 'CENTENARIO URUGUAYO 689', 'LANUS ESTE', '4289-4024', 'gisela / JULIO VAZQUEZ', 'ELAB.ART.GOMA', 'perfigomsrl@yahoo.com.ar ', 'PROVINCIA'),
(1087, '3021', 'DISTRIBUIDORA ESCALADA SRL', '3021', 'LUIS', 'S', 'spkluis@gmail.com ', 'H.YRIGOYEN 5779', 'REMEDIOS DE ESCALADA', '4249-5832/4241-2030', 'LUIS', 'MADERERA', 'Enviar dato faltante !! ', 'GALENO'),
(1088, '3022', 'CENTRO MODELO DEL GNC SRL', '3022', 'MARIELA', 'S', 'silvana.romalde@gmail.com ', 'HIPOLITO YRIGOYEN 3201', 'LANUS OESTE', '4-247-4581', 'MOYA DANIEL/ROMALDE MARIELA', 'VENTA Y COLOC.GNC', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(1089, '3024', 'MILIN INSTALACIONES', '3024', 'VIVIAN', 'S', 'milin@milin.com.ar,administracion@milin.com.ar', 'AV ALSINA 1702', 'LOMAS DE ZAMORA ESTE', '4292-2664', 'NESTOR', 'AIRE ACONDICION', 'administracion@milin.com.ar ', 'GALENO'),
(1090, '3028', 'SANTA LAURA SA', '3028', 'SANTALAURA', 'S', 'santalaurasa@yahoo.com.ar ', 'SUIPACHA 2753', 'REMEDIOS DE ESCALADA ESTE', '4246-3462', 'ANDREA SOSA/ MIGUEL ANGEL', 'CLINICA', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(1091, '3038', 'COLEGIO SANTA TERESA', '3038', 'MONICA', 'S', 'santateresamp@yahoo.com.ar,santateresalanus@yahoo.com.ar', 'DR. MELO 2763', 'LANUS OESTE', '4225-3505', 'ANDREA O CECILIA', 'EDUCATIVA', 'Enviar dato faltante !! ', 'LA HOLANDO'),
(1092, '3046', 'BARDONE TRANSPORTES UNIDOS SA', '3046', 'DARIO', 'S', 'ezequiel@transportesbardone.com.ar,graciela@transportesbardone.com.ar', 'CNO.G.BELGRANO KM10.5 LO 10-11', 'PARQUE INDUSTRIAL QUILMES', '4270-1158/1145', 'CAROLINA', 'TRANSPORTE', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(1093, '3048', 'FIBRAS SECUNDARIAS SRL', '3048', 'DINO', 'S', 'andreaveronicagarcia@hotmail.com ', 'ALFREDO PALACIOS 4624', 'VALENTIN ALSINA', '4208-6308/7775', 'ALBERTO-FRANCISCO', 'RECORTERA PAPEL', 'andreaveronicagarcia@hotmail.com ', 'GALENO'),
(1094, '3049', 'MAXER SRL', '3049', 'MAXER', 'S', 'leonardo@anzus.com.ar,mercedes.selva@anzus.com.ar', 'FERRE 707', 'LANUS ESTE', '4241-7470/4115-5070', 'MIRIAM-CARLOS', 'METALURGICA', 'Enviar dato faltante !! ', 'VICTORIA'),
(1095, '3051', 'LUONGO HNOS S.DE H.', '3051', 'JOSE', 'S', 'italperfil@italperfil.com.ar ', 'PLUMERILLO 571', 'BANFIELD OESTE', '4286-6048', 'FABRIZIA/JOSE/PEDRO/JULIO', 'METALURGICA', 'Enviar dato faltante !! ', 'SMG'),
(1096, '3057', 'OXICORTE SA', '3057', 'CAROLINA', 'S', 'contable@oxicorte.com.ar ', 'J.M.FREYRE 760', 'AVELLANEDA OESTE', '4-208-8925', 'NESTOR BARDELLA/VIRG', 'METALURGICA', 'Enviar dato faltante !! ', 'PREVENCION'),
(1097, '3068', 'CARATOZZOLO SALVADOR', '3068', 'SALVADOR', 'S', 'verocarato@hotmail.com ', 'SUIPACHA 1464', 'AVELLANEDA ESTE', '4-246-6746', 'VERONICA', 'PANADERIA', 'Enviar dato faltante !! ', 'GALENO'),
(1098, '3073', 'MOCHA S.A.', '3073', 'MARIA', 'S', 'mochapdv@yahoo.com.ar,mocha@pdvsurturdera.com.ar', 'AV. HIPOLITO IRIGOYEN 12101', 'TURDERA', '4231-7718/ 156-0959744', 'ANIBAL MOR/DIEGO', 'EST. DE SERV.', 'Enviar dato faltante !! ', 'LA CAJA'),
(1099, '3075', 'PEROCA S.A.C.I.F.I. y A.', '3075', 'VICTOR', 'S', 'peroca@speedy.com.ar ', 'TUCUMAN 3343', 'LANUS ESTE', '4246-0821', 'VICTOR LOPEZ', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1100, '3082', 'PIROLO Y PIROLO S.A.', '3082', 'NORBERTO', 'S', 'piroloypirolo@speedy.com.ar ', 'DIP.PEDRERA 1596 ESQ.FORMOSA', 'LANUS OESTE', '4209-3808/4208-5364', 'NORBERTO/MARIA ROSA', 'ACCESORI.AUTOMO', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1101, '3083', 'NAHUEL LONAS S.R.L.', '3083', 'JIMENA', 'S', 'nahuel@nahuelcamping.com.ar,administracion@nahuelcamping.com.ar', 'AV. EVA PERON 2260', 'TEMPERLEY', '4264-0354/9058', 'ROMINA O PABLO', 'FCA. PILETAS', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(1102, '3087', 'INDUBOM SRL', '3087', 'ADOLFO', 'S', 'indubom@indubom.com.ar ', 'CHOLELE CHOEL 1943', 'AVELLANEDA OESTE', '4218-0772', 'DA SILVA JOSE/ ADOLF', 'METALURGICA', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(1103, '3088', 'ENRIFER SRL', '3088', 'ENRIFER', 'S', 'fadabi@fadabi.com.ar,sabinamartino@speedy.com.ar', 'BOULOGNE SUR MER 1721', 'BANFIELD ESTE', '4202-6523', 'MARTIN/CRISTIAN', 'FCA. ABERTURAS', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(1104, '3105', 'TORNERIA ESCALADA SRL', '3105', 'DANIELA', 'S', 'info@torneriaescalada.com.ar ', 'TIMOTE 4352', 'R. DE ESCALADA OESTE', '4202-7075', 'DANIELA', 'METALURGICA', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(1105, '3111', 'MACROPEL SRL', '3111', 'ALBERTO', 'S', 'macropelventas@yahoo.com.ar ', 'LARRAZABAL 1176', 'AVELLANEDA ESTE', '4203-3715', 'LUIS/OSVALDO', 'PAPELERA', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(1106, '3114', 'PESUES S.A.', '3114', 'ROSANA', 'S', 'pesues@gncplus.com.ar,michelinij@gncplus.com.ar,mazzucac@audes.com.ar,yanisr@gncplus.com.ar', 'BUSTAMANTE 1612', 'LANUS ESTE', '4204-9597/4265-0577', 'JUAN P. MORENO', 'EXP. DE NAFTA', 'Enviar dato faltante !! ', 'PREVENCION'),
(1107, '3127', 'GANDARA SA', '3127', 'ROCIO', 'S', 'marcel@gandaranet.com.ar,rita@gandaranet.com.ar', 'TALCAHUANO 2747', 'LANUS OESTE', '4209-1511', 'CELESTINO GANDARA', 'FCA. DE CALZADO', 'marcel@gandaranet.com.ar ', 'BERKLEY INTERNATIONAL'),
(1108, '3136', 'PINTURERIA PROFESIONAL S.A.', '3136', 'PINTURERIA', 'S', 'jclezama@pintureriaprofesional.com ', 'RIVADAVIA 901', 'AVELLANEDA OESTE', '4208-1770', 'JUAN CRUZ', 'PINTURERIA', 'nmagnani@pintureriaprofesional.com.ar ', 'MAPFRE'),
(1109, '3139', 'FEDERACION PATRONAL SEGUROS SA', '3139', 'FEDERACION', 'S', 'rsagarna@fedpat.com.ar, sgugliar@fedpat.com.ar', 'ALSINA 815 B', 'CIUDAD AUTONOMA DE BUENOS AIRE', '4863-2222', 'JORGE RIBAS', 'Enviar dato faltante !!', 'rzagarna@fedpat.com.ar, sgugliar@fedpat.com.ar', 'FEDERACION PATRONAL'),
(1110, '3140', 'PATANE Y MOREIRA SA', '3140', 'MONICA', 'S', 'bmonica@pm-sa.com.ar ', 'CNEL DIAZ 1367', 'AVELLANEDA OESTE', '4208-0196/6032', 'JORGE N PATANE/NATALIA', 'DIST. DE BEBIDA', 'Enviar dato faltante !! ', 'GALENO'),
(1111, '3155', 'IRMET S.A.I.C', '3155', 'LUIS', 'S', 'luis.bedetti@irmet.com.ar ', 'BLANCO ENCALADA 2715', 'LANUS ESTE', '4246-4041/4043', 'JORGE/LUIS BEDETTI', 'METALURGICA', 'marisa.petenello@irmet.com.ar ', 'PROVINCIA'),
(1112, '3168', 'COOP.DE TRAB.PEVERI LTDA', '3168', 'ANDREA', 'S', 'info@metalurgicapever.com.ar ', 'CONDARCO 290', 'LANUS ESTE', '4220-4002/4246-4223', 'JUAN CARLOS-ANDREA P', 'METALURGICA', 'Enviar dato faltante !! ', 'NO POSEE ART'),
(1113, '3176', 'DIPROEL SRL', '3176', 'ENRIQUE', 'S', 'diproelsrl@speedy.com.ar ', 'LAVALLE 461 PB- OSORIO 2155 (LIBRETAS)', 'BERNAL- LANUS OESTE (LIBRETA', '4259-5201/4969', 'ENRIQUE-SONIA', 'DISTRIBUIDORA', 'diproelsrl@diproel.com.ar,sonia@diproel.com.ar', 'PROVINCIA'),
(1114, '3177', 'GARCIA JAVIER ALEJANDRO', '3177', 'JAVIER', 'S', 'info@conaire.com.ar,conaire@speedy.com.ar ', 'CNO GRAL BELGRANO 10,5 NAVE 8', 'BERNAL (PARQ. IND. QUIL', '4270-0597/1536817000', 'EMILIO GARCIA', 'METALURGICA', 'Enviar dato faltante !! ', 'QBE'),
(1115, '3179', 'TARANTO SAN JUAN SA', '3179', 'RRHH', 'S', 'rrhh@taranto.com.ar,mezay@taranto.com.ar,carballos@taranto.com.ar,valler@taranto.com.ar', 'MARIO BRAVO 641', 'AVELLANEDA OESTE', '4135-9000 0264-4293901', 'NATALIA-BARBARA-SABRINA', 'METALURGICA', 'furlani@taranto.com.ar,correan@taranto.com.ar,correae@taranto.com.ar', 'LA SEGUNDA'),
(1116, '3190', 'REBRON SRL', '3190', 'CLAUDIA', 'S', 'info@rebron.com.ar ', 'GUTEMBERG 2042', 'AVELLANEDA OESTE', '4208-1676', 'ALBERTO', 'METALURGICA', 'Enviar dato faltante !! ', 'SMG'),
(1117, '3192', 'GRE MAN E HIJOS SRL', '3192', 'GREMAN', 'S', 'greman@uolsinectis.com.ar,antonio.russo@speedy.com.ar', 'MURGUIONDO 233/45', 'VALENTIN ALSINA', '4208-5232/0807', 'SERGIO', 'ZAPATER.MAYORIS', 'info@greman.com.ar ', 'SMG'),
(1118, '3206', 'FALCON FRANCISCO', '3206', 'FRANCISCO', 'S', 'matriceriafalcon@hotmail.com,falconmet@live.com.ar', 'JOSE INGENIERO 654', 'AVELLANEDA ESTE', '4204-3378', 'HECTOR', 'METALURGICA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1119, '3236', 'FUNDARO FORTUNATA', '3236', 'TINA', 'S', 'residenciamisnonos@yahoo.com.ar ', 'O HIGGINS 253', 'SARANDI', '4203-8971/4205-2009', 'RUBEN/TINA', 'GERIATRICO', 'Enviar dato faltante !! ', 'GALENO'),
(1120, '3259', 'CONS. PROP.29 DE SEPT. 1960/72', '3259', 'GABRIEL', 'S', 'gabrielcosentino@infovia.com.ar ', 'ANATOLE FRANCE 1617', 'LANUS ESTE', '4249-1877', 'ADMINISTRACION COSENTINO', 'CONSORCIO', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(1121, '3265', 'OMLAN S.R.L.', '3265', 'SILVINA', 'S', 'silvinacarre@hotmail.com,omlamsrl@hotmail.com ', 'EVA PERON 1270', 'LANUS ESTE', '4-240-8164', 'CARREIRO RUBEN', 'TRANSPORTE', 'Enviar dato faltante !! ', 'GALENO'),
(1122, '3274', 'IRONPLAST S.R.L.', '3274', 'HORACIO', 'S', 'ironplast@sinectis.com.ar ', 'LAS PIEDRAS 2860', 'LANUS ESTE', '4246-3089', 'FERNANDO', 'PLASTICOS INYEC', 'Enviar dato faltante !! ', 'PREVENCION'),
(1123, '3275', 'NIDERA S.A.', '3275', 'MARCELA', 'S', 'monorato@nidera.com.ar,pferrero@nidera.com.ar ', 'C.PELLEGRINI 4370', 'V.ALSINA', '4135-7700 MARCELA (7705)', 'ING. BURDISSSO BIMES', '15-5464-5522 AN', 'Enviar dato faltante !! ', 'GALENO'),
(1124, '3277', 'HERMES TRISMEGISTO S.A.', '3277', 'VICTORIA', 'S', 'vtoti@hermestrismegisto.com.ar,vbaldez@hermestrismegisto.com.ar', 'PEDERNERA 2950', 'LANUS ESTE', '4246-7965/8606', 'VICTORIA', 'INDUMENTARIA', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(1125, '3282', 'NOMASGOT. S.R.L.', '3282', 'HECTOR', 'S', 'nomasgot@gmail.com ', 'R. FALCON 1561', 'L. DE ZAMORA', '4282-6694', 'HECTOR/ GUSTAVO', 'FABRICA DE PINTURAS', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(1126, '3283', 'METALURGICA LASER SRL', '3283', 'LASER', 'S', 'metalurgicalaser@hotmail.com,info@metalurgicalaser.com.ar', 'COMBATIENTES DE MALVINAS 1365', 'DOCK SUD', '4201-0734/9432', 'KATY/DANIEL', 'METALURGICA', 'Enviar dato faltante !! ', 'QBE'),
(1127, '3293', 'CORP. LAB. AMB. LATINOAMERICA', '3293', 'ANALIA', 'S', 'pablo.venero@alsglobal.com, acavallo@corplab.net', 'C.PI#EIRO 358', 'SARANDI', '4265-0945/.4265-2000 I 10', 'Enviar dato faltante !!', 'SERVICIOS', 'pablo.venero@alsglobal.com ', 'ASOCIART A.R.T.'),
(1128, '3294', 'FARMACIA UOM LANUS SCS', '3294', 'UOM', 'S', 'acomandatore@uomlanus.com.ar,jgoldberg@uomlanus.com.ar,pgonta@uomlanus.com.ar,tsciscivo@uomlanus.com.ar', 'H. YRIGOYEN 4414', 'LANUS OESTE', '4357-9050', 'ALEJANDRA', 'FARMACIA', 'administracion@uomlanus.com.ar,jmontes@uomlanus.com.ar', 'QBE'),
(1129, '3295', 'INDUSTRIAS TESI S.A.', '3295', 'JUAN', 'S', 'industriastesi@yahoo.com.ar ', 'CHOELE CHOEL 1854', 'AVELLANEDA', '4208-3662', 'JUAN / VICENTE', 'METALURGICA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1130, '3299', 'TUR S.C.A.', '3299', 'NORMA', 'S', 'tursca@tursca.com.ar,marianou@tursca.com.ar ', 'COLECTORA AUT. CAÑUELAS KM. 41,5', 'TRISTAN SUAREZ', '4208-4010 /4209-0431', 'MARIANO', 'METALURGICA', 'tursca@tursca.com.ar,marianou@tursca.com.ar,martin@tursca.com.ar', 'ASOCIART A.R.T.'),
(1131, '3337', 'POLTER ARGENTINA S.R.L.', '3337', 'MARTA', 'S', 'aliciasaravia@polter.com.ar,compras@polter.com.ar', 'ARENALES 2172', 'AVELLANEDA OESTE', '4208-3586/4228-7657', 'ALICIA', 'PLASTICOS', 'compras@polter.com.ar ', 'PROVINCIA'),
(1132, '3350', 'BARDISA ANGEL FRANCISCO', '3350', 'BARDISA', 'S', 'angelbardisa@yahoo.com.ar ', 'SARMIENTO 671', 'LANUS ESTE', '4240-1078', 'JORGE', 'VIDRIERIA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1133, '3352', 'CENSIMA S.A.I.C.', '3352', 'KARINA', 'S', 'ksosa@censima.com.ar ', 'CANGALLO 4802', 'VILLA DOMINICO', '4246-8585/75/ 4246-1334', 'OSCAR', 'CHATARRA', 'florencia@censima.com.ar ', 'FEDERACION PATRONAL'),
(1134, '3354', 'PAFUMI EDUARDO D. Y CARNUCCIO', '3354', 'DANIEL', 'S', 'maderaspafumi@hotmail.com ', 'C. GLADE 389', 'BANFIELD', '4248-7451', 'PAFUMI EDUARDO', 'MADERERA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1135, '3355', 'LIBRERIA PLATERO (SCHIAFFINO EDUARDO)', '3355', 'EDUARDO', 'S', 'schiaffino.fede@gmail.com,oficina.platero@gmail.com', 'H. IRIGOYEN 4428', 'LANUS', '4-240-8219', 'FEDERICO-EDUARDO/MARIANA', 'LIBRERIA', 'oficina.platero@gmail.com ', 'FEDERACION PATRONAL'),
(1136, '3365', 'INSTITUTO SAN FRANCISCO DE ASIS', '3365', 'MARIELA', 'S', 'isfa_lanus@hotmail.com.ar,sanfrancisco_fatima@hotmail.com,eliamato@yahoo.com.ar', 'LITUANIA 3144', 'REMEDIOS DE ESCALADA', '4267-6577', 'ADA - HERMANA VALENTINA', 'EDUCATIVA', 'mlruscica@hotmail.com ', 'ASOCIART A.R.T.'),
(1137, '3366', 'INDUSTRIAS DELGADO S.A.', '3366', 'MIGUEL', 'S', 'maraneo@industriasdelgado.com.ar,ximena@industriasdelgado.com.ar', 'VILLA DE LUJAN 1349', 'SARANDI', '4246-0090 INT 112', 'LUIS DELGADO/MARIA', 'METALURGICA', 'Enviar dato faltante !! ', 'LA SEGUNDA'),
(1138, '3369', 'MENTUCCI NOLBERTO (SUCESION)', '3369', 'MARIA', 'S', 'norflex.info@gmail.com ', 'CNEL MOLINEDO 2841', 'LANUS OESTE', '4208-1633/4228-4332', 'KARINA - MARIA', 'METALURGICA', 'norflex.info@gmail.com ', 'LA CAJA'),
(1139, '3375', 'BIYEMAS S.A.', '3375', 'ADRIANA', 'S', 'aperdomo@grupoagg.com,eperez@grupoagg.com,cmazza@grupoagg.com,lfigueroa@grupoagg.com', 'MAIPU 101', 'AVELLANEDA ESTE', '4229-0900', 'ADRIANA-ERIKA', 'BINGO', 'proveedores@grupoagg.com ', 'ASOCIART A.R.T.'),
(1140, '3380', 'LA PRIMERA GALICIA S.R.L.', '3380', 'CLAUDIO', 'S', 'NO MAIL 2 ', 'CHARCAS 415', 'TEMPERLEY ESTE', '4264-2300', 'SR. CLAUDIO O JOSE', 'FCA. DE PASTAS', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1141, '3382', 'DAYKENT S.A.', '3382', 'MARTHA', 'S', 'marthabenitez_daykent@hotmail.com ', 'BLANCO ENCALADA 2894', 'LANUS ESTE', '4230-9595', 'MARTHA BENITEZ/DIEGO', 'QUIMICA', 'marthabenitez_daykent@hotmail.com ', 'GALENO'),
(1142, '3385', 'METALIX S.R.L.', '3385', 'GISELA', 'S', 'info@metalix.com.ar ', 'REMEDIOS DE ESCALADA 1243', 'LANUS OESTE', '4218-0882', '/GABRIEL', 'METALURGICA', 'Enviar dato faltante !! ', 'LA CAJA'),
(1143, '3391', 'MARKET AVELLANEDA S.R.L.', '3391', 'PABLO', 'S', 'aca66@fibertel.com.ar ', 'AV MITRE 985', 'AVELLANEDA', '4222-9772 .4222-9966', 'SR. PABLO - HORACIO', 'EST. DE SERV.', 'aca66@fibertel.com.ar ', 'PROVINCIA'),
(1144, '3393', 'MANUFACTURA SUREÑA S.R.L.', '3393', 'SEBASTIAN', 'S', 'VELASMS@GMAIL.COM ', 'OLIDEN 2990', 'LANUS OESTE', '4208-3912/14', 'REGINA/ALBERTO', 'FCA DE BROCHAS', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(1145, '3400', 'PORZIO LOGISTICA SA', '3400', 'PATRICIA', 'S', 'porzio@speedy.com.ar,patricia@porziologistica.com.ar', 'CJAL. HECTOR NOYA 1540', 'LANUS OESTE', '4247-8046/4249-0810', 'PATRICIA', 'TRANSPORTE', 'Enviar dato faltante !! ', 'QBE'),
(1146, '3425', 'TEXTIL FIBREX S.A.I.C.', '3425', 'JULIO', 'S', 'ventas@campomar.com.ar,compras@campomar.com.ar', 'TTE. GRAL. JUAN D. PERON 3741', 'VALENTIN ALSINA', '4208-5413/6', 'JULIO/CARLA/BETTY', 'TEXTIL', 'finanzas@campomar.com.ar ', 'QBE'),
(1147, '3427', 'AIR PRODUCTS S.R.L.', '3427', 'MARCELA', 'S', 'osman@ciudad.com.ar,ventas@aire-comprimido.com.ar', 'LAPRIDA 631/5', 'AVELLANEDA', '4222-1008', 'MARCELA/LAURA/ESTELA', 'METALURGICA', 'Enviar dato faltante !! ', 'QBE'),
(1148, '3429', 'NAPAL ROXANA VERONICA', '3429', 'ROXANA1', 'S', 'uniformar@ciudad.com.ar ', 'PTE. SARMIENTO 1372', 'LANUS ESTE', '4225-8666', 'ROXANA NAPAL', 'TEXTIL', 'Enviar dato faltante !! ', 'GALENO'),
(1149, '3457', 'CASAMEN S.A.C.I.A', '3457', 'CARLOS', 'S', 'antonio@mendia.com.ar,sistemas@mendia.com.ar,diegodigiano@mendia.com.ar', 'M. PAZ 963', 'LANUS OESTE', '4-241-0161/8984 INT114 RH', 'ANTONIO/DIEGO', 'PASTAS', 'antonio@mendia.com.ar,florencia@mendia.com.ar,emanuel@mendia.com.ar', 'ASOCIART A.R.T.'),
(1150, '3470', 'RECTIFICADORA COLON S.R.L.', '3470', 'LIDIA', 'S', 'rectificadoracolon@hotmail.com,administracion@rectificadora-colon.com.ar', 'COLON 3660', 'LANUS OESTE', '4267-4443 8532 4139-7587', 'adrian lidia', 'RECTIFICADORA', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(1151, '3477', 'ATAUDES CENTENARIO', '3477', 'ROBERTO', 'S', 'ataudescentenario@outlook.com ', 'SUIPACHA 2734', 'LANUS ESTE', '4230-7641', 'R. DI PASQUA/ALBERTO/GEORGINA', 'FABRICA', 'ataudescentenario@outlook.com ', 'PROVINCIA'),
(1152, '3478', 'IACONO RAFAEL', '3478', 'ALEXANDRA', 'S', 'soldeezpeleta@hotmail.com,ypflabanderita@hotmail.com', 'AV CENTENARIO 4685', 'EZPELETA', '4216-8321/4207-9670', 'RAFAEL', 'EST. DE SERV.', 'Enviar dato faltante !! ', 'SMG'),
(1153, '3479', 'LLAMITA S.A.', '3479', 'HECTOR', 'S', 'llamita_sa@ciudad.com.ar,llamita-sa@hotmail.com', 'UCRANIA 1584', 'VALENTIN ALSINA', '4209-0938', 'HECTOR JUGUERA', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1154, '3481', 'MUSICPLAST SRL', '3481', 'RICARDO', 'S', 'ricardoricardo180@hotmail.com ', 'RUCCI 2032', 'VALENTIN ALSINA', '5290-2704', 'KUNGEL RICARDO', 'PLASTICOS', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(1155, '3490', 'TELGATEX SRL', '3490', 'LORENA', 'S', 'telgatex@speedy.com.ar,flaviabruno@telgatex.com.ar', 'ANCHORIS 2677', 'TEMPERLEY', '4264-0868/0980/3048', 'FLAVIA BRUNO/ LORENA BRUNO', 'TEXTIL', 'flaviabruno@telgatex.com.ar ', 'ASOCIART A.R.T.'),
(1156, '3496', 'CARTONERIA ACEVEDO SACI', '3496', 'CLAUDIA', 'S', 'recepcion@cartacevedo.com.ar,ventas@cartacevedo.com.ar,administracion@cartacevedo.com.ar', 'GUTEMBERG 1872', 'AVELLANEDA OESTE', '4208-8334/9548', 'MONICA 15-5226-9905', 'ENVASES FCA', 'Enviar dato faltante !! ', 'ART LIDERAR SA'),
(1157, '3506', 'ASADOR CRIOLLO DE OLIVERA GALE', '3506', 'GERARDO', 'S', 'gerardo.87@hotmail.com ', 'R. DE E. SAN MARTIN 2087', 'LANUS OESTE', '4209-8137/1154518799', 'MAURO / GERMAN', 'CASA DE COMIDAS', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1158, '3516', 'CONSORCIO JUAN DE GARAY 26/34', '3516', 'MARISA', 'S', 'mrech_adm@yahoo.com.ar ', 'JUAN DE GARAY 34', 'REMEDIOS DE ESCALADA OESTE', '3528-4932/155515-3977', 'MARISA RECH', 'CONSORCIO', 'Enviar dato faltante !! ', 'PREVENCION'),
(1159, '3518', 'GEN ROD S.A.', '3518', 'NESTOR', 'S', 'personal@genrod.com.ar ', 'JUAN D. PERON 3989', 'BANFIELD OESTE', '4286-1198', 'MARIA GARCIA/NESTOR', 'METALURGICA', 'mrodriguez@genrod.com.ar ', 'GALENO'),
(1160, '3519', 'JELUZ SACIFIA', '3519', 'GABRIELA', 'S', 'gabriela@jeluz.net,romina@jeluz.net ', 'EJERCITO DE LOS ANDES 458', 'BANFIELD OESTE', '4286-8446 / 15-542-62838', 'GENTILE/DONATO', 'FAB.PROD. ELECT', 'carolina@jeluz.net,gonzalo@jeluz.net,carolinaborda@jeluz.net', 'LA CAJA'),
(1161, '3531', 'CERAMICA SANITARIA 8 DE JULIO SRL', '3531', 'JORGE', 'S', '8dejulio@live.com.ar ', '1 DE MAYO 3551', 'LANUS OESTE', '4-262-4972', 'JORGE/VICTOR WORONCZUK', 'CERAMICAS', '8dejulio@live.com.ar ', 'MAPFRE'),
(1162, '3537', 'DI CANDIA HNOS', '3537', 'MARCOS', 'S', 'pastadicandia@yahoo.com.ar ', 'AV. PTE PERON 1432', 'LOMAS DE ZAMORA', '4282-8323', 'LILIANA CANDIA', 'FCA DE PASTAS', 'Enviar dato faltante !! ', 'QBE'),
(1163, '3557', 'INDUSTRIAS MET. MOLITEC S.R.L.', '3557', 'IVANA', 'S', 'oscarmolinero@hotmail.com,molitec@gmail.com ', 'JUAN FARREL 1824', 'VALENTIN ALSINA', '4228-7558', 'OSCAR MOLINERO', 'METALURGICA', 'Enviar dato faltante !! ', 'QBE'),
(1164, '3558', 'VEMART S.A.', '3558', 'DAMIAN', 'S', 'info@vemart.com.ar ', 'CRISOLOGO LARRALDE 2869', 'AVELLANEDA ESTE', '4205-0622', 'CRISTIAN NOVELLO', 'DIST. MAYORISTA', 'Enviar dato faltante !! ', 'GALENO'),
(1165, '3560', 'GOYA CORRIENTES SRL', '3560', 'LEILA', 'S', 'andrea.sanchez@servicioargentino.com.ar ', 'M.T. DE ALVEAR 1369/71', 'CAPITAL FEDERAL', '48152576 48100916/17', 'LEYLA/JUAN J.LOPEZ', 'SEGURIDAD', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1166, '3562', 'COLEGIO SANTO CRISTO', '3562', 'VIVIANA', 'S', 'novasluis@hotmail.com ', 'PERGAMINO 1749', 'LANUS ESTE', '4247-7941-4249-1442', 'LUIS NOVAS', 'EDUCATIVA', 'Enviar dato faltante !! ', 'GALENO'),
(1167, '3571', 'TALLERES NEUQUEN S.A.', '3571', 'GLENDA', 'S', 'talleresneuquen@talleresneuquen.com.ar ', 'PERIODISTA PRIETO 366 PB', 'LANUS ESTE', '4205-7279/7340', 'GUSTAVO', 'METALURGICA', 'Enviar dato faltante !! ', 'LA CAJA'),
(1168, '3576', 'MINUTOLO S.R.L.', '3576', 'EDGARDO', 'S', 'info@minutolo.com.ar,carlos@minutolo.com.ar ', 'TTE. GENERAL PERON 847', 'LANUS OESTE', '4241-4496/7820', 'SERGIO ALVAREZ/VICENTE MINUTOLO', 'METALURGICA', 'cobranzas@minutolo.com.ar ', 'QBE'),
(1169, '3577', 'CARTOCAN SRL', '3577', 'LILIANA', 'S', 'info@cartocan.com.ar ', 'LA RIOJA 1642', 'AVELLANEDA OESTE', '4208-0997/2891', 'LILIANA', 'IND. PAPELERA', 'Enviar dato faltante !! ', 'GALENO'),
(1170, '3582', 'RODRIGO MONICA EDITH', '3582', 'MONICA', 'S', 'monica.casella@yahoo.com.ar ', 'VERON DE ASTRADA 3945', 'LANUS ESTE', '4246-0771', 'MONICA', 'TRANSPORTE', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1171, '3609', 'ALIMENTO SARANDI S.R.L.', '3609', 'PAOLA', 'S', 'paola_archanco@yahoo.com.ar,analia@deliciasdoradas.com', 'AV BELGRANO 2565', 'SARANDI', '4204-9407/4215-0126', 'PABLO- PAOLA', 'FCA DE PASTAS', 'proveedores@deliciasdoradas.com ', 'FEDERACION PATRONAL'),
(1172, '3621', 'ROS METAL S.R.L.', '3621', 'CARLOS', 'S', 'rosmetalsrl@gmail.com,ventas@rosmetal.com.ar ', 'AV. RIVADAVIA 1480', 'AVELLANEDA OESTE', '4208-9136/4218-0243/4209-', 'CARLOS V./M. TERESA', 'METALURGICA', 'Enviar dato faltante !! ', 'LA HOLANDO'),
(1173, '3622', 'SPORT SHOES S.R.L.', '3622', 'NORMA', 'S', 'info@skewer.com.ar ', 'PAMPA 937', 'VALENTIN ALSINA', '/4228-2356', 'NORMA', 'FCA. DE CALZADO', 'info@skewer.com.ar ', 'QBE'),
(1174, '3632', 'TECMATEX S.R.L.', '3632', 'ELIZABETH', 'S', 'elizabethl@speedy.com.ar,marielabbq@gmail.com ', 'CNEL WARNES 3040', 'VALENTIN ALSINA', '4208-4475/4209-3502', 'FERNANDO LUKCS', 'TEXTIL', 'Enviar dato faltante !! ', 'GALENO'),
(1175, '3638', 'CONS.PRENSA ARGENTINA 1850', '3638', 'MARGARITA', 'S', 'snveliz1@hotmail.com ', 'PRENSA ARGENTINA 1850', 'BANFIELD', '4286-7612 1562212270', 'MARGARITA', 'CONSORCIO', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(1176, '3654', 'VICTORIA & TEMPERLEY SRL', '3654', 'SERGIO', 'S', 'jlsobrino@hotmail.com,sergioschirripa@hotmail.com', 'AV. ROLDAN 940', 'BANFIELD ESTE', '15-5523-9631/42/4246-6964', 'Enviar dato faltante !!', 'MADERERA', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(1177, '3655', 'FIPA S.R.L.', '3655', 'MARCELA', 'S', 'fipasrl@hotmail.com ', 'POSADAS 1047', 'LANUS ESTE', '4246-2438', 'NESTOR VAIRO/BAUMAN', 'METALURGICA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1178, '3657', 'SERVICE VIAL S.A.', '3657', 'ALEJANDRA', 'S', 'serv_vial@hotmail.com,servicevial@speedy.com.ar', 'AV. MITRE 686', 'AVELLANEDA', '4249-1296/5792', 'ALEJANDRA', 'METALURGICA', 'Enviar dato faltante !! ', 'QBE'),
(1179, '3658', 'HECTOR TERRONE E HIJOS S.A.', '3658', 'ALEJANDRA', 'S', 'hectorterrone@speedy.com.ar,ser_vial@hotmail.com,servicevial@speedy.com.ar', 'J.D.PERON 620', 'LANUS OESTE', '4249-1296/4247-4775', 'ALEJANDRA', 'METALURGICA', 'Enviar dato faltante !! ', 'SMG'),
(1180, '3659', 'VAN GOGH(DE ALEJANDRO CASADIO)', '3659', 'ULISES', 'S', 'muebles@vangogh.com.ar ', 'ITAPIRU 488', 'VALENTIN ALSINA', '4208-7486', 'JOSE CASTILLO', 'MUEBLERIA', 'Enviar dato faltante !! ', 'PREVENCION'),
(1181, '3663', 'GAS LANUS S.A.', '3663', 'LORENA', 'S', 'gaslanus@hotmail.com ', '25 DE MAYO 883', 'LANUS OESTE', '4249-6556', 'LORENA/ TAMARA1532342928', 'EST. SERV.', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1182, '3665', 'NITARGEN SAIC', '3665', 'HUGO', 'S', 'info@nitargen.com,agustina@nitargen.com,hugos@nitargen.com,joseojeda@nitargen.com', 'QUINDIMIL 4505', 'VALENTIN ALSINA', '42084596', 'CARLOS AGUI', 'QUIMICA', 'joseojeda@nitargen.com ', 'PROVINCIA'),
(1183, '3675', 'METAL WORLD ARGENTINA S.A.', '3675', 'LIA', 'S', 'info@metalworldarg.com ', 'EVA PERON 2566 1ER PISO', 'LANUS ESTE', '4249-5444', 'LIA/DANIEL PALACIOS', 'HERRAMIENTAS EL', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(1184, '3677', 'JARDIN DE INFANTES 913', '3677', 'NORA', 'S', 'noraolin@yahoo.com.ar ', 'FERRE 2180', 'LANUS ESTE', '4241-3845', 'SILVIA-TERESA-NORA', 'EDUCATIVA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1185, '3700', 'COLEGIO SAN MIGUEL ARCANGEL', '3700', 'HUGO', 'S', 'egbisma@hotmail.com ', 'CNEL MOLINEDO 2406', 'LANUS OESTE', '4208-9816', 'AREA MARIANA FORESTE', 'EDUCATIVA', 'Enviar dato faltante !! ', 'SMG'),
(1186, '3704', 'TALLERES VENUS S.A.', '3704', 'CARLOS', 'S', 'talleres_venus@ciudad.com.ar ', 'CORDERO 145', 'AVELLANEDA OESTE', '4201-7565', 'CARLOS SUAREZ/NANCY', 'METALURGICA', 'Enviar dato faltante !! ', 'MAPFRE'),
(1187, '3705', 'TERMOPAL S.A.', '3705', 'GRACIELA', 'S', 'termopalsa@speedy.com.ar,arielpalumbo@hotmail.com,termosheineken@yahoo.com.ar', 'HERNANDARIAS 3845', 'LANUS OESTE', '4-286-1344/3729', 'ARIEL-ANTONIO-GRACIELA', 'METALURGICA', 'Enviar dato faltante !! ', 'LA CAJA'),
(1188, '3721', 'HERRERIA ALFA S.R.L.', '3721', 'PABLO', 'S', 'alfarrhh@yahoo.com.ar,herreriaalfa@speedy.com.ar,soledad_alfa@hotmail.com', 'H. YRIGOYEN 5601/25', 'REMEDIOS DE ESCALADA OESTE', '4240-0884/4249-0497', 'CLAUDIA', 'HERRERIA', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(1189, '3735', 'METALURGICA KURPS S.A.', '3735', 'KURPS', 'S', 'metalurgicakurps@speedy.com.ar ', 'AYACUCHO 2855', 'LANUS ESTE', '4246-8875/8804', 'CARLOS', 'METALURGICA', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(1190, '3741', 'DULZURAS S.A.', '3741', 'DIEGO', 'S', 'dulzurask@yahoo.com.ar ', 'SITIO DE MONTEVIDEO 1150', 'LANUS ESTE', '4225-8403/1549917101', 'SR. DIEGO SERDEIRA', 'KIOSKO', 'dulzurask@yahoo.com.ar ', 'BERKLEY INTERNATIONAL'),
(1191, '3771', 'LIMINDAR S.A.', '3771', 'AUGUSTO', 'S', 'compras@limindar.com.ar,ventas@limindar.com.ar,administracion@limindar.com.ar,tecnica@limindar.com.ar', 'CMO. GRAL. BELGRANO 2041', 'LANUS ESTE', '4265-0089/4204-5303', 'RAUL ESPOSITO', 'INDUSTRIA QUIMI', 'ventas@limindar.com.ar ', 'MAPFRE'),
(1192, '3773', 'SISTEMAS DE FIJACIONES SA', '3773', 'TAMARA', 'S', 'tvilaplana@tel-sa.com,aflores@tel-sa.com,lfuentes@tel-sa.com,rwerenicz@tel-sa.com,mregueiro@tel-sa.com', 'ARISTOBULO DEL VALLE 1057', 'LANUS OESTE', '4249-4474 INT 302 4240-48', 'LEONELA ACC 1559284289', 'METALURGICA', 'vramirez@tel-sa.com ', 'GALENO'),
(1193, '3786', 'EMILIO MARTINO S.R.L.', '3786', 'ANA', 'S', 'emiliomartinosrl@aol.com ', 'PASAJE SARRAT 575', 'AVELLANEDA ESTE', '4203-6625/6650', 'LILIANA/ANA MARIA', 'METALURGICA', 'Enviar dato faltante !! ', 'LA CAJA'),
(1194, '3795', 'MONTEVERDE SA', '3795', 'FERNANDA', 'S', 'monteverde@gncplus.com.ar ', 'MARTIN RODRIGUEZ 184', 'LOMAS DE ZAMORA OESTE', '4267-4343 INT1', 'FERNANDA ARIAS', 'EST. DE SERV.', 'monteverde@gncplus.com.ar ', 'ART LIDERAR SA'),
(1195, '3802', 'COOP. DE TRABAJO GOLDEN QUALL', '3802', 'FRANCO', 'S', 'ventas@goldenquail.com.ar,franco@goldenquail.com.ar', 'CAMPICHUELO 2441/3', 'SARANDI', '4203-0792', 'FRANCO/ SEBASTIAN', 'PELADERO DE AVE', 'Enviar dato faltante !! ', 'NO POSEE ART'),
(1196, '3809', 'G.N.C. OLIMPO SRL', '3809', 'LAURA', 'S', 'gncolimpo@hotmail.com.ar ', 'CNO DE CINTURA Y OLIMPO', '9 DE ABRIL', '4693-1621/2993', 'LAURA VARGAS', 'ESTACION DE SERVICIO', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(1197, '3817', 'PEREZ ROBERTO ARIEL', '3817', 'ROBERTO', 'S', 'arielperez@datamarkets.com.ar,lanus2@datamarkets.com.ar', 'O HIGGINS 1960', 'LANUS E.', '4241-3872', 'ARIEL / NORMA', 'PLASTICOS', 'Enviar dato faltante !! ', 'GALENO'),
(1198, '3830', 'OLIVERA MARIA DE LOS ANGELES (INDUSPALLETS)', '3830', 'MARIA', 'S', 'info@induspallets.com ', 'LAS HIGUERITAS 548', 'LANUS ESTE', '4220-9600', 'MARIA OLIVERA/ANTONI', 'MADERERA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1199, '3845', 'SIBA UTE', '3845', 'SOLEDADMARIA', 'S', 'mblanco2140@yahoo.com.ar ', 'FELIPE AMOEDO 3450', 'QUILMES', '4250-4145 (MALENA', '1563758129 ING.155240-3744(BRAVO GABRIEL', '1561932006 PICO', 'gloureiro@sibaute.com.ar ', 'SMG'),
(1200, '3859', 'NISURT SH', '3859', 'ROXANA', 'S', 'rcalamita@nisurt.com.ar ', 'J.P. ANGULO 1488', 'DOCK SUD', '4201-1056', 'LORENZO', 'REPARACIONES', 'rcalamita@nisurt.com.ar ', 'QBE'),
(1201, '3879', 'COPLASTIC SRL', '3879', 'MARIA', 'S', 'plasticos@escape.com.ar ', 'GRAL MADARIAGA 1871', 'SARANDI', '4205-1621', 'CRISTIAN CAMPILLO', 'PLASTICOS', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1202, '3884', 'CONS. UCRANIA 554', '3884', 'MARCELO', 'S', 'silviaghianda@uolsinectis.com.ar ', 'AV. SAN JUAN 572 6° B', 'CAPITAL FEDERAL', '4307-0907', 'SILVIA GHIANDA', 'CONSORCIO', 'Enviar dato faltante !! ', 'GALENO'),
(1203, '3905', 'MIGUEL ANGEL BOAN', '3905', 'ALEJANDRA', 'S', 'mab@netex.com.ar ', 'SAENZ PENA 378', 'REMEDIOS DE ESCALADA OESTE', '4241-8123 4225-5990', 'JAVIER O ALEJANDRA', 'TALLER GRAFICO', 'Enviar dato faltante !! ', 'LA CAJA'),
(1204, '3910', 'JUAN NIERI SRL', '3910', 'ROXANA', 'S', 'rcalamita@nisurt.com.ar ', 'J.P ANGULO 1488', 'DOCK SUD', '4201-1056', 'LORENZO/ROBERTO/ZULMA', 'REPARACIONES', 'Enviar dato faltante !! ', 'QBE'),
(1205, '3913', 'ABANILLAS S.A.', '3913', 'LORENA', 'S', 'michelinij@gncplus.com,abanillas@audes.com.ar,mazzucac@audes.com.ar', 'LAS HERAS 1740', 'LOMAS DE ZAMORA', '4202-4955/15-3033-9974/40', 'LORENA', 'ESTACION DE SERVICIO', 'Enviar dato faltante !! ', 'LA CAJA'),
(1206, '3916', 'COMPUTEL SRL', '3916', 'JORGE', 'S', 'computelsrl@computelsrl.com.ar,jorgedorta@computel.com.ar,jorgeedorta@yahoo.com.ar', 'PRINGLES 2859', 'LANUS ESTE', '4246-3009', 'MIRIAM', 'FCA.DE CABLES', 'Enviar dato faltante !! ', 'LA CAJA'),
(1207, '3917', 'TEX COLOUR SRL EN FORMACION', '3917', 'OSCAR', 'S', 'texcolour.ad@gmail.com ', 'COTAGAITA 1755', 'LANUS ESTE', '4230-9009/4220-7543', 'CRISTIAN O VERONICA', 'ESTAMPADOS', 'galoocristian@yahoo.com.ar ', 'LA CAJA'),
(1208, '3923', 'JORGE RUBEN RODRIGUEZ', '3923', 'JORGE', 'S', 'NO MAIL 2 ', 'BUERAS 1072', 'LANUS ESTE', '4247-7946', 'JORGE', 'GALLETITERIA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1209, '3936', 'GUZMAN NACICH', '3936', 'GUZMAN', 'S', 'mduduletz@guzman-nacich.com.ar,rrhh@guzman-nacich.com.ar,ncaputo@guzman-nacich.com.ar', 'C.URUGUAYO 1692', 'VILLA DOMINICO', '4246-1100', 'MATIAS DUDULET/NADIA', 'METALAURGICA', 'amendoza@guzman-nacich.com.ar ', 'PREVENCION'),
(1210, '3937', 'KANDIKO S.A.', '3937', 'ADRIANA', 'S', 'aperdomo@grupoagg.com,eperez@grupoagg.com,cmazza@grupoagg.com,lfigueroa@grupoagg.com', 'MAIPU 101', 'AVELLANEDA ESTE', '4229-0900', 'ADRIANA-ERIKA', 'BINGO', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(1211, '3941', 'CONS DE COOP MELO 2997 LANUS', '3941', 'ANA', 'S', 'civeira_romero@yahoo.com.ar,estudioromero@yahoo.com.ar', 'DR MELO 2997', 'LANUS OESTE', '4240-7256', 'ANA ROMERO', 'CONSORCIO', 'Enviar dato faltante !! ', 'GALENO'),
(1212, '3942', 'REVIDIEGO ALBERTO', '3942', 'GISELA', 'S', 'albertorevidiego@speedy.com.ar ', 'CON GRAL BELGRANO KM 10,5 PQUE INDUSTRIA', 'BERNAL OESTE', '4270-0839', 'WALTER', 'DEPOSITO', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1213, '4001', 'TRANSP. ALTE BROWN SA (L 33)', '4001', 'LUCIANA', 'S', 'personal@mo45.com.ar,linea45@mo45.com.ar,control@mo45.com.ar,l33@mo45.com.ar,siniestros@mo45.com.ar', '14 DE JULIO 4100', 'R.DE ESCALADA OESTE', '4241-4351/4212-5994', '', 'TRANSPORTE DE PASAJEROS', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1214, '4008', 'COLEGIO SANTA MARIA GORETTI', '4008', 'STAMARIA', 'S', 'gorettitemperley@yahoo.com.ar ', 'ARMESTI 2969', 'TEMPERLEY', '4260-0049', 'MARIA ANGELICA MOSO', 'EDUCATIVA', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(1215, '4014', 'TRAFI CORTE SRL', '4014', 'ALICIA', 'S', 'traficortesrl@hotmail.com ', 'REMEDIOS DE ESCALADA 2701', 'VALENTIN ALSINA', '4209-1146/8919', 'ALICIA ORTEGA', 'CONSTRUCCION', 'Enviar dato faltante !! ', 'LA HOLANDO'),
(1216, '4044', 'HERALSUR SH', '4044', 'HERALSUR', 'S', 'heralsursh@yahoo.com.ar ', 'PIO COLIVADINO 747', 'TEMPERLEY ESTE', '.15-6287-6976', 'LUIS', 'METALAURGICA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1217, '4045', 'MURGIA WALTER PABLO', '4045', 'WALTER', 'S', 'metalmurgia@hotmail.com ', 'BRASIL 3467/71', 'VALENTIN ALSINA', '4228-9159', 'EDUARDO', 'METALURGICA', 'Enviar dato faltante !! ', 'NO POSEE ART'),
(1218, '4048', 'CONFITERIA LA PORTENA', '4048', 'TITO', 'S', 'NO MAIL 2 ', '25 DE MAYO 355', 'LANUS OESTE', '4241-8451', 'TITO O TRINI', 'CONFITERIA', 'Enviar dato faltante !! ', 'SMG'),
(1219, '4052', 'FERNANDEZ PEDRO OMAR', '4052', 'PEDRO', 'S', 'ppilcomayo@yahoo.com.ar ', 'AV CABILDO 1895', 'AVELLANEDA OESTE', '4208-4775', 'PEDRO', 'TALLER', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1220, '4059', 'PARANAY SA', '4059', 'ADELA', 'S', 'paranaysa@speedy.com.ar,paranay2@hotmail.com,paranaysa2@gmail.com.ar', 'PTE. PERON 195', 'LOMAS DE ZAMORA', '4282-1846', 'HILARIO WEBER /15-4997-0144', 'EST. DE SERV.', 'Enviar dato faltante !! ', 'SMG'),
(1221, '4071', 'TRANSPORTE 270 SA', '4071', 'LUCIANA', 'S', 'personal@mo45.com.ar,linea45@mo45.com.ar,control@mo45.com.ar,linea70@mo45.com.ar,siniestros@mo45.com.ar', '14 DE JULIO 4100', 'R. DE ESCALADA OESTE', '4241-4351/4919-1249', 'HECTOR CASTRO/DIAZ', 'TRANSPORTE DE PASAJEROS', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1222, '4075', 'CONSORCIO QUINTANA 4/10', '4075', 'ADOLFO', 'S', 'expensas@administracion.net.ar ', 'QUINTANA 4/10', 'CAP. FED.', '4371-8746-', 'ELENA', 'CONSORCIO', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1223, '4076', 'TOP - TIP S.R.L.', '4076', 'JAVIER', 'S', 'essotoptip@hotmail.com ', 'JUAN D. PERON 2330', 'BANFIELD OESTE', '4285-4219', 'JAVIER RODRIGUEZ', 'EST. DE SERV.', 'Enviar dato faltante !! ', 'QBE'),
(1224, '4085', 'QUIMARGEN SRL', '4085', 'MARIA', 'S', 'mltoma@quimargen.com.ar,mgali@quimargen.com.ar', 'ARMESTI 3673', 'TEMPERLEY', '4260-0045 /0117 LAURA', 'LAURA', 'QUIMICA', 'iciacciarelli@quimargen.com.ar ', 'LA CAJA'),
(1225, '4086', 'GUNDIN ROGELIO', '4086', 'GUNDIN', 'S', 'NO MAIL 2 ', 'GUIFRA 970', 'AVELLANEDA', '4201-1563', 'GUNDIN NORBERTO', 'ZINGUERIA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1226, '4102', 'INCOTT S.A. (SPA)', '4102', 'CLAUDIA', 'S', 'rrhh@incott.com.ar,dsuarez@incott.com.ar ', 'MILLAN 80', 'BANFIELD OESTE', '4693-1624/4286-8531', 'DANIEL SUAREZ FAX', 'ALGODONERA', 'notero@incott.com.ar ', 'ASOCIART A.R.T.'),
(1227, '4108', 'IND. PLASTICAS FLODI SA', '4108', 'ISMAEL', 'S', 'ismaelflorek@hotmail.com ', 'MILLER 3028', 'LANUS OESTE', '4241-1211/4240-7248', 'ISMAEL/DARIO', 'PLASTICOS', 'difplast@hotmail.com ', 'SMG'),
(1228, '4118', 'COMUNIDAD HEBREA DR HERZL', '4118', 'DANIELA', 'S', 'kehila_herzlomas@speedy.com.ar ', 'MEEKS 356', 'LOMAS DE ZAMORA OESTE', '4243-0814', 'SEBASTIAN O ALICIA', 'ENTIDAD RELIGIO', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1229, '4124', 'JOSE FIDEL PEREZ SA', '4124', 'CLAUDIA', 'S', 'fidelperez@speedy.com.ar,info@josefidelperez.com', 'ROMA 3925', 'LANUS ESTE', '4242-5292', 'FRUTOS MARTA', '5', 'Enviar dato faltante !! ', 'SMG'),
(1230, '4125', 'CIA. ARG. SEG. VICTORIA', '4125', 'VICTORIA', 'S', 'adriana_stealla@yahoo.com.ar,ingresos_medinth@speedy.com.ar,artmedicina@victoria.com.ar', 'FLORIDA 556', 'CIUDAD AUTONOMA DE BUENOS AIRE', '4322-1100 INT 111', 'MONICA/CLAUDIA', 'ART.DEL HOGAR', 'Enviar dato faltante !! ', 'VICTORIA'),
(1231, '4130', 'LA HOLANDO ART', '4130', 'LAHOLANDO', 'S', 'artmedcasos@holansud.com.ar ', 'SARMIENTO 309 5TO PISO', 'CIUDAD AUTONOMA DE BUENOS AIRE', '4321-7600', '', 'ART', 'lhuizi@holansud.com.ar ', 'LA HOLANDO'),
(1232, '4139', 'EL URBANO SRL', '4139', 'LUCIANA', 'S', 'personal@mo45.com.ar,linea45@mo45.com.ar,control@mo45.com.ar,rrhh@mo45.com.ar', 'TENIENTE RANGUGNI 4059', 'REM. DE ESCALADA OESTE', '4249-8754', 'CASTRO/LILIANA', 'TRANSPORTE DE PASAJEROS', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1233, '4146', 'GALDIERI MARCELO ANTONIO', '4146', 'MARCELO', 'S', 'kevin-pel0802@hotmail.com ', 'BURELAS 4633', 'LANUS', '4246-8567', 'MARCELO', 'CARTONERIA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1234, '4159', 'YPF LA BANDERITA', '4159', 'CRISTIAN', 'S', 'ypflabanderita@hotmail.com,soldeezpeleta@hotmail.com', 'AV RAMON FRANCO 6287', 'WILDE', '4207-9670', 'RAFAEL', 'EST. DE SERV.', 'Enviar dato faltante !! ', 'SMG'),
(1235, '4161', 'FERNANDEZ INSUA SA', '4161', 'VALERIA', 'S', 'administracion@fdez-insua.com.ar ', 'AV PAVON 1412', 'AVELLANEDA OESTE', '4228-8058', 'SANDRA PARILLA', 'VTA RTO MAQ. VI', 'Enviar dato faltante !! ', 'PREVENCION'),
(1236, '4162', 'KIOSHI COMPRESION SA', '4162', 'FERNANDA', 'S', 'reccaf@kioshicompresion.com,fiorenzag@kioshicompresion.com,lysyckyd@kioshicompresion.com', 'SANCHEZ DE BUSTAMANTE 1688', 'LANUS ESTE', '4205-4515', 'FERNANDA-GASTON', 'METALURGICA', 'fiscellar@kioshicompresion.com ', 'FEDERACION PATRONAL'),
(1237, '4222', 'MARISSI Y VILLANUEVA', '4222', 'ALEJANDRO', 'S', 'marissi_y_villanueva@yahoo.com.ar,marissivillanueva@hotmail.com', 'AV SAN MARTIN 4199', 'LANUS OESTE', '4267-6064', 'LUIS O JESUS FERNAND', 'EST. DE SERV.', 'Enviar dato faltante !! ', 'LA HOLANDO'),
(1238, '4232', 'D ADDESE ALDO Y BRUNO', '4232', 'DADDESE HNOS', 'N', 'monicaparrilla@live.com.ar ', 'C. URUGUAYO 3373', 'LANUS ESTE', '4246-8695', 'MONICA', 'EMP. ATMOSFERICOS', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1239, '4244', 'GABUCCI (ELLIANT SA)', '4244', 'JOSE', 'S', 'joseferraro@fibertel.com.ar ', 'LAPRIDA 367', 'LOMAS DE ZAMORA', '4245-8301 / 4243-6424', 'JOSE O ANGELICA', 'COMERCIO', 'Enviar dato faltante !! ', 'QBE'),
(1240, '4246', 'ERCO OLEO HIDRAULICA', '4246', 'ERNESTO', 'S', 'info@oleohidraulicaerco.com.ar ', 'ROMA 3042', 'LANUS ESTE', '4220-5677', 'ERNESTO/ KARINA', 'HIDRAULICA', 'Enviar dato faltante !! ', 'SMG'),
(1241, '4256', 'TALCAHUANO SRL', '4256', 'GUILLERMO', 'S', 'estudiofasano@live.com.ar ', 'TALCAHUANO 2610', 'VALENTIN ALSINA', '4209-6325', 'CARLOS O GUILLERMO', 'FRIGORIFICO', 'Enviar dato faltante !! ', 'SMG'),
(1242, '4269', 'FAB. CARROCERIAS LOS 4 ASES', '4269', 'JORGE', 'S', 'carrocerias@loscuatroa.com.ar,info@loscuatroa.com.ar', 'CNO. DE CINTURA KM 17', '9 DE ABRIL', '4693-0388/0387', 'JORGE FERNANDEZ', 'CARTELES FCA', 'Enviar dato faltante !! ', 'GALENO'),
(1243, '4331', 'LASSERRE S.A.', '4331', 'LUIS', 'S', 'lasserre@outlook.com ', 'PJE. CUBA 748', 'AVELLANEDA OESTE', '4201-9116/1218', 'LUIS/ MARINA', 'FCA. DE ARMAS', 'marina@rexioarms.com.ar ', 'BERKLEY INTERNATIONAL'),
(1244, '4364', 'CORBALAN HECTOR ALBERTO', '4364', 'HECTOR', 'S', 'hac_servicios@yahoo.com.ar ', 'IGNACIO RUCCI 1051 (ABB)', 'VALENTIN ALSINA', '4752-3277/ 156104-0013', 'HECTOR', 'CONTRATISTA', 'Enviar dato faltante !! ', 'GALENO'),
(1245, '4370', 'CURTIEMBRE BIONDO SRL', '4370', 'ROBERTO', 'S', 'cur_biondo@uol.com.ar,roberto_biondo@hotmail.com,curtiembrebiondo@hotmail.com', 'M. OCAMPO 2754', 'VALENTIN ALSINA', '4208-9384', 'ROBERTO O RICARDO', 'CURTIEMBRE', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1246, '4371', 'GRIGAS PEDRO', '4371', 'SABRINA', 'S', 'graficalsina@gmail.com ', 'ARMENIA 244', 'VALENTIN ALSINA', '4208-1695 4228-6985', 'SABRINA/PEDRO/ DANIEL', 'GRAFICA', 'Enviar dato faltante !! ', 'PREVENCION'),
(1247, '4374', 'POSE LUIS ALBERTO', '4374', 'POSE', 'S', 'envasesml@live.com ', 'MENDOZA 1971', 'AVELLANEDA', '4602-9431', 'LUIS', 'PAPELERA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1248, '4378', 'DISTRIBUIDORA SUR SRL', '4378', 'GRACIELA', 'S', 'admin@distribuidorasursrl.com.ar ', 'CABILDO 91', 'AVELLANEDA OESTE', '4208-6967/7734', 'GRACIELA GERVASI', 'REPUESTOS', 'admin@distribuidorasursrl.com.ar ', 'FEDERACION PATRONAL'),
(1249, '4410', 'CONS. A. DEL VALLE 165..', '4410', 'JORGE', 'S', 'ingjorgegomez@gmail.com ', 'ARITOBULO DEL VALLE 462 6P A', 'LANUS OESTE', '4-240-6249/4247-6332', 'GOMEZ SIRIMARCO', 'CONSORCIO', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(1250, '4513', 'TRIPER S.A.', '4513', 'CRISTIAN', 'S', 'triper@opcionestelmex.com.ar,admtotal2011@yahoo.com.ar,estudio@bustosmurilloyasoc.com.ar', 'M.T.DEALVEAR 1261 PISO 2 OF 20', 'CAPITAL', '4289-3780/3781.', 'CRISTIAN', 'TRANSPORTE', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1251, '4520', 'CRUCESITA S.A.', '4520', 'LIDIA.', 'S', 'servicruce@speedy.com.ar ', 'AV. MITRE 1266', 'AVELLANEDA', '4-222-5729/7137', 'MELINA', 'EST. DE SERV.', 'Enviar dato faltante !! ', 'LA CAJA'),
(1252, '4527', 'PINOPLAS S.A.', '4527', 'MIGUEL', 'S', 'miguel@pinoplas.com.ar,carlos@pinoplas.com.ar ', 'MANUEL ESTEVEZ 553', 'AVELLANEDA ESTE', '4-201-8638', 'CARLOS PIGNOLO', 'MADERERA', 'Enviar dato faltante !! ', 'GALENO'),
(1253, '4528', 'CONS. PROP. ARTURO BAS 135', '4528', 'MARISA', 'S', 'mrech_adm@yahoo.com.ar ', 'ARTURO BAS 135', 'REMEDIOS DE ESCALADA', '3528-4932', 'MARISA RECH', 'CONSORCIO', 'Enviar dato faltante !! ', 'PREVENCION'),
(1254, '4531', 'ORGANIZACION SUR S.A.', '4531', 'NATALIA', 'S', 'natalia_forciniti@orgsurvw.com.ar,cpallavisini@orgsurvw.com.ar', 'H. IRIGOYEN 3199', 'LANUS O.', '4225-0111', 'DANIEL FORCINITI', 'AUTOMOTORES', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(1255, '4534', 'AMERICAN MEALS SRL', '4534', 'ROXANA', 'S', 'roxanaspinetta@americanmeals.com.ar ', 'YAPEYU 2352', 'MORENO', '02374624705/155', 'SILVIA LAVALLE', 'GASTRONOMIA', 'roxanaspinetta@americanmeals.com.ar ', 'GALENO'),
(1256, '4537', 'ESTABLECIMIENTO ORLOC SRL ( VD Y AP)', '4537', 'LAURA', 'S', 'orloc@ravana.com.ar,ldimeglio@ravana.com.ar ', 'REP. ARGENTINA 2021', 'LANUS OESTE', '4208-2175/4218-3111', 'LAURA', 'ALIMENTOS', 'amerlo@ravana.com.ar ', 'ASOCIART A.R.T.'),
(1257, '4542', 'COLEGIO DURHAM S.R.L.', '4542', 'VERONICA', 'S', 'colegiodurham@yahoo.com.ar ', 'MURGIONDO 890/98', 'VALENTIN ALSINA', '4-208-2985', 'NORMA/NERONICA/SILVIA', 'EDUCATIVA', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(1258, '4554', 'OLEO HIDRAULICA DE CERSOSIMO', '4554', 'CESAR', 'S', 'oleohidraulicakeiper@hotmail.com.ar ', 'PALACIOS 698', 'LOMAS DE ZAMORA ESTE', '4244-5091', 'NICOLAS KEIPERT', 'METALURGICA', 'gabriela.kei@hotmail.com ', 'BERKLEY INTERNATIONAL');
INSERT INTO `Usuarios` (`id`, `Codigopal`, `Nombrepal`, `Clave`, `Usuario`, `Activo`, `Correo`, `Domicilio`, `Localidad`, `Telefono`, `Contacto`, `Actividad`, `Correop`, `Nombart`) VALUES
(1259, '4563', 'CENTROMEC SRL', '4563', 'LUCIA', 'S', 'administracion@centromecsrl.com,albertohaas.centromec@gmail.com', 'FORMOSA 1434', 'LANUS OESTE', '4209-6885', 'ANA O ALBERTO HAAS', 'METALAURGICA', 'administracion@centromecsrl.com ', 'GALENO'),
(1260, '4571', 'HIERROS PARROTTA S.A.', '4571', 'SILVIA', 'S', 'proveedores@hierrosparrotta.com,silviadarcangel@yahoo.com.ar', 'DE BENEDETTI 2851', 'AVELLANEDA ESTE', '4354-9000', 'SILVIA', 'HERRERIA', 'administracionproveedores@hierrosparrotta.com ', 'GALENO'),
(1261, '4576', 'FOTOCROMOS BOLIVAR S.R.L.', '4576', 'ROMINA', 'S', 'trabajos@fotocromosbolivar.com.ar,adm@fotocromosbolivar.com.ar', 'MANSILLA 232', 'AVELLANEDA OESTE', '4201-8756/5109', 'OSCAR', 'GRAFICA SERIGR.', 'trabajosbolivar@gmail.com ', 'BERKLEY INTERNATIONAL'),
(1262, '4581', 'PAL MEDICINA LABORAL SRL', '4581', 'DAMIAN', 'S', 'consultas@palmedicinalaboral.com.ar ', 'ITUZAINGO 1348', 'LANUS ESTE', '4247-7093', 'DAMIAN BABOR', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'PREVENCION'),
(1263, '4584', 'ASOCIACION ODONTOLOGICA DE L Z', '4584', 'ALEJANDRO', 'S', 'aolz@fibertel.com.ar ', 'ALEM 243', 'LOMAS DE ZAMORA OESTE', '4243-2297', 'ALEJANDRO', 'ASOCIAR', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(1264, '4586', 'COOP.DE TRAB.GRAL SAN MARTIN', '4586', 'LEILA', 'S', 'leila.gimenez@servicioargentino.com.ar ', 'PIEDRAS 1655', 'CAPITAL FEDERAL', '0221-4235758/ 4300-8775', 'AREA PROTEGIDA', 'LIMPIEZA', 'Enviar dato faltante !! ', 'NO POSEE ART'),
(1265, '4593', 'MAGENTA IMPRESORES S.R.L.', '4593', 'ANA', 'S', 'administracion@magentaimpresores.com.ar ', 'RICARDO BALBIN 2101', 'VALENTIN ALSINA', '4218-2396/2397/1124/0669', 'LAURA / ANA MARIA', 'GRAFICA SERIGR.', 'administracion@magentaimpresores.com.ar ', 'GALENO'),
(1266, '4603', 'WALZER SRL', '4603', 'AMALIA', 'S', 'info@walzer.com.ar ', 'HUMBERTO PRIMO 1860', 'AVELLANEDA OESTE', '4208-8297', 'AMALIA', 'METALURGICA', 'Enviar dato faltante !! ', 'SMG'),
(1267, '4608', 'EMDABER SRL', '4608', 'EMDABER', 'S', 'info@emdaber.com,claudio@emdaber.com ', 'EMILIO CASTRO 2543', 'LANUS OESTE', '4228-8752/4218-1679', 'Enviar dato faltante !!', 'CARPINTERIA', 'Enviar dato faltante !! ', 'MAPFRE'),
(1268, '4612', 'SECURITY SUPPLY SA', '4612', 'ARTURO', 'S', 'abruzzo@kamet.com.ar,cmorales@kamet.com.ar ', 'YATAY 780', 'VALENTIN ALSINA', '4208-1697', 'DANIEL FERNANDEZ', 'ZAPATER.MAYORIS', 'cmorales@kamet.com.ar ', 'QBE'),
(1269, '4617', 'INDUSTRIAS VESUBIO S.R.L.', '4617', 'JUAN', 'S', 'info@industriasvesubio.com.ar,ivesubio@gmail.com', 'VIRREY LINIERS 2888', 'VALENTIN ALSINA', '4116-3557/4116-3558', 'LORENA', 'METALAURGICA', 'lorena.rodriguez@industriasvesubio.com.ar ', 'PREVENCION'),
(1270, '4618', 'PRADEMA SAIC', '4618', 'GABRIELA', 'S', 'compras@herbo.com.ar,fabrica@herbo.com.ar,herbo@herbo.com.ar', 'CHILE 2', 'AVELLANEDA OESTE', '4208-7100/ 4204-0330', 'RICARDO BORELLI-ADRIANA', 'Enviar dato faltante !!', 'contaduria_herbo@sion.com,info@herbo.com.ar ', 'GALENO'),
(1271, '4633', 'FRENOS ARGENTINOS SRL', '4633', 'EMILIO', 'S', 'info@nacionalfrenos.com.ar ', 'EL PLUMERILLO 540', 'LOMAS DE ZAMORA', '4286-4977', 'EMILIO LUONGO', 'METALAURGICA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1272, '4657', 'LOGISTICA TRANSNORTE S.R.L.', '4657', 'GUSTAVO', 'S', 'grmere@yahoo.com.ar ', 'ZEBALLOS 2650', 'AVELLANEDA', '4346-4000 INT 11282 (ANDR', 'ANDRES MIERES', 'TRANDPORTE', 'Enviar dato faltante !! ', 'GALENO'),
(1273, '4661', 'COOP.DE TRAB.EX T.SAN REMO LTD', '4661', 'GLADYS', 'S', 'nuevasanremo@yahoo.com.ar ', 'BOLA#OS 1136', 'LANUS ESTE', '4225-2138/4240-9572 INT 1', 'ROSA/GLADYS', 'TEXTIL', 'Enviar dato faltante !! ', 'NO POSEE ART'),
(1274, '4664', 'SERVIAMBIENTAL SRL', '4664', 'DINO', 'S', 'andreaveronicagarcia@hotmail.com ', 'NEUQUEN 600', 'CAPITAL FEDERAL', '4208-6308/ 4585-7426 PEDR', 'Enviar dato faltante !!', 'Enviar dato faltante !!', 'andreaveronicagarcia@hotmail.com ', 'GALENO'),
(1275, '4667', 'LA MERIDIONAL CIA ARG. SEG.', '4667', 'MERIDIONAL', 'S', 'no ', 'PTE J.D. PERON 646 4to P.', 'CIUDAD AUTONOMA DE BUENOS AIRE', '4909-7307-7092/7094', 'CASTRO 155-3087877', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'LA CAJA'),
(1276, '4674', 'CAZELLA HNOS SRL', '4674', 'MARIA', 'S', 'cristina@auxilioscazella.com.ar ', 'V. VERGARA 1545', 'BANFIELD ESTE', '4202-4242/6669', 'SILVIA O MARTIN', 'REMOLQUES', 'Enviar dato faltante !! ', 'LA CAJA'),
(1277, '4683', 'JORGE D. LUDOVICO', '4683', 'JORGE', 'S', 'fledpack_envases@hotmail.com ', 'PARAGUAY 2343', 'LANUS OESTE', '4218-0823', 'SRA. LILIANA', 'GRAFIC.PAPELERA', 'Enviar dato faltante !! ', 'LA CAJA'),
(1278, '4685', 'CONS. HIPOLITO IRIGOYEN 2939', '4685', 'ANAMARIA', 'S', 'estudiomozzone@hotmail.com ', 'H. IRIGOYEN 2939', 'LANUS OESTE', '4240-3566', 'DRA ANA MARIA MOZZONE', 'CONSORCIO', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(1279, '4697', 'HIPERPEL SA', '4697', 'JESICA', 'S', 'hiperpel@ciudad.com.ar,hiperpel@hiperpel.com.ar', 'DAMONTE 1239/45', 'LANUS ESTE', '4247-6622', 'DIEGO/ALICIA', 'PAPELERA', 'Enviar dato faltante !! ', 'GALENO'),
(1280, '4704', 'FCIA NUEVA MOURIN SCS', '4704', 'WALTER', 'S', 'dpernico@puntofarma.com,hesteban@puntofarma.com,personal@puntofarma.com', '25 DE MAYO 376', 'LANUS OESTE', '4240-4340/42', 'PUNTOFARMA', 'FARMACIA', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(1281, '4705', 'SIND. DEL PAPEL (FCIA PEDRAZA)', '4705', 'WALTER', 'S', 'dpernico@puntofarma.com,hesteban@puntofarma.com,personal@puntofarma.com', 'AV. SAN MARTIN 3345', 'LANUS OESTE', '4240-4340/42', 'PUNTOFARMA', 'FARMACIA', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(1282, '4707', 'FCIA ROCALAN SCS', '4707', 'WALTER', 'S', 'dpernico@puntofarma.com,hesteban@puntofarma.com,personal@puntofarma.com', 'PTE. DERQUI 1699', 'LANUS O.', '4240-4340/42', 'PUNTOFARMA', 'FARMACIA', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(1283, '4708', 'FARMACIA ESCALADA DE LANUS SCS', '4708', 'WALTER', 'S', 'dpernico@puntofarma.com,hesteban@puntofarma.com,personal@puntofarma.com', 'AV. H. YRIGOYEN 6201', 'LANUS OESTE', '4240-4340/42', 'PUNTOFARMA', 'FARMACIA', 'recepcion@puntofarma.com ', 'SMG'),
(1284, '4712', 'CIA. RESIPLAST SA', '4712', 'ANGEL', 'S', 'antonellacorrea@resiplast.com.ar ', 'B. ENCALADA 2936', 'LANUS ESTE', '4220-1915', 'PEDRO CORREA', 'PLASTICOS', 'Enviar dato faltante !! ', 'SMG'),
(1285, '4714', 'SOPLASUR SRL', '4714', 'FERNANDO', 'S', 'soplasur@gmail.com ', 'RONDEAU 1574', 'LANUS ESTE', '4246-4150', 'GUILLERMO', 'PLASTICO', 'Enviar dato faltante !! ', 'LA CAJA'),
(1286, '4716', 'GARCIA ANTONIO', '4716', 'ANTONIO', 'S', 'mng_smirnoff@hotmail.com ', 'J.D.PERON 2626', 'VALENTIN ALSINA', '4228-2995', 'ANTONIO', 'CARNICERIA', 'Enviar dato faltante !! ', 'MAPFRE'),
(1287, '4724', 'RODRIGUEZ RUBEN ROBERTO', '4724', 'RUBEN', 'S', 'lavaderoestrella@ciudad.com.ar,info@lavaderoestrella.com.ar', 'PALACIOS 2071', 'LANUS OESTE', '209-3091', 'RUBEN/PABLO RODRIGUE', 'LAVADERO', 'Enviar dato faltante !! ', 'LA CAJA'),
(1288, '4739', 'PELOSI BENEDETTO Y PELOSI ELIO', '4739', 'ELIO', 'S', 'info@pelosi.com.ar ', 'ROCA 939', 'REMEDIOS DE ESCALADA OESTE', '4248/6203/4096', 'VICTOR/ELIO', 'METALURGICA', 'Enviar dato faltante !! ', 'GALENO'),
(1289, '4749', 'CONSORCIO BASAVILBASO 2208', '4749', 'VANESA', 'S', 'gabrielcosentino@infovia.com.ar ', 'BASAVILBADO 2208', 'LANUS ESTE', '4249-1877/', 'FERNANDO', 'CONSORCIO', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(1290, '4755', 'TINTOSUR SA', '4755', 'OANA', 'S', 'tintosur@fibertel.com.ar,lmatrijow@tintosurweb.com.ar', 'RUCCI 2550', 'LANUS OESTE', '4228-4764', 'LILIANA', 'TEXTIL', 'tintosur@fibertel.com.ar ', 'GALENO'),
(1291, '4756', 'COPANGAS SRL', '4756', 'MARIA', 'S', 'torpedo_argentina@hotmail.com,torpedo@torpedoargentina.com.ar', 'DONIZETTI 160', 'LOMAS DE ZAMORA', '4282-1668', 'ADRIAN MAISONNAVE', 'METALURGICA', 'Enviar dato faltante !! ', 'LA CAJA'),
(1292, '4757', 'RECONQUISTA ART S.A.', '4757', 'RECONQUISTA', 'S', 'cme@reconquistart.com.ar,dra@reconquistart.com.ar', 'AV. C. PELLEGRINI 1069 6to P.', 'CIUDAD AUTONOMA DE BUENOS AIRE', '4322-1394/95', 'Dr. DANIEL NEZHODA', 'ART', 'ral@reconquista.com.ar ', 'RECONQUISTA ART'),
(1293, '4758', 'TECMEL SRL', '4758', 'RAMON', 'S', 'tecmel_srl@yahoo.com.ar ', 'SARMIENTO 527', 'LANUS ESTE', '4247-4462', 'JACINTO MORENO', 'METALAURGICA', 'Enviar dato faltante !! ', 'GALENO'),
(1294, '4763', 'AUXILIOS CAZELLA', '4763', 'MARTIN', 'S', 'cristina@auxilioscazella.com.ar,cmartin@auxilioscazella.com.ar', 'V. VERGARA 1545', 'BANFIELD ESTE', '4202-4242', 'SILVIA (CAZELLA HNOS', 'REMOLQUES', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1295, '4765', 'CARLOS SANCHEZ e HIJOS', '4765', 'CARLOS', 'S', 'azufrealivio@gmail.com ', 'TAPALQUE 1467', 'AVELLANEDA ESTE', '4204-0068', 'SR. ROBERTO', 'MINERIA', 'Enviar dato faltante !! ', 'QBE'),
(1296, '4774', 'EVOLUCION S.R.L.', '4774', 'ARIEL', 'S', 'evolucionsrl@hotmail.com ', 'DEL VALLE IBERLUCEA 4451', 'R. DE ESCALADA OESTE', '4202-5066/5098', 'RAQUEL-ELBA', 'CENTRO DE DIA', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(1297, '4775', 'NAGIR SAHIN', '4775', 'SAHIN', 'S', 'talin_bos@hotmail.com,autoserviciotito@yahoo.com.ar', 'MAXIMO PAZ 985', 'LANUS OESTE', '4225-7481', 'SAHIN', 'ALIMENTICIO', 'Enviar dato faltante !! ', 'GALENO'),
(1298, '4788', 'CONSORCIO PAZ', '4788', 'MARIA', 'S', 'ad.consorcio.abril@gmail.com ', '9 DE JULIO 1558', 'LANUS ESTE', '4240-8409', 'Enviar dato faltante !!', 'CONSORCIO', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(1299, '4799', 'TALLER ALSINA (LAURENCE A.)', '4799', 'ANTONIO', 'S', 'info@talleralsina.com.ar ', 'DARRAGUEIRA 601', 'VALENTIN ALSINA', '4208-1427', 'ANTONIO-CLAUDIA', 'TALLER MECANICO', 'Enviar dato faltante !! ', 'QBE'),
(1300, '4816', 'COOPTEM LTDA.', '4816', 'ROBERTO', 'S', 'cooptem@hotmail.com ', 'OYUELAS 875', 'VILLA DOMINICO-AVELLANEDA', '4227-6709/4217-2583', 'SILVIA O DANIELA', 'VIDRIOS', 'Enviar dato faltante !! ', 'QBE'),
(1301, '4826', 'MECANICA INTEGRAL CLAUDIO', '4826', 'CLAUDIO', 'S', 'jorgeclaudio2@yahoo.com.ar ', 'HELGUERA 893', 'AVELLANEDA', '4204-0100/1540447347', 'CLAUDIO FERNANDEZ', 'MECANICA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1302, '4828', 'PAPELERA BRANDSEN S.H.', '4828', 'RITA', 'S', 'info@papelerabrandsen.com.ar ', 'CATAMARCA 1572', 'LANUS OESTE', '4262-8081', 'RITA', 'PAPELERA', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(1303, '4831', 'CORRUGADORES ASOCIADOS S.A.', '4831', 'ADRIANA', 'S', 'adrianacorruga@hotmail.com ', 'PEDERNERA 725 P', 'LANUS ESTE', '4289-4128/4289-4798', 'Enviar dato faltante !!', 'PAPELERA', 'Enviar dato faltante !! ', 'LA CAJA'),
(1304, '4835', 'FLOREK HNOS Y ALTOMARE STL', '4835', 'MARIA', 'S', 'florekaltomare@ciudad.com.ar ', 'E.CASTRO 2726', 'LAMUS OESTE', '4208-9973', 'MIGUEL FLOREK/ANDREA', 'PLASTICO', 'cobranzasflorek@ciudad.com.ar ', 'GALENO'),
(1305, '4840', 'TODO SUR S.R.L.', '4840', 'RICARDO', 'S', 'todo.sur@hotmail.es,distribuidora-libra@hotmail.com', 'SARMIENTO 761', 'LANUS ESTE', '4225-2908', 'RICARDO ESTEVEZ', 'TRANSPORTE', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1306, '4844', 'MARINO STAMPI SRL', '4844', 'VANESA', 'S', 'marinobases@hotmail.com ', 'CONDARCO 2546', 'LANUS ESTE', '4246-5464', 'CARLOS MARINO/YANINA', 'PLASTICOS', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(1307, '4845', 'GALUZ MIRIAM', '4845', 'MIRIAM', 'S', 'miriamgaluz@yahoo.com.ar ', '9 DE JULIO 1341', 'LANUS ESTE', '4241-1199/4241-7170', 'SR. RAUL', 'LENCERIA', 'Enviar dato faltante !! ', 'PREVENCION'),
(1308, '4848', 'CELULOSA CAMPANA S.A.', '4848', 'HECTOR', 'S', 'hferrari@celulosacampana.com.ar ', 'DARRAGUEIRA 1261', 'VALENTIN ALSINA', '4208-4831', 'HECTOR FERRARI/ NELIDA MUIÑA', 'FAB. ART DE CARTON', 'proveedeores@celulosacampana.com.ar ', 'PRODUCTORES DE FRUTAS ARGENTINAS'),
(1309, '4856', 'LAKE INTERNACIONAL SA', '4856', 'DANIEL', 'S', 'info@proforce.com.ar ', 'REMEDIOS DE ESCALADA 524', 'LANUS OESTE', '4247- 4422/4848 4241-85', 'CASA KORUK', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'SMG'),
(1310, '4861', 'BLANCO S. Y CARRE#O M S.H.', '4861', 'BLANCO', 'S', 'lcentral@ciudad.com.ar ', 'HIPOLITO YRIGOYEN 4250', 'LANUS OESTE', '4240-9444', 'SERGIO - WALTER', 'FCA. DE PASTAS', 'Enviar dato faltante !! ', 'QBE'),
(1311, '4862', 'KRAWCZUK MARTIN ESTEBAM', '4862', 'MARTIN', 'S', 'mek_ezequiel@yahoo.com.ar ', 'MORENO 2581', 'LANUS OESTE', '4368-7198', 'CARLOS LORENZO', 'CARPINTERIA', 'Enviar dato faltante !! ', 'QBE'),
(1312, '4864', 'EXPLENDOR S.R.L.', '4864', 'NATALIA', 'S', 'dimensionsulu@yahoo.com.ar ', 'SALTA 2034', 'AVELLANEDA ESTE', '4205-1772/5387', 'BLAS ROBERTO M.', 'FABRI.CALZADO', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(1313, '4873', 'RAÑA OBDULIO', '4873', 'MARIA', 'S', 'papelerarania@sypred.com ', 'BOLIVIA 2051', 'LANUS OESTE', '4240-2101', 'MARIA LAURA', 'PAPELERA', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(1314, '4879', 'REVESTIMIENTOS CAR S.A.', '4879', 'CARLOS', 'S', 'rosmetal@sion.com ', 'RIVADAVIA 1478', 'AVELLANEDA', '4-208-9136/4218-0243', 'ROS METAL', 'METALURGICA', 'Enviar dato faltante !! ', 'LA HOLANDO'),
(1315, '4888', 'LA COLIMBA S.R.L.', '4888', 'COLIMBA', 'S', 'lacolimbasrl@live.com.ar ', 'SARMIENTO 1292', 'LANUS ESTE', '4241-1986', 'JUAN O JORGE DIAZ', 'EST. DE SERV.', 'Enviar dato faltante !! ', 'GALENO'),
(1316, '4889', 'FORMAS CALCOMANIAS S.H.', '4889', 'FERNANDO', 'S', 'formascalcos@infovia.com.ar,formas@speedy.com.ar', 'CNO. GRAL. BEGRANO 1200', 'AVELLANEDA ESTE', '4203-5621', 'FERNANDO O CARLOS', 'SERIGRAFIA', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(1317, '4891', 'GRAMTOR S.A.', '4891', 'NICOLAS', 'S', 'contaduria_gramtor@speedy.com.ar,gastonadolfo@gramtor.com', 'MONTES DE OCA 15 3ro A', 'CAP. FED.', '4362-5100 INT 38', 'EST. MET. SORIANO', 'METALURGICA', 'gastonadolfo@gramtor.com ', 'PROVINCIA'),
(1318, '4900', 'CLINICA SAN MICHELE', '4900', 'MARIELA', 'S', 'smichele@uolsinectis.com.ar ', 'MALABIA 432', 'R. DE ESCALADA ESTE', '4242-4500/0812', 'MARIELA', 'CLINICA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1319, '4904', 'RESTAURANT BAR DON NICOLA SRL', '4904', 'NICOLA', 'S', 'restodonnicola@hotmail.com.ar,garciayasociados@speedy.com.ar', 'GARCIA 115', 'AVELLANEDA', '4201-3673', 'PABLO (TITULAR)', 'RESTAURANT', 'Enviar dato faltante !! ', 'MAPFRE'),
(1320, '4908', 'DISTRIBUIDORA CIDI SRL', '4908', 'GABRIELA', 'S', 'cidistribuidor@gmail.com ', 'EVA PERON 3300', 'TEMPERLEY', '4264-5280', 'CECILIA', 'ALIMENTICIO', 'ceciliagaggero@gmail.com ', 'QBE'),
(1321, '4915', 'ESPADA FARALDO MARGARITA', '4915', 'MARGARITA', 'S', 'mespadafaraldo@yahoo.com ', 'AV CORDOBA 1200', 'CAPITAL FEDERAL', '4372-9990', 'MARGARITA', 'SANITARIOS', 'Enviar dato faltante !! ', 'PREVENCION'),
(1322, '4920', 'SESYTEL', '4920', 'WALTER', 'S', 'sesytel@sesytel.com.ar,walter@sesytel.com.ar ', 'J.D.PERON 3455', 'VALENTIN ALSINA', '4209-0856', 'WALTER', 'TELEFONIA', 'sesytel@sesytel.com.ar ', 'FEDERACION PATRONAL'),
(1323, '4926', 'RICARDO LUACES', '4926', 'RICARDO', 'S', 'veluaargentina@yahoo.com.ar ', 'ESTANISLAO DEL CAMPO 2120', 'AVELLANEDA ESTE', '4203-2316/1562940500', 'RICARDO LUACES', 'CARPINTERIA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1324, '4929', 'TREFCO S.A.', '4929', 'ALEJANDRO', 'S', 'trefco@speedy.com.ar ', 'DEAN GREGORIO FUNES 452', 'AVELLANEDA ESTE', '4222-9570/52', 'ALEJANDRO/OSVALDO/JUAN 153107 6797', 'METALURGICA', 'trefco@speedy.com.ar ', 'PREVENCION'),
(1325, '4931', 'RECAUDADORA SA', '4931', 'MACARENA', 'S', 'rrhh@recsa.com.ar ', 'SARMIENTO 669 5TO PISO', 'capital federal', '4105-9100/32/91/19', 'MARTA 1532579456', 'RECAUDADORA', 'sortigoza@recsa.com.ar ', 'PROVINCIA'),
(1326, '4934', 'SALON DEL PEINADOR S.R.L.', '4934', 'SILVINA', 'S', 'ofisp@ciudad.com.ar ', 'H. YRIGOYEN 4561', 'LANUS O.', '4247-6547 4249-5909', 'MARCELO T.', 'PELUQUERIA', 'Enviar dato faltante !! ', 'LA HOLANDO'),
(1327, '4936', 'MARCELO FERNANDEZ', '4936', 'SILVINA', 'S', 'ofisp@ciudad.com.ar ', '25 DE MAYO 110', 'LANUS O.', '4247-6547 4249-5909', 'MARCELO T.', 'PELUQUERIA', 'Enviar dato faltante !! ', 'GALENO'),
(1328, '4955', 'PRODUCTORES DE FRUTAS ARG. ART', '4955', 'PRODUCTORES DE FRUTA', 'N', 'no ', 'ANCHORENA 670', 'CIUDAD AUTONOMA DE BUENOS AIRE', '4862-8296/6961', 'J. HERNANDEZ/FRITMAN', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'PRODUCTORES DE FRUTAS ARGENTINAS'),
(1329, '4956', 'LA SEGUNDA ART', '4956', 'LASEGUNDA', 'S', 'sgeniola@lasegunda.com.ar,yferreyra@lasegunda.com.ar,sergioplenge@gmail.com', 'PARAGUAY AL 1000 PB', 'CIUDAD AUTONOMA DE BUENOS AIRE', '0800-444-2782/0800-555-05', 'A. RUBIOLO/SAMANTA', 'ART', 'gtornatore@lasegunda.com.ar ', 'LA SEGUNDA'),
(1330, '4973', 'GESTION LABORAL S.A.', '4973', 'DANILO', 'S', 'mghislanzoni@grupo-gestion.com.ar,yperalta@grupo-gestion.com.ar,adp-avellaneda@grupo-gestion.com.ar', 'tte gral d peron 1628 7', 'CAPITAL FEDERAL', '4229-9955/9966/9494', 'cristian jimenez', 'AGENCIA DE EMP.', 'fcasella@grupo-gestion.com.ar ', 'GALENO'),
(1331, '4978', 'FABRICAR EL MONCAYO S.A.', '4978', 'ANA', 'S', 'fabricar-elmoncayo@hotmail.com ', 'LOS POZOS 3856/70 ENTRE DEHEZA Y SUIPACH', 'SARANDI', '4246-8990/9125', 'MILA - ANA', 'FABRICA DE CARTON', 'Enviar dato faltante !! ', 'SMG'),
(1332, '4980', 'IND. MET. RONANFER S.A.', '4980', 'FABIAN', 'S', 'ronanfer@fullzero.com.ar,ronanfer@hotmail.com ', 'MADARIAGA 1725', 'SARANDI', '4204-3882/4205-7264', 'FABIAN-NANCY-ROSA', 'METALAURGICA', 'Enviar dato faltante !! ', 'PREVENCION'),
(1333, '4991', 'VASPIA S.A.I.C.', '4991', 'MARTHA', 'S', 'marthamilano@vaspia.com,diegobais@vaspia.com,tesoreria@vaspia.com', 'E. PERON 2746', 'LANUS ESTE', '4-246-8282/9423/2746', 'MARTHA MILANO/CARLOS MACHADO', 'METALURGICA', 'tesoreria@vaspia.com ', 'LA SEGUNDA'),
(1334, '5000', 'DANIEL SCOCCIMARRO S.R.L.', '5000', 'MARIA', 'S', 'eticsa@eticasa.com.ar,juancarlos@eticsa.com.ar,teresa@eticsa.com.ar', 'SANTIAGO DEL ESTERO 3230', 'LANUS OESTE', '4-247-8833/4444', 'PATRICIA', 'ELECTROMECANICA', 'Enviar dato faltante !! ', 'SMG'),
(1335, '5003', 'PANADERIA SANTANDER', '5003', 'TERESA', 'S', 'NO MAIL 2 ', 'MAGALLANES 4099', 'LANUS OESTE', '4286-3816', 'RAUL', 'PANADERIA', 'Enviar dato faltante !! ', 'SMG'),
(1336, '5007', 'BHQ SRL', '5007', 'MARIA', 'S', 'norflex.info@gmail.com ', 'YATAY 3330', 'LANUS OESTE', '4218-0176/4228-4332', 'KARINA-MARIA', 'CAUCHO', 'norflex.info@gmail.com ', 'LA CAJA'),
(1337, '5020', 'KATZINA S.R.L.', '5020', 'FRANCO', 'S', 'joehopi@infovia.com.ar ', 'ONCATIVO 1860', 'LANUS ESTE', '4249.7240', 'SR.FRANCO', 'FAB PREN VESTIR', 'Enviar dato faltante !! ', 'LA HOLANDO'),
(1338, '5036', 'OSCAR H. AFRANCHINO SRL', '5036', 'OSVALDO', 'S', 'ovafran@yahoo.com.ar ', 'ARTIGAS 22', 'AVELLANEDA OESTE', '4208-6029', 'OSVALDO', 'PLASTICO', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1339, '5041', 'FLUMAT SRL', '5041', 'LUCAS', 'S', 'maxi_tobio@hotmail.com,shellflumat@gmail.com ', 'SALTA 893', 'LANUS ESTE', '4-241-1655/3180', 'JOSE LUIS', 'EST. DE SERV.', 'Enviar dato faltante !! ', 'SMG'),
(1340, '5045', 'CONS. HIPOLITO YRIGOYEN 6669', '5045', 'FAVIO', 'S', 'adm.azcui@gmail.com ', 'HIPOLITO YRIGOYEN 6669', 'LANUS OESTE', '4249-6479', 'FAVIO', 'CONSORCIO', 'adm.azcui@gmail.com ', 'PROVINCIA'),
(1341, '5053', 'LOMAS FELCAR SA', '5053', 'CRISTIAN', 'S', 'lomasfelcar@gmail.com ', 'ALSINA 1711', 'LOMAS DE ZAMORA', '4292-2228/1538347092', 'NICOLAS-FERNANDO-JOR', 'EST. DE SERV.', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(1342, '5073', 'MYR CA SRL', '5073', 'ESTELA', 'S', 'myrcasrl@hotmail.com ', 'BRASIL 3129', 'VALENTIN ALSINA', '4-208-9585', 'ESTELA/ CARLOS', 'TEXTIL', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1343, '5086', 'LOGISTICA Y TRANS.RODRIGUEZ SA', '5086', 'MARIANA', 'S', 'rodriguez_sa@speedy.com.ar ', 'BUERAS 2820', 'LANUS ESTE', '42078211/4378- 56487132/3', 'ALICIA', 'TRANSPORTE', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1344, '5098', 'FONDOS TOTYS SRL', '5098', 'MARCELO', 'S', 'fondos_totys@hotmail.com ', 'RIO DE JANEIRO 1323', 'LANUS OESTE', '4240-3149', 'MARCELO O GUILLERMO', 'ZAPATERIA', 'Enviar dato faltante !! ', 'GALENO'),
(1345, '5104', 'DISTRIBUIDORA LOS NENES SRL', '5104', 'ANTONIO', 'S', 'distrilosnenes@ciudad.com.ar ', 'TTE. RANGGUNI 3672', 'LANUS OESTE', '4225-6947/6187', 'ANTONIO', 'DIST. DE ART.EM', 'Enviar dato faltante !! ', 'GALENO'),
(1346, '5110', 'POLIMIX ARGENTINA SRL', '5110', 'MANUEL', 'S', 'polimixargentina@speedy.com.ar ', 'CALLAO 2985', 'VALENTIN ALSINA', '4208-6298', 'MANUEL/CLAUDI', 'FCA. DE PVC', 'polimixargentina@speedy.com.ar ', 'QBE'),
(1347, '5115', 'GAVI S.A.', '5115', 'SABRINA', 'S', 'info@casagavi.com.ar ', 'EMILIO MITRE 1465', 'LANUS ESTE', '4249-0947/4241-5417', 'SABRINA', 'MARROQUINERIA', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(1348, '5123', 'PIRA SRL', '5123', 'ALEJANDRO', 'S', 'pirasrl@gmail.com ', 'MANUEL CASTRO 3344', 'LANUS OESTE', '4262-6584/6609', 'ALEJANDRO/HILDA', 'FABRI.CALZADO', 'pirasrl@gmail.com ', 'GALENO'),
(1349, '5124', 'JARDIN NTRA SEÑORA DE LA CONSOLACION', '5124', 'RITA', 'S', 'ritafabio2@hotmail.com,ritafabio2@gamail.com ', 'RONDEAU 509', 'MONTE CHINGOLO - LANUS ESTE', '4246-7216', 'VIVIANA-RITA', 'JARDIN DE INF.', 'ritafabio2@gamail.com ', 'GALENO'),
(1350, '5128', 'PAPELERA CONSTITUCION', '5128', 'RICARDO', 'S', 'papeleraconstitucionsa@hotmail.com ', 'HIPOLITO YRIGOYEN 5477', 'LANUS OESTE', '4241-9797', 'Enviar dato faltante !!', 'PAPELERA', 'Enviar dato faltante !! ', 'QBE'),
(1351, '5134', 'INCARCOR CORRUGADO SRL', '5134', 'BELEN', 'S', 'incarcor@gmail.com,info@incarcor.com.ar ', 'ALFREDO L. PALACIOS 2951', 'VALENTIN ALSINA', '4208-6442', 'SILVIA', 'CARTONES CORR.', 'Enviar dato faltante !! ', 'SMG'),
(1352, '5137', 'DEGAFAR TAP SA', '5137', 'JOSE', 'S', 'info@degafar.com.ar ', 'CATAMARCA 3130/44', 'LANUS OESTE', '4115-6660/6661', 'RUBEN FORCINITI', 'METALAURGICA', 'Enviar dato faltante !! ', 'QBE'),
(1353, '5142', 'TOM PLAST DE VICTOR H. VITULLI', '5142', 'VICTOR', 'S', 'tomplast@sion.com ', '14 DE JULIO 3588', 'LANUS OESTE', '4240-6457/ 4249-6044', 'VICTOR/ROMINA', 'PLASTICO', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(1354, '5143', 'FILOCOMO LILIANA DOLORES', '5143', 'DANIEL', 'S', 'ingear@speedy.com.ar ', 'BOQUERON 2330', 'LANUS OESTE', '4218-3769', 'DANIEL', 'METALURGICA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1355, '5145', 'INST. GRAL. SAN MARTIN', '5145', 'OLGA', 'S', 'marianasecre@hotmail.com,guma.32@hotmail.com ', 'EJ. DE LOS ANDES 648', 'BANFILED OESTE', '4267-3999 4286-2984', 'AREA PROTEG.', 'EDUCATIVA', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(1356, '5150', 'CIRILLO DANIEL OMAR', '5150', 'CIRILLO', 'S', 'construccionescirillodaniel@hotmail.com ', 'SALTA 522 DTO A O B', 'LANUS ESTE', '155-3177661', 'ITALIA/SILVIA', 'CONST. Y MONTAJ', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1357, '5153', 'RECORTERA ARGENTINA S.A.', '5153', 'HECTOR', 'S', 'hferrari@celulosacampana.com.ar ', 'DARRAGUEIRA 1261', 'VALENTIN ALSINA', '4208-4831', 'HECTOR FERRARI/ CARLOS LEIS', 'PAPELERA', 'proveedeores@celulosacampana.com.ar ', 'SMG'),
(1358, '5183', 'CENTROIL S.R.L.', '5183', 'DANIEL', 'S', 'centroil@hotmail.com ', 'HIPOLITO YRIGOYEN 2124/8', 'AVELLANEDA OESTE', '4228-4644 4208-3932', 'MARTA-DIEGO', 'EST. DE SERV.', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1359, '5194', 'EMPIMET S.R.L.', '5194', 'NATALIA', 'S', 'esteban@empimetsrl.com.ar ', 'CHACO 1436', 'LANUS OESTE', '4262-8550', 'OSCAR ARCARA/LORENA', 'METALAURGICA', 'Enviar dato faltante !! ', 'INTERACCION - COLONIA SUIZA'),
(1360, '5198', 'TRANSPORTE LOSADA S.R.L.', '5198', 'ELENA', 'S', 'logistical@ciudad.com.ar ', 'GRAL SPIKA 1675', 'CAP FEDERAL', '4301-8159/4301-6447', 'CARLOS O ELENA', 'TRASPORTE', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1361, '5206', 'VILAR ADRIAN ANIBAL', '5206', 'ADRIAN', 'S', 'aagraneles@hotmail.com ', 'PITAGORAS 1501', 'AVELLANEDA ESTE', '1554264335/4204-0594', 'GERARDO', 'TRANSPORTE', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(1362, '5211', 'ANTONIO BIUSO SH', '5211', 'ANTONIO', 'S', 'antoniobiuso@yahoo.com.ar ', 'ROCA 646', 'RDIOS DE ESCALADA', '4242-3676', 'NO MENCIONA', 'FUNDICION', 'Enviar dato faltante !! ', 'SMG'),
(1363, '5240', 'EXPIM S.A.', '5240', 'PABLO', 'S', 'pciapa@autoperforantes.com ', 'TTE RANGUNI 2867', 'LANUS OESTE', '4240-1166', 'ROMINA', 'Enviar dato faltante !!', 'aiannicello@autoperforantes.com ', 'GALENO'),
(1364, '5245', 'WAISMAN ALEJANDRA Y GAB. SH', '5245', 'ALEJANDRA', 'S', 'waismanmyg@hotmail.com ', '9 DE JULIO 1123', 'LANUS ESTE', '4241-4465', 'ALEJANDRA/GABRIELA', 'COMERCIO BAZAR', 'Enviar dato faltante !! ', 'QBE'),
(1365, '5255', 'DISTRICALZ S.R.L.', '5255', 'DISTRICALZ', 'S', 'rrhh@gaelle.com.ar,personal@gaelle.com.ar ', 'BRASIL 357', 'AVELLANEDA OESTE', '4208-9781', 'ADRIAN GOMEZ/KARINA PENNINO', 'CALZADO', 'pagos@gaelle.com.ar ', 'GALENO'),
(1366, '5257', 'IPC (MARTINEZ NESTOR)', '5257', 'NESTOR', 'S', 'nestor_e_martinez@hotmail.com ', '20 DE SEPTIEMBRE 3765', 'LANUS OESTE', '4249-0012', 'SANDRA', 'ASCENSORES', 'Enviar dato faltante !! ', 'SMG'),
(1367, '5258', 'ACCESORIO ZALOZE SRL', '5258', 'YESICA', 'S', 'info@accesorioszaloze.com ', 'CAMINO GRAL BELGRANO 3065', 'LANUS ESTE', '4246-7324', 'NICOLAS/GLADYS', 'METALAURGICA', 'ventas@accesorioszaloze.com ', 'LA CAJA'),
(1368, '5259', 'MOUSSE (N.D.R.)', '5259', 'DANIEL', 'S', 'NO MAIL 2 ', 'AV. SAN MARTIN 1960', 'LANUS OESTE', '4225-1883 / 4249-0797', 'Enviar dato faltante !!', 'PANADERIA', 'Enviar dato faltante !! ', 'SMG'),
(1369, '5262', 'PLUS SRL(PUEYRREDON)', '5262', 'DIEGO', 'S', 'swingregalos@hotmail.com ', 'PEYRREDON 218', 'CAPITAL FED', '4862-8297', 'LEANDRO', 'COMERCIO BAZAR', 'Enviar dato faltante !! ', 'LA SEGUNDA'),
(1370, '5263', 'CELESTINO SPAHN SRL (LINIERS)', '5263', 'DIEGO', 'S', 'swingregalos@hotmail.com ', 'RIVADAVIA 11510', 'LINIERS', '4644-5960', 'GABRIELA', 'COMERCIO BAZAR', 'Enviar dato faltante !! ', 'LA SEGUNDA'),
(1371, '5268', 'INTER-RUEDAS S.R.L.', '5268', 'FERNANDO', 'S', 'kgaudiano@polimeratires.com ', 'BOUCHARD 2889', 'LANUS ESTE', '4246-0149 / 0702', 'LAURA-LILIANA', 'FAB. CUBIERTAS', 'Enviar dato faltante !! ', 'GALENO'),
(1372, '5276', 'ESPINOSA HNOS. S.A.', '5276', 'ESPINOSADOS', 'S', 'espinosapersonal@gmail.com ', 'AV.H. YRIGOYEN 6840', 'R.DE ESCALADA OESTE (LIBRETAS', '4242/0566/4242-4015', 'MIRTA/ TITULAR: SR DELCHIOR', 'FUNDICION', 'Enviar dato faltante !! ', 'GALENO'),
(1373, '5297', 'GENESIS (DE APARICIO NELSON R)', '5297', 'NELSON', 'S', 'info@matriceriagenesis.com.ar ', 'AV. RAVANAL 3220 1° VF 12', 'CAPITAL', '4919-8242', 'NELSON-ADRIANA', 'METALURGICA', 'Enviar dato faltante !! ', 'LA SEGUNDA'),
(1374, '5306', 'CELULOSA DEL SUR', '5306', 'DINO', 'S', 'andreaveronicagarcia@hotmail.com ', 'PORTELA 3479', 'CAPITAL FEDERAL', '4208-6308', 'ALBERTO/FRANCISCO', 'Enviar dato faltante !!', 'andreaveronicagarcia@hotmail.com ', 'GALENO'),
(1375, '5317', 'GALEA L.G. S.A.', '5317', 'ANALIA', 'S', 'info@galealg.com.ar,galea_mangel@hotmail.com,gerencia@galea.com.ar,administracion@galealg.com.ar', 'REPUBLICA ARGENTINA 946', 'LANUS OESTE', '4228-3330', '', 'FAB.ART.ELECTR.', 'proveedoresgalealg@gmail.com. ', 'ART INTERACCION SA'),
(1376, '5326', 'DEL SUR AUTOS S.A.', '5326', 'CECILIA', 'S', 'mcardozo@delsurautos.com.ar ', 'H. YRIGOYEN 3365', 'LANUS OESTE', '4247-3700/5225', 'MARIANA INT 135', 'CONCESIONARIA', 'tesorerial@delsurautos.com.ar ', 'ART INTERACCION SA'),
(1377, '5361', 'BUENOS AIRES SURFING S.R.L.', '5361', 'ALBA', 'S', 'basurfing@hotmail.com ', 'BLANCO ENCALADA 357', 'GERLI OESTE', '4241-0591', 'ALBA', 'TEXTIL', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1378, '5365', 'PRODUCTOS ELECTRICOS IDT S.A.', '5365', 'STELLA', 'S', 'stella@idtsa.com.ar,idtsabrina@idtsa.com.ar ', 'URUGUAY 469 PISO 8 DPTO B', 'CAP FED', '4220-0222/5307-5652', 'airsun', 'MAT.ELECTR.FCA', 'vanina@idtsa.com.ar ', 'LA CAJA'),
(1379, '5371', 'LA CASA DEL 1/2 SOMBRA', '5371', 'DANTE', 'S', 'casadelamembrana@hotmail.com ', 'REMEDIOS DE ESCALADA 538', 'LANUS OESTE', '4240-6667', 'EDUARDO/DANTE', 'COMERCIO', 'Enviar dato faltante !! ', 'LA CAJA'),
(1380, '5372', 'METALURGICA J.M.D. S.A.', '5372', 'DIEGO', 'S', 'lahacendosa@yahoo.com ', 'LUCENA 5361', 'WILDE', '4227-7886', 'GISELLA', 'METALURGICA', 'tesoreria@metalurgicajmd.com.ar ', 'SMG'),
(1381, '5383', 'MIGUEL ANGEL BOSCARIOL', '5383', 'DAVID', 'S', 'daboscariol@conman.com.ar,ricardo_herrera80@yahoo.com.ar', 'R. DE E. DE SAN MARTIN 3771', 'VALENTIN ALSINA', '4208-5568/4228-0588', 'MIGUEL/DAVID', 'METALAURGICA', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(1382, '5387', 'CONS. MARGARITA WEILD 1266', '5387', 'MARISA', 'S', 'mrech_adm@yahoo.com.ar ', 'MARGARITA WEILD 1266', 'LANUS ESTE', '3528-4932', 'MARISA', 'CONSORCIO', 'Enviar dato faltante !! ', 'PREVENCION'),
(1383, '5413', 'LOS CUATRO DEL SUR S.R.L.', '5413', 'CLAUDIA', 'S', 'loscuatrodelsur@speedy.com.ar ', 'CONDARCO 1437', 'LANUS ESTE', '4202-2751/1879', 'LAFUENTE/SIGAUT', 'DISTRIBUIDORA', 'Enviar dato faltante !! ', 'PREVENCION'),
(1384, '5428', 'NUXEN S.R.L.', '5428', 'GRISELDA', 'S', 'administracion@deflex.com.ar ', 'SITIO DE MONTEVIDEO 2381', 'LANUS ESTE', '4241-7762', 'GRISELDA', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'LA CAJA'),
(1385, '5454', 'PRINGLES SAN LUIS S.A.', '5454', 'ADALBERTO', 'S', 'rrhh@pringlessanluis.com.ar,julieta.pangaro@pringlessanluis.com.ar', 'TELEMIAN CONDIE 1950', 'PARQUE 9 DE ABRIL E.ECHEVERRIA', '4693-4693, (15)6815-5002', 'VERONICA/ADALBERTO', 'PLASTICO', 'juan@pringlessanluis.com.ar ', 'PROVINCIA'),
(1386, '5473', 'PAPELTEX ARGENTINA SAIYC', '5473', 'PABLO', 'S', 'ptx@ciudad.com.ar ', 'BOLIVIA 1960', 'LANUS O.', '4225-8020', 'LAURA/PABLO', 'PAPELERA', 'administraptx@gmail.com ', 'PROVINCIA'),
(1387, '5500', 'LANSS SA', '5500', 'ELIZABETH', 'S', 'administracion@lanss.com.ar,marianamiguens@lanss.com.ar', 'UCRANIA 463', 'VALENTIN ALSINA', '4228-4681', 'CARLOS MIGUENS / MARIANA/JULIA AUTORIZAD', 'FILTROS', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1388, '5512', 'D ALIK S.R.L.', '5512', 'DALIK', 'S', 'elroble@rcc.com.ar ', 'CHACO 2580', 'LANUS OESTE', '4208-8500/4218-4603', 'HERNAN-NORMA', 'METALURGICA', 'Enviar dato faltante !! ', 'SMG'),
(1389, '5557', 'HIERROS DEL SUR', '5557', 'GRACIELA', 'S', 'sesurhierros@hotmail.com ', 'SALTA 439', 'LANUS ESTE', '4225-6972', 'HERNAN', 'METALURGICA', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(1390, '5588', 'MARLEY ( GONZALEZHORACIO )', '5588', 'HORACIO', 'S', 'marley.srl@hotmail.com ', 'PORTUGAL 960', 'TEMPERLEY', '4260-2344', 'SUSANA', 'PLASTICO', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(1391, '5590', 'TALLERES MAO S.R.L.', '5590', 'HAYDEE', 'S', 'maotero58@ciudad.com.ar,talleresmaosrl@fibertel.com.ar', 'RIO DE JANEIRO 1353', 'LANUS OESTE', '4247-1741', 'NORA/JORGE', 'METALURGICA', 'Enviar dato faltante !! ', 'PREVENCION'),
(1392, '5594', 'METALCROM S.A.', '5594', 'LORENA', 'S', 'info@metalcrom.com.ar,lgoites@metalcrom.com.ar', 'MEXICO 969', 'AVELLANEDA OESTE', '4208-0170/4209-5963 INT 1', 'LORENA', 'METALURGICA', 'Enviar dato faltante !! ', 'PREVENCION'),
(1393, '5606', 'AUTORADIO ESCALADA SRL', '5606', 'LILIAN', 'S', 'arescalada@arescalada.com.ar,lilian@arescalada.com.ar,leonardo@arescalada.com. Ar,lilian@arescalada.com.ar', 'AV. HIPOLITO IRIGOYEN 6828', 'REMEDIOS DE ESCALADA', '4248-9389', 'LEONARDO/JORGE/LILIAN', 'IST.ELEC.', 'lilian@arescalada.com.ar ', 'GALENO'),
(1394, '5609', 'GRECO PUBLICIDAD S.R.L.', '5609', 'PATRICIA', 'S', 'grecopublicidad@speedy.com.ar ', 'J. B. VAGO 485', 'TEMPERLEY OESTE', '4298-8365 4231-6604', 'MONICA/LEONARDO', 'PUBLICIDAD', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1395, '5633', 'CELEC S.A.', '5633', 'FERNANDO', 'S', 'celec@sion.com ', 'OLIDEN 1669', 'VALENTIN ALSINA', '4218-5500', 'FERNANDO RODRIGUEZ', 'DIST. MAT. ELEC', 'admcelec@yahoo.com.ar ', 'ASOCIART A.R.T.'),
(1396, '5641', 'TACCA HNOS SA', '5641', 'GRACIELA', 'S', 'info@taccahnos.com.ar,tacca@ciudad.com.ar ', 'CONDOR 2047', 'CAPITAL FEDERAL', '4918-9061/9950', 'GRACIELA TACCA', 'METALAURGICA', 'Enviar dato faltante !! ', 'GALENO'),
(1397, '5655', 'COMPLEJO E.P.NTRA.SRA REMEDIOS', '5655', 'MARTA', 'S', 'complejoeducativo4@yahoo.com.ar ', 'ROSALES 1036', 'REMEDIOS DE ESCALADA OESTE', '4242-0952/4242-5802 (PRIM', 'AREA PROTEGIDA MARTA', 'EDUCATIVA', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(1398, '5664', 'PUERTO TEXTIL SA', '5664', 'FLAVIO', 'S', 'info@puertotextil.com.ar,ventas@puertotextil.com.ar,administracion@puertotextil.com.ar', 'PERITO MORENO 375', 'CIUDADELA', '4115-7390', 'CLARA/DAMIAN/ARIEL', 'TEXTIL', 'Enviar dato faltante !! ', 'MAPFRE'),
(1399, '5669', 'SOLMAYA S.A', '5669', 'VIVIANA', 'S', 'vivianaperrupato@gmail.com ', 'MEEKS 170', 'LOMAS DE ZAMORA', '4244-3148/4292-4424/', 'GABRIEL', 'CONSTRUCCION', 'ferrari_manestar@yahoo.com.ar ', 'ART INTERACCION SA'),
(1400, '5670', 'TEJIDOS CATAMARCA S.R.L.', '5670', 'LILIANA', 'S', 'pablo@tejidoscatamarca.com.ar ', 'JOSE IGNACIO RUCCI 1580', 'LANUS OESTE', '4368-6085', 'LILIANA-CLAUDIA', 'TEXTIL', 'ivana@tejidoscatamarca.com.ar ', 'PROVINCIA'),
(1401, '5679', 'G.N.C. EL PROGRESO S.R.L.', '5679', 'DIEGO', 'S', 'gncelprogresosrl@yahoo.com.ar ', 'AV. ALVAREZ THOMAS FLORES 2999', 'BERNAL OESTE', '4270-6778', 'YANINA', 'EST. DE SERV.', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1402, '5680', 'KOMCARBUS S.R.L.', '5680', 'JUAN CARLOS', 'S', 'komcar@infovia.com.ar ', 'AV. SAN MARTIN 948', 'LANUS OESTE', '4363-3100 INT. 2393', '1551824070 JUAN CARL', 'TRANSPORTE', 'Enviar dato faltante !! ', 'GALENO'),
(1403, '5693', 'LOS LEONES(DE ENRIQUE MIGLIANI', '5693', 'EDUARDO', 'S', 'publicidadlosleones@opcionestelmex.com.ar,losleonessolutions@gmail.com', 'AV PTE SARMIENTO 1031', 'AVELLANEDA ESTE', '4116-2029/2032', 'EDUARDO', 'CARTELERIA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1404, '5694', 'VICENTE LUCIANO E HIJOS SRL', '5694', 'ANALIA', 'S', 'contacto@curtiembreluciano.com.ar ', 'FLORIDA 252', 'VALENTIN ALSINA', '4-208-6633', 'LUCIANO MARCELO/ANALIA', 'CURTIEMBRE', 'analia@curtiembreluciano.com.ar,curtiembreluciano@speedy.com.ar', 'PROVINCIA'),
(1405, '5720', 'ACEROPLAT S.A.', '5720', 'PABLO', 'S', 'ventas@aceroplat.com.ar,pablocortese@hotmail.com', 'CRISOLOGO LARRALDE 2615', 'AVELLANEDA ESTE', '4203-8080', 'ACERO PLAT', 'METALAURGICA', 'Enviar dato faltante !! ', 'GALENO'),
(1406, '5725', 'ENVAR SOCIEDAD ANONIMA', '5725', 'SECO', 'S', 'envarfca@speedy.com.ar,envarrrhh@speedy.com.ar', 'AV LOS QUILMES 870', 'BERNAL OESTE', '4388-0167/4252-1019/0964', 'GERARDO SALINAS 1568113143', 'PLASTICO', 'envarrrhh@speedy.com.ar ', 'ART INTERACCION SA'),
(1407, '5731', 'SUPERMERCADO LAS MARIAS', '5731', 'EUGENIA', 'S', 'm.eugeniacanedo@hotmail.com,angelsurdi@live.com', 'MADARIAGA 443', 'LANUS ESTE', '4240-2628', 'ANGEL/MARIA TERESA', 'SUPERMERC.', 'Enviar dato faltante !! ', 'PREVENCION'),
(1408, '5744', 'ANTON LAURA SABINA', '5744', 'PAULA', 'S', 'mercadoalsina@pedidos.com.ar,mpcosta@live.com.ar,pedidos@mercadoalsina.com.ar', 'J.D. PERON 3242', 'V. ALSINA', '4-218-2549', 'LAURA ANTON', 'SUPERMERC.', 'Enviar dato faltante !! ', 'QBE'),
(1409, '5750', 'LIAND S.A.', '5750', 'JUAN', 'S', 'juant@liandsa.com.ar,arcalg@liandsa.com.ar ', 'SANTO DOMONGO 3849', 'CAPITAL FEDERAL', '4116/9929 4116/9918', 'WALTER/NIEVES', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(1410, '5753', 'ANGA S.A.', '5753', 'LUIS', 'S', 'anga@sion.com ', 'MANUELA PEDRAZA 2854', 'V.ALSINA', '4208-9096', 'ADRIANA', 'FAB. RODILLOS', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1411, '5754', 'I.M.P. ARGENTINA S.R.L.', '5754', 'LAURA', 'S', 'laura.rodriguez@plaul.com.ar,administracion@plaul.com.ar', 'YATAY 3321', 'LANUS', '4208-2483', 'RODRIGUE JUAN CARLOS (PLAUL)', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'GALENO'),
(1412, '5756', 'NARDI SALVADOR', '5756', 'CATALINA', 'S', 'amnardi@speedy.com.ar,narocca@narocca.com.ar ', 'CNEL DELIA 2135', 'LANUS OESTE', '4262-1020', 'CATALINA/SILVIA/ALEJANDRA', 'TEXTIL', 'narocca@narocca.com.ar ', 'GALENO'),
(1413, '5764', 'BASTIDA S.A. SEG.PRIV.INTEGRAL', '5764', 'MARIELA', 'S', 'bastidaseguridad@hotmail.com ', 'HIPOLITO YRIGOYEN 7799', 'LOMAS DE ZAMORA OESTE', '4242-3071', 'LUIS PONCE/BASTIDA', 'SEGURIDAD', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(1414, '5790', 'ALFAPLAS S.R.L.', '5790', 'MARIA', 'S', 'alfaplastsrl@hotmail.com ', 'CORDOBA 1250', 'AVELLANEDA ESTE', '4204-5321', 'MARIA/MIGUEL', 'PLASTICO', 'Enviar dato faltante !! ', 'LA CAJA'),
(1415, '5809', 'MARIO DANA S.R.L.', '5809', 'DANA', 'S', 'danadeportes@speedy.com.ar ', '25 DE MAYO 26', 'LANUS OESTE', '4249-4200/4357-0099', 'SILVIA/ALEJANDRO', 'COMERCIO BAZAR', 'Enviar dato faltante !! ', 'GALENO'),
(1416, '5810', 'AMC METALURGICA S.R.L.', '5810', 'ALBERTO', 'S', 'amc.metalurgica2013@gmail.com ', 'TERRADA 2051 Piso:pb - ENTRE LAS CALLES:', 'CAPITAL FEDERAL', '15-5226-2616', 'ALBERTO', 'Enviar dato faltante !!', 'amc.metalurgica2013@gmail.com ', 'ART INTERACCION SA'),
(1417, '5815', 'LA BUENOS AIRES SEGUROS ART', '5815', 'LA BUENOS AIRES SEGU', 'N', 'no ', 'FLORIDA 229', 'CIUDAD AUTONOMA DE BUENOS AIRE', '4348-5600', 'Enviar dato faltante !!', 'ART', 'Enviar dato faltante !! ', 'Enviar dato faltante !!'),
(1418, '5825', 'METAL TIME SA', '5825', 'ARIEL', 'S', 'metaltime@gmail.com ', 'CALLAO 2356', 'LANUS OESTE', '4218-1299', 'OSCAR OLIVA(BETKA)', 'METALAURGICA', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(1419, '5831', 'GALVAFER S.R.L.', '5831', 'GALVAFER', 'S', 'galvafer@opcionestelmex.com.ar,md@opcionestelmex.com.ar', 'SASN VLADIMIRO 5860', 'VALENTIN ALSINA', '4208-0133', 'MIGUEL', 'METALURGICA', 'galvafer@opcionestelmex.com.ar ', 'QBE'),
(1420, '5834', 'TAURO HNOS (ANTONIO Y CARLOS)', '5834', 'ANTONIO', 'S', 'taurohnos@yahoo.com.ar,germanpisacreta_03@hotmail.com', 'DR.BALBIN 2663', 'VALENTIN ALSINA', '4209-2171', 'ANTONIO', 'CARPINTERIA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1421, '5869', 'BRABA S.R.L. (LANUS OESTE)', '5869', 'BRABASRL', 'S', 'braba.vete@gmail.com ', 'AV. SAN MARTIN 3301/11', 'LANUS OESTE', '4357-0043/4247-1282/4225-', 'ALICIA', 'VETERINARIA', 'Enviar dato faltante !! ', 'QBE'),
(1422, '5870', 'BRABA S.R.L. (LANUS ESTE)', '5870', 'BRABA', 'S', 'braba.vete@gmail.com ', 'EVA PERON 2251', 'LANUS ESTE', '4357-0043/4247-1282', 'ALICIA', 'VETERINARIA', 'Enviar dato faltante !! ', 'QBE'),
(1423, '5873', 'TECNOTENIS S.R.L.', '5873', 'JESICA', 'S', 'jguaquinsoy@atomik.com.ar ', 'A. DEL VALLE 2985', 'LANUS OESTE', '4-368-6306/05 4218-6538', 'GUSTAVO BRACAMONTE', 'CALZADO', 'rmakuch@atomik.com.ar ', 'LA CAJA'),
(1424, '5883', 'H. BENELLI Y CIA S.R.L.', '5883', 'KAREN', 'S', 'hbenelli@ciudad.com.ar,info@hbenelli.com.ar ', 'UDAONDO 2078', 'LANUS OESTE', '4225-8389/8390', 'MIRIAM/KAREN', 'FAB. HILADOS', 'info@hbenelli.com.ar ', 'FEDERACION PATRONAL'),
(1425, '5901', 'FEDERACION. PATRONAL. (AC.PERSONALES)S.A.', '5901', 'FEDERACIONPERSONAL', 'S', 'lleiva@fedpat.com.ar, aripoll@fedpat.com.ar ', 'ALSINA 860 PB', 'CAPITAL FEDERAL', '0221-429-0200', 'CARLOS BERNASCONI', 'SEGUROS', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL ACC PERSONALES'),
(1426, '5909', 'FAMILIBEL S.A.', '5909', 'FAMILIBEL', 'S', 'familibel@yahoo.com.ar ', 'SAN MARTIN 701 ESQ.J.JAURES', 'LANUS OESTE', '4368-6415/ 4240-9946 DESP', 'MIRTA', 'DISTRIBUIDORA', 'Enviar dato faltante !! ', 'SMG'),
(1427, '5910', 'G.N.C. GERLI S.R.L.', '5910', 'ARIEL', 'S', 'adriansiri@hotmail.com,joelsiri@hotmail.com ', 'HIPOLITO YRIGOYEN 2298', 'AVELLANEDA OESTE', '4218-5936', 'JOEL', 'EST. DE SERV.', 'Enviar dato faltante !! ', 'SMG'),
(1428, '5913', 'MOLDURAS DEL PLATA SRL', '5913', 'CLAUDIA', 'S', 'ventas@moldurasdelplata.com,proveedores@moldurasdelplata.com.ar', 'REP.ARGENTINA 2359', 'VALENTIN ALSINA', '4116-3371/4541', 'FEDERICO JUAN CARLOS FLORENCIA', 'MADERERA', 'Enviar dato faltante !! ', 'GALENO'),
(1429, '5961', 'TALSIUM S.A. ( CONFECOR )', '5961', 'MARCELO', 'S', 'confecor@confecor.com.ar,personal@confecor.com.ar,marcelopulvirenti@talsium.com.ar,nataliagelos@talsium.com.ar', 'PIEDRAS 348 P PB.', 'CAPITAL FEDERAL', '4342-6800/4342-1432', 'MARCELO', 'PERS.TEMPORARIO', 'pablofiorentini@talsium.com.ar ', 'ART INTERACCION SA'),
(1430, '5966', 'GASTROINDUSTRIA S.R.L.', '5966', 'EDUARDO', 'S', 'emiliano@gastroindustria.org ', 'LAS HERAS 1661', 'BANFIELD O', '4202.6913', 'EDUARDO', 'METALAURGICA', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(1431, '5969', 'EXPOCONSUMO S.R.L.', '5969', 'ARIEL', 'S', 'antogiardino@hotmail.com,arielgiardino@ciudad.com.ar', 'MAXIMO PAZ 177', 'LANUS OESTE', '4241-3111', 'RAUL-ANTONELLA', 'DISTRIBUIDORA', 'Enviar dato faltante !! ', 'SMG'),
(1432, '5970', 'METALURGICA TASIA S.R.L.', '5970', 'TASIA', 'S', 'comercial@metalurgicatasia.com,pbellini@metalurgicatasia.com', 'J.D. PERON 700', 'LOMAS DE ZAMORA OESTE', '4282-7140/1551835914/', 'VIVIANA15-5713-3427', 'METALURGICA', 'pbellini@metalurgicatasia.com,compras@metalurgicatasia.com', 'GALENO'),
(1433, '5971', 'FORMACO S.R.L.', '5971', 'WALTER', 'S', 'walter@formaco.com.ar,info@formaco.com.ar,vanesa@formaco.com.ar', 'BELELLI 557', 'LOMAS DE ZAMORA OESTE', '4282-9919/6869', 'WALTER', 'FAB. INSUMOS', 'proveedores@formaco.com.ar,vanesa@formaco.com.ar', 'GALENO'),
(1434, '5986', 'SUSANA VAZQUEZ Y ASOC.', '5986', 'SUSANA', 'S', 'svadministracion@fibertel.com.ar ', 'YRIGOYEN HIPOLITO 3838 PB', 'CAPITAL FEDERAL', '4951-9413/4222-0641', 'ANDREA/SUSANA', 'INSTALACIONES', 'svadministracion@fibertel.com.ar ', 'PREVENCION'),
(1435, '5989', 'WORKS GROUP SRL', '5989', 'DPLUIS', 'S', 'worksgroupsrl@yahoo.com.ar,danielworks@speedy.com.ar', 'MENTRUY 1953', 'BANFIELD', '/4248-6501', 'Enviar dato faltante !!', 'CONSTRUC. MAT.', 'Enviar dato faltante !! ', 'SMG'),
(1436, '5995', 'HERRAJES FRAGATA S.A.', '5995', 'FRAGATA', 'S', 'info@herrajesfragata.com.ar,ventas@herrajesfragata.com.ar,administracion@herrajesfragata.com.ar', 'WARNES 1062', 'CAPITAL', '4856-3776', 'GUSTAVO', 'HERRAJES', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(1437, '6023', 'LA FLOR DE CARAZA(S.G.NEGREIRA', '6023', 'LUISA', 'S', 'NO MAIL 2 ', 'MARCOS AVELLANEDA 3899', 'LANUS OESTE', '4262-0344', 'LUISA', 'PANADERIA CONF.', 'Enviar dato faltante !! ', 'SMG'),
(1438, '6027', 'INSTITUTO SAN JUAN BAUTISTA', '6027', 'MARIA INES', 'S', 'egbsanjuanbautista@yahoo.com.ar ', 'PTE PERON 2998', 'VALENTIN ALSINA', '4228-2038/4218-5385', 'MARIA INES', 'EDUCATIVA', 'Enviar dato faltante !! ', 'GALENO'),
(1439, '6031', 'VICTOR HUGO S.R.L.', '6031', 'ANDRES', 'S', 'vhcorralonsrl@yahoo.com.ar ', 'RODRIGUEZ PENA 1802', 'BANFIELD OESTE', '4248-3340', 'VICTOR', 'MAT. CONST.', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1440, '6054', 'E.P. NRO 18', '6054', 'E.G.B. NRO 18', 'N', 'egb18lanus@hotmail.com ', 'MADARIAGA 1960', 'LANUS ESTE', '4241-4576', 'AREA PROTEGIDA', 'EDUCATIVA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1441, '6059', 'CIOCCA PLAST S.R.L.', '6059', 'YAMILA', 'S', 'info@cioccaplast.com.ar ', 'CARLOS GLADE 950', 'BANFIELD OESTE', '4248-6654/4288-1269', 'LORENA-MARY', 'PLASTICO', 'Enviar dato faltante !! ', 'GALENO'),
(1442, '6079', 'AMIC S.A.C.I.F.A.I.', '6079', 'BEATRIZ', 'S', 'beatriz.cordoba@amic.com.ar ', 'CORONEL RAM0S 49', 'LANUS OESTE', '4241-7577', 'BEATRIZ', 'METALURGICA', 'Enviar dato faltante !! ', 'LA CAJA'),
(1443, '6082', 'ALEMAR (JARA ALEJANDRO HERNAN)', '6082', 'ALEJANDRO', 'S', 'alemarmuebles@speedy.com.ar ', 'CATAMARCA 2950', 'LANUS OESTE', '4262-9142', 'OSVALDO', 'MUEBLERIA', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(1444, '6084', 'ZOELA S.A.', '6084', 'FABIAN', 'S', 'lramirez@misadon.com.ar ', 'SENADOS QUINDIMIL 3451', 'V.ALSINA', '4229-6800', 'FABIANA/GABRIEL BIRO/FERNANDO LICARI', 'CURTIEMBRE', 'spereyra@misadon.com.ar,gdoniquian@misadon.com.ar', 'SMG'),
(1445, '6102', 'NIALJO S.A.', '6102', 'NIALJO', 'S', 'nialjosa@hotmail.com.ar ', 'REMEDIOS DE ESCALADA 4312', 'LANUS OESTE', '.4286-7699/4762', 'MANUEL PEREZ', 'CONSTRUCCION', 'Enviar dato faltante !! ', 'LA CAJA'),
(1446, '6107', 'TECNOLOGIA EN CAUCHO S.R.L.', '6107', 'BARTOLOME', 'S', 'tecsrl@velocom.com.ar,burletestec@hotmail.com ', 'SARMIENTO 248', 'LANUS ESTE', '4204-5401/1552269214', 'BARTOLOME', 'CAUCHO', 'Enviar dato faltante !! ', 'GALENO'),
(1447, '6108', 'MONGIELLO MIGUEL ANGEL', '6108', 'MONGIELLO', 'S', 'creacionesbrache@hotmail.com ', 'RUCCI 1827', 'LANUS OESTE', '4218-3750', 'MIGUEL/LUIS', 'CALZADO', 'Enviar dato faltante !! ', 'QBE'),
(1448, '6140', 'LADYCHUK ADRIAN', '6140', 'ADRIAN', 'S', 'alaluminio@speedy.com.ar,info@aluminio.com.ar ', 'UCRANIA 471', 'V. ALSINA', '4218-5155', 'ADRIAN/GRACIELA', 'ACCESORI.AUTOMO', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1449, '6142', 'WORK AND SERVICE S.R.L', '6142', 'CLAUDIO', 'S', 'rrhhwork@speedy.com.ar ', 'AV.CORRIENTES 2330 5TO P.OF503', 'CAP.FED', '4952-9996', 'ANA MARIA', 'AGENCIA DE EMPLEOS', 'work.service@speedy.com.ar ', 'ASOCIART A.R.T.'),
(1450, '6147', 'SURIANO S.A.', '6147', 'MARCELO', 'S', 'rhsuriano@uniformes-saber.com.ar,mhs@uniformes-saber.com.ar', 'ALVARADO 3155 (LIBRETAS) MEMBRILLAR 69', 'LANUS ESTE (LIBRETAS) CAP FED', '4246-0599', 'ESTELA, MARCELO SANGUINETTI', 'FCA GUARDAPOLVO', 'Enviar dato faltante !! ', 'GALENO'),
(1451, '6162', 'LAMINADOS AVELLANEDA S.R.L.', '6162', 'FERNANDO', 'S', 'laminadosavellaneda@hotmail.com ', '3 SARGENTOS 1251', 'AVELLANEDA', '4204-6870', 'FERNANDO/RAUL', 'METALURGICA', 'Enviar dato faltante !! ', 'PREVENCION'),
(1452, '6166', 'CHAPAFERRO S.R.L.', '6166', 'VIVIANA', 'S', 'chapaferro@sion.com,administracion@chapaferrosrl.com.ar', 'CNO GRAL BELGRANO 6060', 'WILDE', '4246-5141', 'VIVIANA/MARTA', 'METALURGICA', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(1453, '6186', 'AQUARINE S.R.L', '6186', 'DARIO', 'S', 'aquarinesrl@hotmail.com ', 'FRAY JULIAN LAGOS 1953', 'LANUS OESTE', '4247-8200', 'DARIO ALARCON', 'DISTRIB. FABRIC DE ALIMENTOS', 'Enviar dato faltante !! ', 'GALENO'),
(1454, '6194', 'PROACTIVA AVELLANEDA S.A', '6194', 'RODRIGO', 'S', 'rodrigo.aliendo@veolia.com,ricardo.gomez@veolia.com,florencia.daboul@veolia.com,dr_ramirezr@yahoo.com.ar', 'CAMACUA 5335', 'WILDE', '4220-2444//1582/0300', 'RODRIGO ALIENDO', 'RECOLECCION RESIDIUOS', 'maximiliano.turturro@veolia.com ', 'GALENO'),
(1455, '6228', 'CUEROS FERRAS S.R.L.', '6228', 'MARIA', 'S', 'francisco@ferras.com.ar,info@cuerosferras.com.ar', 'HIPOLITO YRIGOYEN 847', 'AVELLANEDA OESTE', '4201-7580/6798 4222-4572', 'MARIA/MARCELO', 'CURTIEMBRE', 'Enviar dato faltante !! ', 'QBE'),
(1456, '6237', 'VIDRIOS CASTELAR S.A.', '6237', 'CARINA', 'S', 'ventas@vidrioscastelarsa.com.ar,info@vidrioscastelarsa.com.ar,compras@vidrioscastelarsa.com.ar', 'SAN MARTIN 2563', 'LANUS OESTE', '4262-7837/0818 3750-7192', 'KARINA', 'VIDRIOS', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1457, '6243', 'INDAR TEXTIL', '6243', 'MARIA', 'S', 'indartextil@yahoo.com.ar ', 'CHACO 973/975', 'LANUS OESTE', '4139-4703/20', 'CLAUDIO', 'BORDADOS', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1458, '6273', 'INTERLUX (CARRERA WALTER NELSON)', '6273', 'WALTER', 'S', 'carrerawalter@yahoo.com.ar ', 'CABILDO 229', 'AVELLANEDA OESTE', '4209-5474', 'WALTER', 'LUBRICENTRO', 'Enviar dato faltante !! ', 'GALENO'),
(1459, '6280', 'GRACIANO FALAGUERRA', '6280', 'GRACIANO', 'S', 'fal-mar@hotmail.com ', 'MARCO AVELLANEDA 2848', 'R.DE ESCALADA OESTE', '4267-6653', 'GRACIANO/CECILIA', 'TALLER', 'Enviar dato faltante !! ', 'GALENO'),
(1460, '6291', 'ESTAMPADOS CRISOL (DIAZ-BERTOG', '6291', 'DIAZ', 'S', 'estampadoscrisol@yahoo.com.ar ', 'PILCOMAYO 3262', 'LANUS OESTE', '4262-3996', 'GUSTAVO', 'ESTAMPADOS', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(1461, '6294', 'MECANICA DE PRECISION DYG SRL', '6294', 'MECANICA', 'S', 'mpdyg@ciudad.com.ar ', 'CORVALAN 250', 'WILDE', '4207-9040', 'EMILIA', 'METALURGICA', 'Enviar dato faltante !! ', 'QBE'),
(1462, '6295', 'LURASCHI MARTA SUSANA', '6295', 'GUILLERMO', 'S', 'info@saloncostamarfil.com.ar ', 'AV. HIPOLITO YRIGOYEN 1360', 'AVELLANEDA OESTE', '4218-5591/1537968955', 'GUILLERMO', 'CATERING', 'info@saloncostamarfil.com.ar ', 'PROVINCIA'),
(1463, '6303', 'PROVIDENCE', '6303', 'PROVIDENCE', 'S', 'providencecentral@hotmail.com ', 'ITUZAINGO 1194', 'LANUS ESTE', '4225-8955 / 9526 4292-954', 'CARLOS/NORMA', 'COMERCIO BAZAR', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1464, '6309', 'CENTRO MISIONAL BARAGA', '6309', 'ANDREA', 'S', 'mariareinacont@hotmail.com,h-san-vicente@hotmail.com', 'MONSE#OR HLADNIK 4029', 'REMEDIOS DE ESCALADA OESTE', '4267-1082/84/83 PED', 'MARIA 4286-8124', 'EDUCATIVA', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(1465, '6310', 'CNTRO MISIONAL BARAGA', '6310', 'ANDREA', 'S', 'mariareinacont@hotmail.com,h-san-vicente@hotmail.com', 'MONSEÑOR HLADNIK 4029', 'REMEDIOS DE ESCALADA OESTE', '4286-8124 (4267-1083 PED', 'AREA PROTEGIDA', 'EDUCATIVA', 'Enviar dato faltante !! ', 'QBE'),
(1466, '6324', 'CTRO MISIONAL BARAGA(HOG Y PAD', '6324', 'ANDREA', 'S', 'mariareinacont@hotmail.com,h-san-vicente@hotmail.com', 'MONSE#OR HLANDIK 4029', 'REMEDIOS ESCALADA', '', 'AREA PROTEGIDA', 'EDUCATIVA', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(1467, '6325', 'HIGIENICA SRL', '6325', 'CONSTANZA', 'S', 'constanzapassarelli@hotmail.com ', 'CONDARCO 2364', 'LANUS E', '4289-2030', 'CONSTANZA/NICOLAS', 'FRACCIONADORA', 'Enviar dato faltante !! ', 'PREVENCION'),
(1468, '6345', 'JORGE DANIEL BIELLE', '6345', 'GABRIELA', 'S', 'gabrielajaureguiberry1@gmail.com ', 'HIPOLITO YRIGOYEN 4064', 'LANUS OESTE', '4225-3490 DESP 16 HS/1561', 'GABRIELA', 'CONFITERIA', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(1469, '6346', 'CRISTINI JOSE MARCELO', '6346', 'MARCELO', 'S', 'tintoreriacristini@hotmail.com ', 'JUNCAL 2811', 'LANUS ESTE', '/1569610158', 'Enviar dato faltante !!', 'TINTORERIA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1470, '6353', 'CZERNIAS S.A.', '6353', 'RICARDO', 'S', 'czerniric@yahoo.com.ar ', 'CAMACUA 4840', 'VILLA DOMINICO', '4246-8151', 'SR CZERNIAS', 'METALURGICA', 'Enviar dato faltante !! ', 'SMG'),
(1471, '6359', 'GENEMAX S.R.L.', '6359', 'DIEGO', 'S', 'genemaxgnc@hotmail.com ', 'MADARIAGA 1305', 'LANUS ESTE', '4249-3531/1551850799', 'DIEGO/CRISTIAN BOSIO', 'EST. DE SERV.', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL');
INSERT INTO `Usuarios` (`id`, `Codigopal`, `Nombrepal`, `Clave`, `Usuario`, `Activo`, `Correo`, `Domicilio`, `Localidad`, `Telefono`, `Contacto`, `Actividad`, `Correop`, `Nombart`) VALUES
(1472, '6377', 'ESCUELA NRO. 8 (OCHO)', '6377', 'MARTA', 'S', 'escuela8argentina@hotmail.com ', 'BOUCHARD 370', 'LANUS OESTE', '4241-4171/4247-2916', 'AREA PROTEGIDA/MARTA KOPILOFF DIRECTORA', 'EDUCATIVA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1473, '6394', 'GERIATRICO LOS SUBLIME', '6394', 'MARIA', 'S', 'gloria_picallo@hotmail.com ', 'AV BELGRANO 3502', 'SARANDI', '4207-9239///4289-6214', 'MARIA EMILSEN', 'GERIATRICO', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1474, '6401', 'EST. MECANICO SALVADOR GRECO', '6401', 'SALVADOR', 'S', 'omargreco@fullzero.com.ar,omargreco60@gmail.com,omargreco@gmail.com', 'PJE SARRAT 799', 'SARANDI', '4207-6689', 'OMAR', 'METALURGICA', 'Enviar dato faltante !! ', 'SMG'),
(1475, '6413', 'SOMAVILLA S.A.', '6413', 'CAROLINA', 'S', 'somavilla@gncplus.com ', 'GRAL HORNOS 2102', 'LOMAS DE ZAMORA', '4276-3115', 'CAROLINA', 'EST. DE SERV.', 'Enviar dato faltante !! ', 'SMG'),
(1476, '6417', '5 DE AGOSTO S.R.L', '6417', 'LUCIANA', 'S', 'personal@mo45.com.ar,siniestros@mo45.com.ar ', 'CAZON 1257 PB', 'REMEDIOS DE ESCALADA OESTE', '4241-4351', 'ANA MARIA O CHICHE', 'COLECTIVOS', 'Enviar dato faltante !! ', 'GALENO'),
(1477, '6468', 'DISTRIBUIDORA CENTENARIO (PEDRO LUONGO)', '6468', 'EMILIO', 'S', 'cuentascentenario@speedy.com.ar,rrhh.centenario@yahoo.com.ar', 'EL PLUMERILLO 690', 'BANFIELD', '4-286-4977/2624', 'EMILIO', 'DISTRIBUIDORA', 'cuentascentenario@speedy.com.ar ', 'PROVINCIA'),
(1478, '6477', 'TRASGO S.A.', '6477', 'LAURA', 'S', 'trasgosa@gmail.com ', 'LITUANIA 2643', 'REMEDIOS DE ESCALADA', '4-267-7562', 'Enviar dato faltante !!', 'PAPELERA', 'Enviar dato faltante !! ', 'GALENO'),
(1479, '6493', 'COOP.DE TRAB.FAST LIMITADA', '6493', 'ELINA', 'S', 'cooperativadetrabajo@fibertel.com.ar ', 'HUMBERTO PRIMO 985 PISO 7 OF. 4', 'CAP FED', '4307-2490 ELINA', 'AREA PROTEGIDA', 'CORREO', 'cooperativadetrabajofast@gmail.com ', 'NO POSEE ART'),
(1480, '6514', 'SELLART OSVALDO ENRIQUE', '6514', 'OSVALDO', 'S', 'transportessellart@gmail.com ', 'BUERAS 1673', 'LANUS ESTE', '4249-7296', 'CELIA', 'TRANDPORTE', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(1481, '6525', 'NUEVA ESCUELA LOMAS S.H.', '6525', 'NUEVA', 'S', 'nueva-escuela-lomas@hotmail.com ', 'HIPOLITO YRIGOYEN 9847', 'LOMAS DE ZAMORA', '4392-0505', 'MUNOZ - PRIETO', 'EDUCATIVA', 'Enviar dato faltante !! ', 'MAPFRE'),
(1482, '6535', 'NEUMATICOS MONSA S.A.', '6535', 'GERARDO', 'S', 'neumaticosmonsa@yahoo.com.ar ', 'H. YRIGOYEN 3627', 'LANUS OESTE', '4249-1957/4241-9230', 'GERARDO', 'GOMERIA', 'Enviar dato faltante !! ', 'LA HOLANDO'),
(1483, '6566', 'ATTENDANCE SA', '6566', 'MARTA', 'S', 'lilianpalazzo@attendance.com.ar,administracionlomas@attendance.com.ar', 'AV HIPOLITO YRIGOYEN 9228', 'LOMAS DE ZAMORA', '4292-8900 INT 301', 'MARTA - LILIAN', 'COMERCIO', 'Enviar dato faltante !! ', 'GALENO'),
(1484, '6573', 'DE SIMONE ROBERTO ANDRES', '6573', 'ROBERTO', 'S', 'desimonerobertoandres@yahoo.com.ar ', 'MAXIMO PAZ 419', 'LANUS OESTE', '4249-7639', 'MARIA JOSEFINA/ROBERTO', 'DISTRIBUIDORA', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(1485, '6575', 'ILPEM S.A.', '6575', 'ILPEM', 'S', 'l.astarita@ilpem.com.ar,proveedores@ilpem.com.ar,info@ilpem.com.ar', 'CNO GRAL BELGRANO 778', 'AVELLANEDA ESTE', '4105-4400', 'DANIEL/JUAN MASINO', 'COMERC.DE ACEROS Y ALUM', 'proveedores@ilpem.com.ar ', 'LA CAJA'),
(1486, '6576', 'EURAL S.A.', '6576', 'EURAL', 'S', 'l.astarita@ilpem.com.ar ', 'MARIO BRAVO 992', 'AVELLANEDA ESTE', '4105-4400', 'DANIEL EDUARDO', 'ACCESORI.AUTOMO', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1487, '6610', 'TEKMATIC S.A.', '6610', 'ANA', 'S', 'mgw@tekmatic.com.ar ', 'CUBA 751', 'AVELLANEDA OESTE', '4222-5040', 'MONICA WENK', 'METALURGICO', 'mgw@tekmatic.com.ar ', 'QBE'),
(1488, '6626', 'PETROLEUM', '6626', 'NANCY', 'S', 'petroleum_co@hotmail.com ', '9 DE JULIO 1285', 'LANUS ESTE', '4241-5364 LOCAL', 'EMILCE', 'COMERCIO BAZAR', 'petroleum_co@hotmail.com ', 'LA CAJA'),
(1489, '6631', 'PLASTICOS MATEU (NESTOR MATEU)', '6631', 'NESTOR', 'S', 'plasticosmateu@speedy.com.ar ', 'HELGUERA 2269', 'AVELLANEDA ESTE', '4204-8713', 'TERESA/NESTOR', 'PLASTICOS', 'Enviar dato faltante !! ', 'SMG'),
(1490, '6633', 'F.H. SRL', '6633', 'DEBORA', 'S', 'fhsrl@hotmail.com,fhsrld@gmail.com ', 'SARRATEA 16', 'QUILMES', '4224-7814', 'FARINELLA FRANCISCO', 'CALZADO', 'Enviar dato faltante !! ', 'QBE'),
(1491, '6643', 'AFG DISTRIBUCION SRL', '6643', 'LEONARDO', 'S', 'afg279@hotmail.com ', 'CNO DE CINTURA 8185', '9 DE ABRIL', '4693-1670 - FATIMA 156813', 'LEONARDO', 'EST. DE SERV.', 'Enviar dato faltante !! ', 'LA CAJA'),
(1492, '6650', 'ASOC.CIV.LA ESQ.DE LAS FLORES', '6650', 'REBECA', 'S', 'angelita@esquinadelasflores.com.ar ', 'AV BRUIX 4550', 'CAPITAL FEDERAL', '4831-1648 4832-8528 4813-', 'ANGELICA', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'PROVINCIA COLONIA SUIZA'),
(1493, '6661', 'BRUNO LUIS A. Y BRUNO MANUEL', '6661', 'LUIS', 'S', 'luis511bruno@hotmail.com,indulab@speedy.com.ar', 'AV. TOMAS FLORES 1020', 'QUILMES OESTE', '4270-8668', 'LUIS/ JUAN MANUEL', 'METALURGICA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1494, '6696', 'TRASPOSICION DIDACTICA S.R.L.', '6696', 'VANINA', 'S', 'colegioboston@live.com.ar ', 'HIPOLITO YRIGOYEN 7671', 'BANFIELD OESTE', '4248-6882', 'SUSANA', 'COLEGIO', 'Enviar dato faltante !! ', 'PREVENCION'),
(1495, '6706', 'LUZ ART S.A.', '6706', 'LUZ ART S.A.', 'N', 'no ', 'LIMA 1029', 'CIUDAD AUTONOMA DE BUENOS AIRE', '4305-3010/4530', 'Enviar dato faltante !!', 'ART', 'Enviar dato faltante !! ', 'Enviar dato faltante !!'),
(1496, '6716', 'EMA SERVICIOS S.A', '6716', 'ELBIO', 'S', 'everdun@emaservicios.com.ar,administracion@emaservicios.com.ar', 'AV SAN MARTIN 4970', 'FLORIDA', '4730-1902 INT 209', 'ELVIO VERDUN', 'CORREO PRIVADO', 'Enviar dato faltante !! ', 'CAMINOS PROTEGIDOS ART SA'),
(1497, '6727', 'CONS. MINISTRO BRIN 2736', '6727', 'JORGE', 'S', 'ingjorgegomez@gmail.com ', 'MINISTRO BRIN 2736', 'LANUS OESTE', '4240-6249', '154 938-0194', 'CONSORCIO', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(1498, '6753', 'NESTOR MALVAR SRL', '6753', 'NESTOR', 'S', 'nmalvar@speedy.com.ar,jag2000@fibertel.com.ar ', 'ALFREDO PALACIOS 1735', 'VALENTIN ALSINA', '1544924827/4208-4022', 'NESTOR MALVAR', 'METALAURGICA', 'jag2000@fibertel.com.ar ', 'GALENO'),
(1499, '6764', 'MAXOL SRL', '6764', 'MARIA', 'S', 'ibaio@maxol.com.ar, robaio@hotmail.com ', 'LOLA MORA 457 PISO 7 708 (TORRE HARBOUR)', 'CAPITAL FEDERAL', '46000051', 'INTERGEO', 'CONSTRUCCION', 'Enviar dato faltante !! ', 'QBE'),
(1500, '6768', 'VEGRAV S.R.L.', '6768', 'RODOLFO', 'S', 'info@confiteriaesmeralda.com.ar,esteladelvalletorres@hotmail.com', 'SARMIENTO 1051', 'LANUS ESTE', '4240-1545', 'ESTELA/RODOLFO', 'CONFITERIA', 'Enviar dato faltante !! ', 'INTERACCION - COLONIA SUIZA'),
(1501, '6773', 'IND. ACERMEC S.R.L.', '6773', 'ALEJANDRO', 'S', 'info@acermec.com.ar ', 'CUBA 761', 'AVELLANEDA OESTE', '4201-8020', 'ALEJANDRO', 'METALURGICA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1502, '6805', 'COLORANTES INDUST. ARG. S.A.', '6805', 'EDUARDO', 'S', 'eduardojmartinez@colorantesargentina.com.ar ', 'MANUELA PEDRAZA 2747', 'LANUS OESTE', '4368-0036', 'EDUARDO', 'IMP. PROD QUIMICOS', 'Enviar dato faltante !! ', 'GALENO'),
(1503, '6811', 'GARABEROS S.A.', '6811', 'LORENA', 'S', 'garaberos@gncplus.com,mazzucac@audes.com.ar,michelinij@gncplus.com', 'SANTA FE 303/13', 'LOMAS DE ZAMORA', '4283-0901', 'ROMINA', 'ESTACION DE SERVICIO', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(1504, '6835', 'FACP (DE ESPINOSA R. Y AURORA)', '6835', 'RICARDO', 'S', 'ric_espi@hotmail.com,ricardo@das.com.ar ', 'FLORIDA 477', 'VALENTIN ALSINA', '42084801', 'RICARDO/AURORA/FERNANDO', 'CALZADO', 'Enviar dato faltante !! ', 'GALENO'),
(1505, '6842', 'LABORATORIOS E.T.A.(MAGUITMAN)', '6842', 'JUAN', 'S', 'maguitmanjuan@hotmail.com ', 'ITAPIRU 2625', 'VILLA DIAMANTE', '4209-1175', 'DIEGO', 'QUIMICA', 'Enviar dato faltante !! ', 'LA CAJA'),
(1506, '6844', 'CECILIA VARELA', '6844', 'CECILIA', 'S', 'cj_varela3@hotmail.com,canatalicchio@yahoo.com.ar', 'HIPOLITO YRIGOYEN 4466', 'LANUS OESTE (DISPENSARIO LE)', '4240-8912//1549915361', 'CARLOS', 'COMERCIO', 'Enviar dato faltante !! ', 'QBE'),
(1507, '6849', 'CONS.29 DE SEPT.1960 (GALERIA)', '6849', 'MARISA', 'S', 'gabrielcosentino@infovia.com.ar ', 'ANATOLE FRANCE 1617', 'LANUS ESTE', '4249-1877', 'AREA PROTEGIDA', 'GALLETITERIA', 'Enviar dato faltante !! ', 'INTERACCION - COLONIA SUIZA'),
(1508, '6853', 'TRANSP.LOS HERMANOS (ANDRIOLO)', '6853', 'KARINA', 'S', 'loshermanos@secomp.com.ar ', 'EMILIO CASTRO 3385', 'LANUS OESTE', '4209-2594/42186207', 'KARINA', 'TRANSPORTE', 'Enviar dato faltante !! ', 'SMG'),
(1509, '6854', 'KIM CHUNG UN', '6854', 'ALICIA', 'S', 'danielberatti@yahoo.com.ar ', 'AV. MITRE 823', 'AVELLANEDA ESTE', '4222-3431', 'ALICIA', 'COMERCIO BAZAR', 'Enviar dato faltante !! ', 'NO POSEE ART'),
(1510, '6880', 'MANN + HUMMEL ARGENTINA S.A', '6880', 'ALEJANDRA', 'S', 'departamentomedico@mann-hummel.com,alejandra.turner@mann-hummel.com,martina.pinola@mann-hummel.com', 'SDOR QUINDIMIL 4495', 'VALENTIN ALSINA', '4001/7228/7212/7200', 'PABLO ASPREA', 'FABRICA DE FILTROS', 'diego.gaeta@mann-hummel.com,atencion.proveedores@mann-hummel.com', 'ASOCIART A.R.T.'),
(1511, '6881', 'TRANSPORTES CONTE', '6881', 'DARIO', 'S', 'dario.conte@omnitrans.com.ar,miguel.conte@omnitrans.com.ar,corina.conte@omnitrans.com.ar', 'VELEZ SARFIELD 1138', 'LANUS OESTE', '4241-4451', 'DARIO/MIGUEL', 'TRANSPORTES', 'jimena.pagani@omnitrans.com.ar ', 'GALENO'),
(1512, '6896', 'LAB.QUIMICO COSMET. RETTES SRL', '6896', 'EDUARDO', 'S', 'rettes@gmail.com ', 'NICOLAS AVELLANEDA 2355', 'AVELLANEDA ESTE', '4137-6746/ 6770', 'DIANA/RODRIGO', 'LABORATORIO', 'Enviar dato faltante !! ', 'QBE'),
(1513, '6923', 'RAFAEL GALLO S.H.', '6923', 'RAFAEL', 'S', 'NO MAIL ', 'PASO DE LA PATRIA 144', 'VALENTIN ALSINA', '4208-6696', 'ALBERTO GALLO', 'CURTIEMBRE', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(1514, '6937', 'MANOMETROS 1.4 S.A.', '6937', 'MANOMETROS', 'S', 'andresbresciani@gmail.com,info@protecpersons.com.ar', 'BOLA#OS 1739', 'LANUS ESTE', '4204-8133', 'ANDRES', 'METALURGICO', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(1515, '6940', 'MACONA S.A', '6940', 'LUCIANA', 'S', 'info@macona-sa.com.ar,maconasa@yahoo.com.ar ', 'VIAMONTE 1803', 'LANUS OESTE', '4262-0989/1113', 'LUCIANA/DANIELA', 'CONSTRUCCION', 'vivian@macona-sa.com.ar ', 'PROVINCIA'),
(1516, '6952', 'TRANSPORTE LUCIANI HNOS S.R.L.', '6952', 'PABLO', 'S', 'lucianitran@speedy.com.ar ', 'SITIO DE MONTEVIDEO 2544', 'LANUS ESTE', '4240-4929', 'PABLO/FERNANDO', 'TRANSPORTES', 'Enviar dato faltante !! ', 'LA CAJA'),
(1517, '6956', 'CARRO FABIANA ANDREA', '6956', 'NESTOR', 'S', 'fabiana_carro@hotmail.com ', 'AV. JAURECHE 2653', 'GUERNICA', '02224-477300', 'ALICIA', 'DISTRIBUIDORES', 'fabiana_carro@hotmail.com ', 'PREVENCION'),
(1518, '6964', 'POLIFAL S.R.L.', '6964', 'JOSE', 'S', 'polifal@yahoo.com.ar ', 'SALTA 650', 'SARANDI', '4204/8558/4203-8588', 'HUGO', 'PLASTICO', 'Enviar dato faltante !! ', 'MAPFRE'),
(1519, '7007', 'OSCAR PAGANI E HIJOS S.R.L.', '7007', 'MARIEL', 'S', 'mariel@lonasur.com.ar ', 'CNO BELGRANO KM 10.500', 'BERNAL', '4270-3700', 'MARIEL', 'Enviar dato faltante !!', 'gisela@lonasur.com.ar ', 'GALENO'),
(1520, '7029', 'RAUL ADOLFO DEMILTA', '7029', 'RAUL', 'S', 'demiltex@hotmail.com.ar,bety_415@hotmail.com ', 'EMILIO CASTRO 950', 'LANUS OESTE', '4241-9707', 'BETINA CAAMAÑO', 'METALURGICA', 'Enviar dato faltante !! ', 'GALENO'),
(1521, '7030', 'TANQUESUR S.A.', '7030', 'MARTIN', 'S', 'info@tanquesur.com.ar ', 'SANTIAGO DEL ESTERO 1349', 'LANUS OESTE', '4247-4521', 'LUCIA', 'METALURGICA', 'Enviar dato faltante !! ', 'GALENO'),
(1522, '7031', 'FLEXICUER SA', '7031', 'FERNANDO', 'S', 'flexicuer@yahoo.com.ar,consultas@flexicuer.com.ar', 'CHILE 2052', 'LANUS O', '4228-2770,4139-5170/71', 'FERNANDO PARODI', 'CURTIEMBRE', 'Enviar dato faltante !! ', 'LA SEGUNDA'),
(1523, '7043', 'ACEROS ALSINA SRL', '7043', 'SERGIO', 'S', 'ssanchez@acerosalsina.com.ar ', 'LINIERS 2758', 'VALENTIN ALSINA', '4209-5150 / 4878-5923', 'SERGIO/HERNAN', 'METALAURGICA', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(1524, '7049', 'RIO VARADERO S.A.', '7049', 'INTERACCIONCOLONIA', 'S', 'nicolas.morales@colonia-suiza.com ', 'MEXICO 2993 PB', 'CIUDAD AUTONOMA DE BS AS', '6009 2626', 'COL SUIZA SALUD', 'SALUD', 'Enviar dato faltante !! ', 'PROVINCIA COLONIA SUIZA'),
(1525, '7075', 'JONEMI SOUVENIR S.R.L.', '7075', 'JONATHAN', 'S', 'coronel_mauro@hotmail.com,admjonemisouvenir@hotmail.com', 'LLAVALLOL 2010', 'LAUS OESTE', '4262-9950/1554296931JONAT', 'LURDES-VALERIA', 'INDUMENTARIA', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(1526, '7101', 'VHN SRL', '7101', 'MONICA', 'S', 'administracionfm@fibercorp.com.ar,victor@fitnessmachine.com.ar', 'PASAJE CONGRESO 2948', 'VALENTIN ALSINA', '4241-6096/ 4240-8956', 'MONICA/JESICA', 'METALURGICO', 'comprasfm@speedy.com.ar ', 'QBE'),
(1527, '7105', 'COSME CARUSO', '7105', 'GABRIELA', 'S', 'cosand@speedy.com.ar,info@cosan.com.ar ', 'MENDOZA 1949', 'AVELLANEDA', '4218-3696', 'ANTONIO JURATO', 'METALURGICA', 'Enviar dato faltante !! ', 'MAPFRE'),
(1528, '7111', 'DANA MARCOS DANIEL', '7111', 'MARCOS', 'S', 'mdpolietileno@yahoo.com ', 'LACARRA 332', 'LANUS E', '4265-4071', 'DANIEL/IVA', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1529, '7120', 'J.R. TRANS S.R.L.', '7120', 'JUAN', 'S', 'operacionesadm@jrtrans.com.ar,comercial@jrtrans.com.ar,administracion@jrtrans.com.ar', 'URUGUAY 1136', 'AVELLANEDA', '5290-3706', 'JUAN CARLOS', 'TRANSPORTE', 'administracio@jrtrans.com.ar ', 'PREVENCION'),
(1530, '7130', 'FRANCISCO CORBELLI S.A.', '7130', 'MIRELLA', 'S', 'corbelli@ciudad.com.ar,info@corbellicocinas.com.ar', 'FRIAS 267 // COLECTORA RUTA Nº750 LAVALL', 'LOMAS DE ZAMORA', '4282-0882', 'ADRIANA/LUIS/MARTIN', 'METALURGICA', 'Enviar dato faltante !! ', 'QBE'),
(1531, '7141', 'LOGISTICA LOSADA S.A.', '7141', 'ELENA', 'S', 'logistical@ciudad.com.ar ', 'CHILE 1270', 'LANUS OESTE', '4301-8159/4301-6447', 'TRANSPORTE LOSADA MIRIAM', 'TRANSPORTE', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1532, '7145', 'BANDAS INDUSTRIALES S.A.', '7145', 'CECILIA', 'S', 'n.pennisi@bandasindustriales.com.ar,c.serravalle@bandasindustriales.com.ar,c.serravalle@bandasindustriales.com', 'AV GALICIA 1021', 'AVELLANEDA OESTE', '4218-2906/09', 'PATRICIA', 'TEXTIL', 'p.laudani@bandasindustriales.com.ar ', 'LA CAJA'),
(1533, '7151', 'LABORATORIO FRENCH S.A.', '7151', 'LABORATORIO FRENCH S', 'N', 'sergiosoria@centralab.com.ar ', 'FRENCH 2979', 'CIUDAD AUTONOMA DE BUENOS AIRE', '4805-2009/5199-4825', 'VALERIA TOLOTI', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'Enviar dato faltante !!'),
(1534, '7163', 'GRACIELA LEALE', '7163', 'LEANDRO', 'S', 'lemur2k01@yahoo.com.ar ', 'DE LA PEÑA 3469', 'LANUS ESTE', '4289-4032 /15-54138028', 'GRACIELA/LEANDRO', 'COSTURA.', 'Enviar dato faltante !! ', 'ART LIDERAR SA'),
(1535, '7165', 'ASPEN PRODUCCIONES SRL', '7165', 'BARBARA', 'S', 'aspenproduccionessrl@gmail.com ', 'PICHINCHA 2923', 'LANUS ESTE', '4246-6239/0273', 'JORGE', 'METALURGICA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1536, '7225', 'SUC. DE ROBERTO SEOANE', '7225', 'HORACIO', 'S', 'rectifren@rectifren.com.ar ', 'PARAGUAY 795', 'AVELLANEDA OESTE', '4208-6506/41168635/8655', 'HORACIO SEOANE', 'REPUESTO DEL AUTOMOTOR', 'Enviar dato faltante !! ', 'GALENO'),
(1537, '7238', 'PETRISI S.A.', '7238', 'CLAUDIA', 'S', 'claudiacrocenzi@hotmail.com,petrisi@live.com ', 'HIPOLITO YRIGOYEN 6670', 'REMEDIOS DE ESCALADA OESTE', '4242-5716', 'CLAUDIA', 'TEXTIL', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1538, '7261', 'ROSANA VANINA GUANCO', '7261', 'ROSANA', 'S', 'montaire2@hotmail.com ', 'CATAMARCA 1645', 'AVELLANEDA OESTE', '4218-6555 / 4208-1450', 'VANINA', 'INTALACION', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1539, '7267', 'GALAXIE S.C.', '7267', 'ELDA', 'S', 'eldacalvani37@hotmail.com,administracion@galaxie.com.ar,compras@galaxie.com.ar', 'GRAL VEDIA 215', 'SARANDI', '4204-7019 INT 105', 'ELDA 155120-2514', 'METALURGICA', 'Enviar dato faltante !! ', 'QBE'),
(1540, '7279', 'COLEGIO CRISTO REY', '7279', 'RAMONA', 'S', 'escuelacristorey@yahoo.com.ar ', 'LUJAN 1538', 'LANUS ESTE', '4240-3044', 'LILIANA SANCHEZ', 'EDUCATIVA', 'Enviar dato faltante !! ', 'SMG'),
(1541, '7315', 'TI SE SERVICIOS SRL', '7315', 'CONSTANZA', 'S', 'info@silvats.com,cpisotano@silvats.com,lpsitonano@silvats.com', 'LLAVALLOL 1680', 'LANUS OESTE', '4262-8611/8311', 'CONSTANZA PISOTANO DTO ADM', 'TRANSPORTES', 'plangbart@silvats.com ', 'ASOCIART A.R.T.'),
(1542, '7330', 'FUNDICION AMN S.R.L.', '7330', 'SONIA', 'S', 'correo@fundicion.com.ar,julia@fundicion.com.ar,amn@fundicion.com.ar', 'MELIAN 3375 ESQ.JOSE INGENIEROS', 'BURZACO', '4238-4013/15-57134336', 'SONIA', 'FUNDICION', 'Enviar dato faltante !! ', 'LA CAJA'),
(1543, '7404', 'ALVAREZ JORGE OMAR', '7404', 'JORGE', 'S', 'shioexpendedoras@speedy.com.ar,shioexpendedoras@hotmail.com', 'J.D. PERON 2416', 'VALENTIN ALSINA', '4218-0700', 'JORGE/JOSE LUIS/JOSE MARIA', 'HELADERIA', 'Enviar dato faltante !! ', 'QBE'),
(1544, '7408', 'FARINOLA E HIJOS S.A.', '7408', 'JORGE', 'S', 'oficompras@yahoo.com.ar ', 'GDOR IRIGOYEN 629', 'LANUS OESTE', '4241-7677', 'FARINOLA CARLOS', 'METALURGICA', 'Enviar dato faltante !! ', 'SMG'),
(1545, '7432', 'MSL S.A.', '7432', 'JAVIER', 'S', 'martin@majama.com.ar,federicor@majama.com.ar ', 'ALLENDE 2757', 'REMEDIOS DE ESCALADA ESTE', '4246-0225 (MAJAMA)', 'MAJAMA', 'TRANSPORTE', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(1546, '7449', 'CABLEMAQ S.R.L.', '7449', 'JAVIER', 'S', 'cablemaq@yahoo.com.ar ', '9 DE JULIO 3758', 'LANUS ESTE', '4230-5346', 'MARIO', 'METALAURGICA', 'Enviar dato faltante !! ', 'MAPFRE'),
(1547, '7476', 'SONIA OLIVEIRA', '7476', 'SONIA', 'S', 'juegosol@yahoo.com.ar ', 'HECTOR GUIDI 335/HEREDIA1681 AVELLANEDA', 'LANUS ESTE', '5291-4412/2009-4562', 'SONIA', 'METALURGICA', 'Enviar dato faltante !! ', 'PREVENCION'),
(1548, '7477', 'SCIOCCO S.R.L.', '7477', 'PABLO', 'S', 'sciocco@ciudad.com.ar,electrosciocco@gmail.com,electrodomesticos.sciocco@gmail.com', '9 DE JULIO 1199', 'LANUS ESTE', '4249-1422/4241-7056', 'EDUARDO/PABLO', 'ELECTRODOMESTICOS', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1549, '7496', 'ESTAMPADOS BRUNO SA', '7496', 'BRUNO', 'S', 'estampadosbruno@yahoo.com.ar ', 'UCRANIA 2059', 'VALENTIN ALSINA', '4208-7539/4115-1326', 'MARTA', 'METALURGICA', 'Enviar dato faltante !! ', 'PREVENCION'),
(1550, '7614', 'ARTES GRAFICAS DEL SUR SH', '7614', 'MARIELA', 'S', 'agdelsur@hotmail.com ', 'ALMIRANTE SOLIER 2450 PB', 'SARANDI', '4204-2986/4203-0810', 'MARIELA', 'GRAFICA', 'Enviar dato faltante !! ', 'GALENO'),
(1551, '7630', 'OS.FA.MA. S.A.', '7630', 'MARIANA', 'S', 'rodriguez_sa@speedy.com.ar ', 'AZCUENAGA 75 4TO PISO B', 'CAPITAL FEDERAL', '4207-8211/4378 5648-7132', 'MARIANA', 'TRANSPORTE', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1552, '7633', 'CONS.SITIO MONTEVIDEO 1040/46', '7633', 'ALEJANDRO', 'S', 'alejandro@agconsorcios.com.ar,info@agconsorcios.com.ar', 'SITIO MONTEVIDEO 1040', 'LANUS ESTE', '4-241-4402', 'LIC. ALEJANDRO LEMA', 'CONSORCIO', 'Enviar dato faltante !! ', 'INTERACCION - COLONIA SUIZA'),
(1553, '7645', 'KORAL SERVICIOS INTEGRALES SA', '7645', 'MARIANA', 'S', 'rodriguez_sa@speedy.com.ar ', 'PUERTO DE PALOS 831', 'VILLA DOMINICO', '4207-8211/4378 5648-7132', 'MARIANA', 'TRANSP CAMIONES', 'rodriguez_sa@speedy.com.ar ', 'PROVINCIA'),
(1554, '7647', 'CASANO GRAFICA', '7647', 'GUSTAVO', 'S', 'rrhh@casanografica.com,cobranzas@casanografica.com', 'MINISTRO BRIN 3932', 'R DE ESCALADA', '4249-5562', 'VANESA', 'GRAFICA', 'proveedores@casanografica.com ', 'ASOCIART A.R.T.'),
(1555, '7653', 'ESTACION INTEGRAL LAS AMERICAS SA', '7653', 'MARIA', 'S', 'marype99@hotmail.com,las_americas440@hotmail.com', 'PRESIDENTE PERON 2176', 'BANFIELD OESTE', '4285-6502', 'Sra.F. RUBINETTI/MARIA/NANCY', 'EST. DE SERV.', 'Enviar dato faltante !! ', 'GALENO'),
(1556, '7721', 'FARMACIA DEL SUR', '7721', 'LEONARDO', 'S', 'farmaciadelsur@fibertel.com.ar,comprasur@fibertel.com.ar', 'H IRIGOYEN 4401', 'LANUS O', '4241-2017/42491400', 'ALEJANDRO/VIRGINIA/OMAR/LEANDRO', 'FARMACIA', 'Enviar dato faltante !! ', 'MAPFRE'),
(1557, '7725', 'MAPFRE COLONIA SUIZA GCBA', '7725', 'MAPFRE', 'S', 'Enviar dato faltante !! ', 'JUNIN 321', 'CIUDAD AUTONOMA DE BUENOS AIRE', '5238-9884', 'MARIELA', 'ART', 'Enviar dato faltante !! ', 'MAPFRE'),
(1558, '7726', 'BLANCO GONZALEZ ESPIRITU SANTO', '7726', 'JOSE MANUEL', 'S', 'gerencia@hotelelcabildo.com ', 'LAVALLE 748', 'CAPITAL FEDERAL', '4322-6695', 'TITO', 'HOTEL', 'Enviar dato faltante !! ', 'QBE'),
(1559, '7741', 'AZULCYAN S.R.L.', '7741', 'JUAN', 'S', 'ventas@seurat.com.ar,compras@seurat.com.ar ', 'CHARLONE 950', 'AVELLANEDA OESTE', '4218-1269', 'FRANCA', 'FRANCA 1563839702', 'Enviar dato faltante !! ', 'SMG'),
(1560, '7756', 'COMARGE S.A.', '7756', 'OSCAR', 'S', 'eduvic@gmail.com ', 'JOSE LEON SUAREZ 2753', 'LANUS OESTE', '4276-1060/4790-8706 ANDRE', 'RAMONA', 'GERIATRICO', 'Enviar dato faltante !! ', 'LA CAJA'),
(1561, '7758', 'CONS. MINISTRO BRIN 2988', '7758', 'JORGE', 'S', 'ingjorgegomez@gmail.com ', 'MINISTRO BRIM 2988', 'LANUS OESTE', 'Enviar dato faltante !!', '15499930710', 'CONSORCIO', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(1562, '7789', 'PROYECTO SILOE SRL', '7789', 'LUCRECIA', 'S', 'informes@institutosiloe.com.ar,administracion@institutosiloe.com.ar', 'J.B. JUSTO 3244', 'LANUS ESTE', '4115-1056 INT 1', 'LUCRECIA DOLCE', 'ESCUELA ESPECIAL', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1563, '7798', 'AUTOMACION Y SERVICIOS S.R.L.', '7798', 'NATALIA', 'S', 'natalia.gutierrez@ayssrl.com.ar,nestor.iencinella@ayssrl.com.ar,veronica.iencinella@ayssrl.com.ar', 'VERDI 132', 'LOMAS DE ZAMORA OESTE', '4282-0575', 'NATALIA', 'HIDRAULICA', 'Enviar dato faltante !! ', 'MAPFRE'),
(1564, '7803', 'FUNDACION MENSAJES DEL ALMA', '7803', 'NORBERTO', 'S', 'administracion@mensajesdelalma.org.ar,secretaria@mensajesdelalma.org.ar', '29 DE SEPTIEMBRE 1562', 'LANUS ESTE', '4241-2677', 'NORBERTO', 'EDUCATIVA', 'Enviar dato faltante !! ', 'GALENO'),
(1565, '7812', 'CONSORCIO PROP. AZARA 410', '7812', 'MARISA', 'S', 'mrech_adm@yahoo.com.ar ', 'AZARA 410', 'LOMAS DE ZAMORA', '3528-4932', 'MARISA', 'CONSORCIO', 'Enviar dato faltante !! ', 'PREVENCION'),
(1566, '7859', 'DISEÑO Y METAL S.A.', '7859', 'GABRIEL', 'S', 'mrdesing@idemproductos.com.ar,julieta@idemproductos.com.ar,info@dymet.com.ar', '29 DE SEPTIEMBRE 3356', 'REMEDIOS DE ESCALADA ESTE', '4225-9187', 'MARCELO/GABRIEL', 'METALURGICA', 'Enviar dato faltante !! ', 'MAPFRE'),
(1567, '7867', 'R Y M', '7867', 'RYM', 'S', 'rym-banosycocinas@hotmail.com ', 'EVA PERON 3647 / 51', 'LANUS ESTE', '/4230-0362', 'LORENA', 'AMOBLAMIENTOS', 'Enviar dato faltante !! ', 'QBE'),
(1568, '7870', 'D ERRICO VITO', '7870', 'VITO', 'S', 'pastasdonvito@hotmail.com ', 'ALLENDE 2659', 'LANUS ESTE', '4246-0355', 'DIEGO', 'FABRICA DE PASTAS', 'Enviar dato faltante !! ', 'SMG'),
(1569, '7876', 'GOMERIA C Y M', '7876', 'CARLOS', 'S', 'neumaticoscym@fibertel.com.ar ', 'ONCATIVO 1836', 'LANUS ESTE', '4225-5552', 'CARLOS', 'GOMERIA', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(1570, '7905', 'MATAFUEGOS MEGASEG SRL', '7905', 'MEGASEG', 'S', 'emiliano@megaseg.com.ar ', 'MAXIMO PAZ 99', 'LANUS OESTE', '4249-2004', 'EMILIANO', 'MATAFUEGOS', 'Enviar dato faltante !! ', 'PREVENCION'),
(1571, '7910', 'JOSE BRUSCO', '7910', 'JOSE', 'S', 'jbrusco1930@yahoo.com.ar ', 'SANTIAGO DEL ESTERO 1237', 'LANUS OESTE', '4240-2969', 'MARIA', 'METALURGICA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1572, '7929', 'VIAPTI SRL', '7929', 'JESUS', 'S', 'frig_campos_del_sur@yahoo.com.ar ', 'SARMIENTO 1179', 'LANUS ESTE', '4241-2149', 'ROBERTO O LEONARDO', 'FRIGORIFICO', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1573, '7953', 'TRANSPORTES SANGINETI S.R.L.', '7953', 'GABRIELA', 'S', 'gabrielasangineti@hotmail.com,sangineti@ciudad.com.ar', 'FAMATINA 3305', 'CAP.FED', '4912-5590', 'GABRIELA/FERNANDO', 'TRANSPORTE', 'Enviar dato faltante !! ', 'MAPFRE'),
(1574, '7962', 'MIZZAU SA', '7962', 'CATALINA', 'S', 'mamut@mizzausa.com ', 'MARIO BRAVO 341', 'AVELLANEDA OESTE', '4208-1459', 'MIRTA', 'HERRERIA DE OBRA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1575, '7997', 'GARCIA ANGEL OSVALDO', '7997', 'ANGEL', 'S', 'sapiensfe@gmail.com,fedeacade77@hotmail.com ', 'YATAY 757', 'VALENTIN ALSINA', '4228-7700', 'HORACIO/MANUEL', 'METALURGICA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1576, '8010', 'PICCININNO ALBERTO Y CAMPETELLA ARIEL SH', '8010', 'RUBEN', 'S', 'pycing@speedy.com.ar,pycingenieria@yahoo.com.ar', 'VILLA DE LUJAN 830', 'LANUS ESTE', '4249-7580', 'RUBEN-NESTOR', 'METALURGICA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1577, '8011', 'GAREAM S.A.', '8011', 'RAUL', 'S', 'gaream@speedy.com.ar,sdinardo@speedy.com.ar ', 'FRAY JULIAN LAGOS 1340', 'LANUS OESTE', '4240-0288', 'RAUL ARRIETA/MARIA EUGENIA', 'DISTRIBUIDORA', 'Enviar dato faltante !! ', 'GALENO'),
(1578, '8020', 'ALVEZ GUSTAVO MARCELO y SUAREZ DANIEL OSVALDO SH', '8020', 'GUSTAVO', 'S', 'ventas@con-arte.com.ar,dsuarez@con-arte.com.ar', 'HEREDIA 1261', 'GERLI', '4878-4395/4204-0140', 'GUSTAVO-DANIEL', 'PLASTICO', 'Enviar dato faltante !! ', 'LA CAJA'),
(1579, '8027', 'ZENTO SA', '8027', 'PABLO', 'S', 'precalde@zento.com.ar,info@zento.com.ar,aferraro@zento.com.ar', 'HIPOLITO YRIGOYEN 3202', 'LANUS OESTE (DISPENSARIO LE)', '4225-9900/4357-9900', 'DEBORA, PABLO', 'CONCESIONARIA', 'mrolandi@zento.com.ar ', 'ART INTERACCION SA'),
(1580, '8056', 'SOLAR FITNESS S.R.L.', '8056', 'FITNESS', 'S', 'info@solardeleste.com ', 'ITUZAINGO 1279', 'LANUS E', '4241-9770', 'YAMILA', 'GIMNASIO', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1581, '8071', 'FACAPEL S.A.', '8071', 'ANALIA', 'S', 'info@facapel.com.ar,la_petisa70@hotmail.com,facapelsa@gmail.com', 'SALTO 135', 'AVELLANEDA ESTE', '4206-4505/4207-9390', 'ANALIA/HECTOR', 'CORRUGADORA', 'Enviar dato faltante !! ', 'LA SEGUNDA'),
(1582, '8086', 'COQUETT S.R.L.', '8086', 'EUGENIO', 'S', 'stella_m_blanco@hotmail.com ', 'PJE ESCRIBANO 45', 'CAPITAL', '4982-4517', 'EUGENIO', 'HOTEL', 'Enviar dato faltante !! ', 'GALENO'),
(1583, '8087', 'ALOJAMIENTO 24 DE NOVIEMBRE S.R.L.', '8087', 'EUGENIO', 'S', 'stella_m_blanco@hotmail.com ', '24 DE NOVIEMBRE 334', 'CAPITAL', '4931-5925', 'EUGENIO', 'HOTEL', 'Enviar dato faltante !! ', 'SMG'),
(1584, '8094', 'DI MITO HNOS S.R.L.', '8094', 'LAURA', 'S', 'dimitohnos@speedy.com.ar ', 'BOLONIA1390', 'BANFIELD OESTE', '4286-3898/3193', 'LAURA/MARIEL', 'COMPRA Y VENTA METALES NO FERR', 'Enviar dato faltante !! ', 'QBE'),
(1585, '8099', 'SALGUEIRO HNOS', '8099', 'RICARDO', 'S', 'censoda@yahoo.com.ar ', 'ZAPATA 704', 'BANFIELD OESTE', '4248-6200', 'RAUL', 'SODERIA', 'Enviar dato faltante !! ', 'GALENO'),
(1586, '8105', 'MICROPARQUE S.A.', '8105', 'CLAUDIA', 'S', 'microparquecorrugado@gmail.com,alejandra_nowacki@hotmail.com,microparque@speedy.com.ar', 'MELIAN 2070 PQUE.INDUST. ENT.2', 'BURZACO', '4238-3289/3491', 'OMAR JUAREZ', 'FCA. CORRUG.', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1587, '8113', 'L.P.D. S.A.', '8113', 'LUCIA', 'S', 'compras@lpdargentina.com.ar ', 'EMILIO CASTRO 1443', 'LANUS OESTE', '4262-4340', 'LUCIA-SEBASTIAN', 'METALURGICA', 'compras@lpdargentina.com.ar ', 'GALENO'),
(1588, '8115', 'SUSANA NOEMI PARRILLO', '8115', 'SUSANA', 'S', 'naflomar@yahoo.com.ar ', 'CENTENADIO URUGUAYO 979', 'LANUS ESTE', '156-7981793/ 156-5888149', 'MARCELO', 'ESTACION DE SERVICIO', 'Enviar dato faltante !! ', 'SMG'),
(1589, '8128', 'TOUZON FRANCISCO CARLOS', '8128', 'FRANCISCO', 'S', 'industriasfark@ciudad.com.ar,carlostouzon@yahoo.com.ar', 'GUARRACINO 3428', 'LANUS ESTE', '4202-9123 o 15-6395-5476', 'ALICIA-FERNANDO-ROMINA', 'METALURGICA', 'Enviar dato faltante !! ', 'LA CAJA'),
(1590, '8129', 'COMBUSTIBLES LA COLONIA S.A.', '8129', 'JUAN', 'S', 'combustibleslacolonia@speedy.com.ar ', 'RIVADAVIA 2410', 'LANUS OESTE', '4262-2159/15-5843-2024', 'KARINA', 'ESTACION DE SERVICIO', 'Enviar dato faltante !! ', 'QBE'),
(1591, '8145', 'EL EMPORIO DE LANUS SA', '8145', 'CLAUDIA', 'S', 'proveedoreselemporiodelanus@yahoo.com.ar ', 'SITIO DE MONTEVIDEO 2035', 'LANUS E', '4241-4867 4249-7908', 'DANIEL REDONDO', 'DISTRIBUIDORA', 'Enviar dato faltante !! ', 'LA CAJA'),
(1592, '8169', 'SERIN PLAST S.H.', '8169', 'LORENA', 'S', 'serinplastsh@gmail.com ', 'SAN PATRICIO 629 ESQ. EL MIRASOL', 'TEMPERLEY', '4264-1800', 'ANA/LORENA', 'TEXTIL', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1593, '8170', 'MARCELLO ERNESTO ANTONIO', '8170', 'ERNESTO', 'S', 'farmaciafranco@ciudad.com.ar,emarcello@intesinf.com,farmaciafraco@ciudad.com.ar', 'SARMIENTO 1847 PB D1', 'LANUS ESTE', '4-241-0363', 'ERNESTO', 'FARMACIA', 'Enviar dato faltante !! ', 'GALENO'),
(1594, '8185', 'CAFERMAR SRL', '8185', 'KARINA', 'S', 'cafermar@hotmail.com ', 'REMEDIOS DE ESCALADA 2251', 'V. ALSINA', '4208-4674', 'KARINA', 'ESTACION DE SERVICIO', 'Enviar dato faltante !! ', 'LA SEGUNDA'),
(1595, '8193', 'DONATO DE NICOLA E HIJOS SRL', '8193', 'LUCILA', 'S', 'denicola@ciudad.com.ar,contacto@donatodenicola.com.ar', 'PARAGUAY 2831', 'VALENTIN ALSINA', '4209-3233/4461', 'ROXANA', 'CURTIEMBRE', 'Enviar dato faltante !! ', 'SMG'),
(1596, '8204', 'THOMAS RACING S.R.L.', '8204', 'JORGE', 'S', 'info@thomasracing.com.ar,administracion@tomasracing.com.ar', 'AV. ALVAREZ THOMAS 1723', 'CAPITAL', '03484432043', 'JORGE', 'MENSAJERIA', 'Enviar dato faltante !! ', 'LA HOLANDO'),
(1597, '8211', 'SUELAS SP S.A.', '8211', 'SUELASSP', 'S', 'patrinerguizian@hotmail.com,personalsuelassp@gmail.com', 'ENRIQUE FERNANDEZ 452', 'LANUS O', '4208-8335/4209-7063', 'PATRICIA/ALEXIS/LEONEL', 'CALZADO', 'sp.pedidos@live.com ', 'PROVINCIA'),
(1598, '8249', 'EUROCALZADO S.A.', '8249', 'LAURA', 'S', 'italcalzado@hotmail.com,eurocalzado@hotmail.com', 'GRAL RODRIGUEZ 934', 'LANUS ESTE', '4241-1833', 'LAURA /SISTO', 'FABRI.CABLES', 'Enviar dato faltante !! ', 'QBE'),
(1599, '8257', 'CAÑOS LUZ S.A.', '8257', 'VANESA', 'S', 'mabel@canosluz.com.ar,vanesasardi@canosluz.com.ar', 'LAUREANO OLIVER 1659', 'LOMAS DE ZAMORA OESTE', '4298-0714/3828/2771', 'VANESA/', 'METALURGICA', 'Enviar dato faltante !! ', 'HORIZONTE CIA ARG DE SEGUROS GENERALES D'),
(1600, '8258', 'OVELAR FRANCISCO JAVIER', '8258', 'VANESA', 'S', 'freedomtomi@hotmail.com,basesalsina@hotmail.com', 'RUCCI 139', 'VALENTIN ALSINA', '4209-0165', 'LILIANA', 'CALZADO', 'Enviar dato faltante !! ', 'PREVENCION'),
(1601, '8271', 'CASA PINTO PACK S.R.L.', '8271', 'ALEJANDRO', 'S', 'casapinto@fibertel.com.ar,casapintopagos@fibertel.com.ar,j.a.pinto@hotmail.com', 'JUJUY 2178', 'LANUS OESTE', '4249-1719', 'JUAN', 'EMBALAJES', 'casapintopagos@fibertel.com.ar ', 'PREVENCION'),
(1602, '8317', 'FERROCORT S.R.L.', '8317', 'EMILIANO', 'S', 'emiliano@ferrocort.com,ricardo@ferrocort.com,laura@ferrocort.com', 'NICASIO OROÑO 2243', 'CAPITAL FEDERAL', '4581-1717', 'EMILIANO', 'VENTA DE CHAPAS', 'Enviar dato faltante !! ', 'LA HOLANDO'),
(1603, '8327', 'METALURGICA DIMASI S.R.L.', '8327', 'JOSE', 'S', 'metalurgica_dimasi@yahoo.com.ar ', 'BURELA 4224', 'LANUS ESTE', '4-220-9755', 'JOSE/LEANDRO', 'METALURGICA', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(1604, '8340', 'FARMACIA MARIA LAURA CURRO SCS', '8340', 'WALTER', 'S', 'dpernico@puntofarma.com,hesteban@puntofarma.com,personal@puntofarma.com', 'AV. SAN MARTIN 4151', 'VILLA CARAZA', '4240-4340/42', 'PUNTOFARMA', 'FARMACIA', 'Enviar dato faltante !! ', 'QBE'),
(1605, '8342', 'MARRA ROSARIO ANTONIO Y BELFIORE JOSE S.H.', '8342', 'LILIANA', 'S', 'distribuidoramarbel@speedy.com.ar ', 'TUCUMAN 4710', 'LANUS ESTE', '4289-2220 INT. 31', 'LILIANA/DIEGO/JOSE LUIS', 'DISTRIBUIDORA', 'distribuidoramarbel@speedy.com.ar ', 'LA CAJA'),
(1606, '8347', 'METALURGICA EPSILON S.R.L.', '8347', 'ADRIAN', 'S', 'metalurgicaepsilon@hotmail.com ', 'AYACUCHO 1421', 'LANUS ESTE', '4241-6147', 'ADRIAN/PABLO', 'METALURGICA', 'Enviar dato faltante !! ', 'PREVENCION'),
(1607, '8349', 'RECUSUR SRL', '8349', 'MARIANO', 'S', 'recusursrl@gmail.com ', 'CON BELGRANO KM 10,500 GALPON 21B PQUE I', 'QUILMES OESTE', '4270-4117', 'CARLOS CORRADI', 'PLASTICO', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(1608, '8359', 'INTERGEO VIAL S.R.L.', '8359', 'JORGE', 'S', 'js@intergeovial.com ', 'FRAY MAMERTO ESQUIU 1572', 'LANUS ESTE', '4240-1698 /15-5240-6449', 'JORGE', 'CONSTRUCCCIONES', 'Enviar dato faltante !! ', 'SMG'),
(1609, '8385', 'MIELNIK HERNAN EMILIO', '8385', 'HERNAN', 'S', 'jr_hernan@yahoo.com.ar ', 'MARCO AVELLANEDA 4225', 'LANUS OESTE', '4262-9932', 'HERNAN', 'TORNERIA', 'Enviar dato faltante !! ', 'PREVENCION'),
(1610, '8407', 'MYRNA FERNANDEZ DIAZ Y SELVA LUCIA', '8407', 'MYRNA', 'S', 'velitasnuchy@hotmail.com ', 'CHASCOMUS 2251', 'LANUS ESTE', '4246-0713', 'MYRNA-PATRICIA', 'FABRICA DE VELITAS', 'Enviar dato faltante !! ', 'LA CAJA'),
(1611, '8408', 'VAGROD Y CIA. SRL', '8408', 'GUSTAVO', 'S', 'gerenciavagrod@speedy.com.ar ', 'JUANA AZURDUY 3327', 'REMEDIOS DE ESCALADA ESTE', '4220-6187', 'GUSTAVO///OMAR PLASTINE', 'DISTRIBUIDORA', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(1612, '8411', 'RANCO CHEMICALS S.A', '8411', 'RAUL', 'S', 'rancochem@telviso.com.ar ', 'LAS LILAS 970 PANAMERICANA KM 42', 'DEL VISO', '02320-406489/409411', 'CARLA FLORES/ FEDERICO STEINMETZ', 'FAB DE PROD QUIMICOS', 'Enviar dato faltante !! ', 'SMG'),
(1613, '8413', 'TODO CAÑO (VESPERO LUCIANO GABRIEL)', '8413', 'CELESTE', 'S', 'sol.vespero@hotmail.com ', 'EJERCITO DE LOS ANDES 1122', 'BANFIELD OESTE', '4267-3304/4286-9728/11652', 'AMALIA-LUCIANO', 'METALURGICA', 'Enviar dato faltante !! ', 'PREVENCION'),
(1614, '8431', 'INSTITUTO SAGRADO CORAZON', '8431', 'INSTITUTO', 'S', 'institutosagradocorazon@gmail.com ', 'HUMAHUACA 2456', 'LANUS OESTE', '4262-4321', 'ELENA TEREK-OLGA VOLLOSHIN', 'EDUCATIVA', 'Enviar dato faltante !! ', 'SMG'),
(1615, '8442', 'PACHKO GUSTAVO ALFREDO', '8442', 'VALERIA', 'S', 'vale-cio@hotmail.com,hor.constructorasrl@yahoo.com.ar', 'VERGARA 3671', 'LANUS OESTE', '4878-0788', 'VALERIA/ 1551829491', 'constructora', 'Enviar dato faltante !! ', 'GALENO'),
(1616, '8445', 'FRANDON S.A.', '8445', 'JOSE', 'S', 'famedsa@speedy.com.ar,info@frandon.com.ar ', 'ROCA 749/50', 'REMEDIOS DE ESCALADA OESTE', '4248-9196/4202-8050/4202-', 'GRACIELA', 'PLASTICO', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1617, '8462', 'INTERSERVICE SERVICIOS EMPRESARIOS SA (L1)', '8462', 'LORENA', 'S', 'csierra@bmontajes.com.ar,lvillar@bmontajes.com.ar,lvillar@bmontajes.com.ar', 'AV RIVADAVIA 954 1 PISO', 'CAPITAL FEDERAL', '4331-4355 (BLANCO MONTAJE', 'BLANCO MONTAJE LORENA INT 126', 'CONSTRUCCION', 'Enviar dato faltante !! ', 'PREVENCION'),
(1618, '8470', 'PROVINCIA COLONIA SUIZA', '8470', 'PROVINCIA', 'S', 'no ', 'JUNIN 321', 'CIUDAD AUTONOMA DE BUENOS AIRE', '0-800-444-5000', 'MARTIN GALEANO', 'A.R.T.', 'Enviar dato faltante !! ', 'SMG'),
(1619, '8499', 'INGEMETAL SRL', '8499', 'EDUARDO', 'S', 'ingemetal.rimauro@gmail.com,ingemetal.adm@gmail.com,contadora.lorenat@hotmail.com', 'CONDARCO 1466', 'LANUS ESTE', '4246-5956///4246-7175', 'ANA / EDUARDO / VIVIANA', 'METALURGICO', 'Enviar dato faltante !! ', 'LA CAJA'),
(1620, '8535', 'H&C TRADING SRL', '8535', 'ANALIA', 'S', 'cecilia.ferrari@fibertel.com.ar,hferrari@hyctrading.com.ar,hyctrading@hyctrading.com.ar', 'BOQUERON 2288', 'VALENTIN ALSINA', '42184144', 'ANALIA', 'IND. QUIMICA', 'julietaserral@hyctrading.com.ar ', 'GALENO'),
(1621, '8546', 'NEUMATICOS TAURO (TAURO ADRIAN JOSE)', '8546', 'ADRIAN', 'S', 'neumaticostauro@ciudad.com.ar ', 'SAN MARTIN 3224', 'LANUS OESTE', '4241-0158', 'ADRIAN', 'NEUMATICOS', 'Enviar dato faltante !! ', 'QBE'),
(1622, '8554', 'TALLER DE RECTIFICACION R.E. SRL', '8554', 'CECILIA', 'S', 'rectificadoraelta@ciudad.com.ar,tallerderectificacionresrl@ciudad.com.ar,rectificadoraelta@hotmail.com', 'ARISTOBULO DEL VALLE 1064', 'LANUS OESTE', '4247-4929/4241-5309', 'CECILIA', 'RECTIFICA.MOTOR', 'Enviar dato faltante !! ', 'GALENO'),
(1623, '8567', 'MADERERA IVAN (JUAN MIELNIK MARIÑOJA)', '8567', 'JUAN', 'S', 'NO MAIL 2 ', 'OLAZABAL 4347', 'LANUS OESTE', '15-4557-3174', 'JUAN', 'MADERERA', 'Enviar dato faltante !! ', 'QBE'),
(1624, '8578', 'LA MARPLATENSE( DE PAREDES HORACIO REYNALDO)', '8578', 'HORACIO', 'S', 'lamarplatense@speedy.com.ar ', 'ISLANDIA 4279', 'LANUS OESTE', '4286-4434', 'HORACIO PAREDES', 'TRANSPORTES', 'lamarplatense@speedy.com.ar ', 'PREVENCION'),
(1625, '8591', 'TEXSORVIL SRL', '8591', 'JUAN AMERICO', 'S', 'texsorvillsrl@gmail.com ', 'SENADOR QUINDIMIL 2846', 'V. ALSINA', '4368-6954', 'JUAN AMERICO/ SERGIO', 'TEXTIL', 'Enviar dato faltante !! ', 'LA CAJA'),
(1626, '8616', 'ESTILO Y CONFORT (DEFINO MARIA LAURA)', '8616', 'GLADYS', 'S', 'NO MAIL 2 ', 'COLOMBRES 229', 'LOMAS DE ZAMORA', '4-245-8612', 'ANTONELA', 'PELUQUERIA', 'Enviar dato faltante !! ', 'LA CAJA'),
(1627, '8617', 'REYNALDO OMAR MAZETTELLE', '8617', 'REYNALDO', 'S', 'contaduria@fapa.com.ar ', 'CARLOS PELLEGRINI 337', 'MONTE GRANDE', '4238-6093/ 116-5169285', 'REYNALDO', 'TRANSPORTE', 'Enviar dato faltante !! ', 'PREVENCION'),
(1628, '8622', 'MOROS SRL', '8622', 'RAFAEL', 'S', 'rafa@moros.com.ar ', 'BOLIVIA 429', 'LANUS OESTE', '4241-8200/7439', 'FABIO/RAFAEL/RUBEN', 'METALURGICA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1629, '8646', 'A H A S BRIT AJIM', '8646', 'MABEL', 'S', 'gestiones@britajim.org,administracioncc@britajim.org,mavijad61@hotmail.com', 'TUCUMAN 1264', 'LANUS ESTE', '4241-0151', 'MABEL', 'SOCIEDAD DEPORTIVA', 'Enviar dato faltante !! ', 'PREVENCION'),
(1630, '8668', 'ALEMARK SA', '8668', 'KARINA', 'S', 'alek@alemark.com.ar,karina@alemark.com.ar ', 'CNEL SAYOS 4230', 'V ALSINA', '4228-7007/4878-4878', 'ALEJANDRO-DANIEL', 'FABRICA DE CALZADOS', 'alek@alemark.com.ar,karina@alemark.com.ar ', 'SMG'),
(1631, '8688', 'BEDEME S.A.', '8688', 'SAMANTA', 'S', 'info@pyr-exclusivo.com.ar ', 'GAMARRA 55', 'LUIS GUILLON', '4367-2070 4284-1731', 'GISELA', 'PANIFICADORA', 'Enviar dato faltante !! ', 'LA CAJA'),
(1632, '8740', 'EUROCRON S.R.L.', '8740', 'MARIA', 'S', 'info@eurocron.com.ar,compras@eurocron.com.ar ', 'SAENZ 1549', 'LOMAS DE ZAMORA OESTE', '4282-4790', 'PAOLA/MARIA LAURA', 'METALURGICA', 'compras@eurocron.com.ar ', 'FEDERACION PATRONAL'),
(1633, '8774', 'CARBALLO MARIA ELISA', '8774', 'MARIA', 'S', 'administracion@cementatemp.com.ar ', 'SAN ANTONIO 1059', 'CAPITAL', '4301-8833 / 4303-1041', 'GRACIELA/ELBA/MARCELA', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'QBE'),
(1634, '8785', 'SUPERMERCADOS ALSINA S.A', '8785', 'NORMA', 'S', 'administracion@supermercadosalsina.com.ar ', 'J.D.PERON 3673', 'VALENTIN ALSINA', '4116-2579/80 1568584989 N', 'VUONO NORMA', 'SUPERMERCADO', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1635, '8810', 'PREVENCION COLONIA SUIZA', '8810', 'PREVENCIONART', 'S', 'no ', 'JUNIN 321', 'CIUDAD AUTONOMA DE BUENOS AIRE', '0-800-444-5000', 'MARTIN GALEANO', 'A.R.T.', 'Enviar dato faltante !! ', 'Enviar dato faltante !!'),
(1636, '8811', 'HOGAR DE ANCIANOS SAN JORGE', '8811', 'CARLOS', 'S', 'nuevohogar@speedy.com.ar,cacortes@fibertel.com.ar', 'VIAMONTE 452', 'LOMAS DE ZAMORA ESTE', '4244-1557', 'CARLOS-BETTY', 'GERIATRICO', 'Enviar dato faltante !! ', 'LA CAJA'),
(1637, '8819', 'CERRORE ANTONIA', '8819', 'MARIEL', 'S', 'inyectoraferre@gmail.com ', 'FERRE 1167', 'LANUS ESTE', '4240-9784', 'LEANDRO', 'PLASTICO', 'Enviar dato faltante !! ', 'GALENO'),
(1638, '8848', 'EGEO S.R.L.', '8848', 'SUSANA', 'S', 'lab@egeodental.com.ar ', 'WARNES 1050', 'TEMPERLEY', '4244-0471/4292-5304', 'SUSANA', 'METALURGICA/LABORATORIO DENTAL', 'gabriela@egeodental.com.ar ', 'GALENO'),
(1639, '8913', 'MADEIMPRES S.A.', '8913', 'MARTIN', 'S', 'martin.madeimpres@gmail.com,mary.madeimpres@gmail.com,administracion.madeimpres@gmail.com', 'PRUDAN 447', 'SARANDI', '4205-1451', 'MARTIN-MARIO', 'FCA. MUEBLES', 'administracion.madeimpres@gmail.com ', 'PREVENCION'),
(1640, '8935', 'GANPEL SDH', '8935', 'MIGUEL', 'S', 'ale_gangi@hotmail.com,gampel2@speedy.com.ar,admin_ganpel@fullzero.com.ar', 'FARREL 164', 'VALENTIN ALSINA', '4228-1560/4228-4913', 'MIGUEL-ALEJANDRO', 'PAPELERA', 'Enviar dato faltante !! ', 'PREVENCION'),
(1641, '8949', 'COMPAÑÍA LA FLORENSE S.A.', '8949', 'MAURICIO', 'S', 'matrisur@ciudad.com.ar,mani_83989@hotmail.com ', 'RONDEAU 2158', 'LANUS ESTE', '4246-7888', 'MAURICIO', 'FCA. BOTONES', 'Enviar dato faltante !! ', 'LA SEGUNDA'),
(1642, '8971', 'GENESIS DE DI LEO Y CASTRO S.R.L.', '8971', 'JULIAN', 'S', 'genesis@opcionestelmex.com.ar ', 'PICHINCHA 2697', 'LANUS ESTE', '4246-2789', 'WALTER CASTRO 1566027917', 'PLASTICOS', 'Enviar dato faltante !! ', 'QBE'),
(1643, '8990', 'LE CUER S.R.L.', '8990', 'SERGIO', 'S', 'lecuersrl@yahoo.com.ar ', 'JOSE M. MORENO 2841', 'LANUS OESTE', '4484-7555/9648', 'LAURA/ANGEL', 'CURTIEMBRE', 'Enviar dato faltante !! ', 'PREVENCION'),
(1644, '8994', 'CAREVA (BASILE Y BASILE S.H)', '8994', 'CAROLINA', 'S', 'carolina@careva.com.ar,perrovestido@hotmail.com,careva@careva.com.ar', 'MATIENZO 120', 'ADROGUE', '4284-1571/0717', 'DANIEL-CAROLINA', 'FABRICA DE CALZADO', 'carolina@careva.com.ar ', 'PROVINCIA'),
(1645, '9000', 'MASSA FELIX', '9000', 'ANNERIS', 'S', 'maria@surmetal.com,aberturas@surmetal.com ', 'PLUMERILLO 675', 'BANFIELD O', '4286-6033', 'MASSA FELIX', 'METALURGICA', 'Enviar dato faltante !! ', 'SMG'),
(1646, '9005', 'NAPOLI HNOS S.R.L', '9005', 'SUSANA', 'S', 'napolih@gmail.com,sag_napoli@fibertel.com.ar ', 'ALVAREZ TOMAS 389', 'LOMAS DE ZAMORA', '/245-3989', 'ERNESTO-LUIS', 'HERRERIA DE OBRA', 'Enviar dato faltante !! ', 'SMG'),
(1647, '9038', 'CARTONERIA SAN JOSE S.A.', '9038', 'HUGO', 'S', 'hugo@cartoneriasanjose.com.ar,info@cartoneriasanjose.com.ar', 'BOUCHARD 3066', 'LANUS ESTE', '4220-2222', 'HUGO/LUIS/ROSITA', 'CARTONERIA', 'alejandra@cartoneriasanjose.com.ar,recepcion@cartoneriasanjose.com.ar', 'PREVENCION'),
(1648, '9043', 'TORNILLOS SUIPACHA SRL', '9043', 'MARTIN', 'S', 'tornillosuipacha@telecentro.com.ar ', 'CHASCOMUS 1639', 'VILLA DOMINICO', '4246-9916/5253', 'MARTIN/JUAN', 'METALURGICA', 'Enviar dato faltante !! ', 'PREVENCION'),
(1649, '9046', 'ARROSPIDE FERNANDEZ CARLOS FRANCISCO', '9046', 'CARLOS', 'S', 'NO MAIL ', 'MEDEIROS 3021', 'LANUS OESTE', '4249-2505', 'CARLOS FRANCISCO', 'METALURGICA', 'Enviar dato faltante !! ', 'PREVENCION'),
(1650, '9049', 'ESTABLECIMIENTOS FINESCHI S.A.C.I.F.', '9049', 'FABIAN', 'S', 'fineschi@sinectis.com.ar ', 'CAMACUA 4820', 'VILLA DOMINICO', '4246-7856/7856', 'FABIAN', 'METALURGICA', 'Enviar dato faltante !! ', 'PREVENCION'),
(1651, '9068', 'TUTANKA S.A.', '9068', 'ERICA', 'S', 'admin@tutankasa.com.ar,mery@tutankasa.com.ar ', 'GUEMES 1453', 'AVELLANEDA ESTE', '4203-9143', '-MARISEL', 'TRANSPORTE', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(1652, '9088', 'INGENIERO DANIEL MACHAIN Y ASOC. S.A.', '9088', 'GRACIELA', 'S', 'graciela@machain.com.ar,monica@machain.com.ar ', 'PERU 726', 'CAPITAL FEDERAL', '4300-2484', 'GRACIELA', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'QBE'),
(1653, '9096', 'RICARDO ZDROK', '9096', 'RICARDO', 'S', 'metalurgicazd@gmail.com ', 'DEHEZA 3738', 'LANUS ESTE', '4242-8323', 'RICARDO', 'METALAURGICA', 'Enviar dato faltante !! ', 'SMG'),
(1654, '9097', 'FARMACIA UOM ALSINA SCS', '9097', 'UOM', 'S', 'pgonta@uomlanus.com.ar,acomandatore@uomlanus.com.ar,jgoldberg@uomlanus.com.ar,tsciscivo@uomlanus.com.ar', 'PTE PERON 2693', 'V.ALSINA', '4208-3384 4115-6841/684', 'JORGE-MARCELO3', 'FARMACIA', 'administracion@uomlanus.com.ar,jmontes@uomlanus.com.ar', 'QBE'),
(1655, '9129', 'Jose R. Winter y Sergio A. Winter SH', '9129', 'JOSE', 'S', 'guanteswinter@hotmail.com ', 'CRISOLOGO LARRALDE 6626', 'WILDE', '4-206-4924', 'CECILIA', 'FABRICA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1656, '9130', 'TRANSPORTES DAUJA SRL', '9130', 'ALAN', 'S', 'administracion@dauja.com.ar ', 'HIPOLITO YRIGOYEN 4368 3° D', 'LANUS OESTE', '4241-8323', 'ANABELLA', 'TRANSPORTE', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(1657, '9162', 'EL CINCUENTA S.A.', '9162', 'AUGUSTO', 'S', 'amasa@live.com.ar ', 'REMEDIOS DE ESCALADA 1070', 'LANUS O', '4139-0617/0618', 'AUGUSTO', 'DISTRIBUIDORA', 'Enviar dato faltante !! ', 'LA CAJA'),
(1658, '9180', 'PRINTING BOOKS SA', '9180', 'ADRIANA', 'S', 'printing_books@ciudad.com.ar ', 'MARIO BRAVO 835', 'AVELLANEDA', '4208-3395', 'ROSA DILUYO/PACUAL/FERNANDO', 'IMPRENTA', 'Enviar dato faltante !! ', 'LA CAJA'),
(1659, '9193', 'E.P.B. NRO 42', '9193', 'ESTELA', 'S', 'ep42lanus@live.com.ar ', 'ROMA 2757', 'R. ESCALADA', '4202-0083', 'ESTELA', 'EDUCATIVA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1660, '9254', 'TRANSPORTE MARFER S.A.', '9254', 'LORENA', 'S', 'josenuniez@hotmail.com ', 'ESMERALDA 561', 'CAPITAL', '4208-6943', 'GABRIELA', 'TRANSPORTE', 'josenuniez@hotmail.com ', 'ASOCIART A.R.T.'),
(1661, '9258', 'CORPORACION DE FARINACEOS S.A.', '9258', 'CARLOS', 'S', 'antonio@mendia.com.ar,federico@mendia.com.ar,diegodigiano@mendia.com.ar', 'MAIPU 744', 'CAITAL FEDERAL', '4241-0161/8984', 'CARLOS/ ANTONIO', 'Enviar dato faltante !!', 'florencia@mendia.com.ar,emanuel@mendia.com.ar ', 'QBE'),
(1662, '9276', 'TEMPERED GLASS S.R.L.', '9276', 'NOELIA', 'S', 'temperedglass@hotmail.com ', 'LAVALLOL 1827', 'LANUS OESTE', '4262-1828', 'NOELIA/HECTOR', 'VIDRIOS', 'proveedores@temperedglasssrl.com ', 'PROVINCIA'),
(1663, '9286', 'CAPAS DECORATIVAS PROTECTORAS (ALFIO RUBEN DEL PUP', '9286', 'GUSTAVO', 'S', 'pinturascdp@live.com.ar ', 'CALLE 193 BIS NRO 1050', 'BERNAL', '4270-9012', 'CYNTIA', 'PINTURAS', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1664, '9300', 'BARCHETTA ALBERTO E. Y BARCHETTA SERGIO A. SH', '9300', 'ALBERTO', 'S', 'albertobarchetta@barchettaehijo.com.ar ', 'ESTRADA 2280', 'REMEDIOS DE ESCALADA OESTE', '248-2564', 'SR. ALBERTO/CRISTINA', 'METALURGICA', 'Enviar dato faltante !! ', 'GALENO'),
(1665, '9309', 'INGELSYS SRL', '9309', 'INGELSYS SRL', 'S', 'jamigliorisi@gmail.com ', 'RANGUNI 3763', 'LANUS OESTE', '4241-1353', 'RUBEN FONTE', 'ELECTRICIDAD', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(1666, '9326', 'PASTAS LA PAZ (GUSTAVO ZAPATA)', '9326', 'GUSTAVO', 'S', 'administracion@pastaslapaz.com.ar,pastaslapaz@hotmail.com.ar', 'LUJAN 2900', 'REMEDIOS DE ESCALADA', '4230-5462', 'JORGE', 'FABRICA DE PASTAS', 'Enviar dato faltante !! ', 'LA CAJA'),
(1667, '9353', 'ALVARENGA LUCIO HIPOLITO', '9353', 'LUCIO', 'S', 'polysac@hotmail.com ', '1RO DE MAYO 2854', 'REMEDIOS DE ESCALADA OESTE', '4267-6260', 'CLAUDIA - LUCIO', 'METALURGICA', 'Enviar dato faltante !! ', 'SMG'),
(1668, '9361', 'AUSTEK SRL', '9361', 'CRISTINA', 'S', 'gustavo@drillco.com.ar,cristina@drillco.com.ar', 'SAN LORENZO 2987', 'REMEDIOS DE ESCALADA', '4246-7629', 'JOSE', 'METALURGICA', 'Enviar dato faltante !! ', 'ASOCIART A.R.T.'),
(1669, '9370', 'AGLOLAM S.A.', '9370', 'MABEL', 'S', 'jezitro@yahoo.com.ar,sucursalalsina@aglolam.com.ar,mcasanova@aglolam.com.ar', 'REMEDIOS DE ESCALADA 4201', 'VALENTIN ALSINA', '4228-6602 CASA CENTRAL 42', 'MABEL/ JORGE ORTIZ', 'DISTRIBUIDORA', 'Enviar dato faltante !! ', 'SMG'),
(1670, '9416', 'G.S.A. ENSAYOS S.A.', '9416', 'SILVIA', 'S', 'administracion@gsaensayos.com.ar,gsaensayos@sion.com', 'SANTA MAGDALENA 696', 'CAPITAL FEDERAL', '4303-2466', 'ALEJANDRINA', 'SERVICIOS', 'Enviar dato faltante !! ', 'SMG'),
(1671, '9438', 'PATETTA HORACIO PALMA JOSE Y OTROS SH', '9438', 'MARIANO', 'S', 'ileana_patetta@yahoo.com.ar,horaciopatetta@yahoo.com.ar', 'SARGENTO CABRAL 1365', 'LANUS ESTE', '4203-1059/4115-8601/02', 'DANIEL O JOSE PALMA', 'GRAFICA', 'Enviar dato faltante !! ', 'GALENO'),
(1672, '9442', 'FCHIC 2041 S.R.L.', '9442', 'MARIANELA', 'S', 'fcf@navigo.com.ar,marianelaleone@hotmail.com ', 'CABILDO 2041', 'CAPITAL', '4783-9111', 'FABIO', 'COMERCIO', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(1673, '9457', 'DIOX SRL', '9457', 'FERNANDO', 'S', 'fernando.bobeda@diox.com.ar,info@diox.com.ar,fernandobobeda@yahoo.com', 'SALTA 1166', 'SARANDI', '4204-9680', 'FERNANDO', 'PLASTICOS', 'nancy.karaman@diox.com.ar ', 'LA HOLANDO'),
(1674, '9458', 'CODEFI INDUSTRIAL S.A.', '9458', 'NOELIA', 'S', 'codefi@speedy.com.ar ', 'ALVEAR 2944', 'LANUS ESTE', '4220-9902', 'GUSTAVO/HECTOR', 'METALURGICA', 'codefi@speedy.com.ar ', 'FEDERACION PATRONAL'),
(1675, '9475', 'EP NRO 27', '9475', 'ZULEMA', 'S', 'zulemaoppizzi@hotmail.com ', 'FERRE 2180', 'LANUS ESTE', '4247-4491/15-5514-5255', 'NURIA', 'EDUCATIVA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1676, '9483', 'IMAGMED S.A.', '9483', 'ROSANA', 'S', 'personal.imagmed@gmail.com ', 'HIPOLITO IRIGOYEN 4835', 'LANUS OESTE', '4229-6052,4229-6000 I 352', 'ROSANA-GUILLE 156-3773940 PAGOS NOE I 23', 'CLINICA', 'tesoreria@cmodelo.com.ar ', 'ART INTERACCION SA'),
(1677, '9485', 'SUCESION SINISCALCHI DOMINGO', '9485', 'DANIEL', 'S', 'aluminioguerrero@yahoo.com.ar ', 'JEAN JAURES 1962', 'V ALSINA', '4208-3509', 'DANIEL-NORA', 'METALURGICA', 'Enviar dato faltante !! ', 'PREVENCION'),
(1678, '9486', 'ROANTA SRL', '9486', 'FABIO', 'S', 'roantasrl@hotmail.com ', 'AV. H. IRIGOYEN 7699', 'BANFIELD', '4288-0060/0080', 'DIEGO/FAVIO', 'GOMERIA', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(1679, '9488', 'METALURGICA GAMA (JOAO JAVIER GARCIA DE JESUS)', '9488', 'GAMA', 'S', 'metalurgicagama@hotmail.com ', 'CHASCOMUS 1644', 'VILLA DOMINICO', '4246-3288', 'JAVIER/JOSE', 'METALURGICA', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1680, '9497', 'METALES ALUMINE S.A.', '9497', 'ALUMINE', 'S', 'mdotta@sion.com ', 'E.CASTRO 1149', 'LANUS OESTE', '4247-1587/4241-1298/4249-', 'Enviar dato faltante !!', 'METALURGICA', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(1681, '9542', 'IPCONEX S.A.', '9542', 'MARISOL', 'S', 'acauzzo@activocc.com,mferreras@activocc.com,isciacca@activocc.com', 'FLORIDA 141 3 P', 'CAPITAL FEDERAL', '5352-3900', 'MARISOL', 'CALL CENTER', 'Enviar dato faltante !! ', 'GALENO'),
(1682, '9545', 'SL SER GREEN S.A.', '9545', 'CLAUDIA', 'S', 'slsergreen@opcionestelmex.com.ar ', 'EMILIO CASTRO 1931', 'LANUS OESTE', '5291-1592/3', 'CLAUDIA', 'TRANSPORTE', 'Enviar dato faltante !! ', 'FEDERACION PATRONAL'),
(1683, '9557', 'INDUSTRIAS HULS SA', '9557', 'IRMA', 'S', 'info@industriashuls.com.ar ', 'WARNES 2877', 'LANUS OESTE', '4209-5214/3 /4208-0348', 'IRMA-PAULA', 'CARPINTERIA', 'Enviar dato faltante !! ', 'PROVINCIA');
INSERT INTO `Usuarios` (`id`, `Codigopal`, `Nombrepal`, `Clave`, `Usuario`, `Activo`, `Correo`, `Domicilio`, `Localidad`, `Telefono`, `Contacto`, `Actividad`, `Correop`, `Nombart`) VALUES
(1684, '9580', 'E.P.B. NRO 3', '9580', 'MARILIN', 'S', 'marilu_dellavedova@yahoo.com.ar ', 'PTE PERON 3018', 'VALENTIN ALSINA', '4208-3615', 'NATACHA/MARILIN', 'EDUCATIVA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1685, '9600', 'JULTIN SRL', '9600', 'DIEGO', 'S', 'dlerej@fibertel.com.ar ', '20 DE OCTUBRE 38', 'LANUS OESTE', '241 - 2251', 'H. MATZKIN- D. LEREJ', 'Enviar dato faltante !!', 'Enviar dato faltante !! ', 'GALENO'),
(1686, '9609', 'ETIQUETAS RUVI S.R.L.', '9609', 'MARCELO', 'S', 'info@etiquetasruvi.com.ar,ru_vi@ciudad.com.ar ', 'ALFREDO PALACIOS 2031', 'VALENTIN ALSINA', '4208-4153/8291', 'MARCELO/RUBEN', 'TEXTIL', 'Enviar dato faltante !! ', 'LA HOLANDO'),
(1687, '9610', 'VICBOR S.R.L.', '9610', 'JESICA', 'S', 'jguaquinsoy@atomik.com.ar ', 'HIPOLITO YRIGOYEN 1331', 'AVELLANEDA', '4208-5777', 'ROMINA', 'CALZADO', 'rmakuch@atomik.com.ar,jguaquinsoy@atomik.com.ar', 'LA CAJA'),
(1688, '9614', 'CERAMICA INDUSTRIAL AVELLANEDA S.A.', '9614', 'EDITH', 'S', 'hectorlopresti@ceramica-avellaneda.com.ar ', ',', 'AVELLANEDA', '4204-1464/1348/8574', 'EDITH MARTINEZ', 'MATERIALES DE CONSTRUCCION', 'Enviar dato faltante !! ', 'SMG'),
(1689, '9630', 'SEMATUR S.R.L.', '9630', 'FLORENCIA', 'S', 'info@sematurweb.com.ar,info@sematur.com.ar ', 'ROSALES 531', 'LUIS GUILLON', '4281-4848', 'SERGIO-FLORENCIA', 'TRANSPORTE', 'proveedores@sematur.com.ar ', 'LA CAJA'),
(1690, '9636', 'FONDOS 2001 SRL', '9636', 'GUSTAVO', 'S', 'fondos2001@hotmail.com ', 'BEDOYA 4050', 'REMEDIOS DE ESCALADA', '4248-6919', 'GUSTAVO/OSCAR', 'CALZADOS', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1691, '9669', 'DISTRIBUIDORA BULONES COIRO S.A.', '9669', 'PABLO', 'S', 'info@bulonescoiro.com,luis.iorio@hotmail.com,pablo@bulonescoiro.com', 'AV HIPOLITO YRIGOYEN 9820', 'LOMAS DE ZAMORA', '4245-1201- 42926686', 'PABLO COIRO', 'DISTRIBUIDORA', 'proveedores@bulonescoiro.com ', 'FEDERACION PATRONAL'),
(1692, '9698', 'REYFIL S.R.L.', '9698', 'REYFIL', 'S', 'ventas@reyfilsrl.com.ar ', 'SUIPACHA 3057', 'REMEDIOS DE ESCALADA ESTE', '4289-4083/4084', 'DIEGO REY/NICOLAS DERKACE', 'FABRICA', 'Enviar dato faltante !! ', 'MAPFRE'),
(1693, '9721', 'PINTURERIAS Y FERRETERIAS EL SOL S.A.', '9721', 'ENRIQUE', 'S', 'elsolpinturerias@speedy.com.ar ', 'RIVADAVIA 2077', 'LANUS OESTE', '4228-6477/4208-9033', 'ENRIQUE / CLAUDIA', 'PINTURERIA', 'Enviar dato faltante !! ', 'QBE'),
(1694, '9728', 'TORRES ESTELA DEL VALLE', '9728', 'ESTELA', 'S', 'info@confiteriaesmeralda.com.ar ', 'SARMIENTO 1051', 'LANUS ESTE', '4201-8914', 'GRACIELA/ESTELA', 'CONFITERIA', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(1695, '9731', 'BALLERINI OSCAR RUBEN', '9731', 'OSCAR', 'S', 'isabelledecoraciones@yahoo.com.ar,reye@live.com.ar', 'UCRANIA 907', 'V ALSINA', '4209-9507', 'GLADYS', 'FABRICA CORTINAS', 'Enviar dato faltante !! ', 'SMG'),
(1696, '9758', 'PEREZ ROBERTO SANTIAGO', '9758', 'ROBERTO', 'S', 'lanus2@datamarkets.com.ar ', 'O HIGGINS 1960', 'LANUS ESTE', '4241-3872', 'ROBERTO', 'PLASTICO', 'Enviar dato faltante !! ', 'GALENO'),
(1697, '9780', 'GREGO ROSA MARTA', '9780', 'JESICA', 'S', 'acolpla@hotmail.com ', 'VILLEGAS 330', 'REMEDIOS DE ESCALADA', '4-288-3170/1552493440/155', 'MARTA', 'TEXTIL', 'Enviar dato faltante !! ', 'LA CAJA'),
(1698, '9798', 'ESPERT S.A.', '9798', 'DANIEL', 'S', 'calvodan@hotmail.com,eflopez_espert@yahoo.com.ar,fanarusbeck@hotmail.com', 'RUTA 36 KN 37500', 'EL PATO BERAZATEGUI', '02229-497012/15', 'DANIEL CALVO/YANINA TOLEDO', 'TABACALERA', 'fanarusbeck@hotmail.com ', 'SMG'),
(1699, '9849', 'ZAITI FERNANDO JAVIER Y CARMELO S.H.', '9849', 'FERNANDO', 'S', 'consultas@aberturascarmelo.com ', 'JOAQUIN V.GONZALEZ 1377', 'LANUS OESTE', '4241-8674 1565190727', 'FERNANDO/CARMELO', 'ABERTURAS', 'Enviar dato faltante !! ', 'PREVENCION'),
(1700, '9866', 'LENCINA GABRIEL ROBERTO', '9866', 'GABRIEL', 'S', 'globras@hotmail.com ', 'LOS ANDES 1368', 'BERNAL OESTE', '4270-1523/15-5853-4484', 'GABRIEL', 'CONSTRUCCION', 'Enviar dato faltante !! ', 'GALENO'),
(1701, '9870', 'JOSE VIVAS LUIS QUINTEROS S.H.', '9870', 'JOSE', 'S', 'pulidobustamante@hotmail.com ', 'BUSTAMANTE 1853', 'LANUS ESTE', '4203-3413', 'JOSE-LUIS', 'PULIDO DE ACERO INOXIDABLE', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1702, '9881', 'PARADIX SRL', '9881', 'PARADIX', 'S', 'NO MAIL 2 ', 'QUESADA 415', 'V. FIORITO LOMAS DE ZAMORA', '/4286-1866 paola', 'SR.ROBERTO PEREZ', 'PANIFICADORA', 'Enviar dato faltante !! ', 'LA CAJA'),
(1703, '9882', 'ROARCO SA', '9882', 'ROARCO', 'S', 'info@roarco.com.ar,ezequiel_moreno@roarco.com.ar,obras@roarco.com', 'AV. FERNANDEZ 660', 'TEMPERLEY', '4245-6892', 'EZEQUIEL MORENO', 'CONSTRUCTORA', 'facturas@roarco.com.ar ', 'SMG'),
(1704, '9897', 'EPOJE SRL', '9897', 'CARLOS', 'S', 'epojesrl@gmail.com ', 'ROSETTI 238', 'AVELLANEDA OESTE', '4201-9433', 'CARLOS BLANCO', 'ENCURTIDOS', 'Enviar dato faltante !! ', 'BERKLEY INTERNATIONAL'),
(1705, '9920', 'ALDEMI HIERROS S.A.', '9920', 'ALDEMI', 'S', 'hierrosaldemi@hotmail.com ', 'EJERCITO DE LOS ANDES 835', 'BANFIELD OESTE', '4286-3977', 'MARTA', 'VTA DE HIERROS', 'Enviar dato faltante !! ', 'SMG'),
(1706, '9926', 'UNIPOX S.A.', '9926', 'UNIPOX', 'S', 'marisaunipoxsa@speedy.com.ar,fabianaunipoxsa@speedy.com.ar', 'DR PRONZATO 323', 'LLAVALLOL', '4298-5437/4298-1138', 'VICTOR-OMAR', 'QUIMICA', 'Enviar dato faltante !! ', 'PROVINCIA'),
(1707, '9934', 'TRIAL MR (DE ANTONIO TRIFILETTI)', '9934', 'TRIALMR', 'S', 'trialmr@trialmr.com.ar,trialsale@yahoo.com.ar ', 'MANUEL OCAMPO 1710', 'AVELLANEDA', '4204-7959/15-4470-6742 CA', 'CLAUDIA-CARLOS', 'METALURGICA', 'Enviar dato faltante !! ', 'SMG'),
(1708, '9946', 'CIOCCA PRODUCTOS ELECTRICOS S.R.L.', '9946', 'GABRIELA', 'S', 'info@cioccasrl.com.ar ', 'MARIO BRAVO 1158', 'BANFIELD OESTE', '4286-5858', 'GABRIELA DE CESARE', 'ELCTRICIDAD', 'Enviar dato faltante !! ', 'LA CAJA'),
(1709, '9949', 'CONSORCIO ALCORTA 146', '9949', 'OSCAR', 'S', 'orabuffetti@yahoo.com.ar ', 'ALCORTA 146', 'LANUS OESTE', '4249-6498', 'RABUFETTI', 'CONSORCIO', 'Enviar dato faltante !! ', 'ART INTERACCION SA'),
(1710, '9963', 'CONSORCIO M.BRIN 2982', '9963', 'ANA', 'S', 'estudioromero@yahoo.com.ar ', 'M.BRIN 2982', 'LANUS OESTE', '5991-0022', 'ROMERO ANA KARINA', 'CONSORCIO', 'Enviar dato faltante !! ', 'ART INTERACCION SA');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `acl_classes`
--
ALTER TABLE `acl_classes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_69DD750638A36066` (`class_type`);

--
-- Indices de la tabla `acl_entries`
--
ALTER TABLE `acl_entries`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_46C8B806EA000B103D9AB4A64DEF17BCE4289BF4` (`class_id`,`object_identity_id`,`field_name`,`ace_order`),
  ADD KEY `IDX_46C8B806EA000B103D9AB4A6DF9183C9` (`class_id`,`object_identity_id`,`security_identity_id`),
  ADD KEY `IDX_46C8B806EA000B10` (`class_id`),
  ADD KEY `IDX_46C8B8063D9AB4A6` (`object_identity_id`),
  ADD KEY `IDX_46C8B806DF9183C9` (`security_identity_id`);

--
-- Indices de la tabla `acl_object_identities`
--
ALTER TABLE `acl_object_identities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_9407E5494B12AD6EA000B10` (`object_identifier`,`class_id`),
  ADD KEY `IDX_9407E54977FA751A` (`parent_object_identity_id`);

--
-- Indices de la tabla `acl_object_identity_ancestors`
--
ALTER TABLE `acl_object_identity_ancestors`
  ADD PRIMARY KEY (`object_identity_id`,`ancestor_id`),
  ADD KEY `IDX_825DE2993D9AB4A6` (`object_identity_id`),
  ADD KEY `IDX_825DE299C671CEA1` (`ancestor_id`);

--
-- Indices de la tabla `acl_security_identities`
--
ALTER TABLE `acl_security_identities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8835EE78772E836AF85E0677` (`identifier`,`username`);

--
-- Indices de la tabla `fos_user_group`
--
ALTER TABLE `fos_user_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_583D1F3E5E237E06` (`name`);

--
-- Indices de la tabla `fos_user_user`
--
ALTER TABLE `fos_user_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_C560D76192FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_C560D761A0D96FBF` (`email_canonical`);

--
-- Indices de la tabla `fos_user_user_group`
--
ALTER TABLE `fos_user_user_group`
  ADD PRIMARY KEY (`user_id`,`group_id`),
  ADD KEY `IDX_B3C77447A76ED395` (`user_id`),
  ADD KEY `IDX_B3C77447FE54D947` (`group_id`);

--
-- Indices de la tabla `media__gallery`
--
ALTER TABLE `media__gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `media__gallery_media`
--
ALTER TABLE `media__gallery_media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_80D4C5414E7AF8F` (`gallery_id`),
  ADD KEY `IDX_80D4C541EA9FDD75` (`media_id`);

--
-- Indices de la tabla `media__media`
--
ALTER TABLE `media__media`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ServiciosAccidenteTrabajo`
--
ALTER TABLE `ServiciosAccidenteTrabajo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ServiciosAtencionConsultorio`
--
ALTER TABLE `ServiciosAtencionConsultorio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ServiciosContactUs`
--
ALTER TABLE `ServiciosContactUs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ServiciosEncuestaSatisfaccion`
--
ALTER TABLE `ServiciosEncuestaSatisfaccion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ServiciosExamenMedico`
--
ALTER TABLE `ServiciosExamenMedico`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ServiciosVisitaDomicilio`
--
ALTER TABLE `ServiciosVisitaDomicilio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `SlideCompany`
--
ALTER TABLE `SlideCompany`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_C3E6F40C3DA5256D` (`image_id`);

--
-- Indices de la tabla `Usuarios`
--
ALTER TABLE `Usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `acl_classes`
--
ALTER TABLE `acl_classes`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `acl_entries`
--
ALTER TABLE `acl_entries`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `acl_object_identities`
--
ALTER TABLE `acl_object_identities`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `acl_security_identities`
--
ALTER TABLE `acl_security_identities`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `fos_user_group`
--
ALTER TABLE `fos_user_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `fos_user_user`
--
ALTER TABLE `fos_user_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `media__gallery`
--
ALTER TABLE `media__gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `media__gallery_media`
--
ALTER TABLE `media__gallery_media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `media__media`
--
ALTER TABLE `media__media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `ServiciosAccidenteTrabajo`
--
ALTER TABLE `ServiciosAccidenteTrabajo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `ServiciosAtencionConsultorio`
--
ALTER TABLE `ServiciosAtencionConsultorio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `ServiciosContactUs`
--
ALTER TABLE `ServiciosContactUs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `ServiciosEncuestaSatisfaccion`
--
ALTER TABLE `ServiciosEncuestaSatisfaccion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `ServiciosExamenMedico`
--
ALTER TABLE `ServiciosExamenMedico`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `ServiciosVisitaDomicilio`
--
ALTER TABLE `ServiciosVisitaDomicilio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT de la tabla `SlideCompany`
--
ALTER TABLE `SlideCompany`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `Usuarios`
--
ALTER TABLE `Usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1711;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `acl_entries`
--
ALTER TABLE `acl_entries`
  ADD CONSTRAINT `FK_46C8B8063D9AB4A6` FOREIGN KEY (`object_identity_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_46C8B806DF9183C9` FOREIGN KEY (`security_identity_id`) REFERENCES `acl_security_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_46C8B806EA000B10` FOREIGN KEY (`class_id`) REFERENCES `acl_classes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `acl_object_identities`
--
ALTER TABLE `acl_object_identities`
  ADD CONSTRAINT `FK_9407E54977FA751A` FOREIGN KEY (`parent_object_identity_id`) REFERENCES `acl_object_identities` (`id`);

--
-- Filtros para la tabla `acl_object_identity_ancestors`
--
ALTER TABLE `acl_object_identity_ancestors`
  ADD CONSTRAINT `FK_825DE2993D9AB4A6` FOREIGN KEY (`object_identity_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_825DE299C671CEA1` FOREIGN KEY (`ancestor_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `fos_user_user_group`
--
ALTER TABLE `fos_user_user_group`
  ADD CONSTRAINT `FK_B3C77447A76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user_user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_B3C77447FE54D947` FOREIGN KEY (`group_id`) REFERENCES `fos_user_group` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `media__gallery_media`
--
ALTER TABLE `media__gallery_media`
  ADD CONSTRAINT `FK_80D4C5414E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `media__gallery` (`id`),
  ADD CONSTRAINT `FK_80D4C541EA9FDD75` FOREIGN KEY (`media_id`) REFERENCES `media__media` (`id`);

--
-- Filtros para la tabla `SlideCompany`
--
ALTER TABLE `SlideCompany`
  ADD CONSTRAINT `FK_C3E6F40C3DA5256D` FOREIGN KEY (`image_id`) REFERENCES `media__media` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
